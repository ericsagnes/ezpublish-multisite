<?php /* #?ini charset="utf-8"?

[DatabaseSettings]
DatabaseImplementation=ezmysqli
Server=localhost
Port=
User=root
Password=root
Database=ezp_multisite
Charset=
Socket=disabled

[SiteSettings]
SiteName=サイトA
LoginPage=embedded
AdditionalLoginFormActionURL=/user/login
IndexPage=/content/view/full/59
RootNodeDepth=2

[SiteAccessSettings]
RequireUserLogin=false
ShowHiddenNodes=false
PathPrefix=site_a

[DesignSettings]
SiteDesign=ezwebin
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=base

[RegionalSettings]
Locale=jpn-JP
ContentObjectLocale=jpn-JP
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=jpn-JP
TextTranslation=enabled

[FileSettings]
VarDir=var/ezwebin_site_clean

*/ ?>
