<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveExtensions[]
ActiveExtensions[]=multisite
ActiveExtensions[]=ezmultiupload
ActiveExtensions[]=eztags
ActiveExtensions[]=ezautosave
ActiveExtensions[]=ezjscore
ActiveExtensions[]=ezwt
ActiveExtensions[]=ezstarrating
ActiveExtensions[]=ezgmaplocation
ActiveExtensions[]=ezwebin
ActiveExtensions[]=ezie
ActiveExtensions[]=ezoe
ActiveExtensions[]=ezodf
ActiveExtensions[]=ezprestapiprovider

[Session]
SessionNameHandler=custom

[SiteSettings]
DefaultAccess=site_a
SiteList[]
SiteList[]=front
SiteList[]=jpn
SiteList[]=back
RootNodeDepth=1
SiteName=Website Interface (without demo content)
MetaDataArray[author]=eZ Systems
MetaDataArray[copyright]=eZ Systems
MetaDataArray[description]=Content Management System
MetaDataArray[keywords]=cms, publish, e-commerce, content management, development framework
SiteURL=multisite.ez.arch

[UserSettings]
LogoutRedirect=/

[SiteAccessSettings]
CheckValidity=false
AvailableSiteAccessList[]
#AvailableSiteAccessList[]=front
#AvailableSiteAccessList[]=jpn
AvailableSiteAccessList[]=back
AvailableSiteAccessList[]=back
AvailableSiteAccessList[]=site_a
AvailableSiteAccessList[]=site_b
MatchOrder=host;uri
HostMatchMapItems[]
HostMatchType=regexp
HostMatchElement=0
HostMatchRegexp=^(.+)\.multisite\.ez\.arch$

[DesignSettings]
DesignLocationCache=enabled

[RegionalSettings]
TranslationSA[]
TranslationSA[jpn]=Jpn

[FileSettings]
VarDir=var/ezwebin_site

[MailSettings]
Transport=sendmail
AdminEmail=es@clina.jp
EmailSender=

[EmbedViewModeSettings]
AvailableViewModes[]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]
InlineViewModes[]=embed-inline

#[ContentSettings]
#ViewCaching=disabled
#
#[DebugSettings]
#DebugOutput=enabled
#
#[TemplateSettings]
#Debug=enabled
#ShowXHTMLCode=disabled
#TemplateCompile=disabled
#TemplateCache=disabled
#ShowUsedTemplates=enabled
#
#[OverrideSettings]
#Cache=disabled

*/ ?>
