<?php /* #?ini charset="utf-8"?

[DatabaseSettings]
DatabaseImplementation=ezmysqli
Server=localhost
Port=
User=root
Password=root
Database=ezp_multisite
Charset=
Socket=disabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteName=Website Interface (without demo content)
SiteURL=multisite.ez.arch
DefaultPage=content/dashboard
LoginPage=custom

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=true
RelatedSiteAccessList[]=front
RelatedSiteAccessList[]=jpn
RelatedSiteAccessList[]=back
RelatedSiteAccessList[]=site_a
RelatedSiteAccessList[]=site_b
ShowHiddenNodes=true

[DesignSettings]
SiteDesign=back
AdditionalSiteDesignList[]=admin2
AdditionalSiteDesignList[]=admin

[RegionalSettings]
Locale=jpn-JP
ContentObjectLocale=jpn-JP
ShowUntranslatedObjects=enabled
SiteLanguageList[]=jpn-JP
TextTranslation=enabled

[FileSettings]
VarDir=var/ezwebin_site_clean

[ContentSettings]
CachedViewPreferences[full]=admin_navigation_content=1;admin_children_viewmode=list;admin_list_limit=1
TranslationList=

[MailSettings]
AdminEmail=es@clina.jp
EmailSender=
*/ ?>
