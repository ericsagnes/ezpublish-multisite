<?php
	return array (
  'info' => 
  array (
    10 => 
    array (
      'contentobject_id' => '10',
      'login' => 'anonymous',
      'email' => 'nospam@ez.no',
      'is_enabled' => true,
      'password_hash' => '4e6f6184135228ccd45f8233d72a0363',
      'password_hash_type' => '2',
    ),
  ),
  'groups' => 
  array (
    0 => '42',
    1 => '4',
  ),
  'roles' => 
  array (
    0 => '1',
  ),
  'role_limitations' => 
  array (
    0 => '',
  ),
  'access_array' => 
  array (
    'content' => 
    array (
      'pdf' => 
      array (
        'p_336' => 
        array (
          'Section' => 
          array (
            0 => '1',
          ),
        ),
      ),
      'read' => 
      array (
        'p_337' => 
        array (
          'Section' => 
          array (
            0 => '1',
          ),
        ),
      ),
    ),
    'rss' => 
    array (
      'feed' => 
      array (
        '*' => '*',
      ),
    ),
    'user' => 
    array (
      'login' => 
      array (
        'p_340' => 
        array (
          'SiteAccess' => 
          array (
            0 => '1060414029',
          ),
        ),
        'p_341' => 
        array (
          'SiteAccess' => 
          array (
            0 => '2626204703',
            1 => '92399013',
          ),
        ),
      ),
    ),
  ),
  'discount_rules' => 
  array (
  ),
);
?>
