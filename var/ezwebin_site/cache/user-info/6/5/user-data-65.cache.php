<?php
	return array (
  'info' => 
  array (
    65 => 
    array (
      'contentobject_id' => '65',
      'login' => 'a_admin',
      'email' => 'a@admin.com',
      'is_enabled' => true,
      'password_hash' => '9af170912cc6d694007e1e0cf97a6f56',
      'password_hash_type' => '2',
    ),
  ),
  'groups' => 
  array (
    0 => '63',
    1 => '4',
    2 => '61',
  ),
  'roles' => 
  array (
    0 => '2',
    1 => '5',
  ),
  'role_limitations' => 
  array (
    0 => '/1/2/59/',
    1 => '',
  ),
  'access_array' => 
  array (
    '*' => 
    array (
      '*' => 
      array (
        'p_308_34' => 
        array (
          'User_Subtree' => 
          array (
            0 => '/1/2/59/',
          ),
        ),
      ),
    ),
    'content' => 
    array (
      'read' => 
      array (
        'p_344' => 
        array (
          'Node' => 
          array (
            0 => '2',
            1 => '43',
            2 => '48',
            3 => '5',
            4 => '58',
          ),
        ),
      ),
    ),
  ),
  'discount_rules' => 
  array (
  ),
);
?>
