<?php
// URI:       design:node/view/full.tpl
// Filename:  extension/ezwebin/design/ezwebin/templates/node/view/full.tpl
// Timestamp: 1373586452 (Fri Jul 12 8:47:32 JST 2013)
$oldSetArray_4f56b8828a471e1d70202e41d0e69e8f = isset( $setArray ) ? $setArray : array();
$setArray = array();
$tpl->Level++;
if ( $tpl->Level > 40 )
{
$text = $tpl->MaxLevelWarning;$tpl->Level--;
return;
}
$eZTemplateCompilerCodeDate = 1074699607;
if ( !defined( 'EZ_TEMPLATE_COMPILER_COMMON_CODE' ) )
include_once( 'var/ezwebin_site/cache/template/compiled/common.php' );

$text .= '<div class="border-box">
<div class="border-tl"><div class="border-tr"><div class="border-tc"></div></div></div>
<div class="border-ml"><div class="border-mr"><div class="border-mc float-break">

<div class="content-view-full">
    <div class="class-';
unset( $var );
unset( $var );
$var = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$var1 = compiledFetchAttribute( $var, 'object' );
unset( $var );
$var = $var1;
$var1 = compiledFetchAttribute( $var, 'class_identifier' );
unset( $var );
$var = $var1;
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$text .= ( is_object( $var ) ? compiledFetchText( $tpl, $rootNamespace, $currentNamespace, false, $var ) : $var );
unset( $var );

$text .= '">

    <div class="attribute-header">
        <h1>';
unset( $var );
unset( $var1 );
unset( $var1 );
$var1 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$var2 = compiledFetchAttribute( $var1, 'name' );
unset( $var1 );
$var1 = $var2;
if (! isset( $var1 ) ) $var1 = NULL;
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
$var = htmlspecialchars( $var1 );
unset( $var1 );
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$text .= $var;
unset( $var );

$text .= '</h1>
    </div>

    <ul>
    ';
// foreach begins
$skipDelimiter = true;
if ( !isset( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_1 ) ) $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_1 = array();
$fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_1[] = compact( 'fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1', 'fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_1', 'fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_1', 'fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_1', 'fe_i_c1a23d0a76888f9330f32ba8917d1d7e_1', 'fe_key_c1a23d0a76888f9330f32ba8917d1d7e_1', 'fe_val_c1a23d0a76888f9330f32ba8917d1d7e_1', 'fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1', 'fe_max_c1a23d0a76888f9330f32ba8917d1d7e_1', 'fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_1', 'fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_1', 'fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_1' );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1 );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_11 = compiledFetchAttribute( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1, 'children' );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_11;
if (! isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1 ) ) $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1 = NULL;
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1->templateValue();

$fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_1 = is_array( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1 ) ? array_keys( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1 ) : array();
$fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_1 = count( $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_1 );
$fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_1 = 0;
$fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1 = 0;
$fe_max_c1a23d0a76888f9330f32ba8917d1d7e_1 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_1 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1;
$fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_1 = false;
if ( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1 < 0 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1 >= $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_1 )
{
    $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1 = ( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1 < 0 ) ? 0 : $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_1;
    if ( $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_1 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1 < 0 )
 {
        eZDebug::writeWarning("Invalid 'offset' parameter specified: '$fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1'. Array count: $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_1");   
}
}
if ( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_1 < 0 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1 + $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_1 > $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_1 )
{
    if ( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_1 < 0 )
 eZDebug::writeWarning("Invalid 'max' parameter specified: $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_1");
    $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_1 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_1 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1;
}
if ( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_1 )
{
    $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_1 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_1 - 1 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1;
    $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_1  = 0;
}
else
{
    $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_1 = $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1;
    $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_1  = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_1 - 1;
}
// foreach
for ( $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_1 = $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_1; $fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_1 < $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_1 && ( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_1 ? $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_1 >= $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_1 : $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_1 <= $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_1 ); $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_1 ? $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_1-- : $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_1++ )
{
$fe_key_c1a23d0a76888f9330f32ba8917d1d7e_1 = $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_1[$fe_i_c1a23d0a76888f9330f32ba8917d1d7e_1];
$fe_val_c1a23d0a76888f9330f32ba8917d1d7e_1 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1[$fe_key_c1a23d0a76888f9330f32ba8917d1d7e_1];
$vars[$rootNamespace]['node'] = $fe_val_c1a23d0a76888f9330f32ba8917d1d7e_1;
$text .= '      <li><a href=';
unset( $var );
unset( $var );
$var = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$var1 = compiledFetchAttribute( $var, 'url_alias' );
unset( $var );
$var = $var1;
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$text .= ( is_object( $var ) ? compiledFetchText( $tpl, $rootNamespace, $currentNamespace, false, $var ) : $var );
unset( $var );

$text .= '>';
unset( $var );
unset( $var );
$var = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$var1 = compiledFetchAttribute( $var, 'name' );
unset( $var );
$var = $var1;
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$text .= ( is_object( $var ) ? compiledFetchText( $tpl, $rootNamespace, $currentNamespace, false, $var ) : $var );
unset( $var );

$text .= '</a></li>
    ';
$fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_1++;
} // foreach
$skipDelimiter = false;
if ( count( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_1 ) ) extract( array_pop( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_1 ) );

else
{

unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_1 );

unset( $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_1 );

unset( $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_1 );

unset( $fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_1 );

unset( $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_1 );

unset( $fe_key_c1a23d0a76888f9330f32ba8917d1d7e_1 );

unset( $fe_val_c1a23d0a76888f9330f32ba8917d1d7e_1 );

unset( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_1 );

unset( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_1 );

unset( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_1 );

unset( $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_1 );

unset( $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_1 );

unset( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_1 );

}

// foreach ends
$text .= '    </ul>

    

    ';
// def $name_pattern
unset( $var );
unset( $var1 );
unset( $var2 );
unset( $var2 );
$var2 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$var3 = compiledFetchAttribute( $var2, 'object' );
unset( $var2 );
$var2 = $var3;
$var3 = compiledFetchAttribute( $var2, 'content_class' );
unset( $var2 );
$var2 = $var3;
$var3 = compiledFetchAttribute( $var2, 'contentobject_name' );
unset( $var2 );
$var2 = $var3;
if (! isset( $var2 ) ) $var2 = NULL;
while ( is_object( $var2 ) and method_exists( $var2, 'templateValue' ) )
    $var2 = $var2->templateValue();
while ( is_object( $var2 ) and method_exists( $var2, 'templateValue' ) )
    $var2 = $var2->templateValue();
if ( is_string( $var2 ) )
{
	$var1 = explode( ">", $var2 );
}
else if ( is_array( $var2 ) )
{
	$var1 = array( array_slice( $var2, 0, ">" ), array_slice( $var2, ">" ) );
}
else
{
	$var1 = null;
}
unset( $var2 );
if (! isset( $var1 ) ) $var1 = NULL;
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
$var = implode( ",", $var1 );unset( $var1 );
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
if ( $tpl->hasVariable( 'name_pattern', $rootNamespace ) )
{
    $tpl->warning( 'def', "Variable 'name_pattern' is already defined.", array (
  0 => 
  array (
    0 => 32,
    1 => 4,
    2 => 1349,
  ),
  1 => 
  array (
    0 => 33,
    1 => 143,
    2 => 1584,
  ),
  2 => 'extension/ezwebin/design/ezwebin/templates/node/view/full.tpl',
) );
    $tpl->setVariable( 'name_pattern', $var, $rootNamespace );
}
else
{
    $tpl->setLocalVariable( 'name_pattern', $var, $rootNamespace );
}

// def $name_pattern_array
if ( $tpl->hasVariable( 'name_pattern_array', $rootNamespace ) )
{
    $tpl->warning( 'def', "Variable 'name_pattern_array' is already defined.", array (
  0 => 
  array (
    0 => 32,
    1 => 4,
    2 => 1349,
  ),
  1 => 
  array (
    0 => 33,
    1 => 143,
    2 => 1584,
  ),
  2 => 'extension/ezwebin/design/ezwebin/templates/node/view/full.tpl',
) );
    $tpl->setVariable( 'name_pattern_array', array (
  0 => 'enable_comments',
  1 => 'enable_tipafriend',
  2 => 'show_children',
  3 => 'show_children_exclude',
  4 => 'show_children_pr_page',
), $rootNamespace );
}
else
{
    $tpl->setLocalVariable( 'name_pattern_array', array (
  0 => 'enable_comments',
  1 => 'enable_tipafriend',
  2 => 'show_children',
  3 => 'show_children_exclude',
  4 => 'show_children_pr_page',
), $rootNamespace );
}

$text .= '    ';
unset( $var );
unset( $var1 );
unset( $var2 );
unset( $var2 );
$var2 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'name_pattern', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['name_pattern'] : null;
if (! isset( $var2 ) ) $var2 = NULL;
while ( is_object( $var2 ) and method_exists( $var2, 'templateValue' ) )
    $var2 = $var2->templateValue();
while ( is_object( $var2 ) and method_exists( $var2, 'templateValue' ) )
    $var2 = $var2->templateValue();
if ( is_string( $var2 ) )
{
	$var1 = explode( "|", $var2 );
}
else if ( is_array( $var2 ) )
{
	$var1 = array( array_slice( $var2, 0, "|" ), array_slice( $var2, "|" ) );
}
else
{
	$var1 = null;
}
unset( $var2 );
if (! isset( $var1 ) ) $var1 = NULL;
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
$var = implode( ",", $var1 );unset( $var1 );
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
if ( array_key_exists( $currentNamespace, $vars ) && array_key_exists( 'name_pattern', $vars[$currentNamespace] ) )
{
    $vars[$currentNamespace]['name_pattern'] = $var;
    unset( $var );
}
$text .= '    ';
unset( $var );
unset( $var1 );
unset( $var2 );
unset( $var2 );
$var2 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'name_pattern', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['name_pattern'] : null;
if (! isset( $var2 ) ) $var2 = NULL;
while ( is_object( $var2 ) and method_exists( $var2, 'templateValue' ) )
    $var2 = $var2->templateValue();
while ( is_object( $var2 ) and method_exists( $var2, 'templateValue' ) )
    $var2 = $var2->templateValue();
if ( is_string( $var2 ) )
{
	$var1 = explode( "<", $var2 );
}
else if ( is_array( $var2 ) )
{
	$var1 = array( array_slice( $var2, 0, "<" ), array_slice( $var2, "<" ) );
}
else
{
	$var1 = null;
}
unset( $var2 );
if (! isset( $var1 ) ) $var1 = NULL;
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
$var = implode( ",", $var1 );unset( $var1 );
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
if ( array_key_exists( $currentNamespace, $vars ) && array_key_exists( 'name_pattern', $vars[$currentNamespace] ) )
{
    $vars[$currentNamespace]['name_pattern'] = $var;
    unset( $var );
}
$text .= '    ';
unset( $var );
unset( $var1 );
unset( $var1 );
$var1 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'name_pattern', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['name_pattern'] : null;
if (! isset( $var1 ) ) $var1 = NULL;
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
if ( is_string( $var1 ) )
{
	$var = explode( ",", $var1 );
}
else if ( is_array( $var1 ) )
{
	$var = array( array_slice( $var1, 0, "," ), array_slice( $var1, "," ) );
}
else
{
	$var = null;
}
unset( $var1 );
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
if ( array_key_exists( $currentNamespace, $vars ) && array_key_exists( 'name_pattern', $vars[$currentNamespace] ) )
{
    $vars[$currentNamespace]['name_pattern'] = $var;
    unset( $var );
}
$text .= '    ';
// foreach begins
$skipDelimiter = true;
if ( !isset( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_2 ) ) $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_2 = array();
$fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_2[] = compact( 'fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2', 'fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_2', 'fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_2', 'fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_2', 'fe_i_c1a23d0a76888f9330f32ba8917d1d7e_2', 'fe_key_c1a23d0a76888f9330f32ba8917d1d7e_2', 'fe_val_c1a23d0a76888f9330f32ba8917d1d7e_2', 'fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2', 'fe_max_c1a23d0a76888f9330f32ba8917d1d7e_2', 'fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_2', 'fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_2', 'fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_2' );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2 );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'name_pattern', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['name_pattern'] : null;
if (! isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2 ) ) $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2 = NULL;
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2->templateValue();

$fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_2 = is_array( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2 ) ? array_keys( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2 ) : array();
$fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_2 = count( $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_2 );
$fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_2 = 0;
$fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2 = 0;
$fe_max_c1a23d0a76888f9330f32ba8917d1d7e_2 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_2 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2;
$fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_2 = false;
if ( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2 < 0 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2 >= $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_2 )
{
    $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2 = ( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2 < 0 ) ? 0 : $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_2;
    if ( $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_2 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2 < 0 )
 {
        eZDebug::writeWarning("Invalid 'offset' parameter specified: '$fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2'. Array count: $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_2");   
}
}
if ( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_2 < 0 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2 + $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_2 > $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_2 )
{
    if ( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_2 < 0 )
 eZDebug::writeWarning("Invalid 'max' parameter specified: $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_2");
    $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_2 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_2 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2;
}
if ( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_2 )
{
    $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_2 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_2 - 1 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2;
    $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_2  = 0;
}
else
{
    $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_2 = $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2;
    $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_2  = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_2 - 1;
}
// foreach
for ( $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_2 = $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_2; $fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_2 < $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_2 && ( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_2 ? $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_2 >= $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_2 : $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_2 <= $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_2 ); $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_2 ? $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_2-- : $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_2++ )
{
$fe_key_c1a23d0a76888f9330f32ba8917d1d7e_2 = $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_2[$fe_i_c1a23d0a76888f9330f32ba8917d1d7e_2];
$fe_val_c1a23d0a76888f9330f32ba8917d1d7e_2 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2[$fe_key_c1a23d0a76888f9330f32ba8917d1d7e_2];
$vars[$rootNamespace]['name_pattern_string'] = $fe_val_c1a23d0a76888f9330f32ba8917d1d7e_2;
$text .= '        ';
unset( $var );
unset( $var1 );
unset( $var2 );
unset( $var2 );
$var2 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'name_pattern_string', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['name_pattern_string'] : null;
if (! isset( $var2 ) ) $var2 = NULL;
while ( is_object( $var2 ) and method_exists( $var2, 'templateValue' ) )
    $var2 = $var2->templateValue();
while ( is_object( $var2 ) and method_exists( $var2, 'templateValue' ) )
    $var2 = $var2->templateValue();
$strlenFunc = 'mb_strlen'; $substrFunc = 'mb_substr';
$var1 = trim( $var2 );
unset( $var2 );
if (! isset( $var1 ) ) $var1 = NULL;
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
unset( $var2 );
unset( $var2 );
$var2 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'name_pattern_array', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['name_pattern_array'] : null;
if (! isset( $var2 ) ) $var2 = NULL;
while ( is_object( $var2 ) and method_exists( $var2, 'templateValue' ) )
    $var2 = $var2->templateValue();
while ( is_object( $var2 ) and method_exists( $var2, 'templateValue' ) )
    $var2 = $var2->templateValue();
if ( is_string( $var2 ) )
    $var = $var2 . implode( '', array( $var1 ) );
else if( is_array( $var2 ) )
    $var = array_merge( $var2, array( $var1 ) );
unset( $var1, $var2 );
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
if ( array_key_exists( $currentNamespace, $vars ) && array_key_exists( 'name_pattern_array', $vars[$currentNamespace] ) )
{
    $vars[$currentNamespace]['name_pattern_array'] = $var;
    unset( $var );
}
$text .= '    ';
$fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_2++;
} // foreach
$skipDelimiter = false;
if ( count( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_2 ) ) extract( array_pop( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_2 ) );

else
{

unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_2 );

unset( $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_2 );

unset( $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_2 );

unset( $fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_2 );

unset( $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_2 );

unset( $fe_key_c1a23d0a76888f9330f32ba8917d1d7e_2 );

unset( $fe_val_c1a23d0a76888f9330f32ba8917d1d7e_2 );

unset( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_2 );

unset( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_2 );

unset( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_2 );

unset( $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_2 );

unset( $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_2 );

unset( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_2 );

}

// foreach ends
$text .= '
    ';
// foreach begins
$skipDelimiter = true;
if ( !isset( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_3 ) ) $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_3 = array();
$fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_3[] = compact( 'fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3', 'fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_3', 'fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_3', 'fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_3', 'fe_i_c1a23d0a76888f9330f32ba8917d1d7e_3', 'fe_key_c1a23d0a76888f9330f32ba8917d1d7e_3', 'fe_val_c1a23d0a76888f9330f32ba8917d1d7e_3', 'fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3', 'fe_max_c1a23d0a76888f9330f32ba8917d1d7e_3', 'fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_3', 'fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_3', 'fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_3' );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_31 = compiledFetchAttribute( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3, 'object' );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_31;
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_31 = compiledFetchAttribute( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3, 'contentobject_attributes' );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_31;
if (! isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 ) ) $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 = NULL;
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3->templateValue();

$fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_3 = is_array( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 ) ? array_keys( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 ) : array();
$fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_3 = count( $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_3 );
$fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_3 = 0;
$fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3 = 0;
$fe_max_c1a23d0a76888f9330f32ba8917d1d7e_3 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_3 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3;
$fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_3 = false;
if ( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3 < 0 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3 >= $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_3 )
{
    $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3 = ( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3 < 0 ) ? 0 : $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_3;
    if ( $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_3 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3 < 0 )
 {
        eZDebug::writeWarning("Invalid 'offset' parameter specified: '$fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3'. Array count: $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_3");   
}
}
if ( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_3 < 0 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3 + $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_3 > $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_3 )
{
    if ( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_3 < 0 )
 eZDebug::writeWarning("Invalid 'max' parameter specified: $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_3");
    $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_3 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_3 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3;
}
if ( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_3 )
{
    $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_3 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_3 - 1 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3;
    $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_3  = 0;
}
else
{
    $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_3 = $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3;
    $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_3  = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_3 - 1;
}
// foreach
for ( $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_3 = $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_3; $fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_3 < $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_3 && ( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_3 ? $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_3 >= $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_3 : $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_3 <= $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_3 ); $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_3 ? $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_3-- : $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_3++ )
{
$fe_key_c1a23d0a76888f9330f32ba8917d1d7e_3 = $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_3[$fe_i_c1a23d0a76888f9330f32ba8917d1d7e_3];
$fe_val_c1a23d0a76888f9330f32ba8917d1d7e_3 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3[$fe_key_c1a23d0a76888f9330f32ba8917d1d7e_3];
$vars[$rootNamespace]['attribute'] = $fe_val_c1a23d0a76888f9330f32ba8917d1d7e_3;
$text .= '        ';
// if begins
unset( $if_cond );
unset( $if_cond1 );
unset( $if_cond2 );
unset( $if_cond2 );
$if_cond2 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'name_pattern_array', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['name_pattern_array'] : null;
if (! isset( $if_cond2 ) ) $if_cond2 = NULL;
while ( is_object( $if_cond2 ) and method_exists( $if_cond2, 'templateValue' ) )
    $if_cond2 = $if_cond2->templateValue();
unset( $if_cond3 );
unset( $if_cond3 );
$if_cond3 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'attribute', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['attribute'] : null;
$if_cond4 = compiledFetchAttribute( $if_cond3, 'contentclass_attribute_identifier' );
unset( $if_cond3 );
$if_cond3 = $if_cond4;
if (! isset( $if_cond3 ) ) $if_cond3 = NULL;
while ( is_object( $if_cond3 ) and method_exists( $if_cond3, 'templateValue' ) )
    $if_cond3 = $if_cond3->templateValue();
if( is_string( $if_cond2 ) )
{
  $if_cond1 = ( strpos( $if_cond2, $if_cond3 ) !== false );
}
else if ( is_array( $if_cond2 ) )
{
  $if_cond1 = in_array( $if_cond3, $if_cond2 );
}
else
{
$if_cond1 = false;
}unset( $if_cond2, $if_cond3 );
if (! isset( $if_cond1 ) ) $if_cond1 = NULL;
while ( is_object( $if_cond1 ) and method_exists( $if_cond1, 'templateValue' ) )
    $if_cond1 = $if_cond1->templateValue();
$if_cond = !( $if_cond1 );
unset( $if_cond1 );
if (! isset( $if_cond ) ) $if_cond = NULL;
while ( is_object( $if_cond ) and method_exists( $if_cond, 'templateValue' ) )
    $if_cond = $if_cond->templateValue();

if ( $if_cond )
{
$text .= '            <div class="attribute-';
unset( $var );
unset( $var );
$var = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'attribute', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['attribute'] : null;
$var1 = compiledFetchAttribute( $var, 'contentclass_attribute_identifier' );
unset( $var );
$var = $var1;
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$text .= ( is_object( $var ) ? compiledFetchText( $tpl, $rootNamespace, $currentNamespace, false, $var ) : $var );
unset( $var );

$text .= '">
                ';
$textElements = array();
$tpl->processFunction( 'attribute_view_gui', $textElements,
                       false,
                       array (
  'attribute' => 
  array (
    0 => 
    array (
      0 => 4,
      1 => 
      array (
        0 => '',
        1 => 2,
        2 => 'attribute',
      ),
      2 => false,
    ),
  ),
),
                       array (
  0 => 
  array (
    0 => 44,
    1 => 16,
    2 => 2197,
  ),
  1 => 
  array (
    0 => 44,
    1 => 55,
    2 => 2236,
  ),
  2 => 'extension/ezwebin/design/ezwebin/templates/node/view/full.tpl',
),
                       $rootNamespace, $currentNamespace );
$text .= implode( '', $textElements );

$text .= '            </div>
        ';
}
unset( $if_cond );
// if ends

$text .= '    ';
$fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_3++;
} // foreach
$skipDelimiter = false;
if ( count( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_3 ) ) extract( array_pop( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_3 ) );

else
{

unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_3 );

unset( $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_3 );

unset( $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_3 );

unset( $fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_3 );

unset( $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_3 );

unset( $fe_key_c1a23d0a76888f9330f32ba8917d1d7e_3 );

unset( $fe_val_c1a23d0a76888f9330f32ba8917d1d7e_3 );

unset( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_3 );

unset( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_3 );

unset( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_3 );

unset( $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_3 );

unset( $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_3 );

unset( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_3 );

}

// foreach ends
$text .= '
    ';
// if begins
unset( $if_cond );
unset( $if_cond );
$if_cond = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$if_cond1 = compiledFetchAttribute( $if_cond, 'object' );
unset( $if_cond );
$if_cond = $if_cond1;
$if_cond1 = compiledFetchAttribute( $if_cond, 'content_class' );
unset( $if_cond );
$if_cond = $if_cond1;
$if_cond1 = compiledFetchAttribute( $if_cond, 'is_container' );
unset( $if_cond );
$if_cond = $if_cond1;
if (! isset( $if_cond ) ) $if_cond = NULL;
while ( is_object( $if_cond ) and method_exists( $if_cond, 'templateValue' ) )
    $if_cond = $if_cond->templateValue();

if ( $if_cond )
{
$text .= '        ';
unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_6 );
unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_61 );
unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 );
unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 );
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_63 = compiledFetchAttribute( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62, 'data_map' );
unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 );
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_63;
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_63 = compiledFetchAttribute( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62, 'show_children' );
unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 );
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_63;
if (! isset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 ) ) $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = NULL;
while ( is_object( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 ) and method_exists( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62, 'templateValue' ) )
    $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62->templateValue();
while ( is_object( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 ) and method_exists( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62, 'templateValue' ) )
    $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62->templateValue();
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_61 = isset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 );unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 );
if (! isset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_61 ) ) $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_61 = NULL;
while ( is_object( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_61 ) and method_exists( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_61, 'templateValue' ) )
    $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_61 = $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_61->templateValue();
unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 );
unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 );
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_63 = compiledFetchAttribute( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62, 'data_map' );
unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 );
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_63;
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_63 = compiledFetchAttribute( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62, 'show_children' );
unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 );
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_63;
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_63 = compiledFetchAttribute( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62, 'data_int' );
unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 );
$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_63;
if (! isset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 ) ) $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = NULL;
while ( is_object( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 ) and method_exists( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62, 'templateValue' ) )
    $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62->templateValue();
while ( is_object( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 ) and method_exists( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62, 'templateValue' ) )
    $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 = $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62->templateValue();
if ( !$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_61 )
    $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_6 = false;
else if ( !$elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 )
    $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_6 = false;
else
    $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_6 = $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62;
unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_61, $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_62 );
if (! isset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_6 ) ) $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_6 = NULL;
while ( is_object( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_6 ) and method_exists( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_6, 'templateValue' ) )
    $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_6 = $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_6->templateValue();

// if begins
unset( $if_cond );
unset( $if_cond1 );
unset( $if_cond2 );
unset( $if_cond2 );
$if_cond2 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'versionview_mode', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['versionview_mode'] : null;
if (! isset( $if_cond2 ) ) $if_cond2 = NULL;
while ( is_object( $if_cond2 ) and method_exists( $if_cond2, 'templateValue' ) )
    $if_cond2 = $if_cond2->templateValue();
$if_cond1 = !isset( $if_cond2 );unset( $if_cond2 );
if (! isset( $if_cond1 ) ) $if_cond1 = NULL;
while ( is_object( $if_cond1 ) and method_exists( $if_cond1, 'templateValue' ) )
    $if_cond1 = $if_cond1->templateValue();
unset( $if_cond2 );
unset( $if_cond3 );
unset( $if_cond3 );
$if_cond3 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$if_cond4 = compiledFetchAttribute( $if_cond3, 'data_map' );
unset( $if_cond3 );
$if_cond3 = $if_cond4;
$if_cond4 = compiledFetchAttribute( $if_cond3, 'enable_comments' );
unset( $if_cond3 );
$if_cond3 = $if_cond4;
if (! isset( $if_cond3 ) ) $if_cond3 = NULL;
while ( is_object( $if_cond3 ) and method_exists( $if_cond3, 'templateValue' ) )
    $if_cond3 = $if_cond3->templateValue();
$if_cond2 = isset( $if_cond3 );unset( $if_cond3 );
if (! isset( $if_cond2 ) ) $if_cond2 = NULL;
while ( is_object( $if_cond2 ) and method_exists( $if_cond2, 'templateValue' ) )
    $if_cond2 = $if_cond2->templateValue();
unset( $if_cond3 );
unset( $if_cond3 );
$if_cond3 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$if_cond4 = compiledFetchAttribute( $if_cond3, 'data_map' );
unset( $if_cond3 );
$if_cond3 = $if_cond4;
$if_cond4 = compiledFetchAttribute( $if_cond3, 'enable_comments' );
unset( $if_cond3 );
$if_cond3 = $if_cond4;
$if_cond4 = compiledFetchAttribute( $if_cond3, 'data_int' );
unset( $if_cond3 );
$if_cond3 = $if_cond4;
if (! isset( $if_cond3 ) ) $if_cond3 = NULL;
while ( is_object( $if_cond3 ) and method_exists( $if_cond3, 'templateValue' ) )
    $if_cond3 = $if_cond3->templateValue();
if ( !$if_cond1 )
    $if_cond = false;
else if ( !$if_cond2 )
    $if_cond = false;
else if ( !$if_cond3 )
    $if_cond = false;
else
    $if_cond = $if_cond3;
unset( $if_cond1, $if_cond2, $if_cond3 );
if (! isset( $if_cond ) ) $if_cond = NULL;
while ( is_object( $if_cond ) and method_exists( $if_cond, 'templateValue' ) )
    $if_cond = $if_cond->templateValue();

if ( $if_cond )
{
$text .= '            <h1>コメント</h1>
                <div class="content-view-children">
                    ';
// foreach begins
$skipDelimiter = true;
if ( !isset( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_4 ) ) $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_4 = array();
$fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_4[] = compact( 'fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4', 'fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_4', 'fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_4', 'fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_4', 'fe_i_c1a23d0a76888f9330f32ba8917d1d7e_4', 'fe_key_c1a23d0a76888f9330f32ba8917d1d7e_4', 'fe_val_c1a23d0a76888f9330f32ba8917d1d7e_4', 'fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4', 'fe_max_c1a23d0a76888f9330f32ba8917d1d7e_4', 'fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_4', 'fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_4', 'fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_4' );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4 );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41 );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_42 = compiledFetchAttribute( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41, 'node_id' );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_42;
if (! isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41 ) ) $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41 = NULL;
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41->templateValue();
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41->templateValue();
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4 = call_user_func_array( array( new eZContentFunctionCollection(), 'fetchObjectTree' ),
  array(     'parent_node_id' => $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41,
    'sort_by' => array( "published",
       "0" ),
    'only_translated' => false,
    'language' => false,
    'offset' => false,
    'limit' => false,
    'depth' => 1,
    'depth_operator' => "le",
    'class_id' => false,
    'attribute_filter' => false,
    'extended_attribute_filter' => false,
    'class_filter_type' => "include",
    'class_filter_array' => array( "comment" ),
    'group_by' => false,
    'main_node_only' => false,
    'ignore_visibility' => false,
    'limitation' => null,
    'as_object' => null,
    'objectname_filter' => null,
    'load_data_map' => null ) );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4 = isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4['result'] ) ? $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4['result'] : null;
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_41 );
if (! isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4 ) ) $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4 = NULL;
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4->templateValue();

$fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_4 = is_array( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4 ) ? array_keys( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4 ) : array();
$fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_4 = count( $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_4 );
$fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_4 = 0;
$fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4 = 0;
$fe_max_c1a23d0a76888f9330f32ba8917d1d7e_4 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_4 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4;
$fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_4 = false;
if ( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4 < 0 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4 >= $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_4 )
{
    $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4 = ( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4 < 0 ) ? 0 : $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_4;
    if ( $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_4 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4 < 0 )
 {
        eZDebug::writeWarning("Invalid 'offset' parameter specified: '$fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4'. Array count: $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_4");   
}
}
if ( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_4 < 0 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4 + $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_4 > $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_4 )
{
    if ( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_4 < 0 )
 eZDebug::writeWarning("Invalid 'max' parameter specified: $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_4");
    $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_4 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_4 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4;
}
if ( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_4 )
{
    $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_4 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_4 - 1 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4;
    $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_4  = 0;
}
else
{
    $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_4 = $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4;
    $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_4  = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_4 - 1;
}
// foreach
for ( $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_4 = $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_4; $fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_4 < $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_4 && ( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_4 ? $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_4 >= $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_4 : $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_4 <= $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_4 ); $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_4 ? $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_4-- : $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_4++ )
{
$fe_key_c1a23d0a76888f9330f32ba8917d1d7e_4 = $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_4[$fe_i_c1a23d0a76888f9330f32ba8917d1d7e_4];
$fe_val_c1a23d0a76888f9330f32ba8917d1d7e_4 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4[$fe_key_c1a23d0a76888f9330f32ba8917d1d7e_4];
$vars[$rootNamespace]['comment'] = $fe_val_c1a23d0a76888f9330f32ba8917d1d7e_4;
$text .= '                        ';
unset( $var );
unset( $var );
$var = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'comment', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['comment'] : null;
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
$vars[$namespace]['node'] = $var;
unset( $var );
if ( !isset( $dKeys ) )
{
    $resH = $tpl->resourceHandler( 'design' );
    $dKeys = $resH->keys();
}
if ( !isset( $dKeysStack ) )
{
    $dKeysStack = array();
}
$dKeysStack[] = $dKeys;
unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'node_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["node"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'contentobject_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["object"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'contentclass_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["class"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'section_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["section"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'class_identifier' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["class_identifier"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'match_ingroup_id_list' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["class_group"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'state_id_array' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["state"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'state_identifier_array' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["state_identifier"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'parent_node_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["parent_node"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'depth' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["depth"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'url_alias' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["url_alias"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'remote_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["remote_id"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'remote_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["node_remote_id"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'parent' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'class_identifier' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["parent_class_identifier"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'parent' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'remote_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["parent_node_remote_id"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'parent' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'remote_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["parent_object_remote_id"] = $dKey;
unset( $dKey );

$resourceFound = false;
if ( file_exists( 'var/ezwebin_site/cache/template/compiled/line-a3f211d55d3ceccba88d733e6c17d7e7.php' ) )
{
$resourceFound = true;
$namespaceStack[] = array( $rootNamespace, $currentNamespace );
$currentNamespace = $rootNamespace = !$currentNamespace ? 'NodeView' : ( $currentNamespace . ':' . 'NodeView' );
$tpl->createLocalVariablesList();
$tpl->appendTemplateFetch( 'extension/ezwebin/design/ezwebin/templates/node/view/line.tpl' );
include( '' . 'var/ezwebin_site/cache/template/compiled/line-a3f211d55d3ceccba88d733e6c17d7e7.php' );
$tpl->unsetLocalVariables();
$tpl->destroyLocalVariablesList();
list( $rootNamespace, $currentNamespace ) = array_pop( $namespaceStack );
}
else
{
    $resourceFound = true;
$resourceFound = true;
$namespaceStack[] = array( $rootNamespace, $currentNamespace );
$currentNamespace = $rootNamespace = !$currentNamespace ? 'NodeView' : ( $currentNamespace . ':' . 'NodeView' );
$textElements = array();
$extraParameters = array();
$tpl->processURI( 'extension/ezwebin/design/ezwebin/templates/node/view/line.tpl', true, $extraParameters, $textElements, $rootNamespace, $currentNamespace );
$text .= implode( '', $textElements );
list( $rootNamespace, $currentNamespace ) = array_pop( $namespaceStack );
}

$dKeys = array_pop( $dKeysStack );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $vars[$namespace]['node'] );
$text .= '                    ';
$fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_4++;
} // foreach
$skipDelimiter = false;
if ( count( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_4 ) ) extract( array_pop( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_4 ) );

else
{

unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_4 );

unset( $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_4 );

unset( $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_4 );

unset( $fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_4 );

unset( $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_4 );

unset( $fe_key_c1a23d0a76888f9330f32ba8917d1d7e_4 );

unset( $fe_val_c1a23d0a76888f9330f32ba8917d1d7e_4 );

unset( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_4 );

unset( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_4 );

unset( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_4 );

unset( $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_4 );

unset( $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_4 );

unset( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_4 );

}

// foreach ends
$text .= '                </div>

                ';
// if begins
unset( $if_cond );
unset( $if_cond1 );
unset( $if_cond1 );
$if_cond1 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
if (! isset( $if_cond1 ) ) $if_cond1 = NULL;
while ( is_object( $if_cond1 ) and method_exists( $if_cond1, 'templateValue' ) )
    $if_cond1 = $if_cond1->templateValue();
$if_cond = call_user_func_array( array( new eZContentFunctionCollection(), 'checkAccess' ),
  array(     'access' => "create",
    'contentobject' => $if_cond1,
    'contentclass_id' => "comment",
    'parent_contentclass_id' => false,
    'language' => false ) );
$if_cond = isset( $if_cond['result'] ) ? $if_cond['result'] : null;
unset( $if_cond1 );
if (! isset( $if_cond ) ) $if_cond = NULL;
while ( is_object( $if_cond ) and method_exists( $if_cond, 'templateValue' ) )
    $if_cond = $if_cond->templateValue();

if ( $if_cond )
{
$text .= '                    <form method="post" action="/content/action">
                    <input type="hidden" name="ClassIdentifier" value="comment" />
                    <input type="hidden" name="NodeID" value="';
unset( $var );
unset( $var );
$var = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$var1 = compiledFetchAttribute( $var, 'object' );
unset( $var );
$var = $var1;
$var1 = compiledFetchAttribute( $var, 'main_node' );
unset( $var );
$var = $var1;
$var1 = compiledFetchAttribute( $var, 'node_id' );
unset( $var );
$var = $var1;
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$text .= ( is_object( $var ) ? compiledFetchText( $tpl, $rootNamespace, $currentNamespace, false, $var ) : $var );
unset( $var );

$text .= '" />
                    <input type="hidden" name="ContentLanguageCode" value="jpn-JP" />
                    <input class="button new_comment" type="submit" name="NewButton" value="新しいコメント" />
                    </form>
                ';
}
else
{
$text .= '                    ';
// if begins
unset( $if_cond );
if (! isset( $if_cond ) ) $if_cond = NULL;
while ( is_object( $if_cond ) and method_exists( $if_cond, 'templateValue' ) )
    $if_cond = $if_cond->templateValue();
$if_condData = array( 'value' => $if_cond );
$tpl->processOperator( 'ezmodule',
                       array (
  0 => 
  array (
    0 => 
    array (
      0 => 1,
      1 => 'user/register',
      2 => false,
    ),
  ),
),
                       $rootNamespace, $currentNamespace, $if_condData, false, false );
$if_cond = $if_condData['value'];
unset( $if_condData );
if (! isset( $if_cond ) ) $if_cond = NULL;
while ( is_object( $if_cond ) and method_exists( $if_cond, 'templateValue' ) )
    $if_cond = $if_cond->templateValue();

if ( $if_cond )
{
$text .= '                        <p>';
unset( $var );
$var = '%login_link_startLog in%login_link_end or %create_link_startcreate a user account%create_link_end to comment.';
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$varData = array( 'value' => $var );
$tpl->processOperator( 'i18n',
                       array (
  0 => 
  array (
    0 => 
    array (
      0 => 1,
      1 => 'design/ezwebin/full/article',
      2 => false,
    ),
  ),
  1 => 
  array (
  ),
  2 => 
  array (
    0 => 
    array (
      0 => 6,
      1 => 
      array (
        0 => 'hash',
        1 => 
        array (
          0 => 
          array (
            0 => 1,
            1 => '%login_link_start',
            2 => false,
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            0 => 6,
            1 => 
            array (
              0 => 'concat',
              1 => 
              array (
                0 => 
                array (
                  0 => 1,
                  1 => '<a href="',
                  2 => false,
                ),
              ),
              2 => 
              array (
                0 => 
                array (
                  0 => 1,
                  1 => '/user/login',
                  2 => false,
                ),
                1 => 
                array (
                  0 => 6,
                  1 => 
                  array (
                    0 => 'ezurl',
                    1 => 
                    array (
                      0 => 
                      array (
                        0 => 3,
                        1 => 'no',
                        2 => false,
                      ),
                    ),
                  ),
                  2 => false,
                ),
              ),
              3 => 
              array (
                0 => 
                array (
                  0 => 1,
                  1 => '">',
                  2 => false,
                ),
              ),
            ),
            2 => false,
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            0 => 1,
            1 => '%login_link_end',
            2 => false,
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            0 => 1,
            1 => '</a>',
            2 => false,
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            0 => 1,
            1 => '%create_link_start',
            2 => false,
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            0 => 6,
            1 => 
            array (
              0 => 'concat',
              1 => 
              array (
                0 => 
                array (
                  0 => 1,
                  1 => '<a href="',
                  2 => false,
                ),
              ),
              2 => 
              array (
                0 => 
                array (
                  0 => 1,
                  1 => '/user/register',
                  2 => false,
                ),
                1 => 
                array (
                  0 => 6,
                  1 => 
                  array (
                    0 => 'ezurl',
                    1 => 
                    array (
                      0 => 
                      array (
                        0 => 3,
                        1 => 'no',
                        2 => false,
                      ),
                    ),
                  ),
                  2 => false,
                ),
              ),
              3 => 
              array (
                0 => 
                array (
                  0 => 1,
                  1 => '">',
                  2 => false,
                ),
              ),
            ),
            2 => false,
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            0 => 1,
            1 => '%create_link_end',
            2 => false,
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            0 => 1,
            1 => '</a>',
            2 => false,
          ),
        ),
      ),
      2 => false,
    ),
  ),
),
                       $rootNamespace, $currentNamespace, $varData, false, false );
$var = $varData['value'];
unset( $varData );
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$text .= $var;
unset( $var );

$text .= '</p>
                    ';
}
else
{
$text .= '                        <p>';
unset( $var );
$var = '%login_link_startLog in%login_link_end to comment.';
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$varData = array( 'value' => $var );
$tpl->processOperator( 'i18n',
                       array (
  0 => 
  array (
    0 => 
    array (
      0 => 1,
      1 => 'design/ezwebin/article/comments',
      2 => false,
    ),
  ),
  1 => 
  array (
  ),
  2 => 
  array (
    0 => 
    array (
      0 => 6,
      1 => 
      array (
        0 => 'hash',
        1 => 
        array (
          0 => 
          array (
            0 => 1,
            1 => '%login_link_start',
            2 => false,
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            0 => 6,
            1 => 
            array (
              0 => 'concat',
              1 => 
              array (
                0 => 
                array (
                  0 => 1,
                  1 => '<a href="',
                  2 => false,
                ),
              ),
              2 => 
              array (
                0 => 
                array (
                  0 => 1,
                  1 => '/user/login',
                  2 => false,
                ),
                1 => 
                array (
                  0 => 6,
                  1 => 
                  array (
                    0 => 'ezurl',
                    1 => 
                    array (
                      0 => 
                      array (
                        0 => 3,
                        1 => 'no',
                        2 => false,
                      ),
                    ),
                  ),
                  2 => false,
                ),
              ),
              3 => 
              array (
                0 => 
                array (
                  0 => 1,
                  1 => '">',
                  2 => false,
                ),
              ),
            ),
            2 => false,
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            0 => 1,
            1 => '%login_link_end',
            2 => false,
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            0 => 1,
            1 => '</a>',
            2 => false,
          ),
        ),
      ),
      2 => false,
    ),
  ),
),
                       $rootNamespace, $currentNamespace, $varData, false, false );
$var = $varData['value'];
unset( $varData );
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$text .= $var;
unset( $var );

$text .= '</p>
                    ';
}
unset( $if_cond );
// if ends

$text .= '                ';
}
unset( $if_cond );
// if ends

$text .= '        ';
}
elseif ( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_6 )
{
$text .= '                ';
// def $page_limit
unset( $var );
unset( $var1 );
unset( $var1 );
$var1 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( "node", $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]["node"] : null;
$var2 = compiledFetchAttribute( $var1, "data_map" );
unset( $var1 );
$var1 = $var2;
$var2 = compiledFetchAttribute( $var1, "show_children_pr_page" );
unset( $var1 );
$var1 = $var2;
$var2 = compiledFetchAttribute( $var1, "data_int" );
unset( $var1 );
$var1 = $var2;
if (! isset( $var1 ) ) $var1 = NULL;
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();

if ( isset( $var1 ) )
{
    $var = $var1;
}
else
{
    $var = 10;
}
unset( $var1 );
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
if ( $tpl->hasVariable( 'page_limit', $rootNamespace ) )
{
    $tpl->warning( 'def', "Variable 'page_limit' is already defined.", array (
  0 => 
  array (
    0 => 75,
    1 => 16,
    2 => 4553,
  ),
  1 => 
  array (
    0 => 77,
    1 => 41,
    2 => 4771,
  ),
  2 => 'extension/ezwebin/design/ezwebin/templates/node/view/full.tpl',
) );
    $tpl->setVariable( 'page_limit', $var, $rootNamespace );
}
else
{
    $tpl->setLocalVariable( 'page_limit', $var, $rootNamespace );
}

// def $classes
if ( $tpl->hasVariable( 'classes', $rootNamespace ) )
{
    $tpl->warning( 'def', "Variable 'classes' is already defined.", array (
  0 => 
  array (
    0 => 75,
    1 => 16,
    2 => 4553,
  ),
  1 => 
  array (
    0 => 77,
    1 => 41,
    2 => 4771,
  ),
  2 => 'extension/ezwebin/design/ezwebin/templates/node/view/full.tpl',
) );
    $tpl->setVariable( 'classes', array (
  0 => 'infobox',
), $rootNamespace );
}
else
{
    $tpl->setLocalVariable( 'classes', array (
  0 => 'infobox',
), $rootNamespace );
}

// def $children_count
if ( $tpl->hasVariable( 'children_count', $rootNamespace ) )
{
    $tpl->warning( 'def', "Variable 'children_count' is already defined.", array (
  0 => 
  array (
    0 => 75,
    1 => 16,
    2 => 4553,
  ),
  1 => 
  array (
    0 => 77,
    1 => 41,
    2 => 4771,
  ),
  2 => 'extension/ezwebin/design/ezwebin/templates/node/view/full.tpl',
) );
    $tpl->setVariable( 'children_count', '', $rootNamespace );
}
else
{
    $tpl->setLocalVariable( 'children_count', '', $rootNamespace );
}

$text .= '                ';
// if begins
unset( $if_cond );
unset( $if_cond1 );
unset( $if_cond1 );
$if_cond1 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$if_cond2 = compiledFetchAttribute( $if_cond1, 'data_map' );
unset( $if_cond1 );
$if_cond1 = $if_cond2;
$if_cond2 = compiledFetchAttribute( $if_cond1, 'show_children_exclude' );
unset( $if_cond1 );
$if_cond1 = $if_cond2;
if (! isset( $if_cond1 ) ) $if_cond1 = NULL;
while ( is_object( $if_cond1 ) and method_exists( $if_cond1, 'templateValue' ) )
    $if_cond1 = $if_cond1->templateValue();
$if_cond = isset( $if_cond1 );unset( $if_cond1 );
if (! isset( $if_cond ) ) $if_cond = NULL;
while ( is_object( $if_cond ) and method_exists( $if_cond, 'templateValue' ) )
    $if_cond = $if_cond->templateValue();

if ( $if_cond )
{
$text .= '                    ';
unset( $var );
unset( $var1 );
unset( $var1 );
$var1 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$var2 = compiledFetchAttribute( $var1, 'data_map' );
unset( $var1 );
$var1 = $var2;
$var2 = compiledFetchAttribute( $var1, 'show_children_exclude' );
unset( $var1 );
$var1 = $var2;
$var2 = compiledFetchAttribute( $var1, 'content' );
unset( $var1 );
$var1 = $var2;
if (! isset( $var1 ) ) $var1 = NULL;
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
if ( is_string( $var1 ) )
{
	$var = explode( ",", $var1 );
}
else if ( is_array( $var1 ) )
{
	$var = array( array_slice( $var1, 0, "," ), array_slice( $var1, "," ) );
}
else
{
	$var = null;
}
unset( $var1 );
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
if ( array_key_exists( $currentNamespace, $vars ) && array_key_exists( 'classes', $vars[$currentNamespace] ) )
{
    $vars[$currentNamespace]['classes'] = $var;
    unset( $var );
}
$text .= '                ';
}
unset( $if_cond );
// if ends

$text .= '
                ';
unset( $var );
unset( $var1 );
unset( $var1 );
$var1 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$var2 = compiledFetchAttribute( $var1, 'node_id' );
unset( $var1 );
$var1 = $var2;
if (! isset( $var1 ) ) $var1 = NULL;
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();
unset( $var2 );
unset( $var2 );
$var2 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'classes', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['classes'] : null;
if (! isset( $var2 ) ) $var2 = NULL;
while ( is_object( $var2 ) and method_exists( $var2, 'templateValue' ) )
    $var2 = $var2->templateValue();
while ( is_object( $var2 ) and method_exists( $var2, 'templateValue' ) )
    $var2 = $var2->templateValue();
$var = call_user_func_array( array( new eZContentFunctionCollection(), 'fetchObjectTreeCount' ),
  array(     'parent_node_id' => $var1,
    'only_translated' => false,
    'language' => false,
    'class_filter_type' => "exclude",
    'class_filter_array' => $var2,
    'attribute_filter' => false,
    'depth' => 1,
    'depth_operator' => "le",
    'ignore_visibility' => false,
    'limitation' => null,
    'main_node_only' => false,
    'extended_attribute_filter' => false,
    'objectname_filter' => null ) );
$var = isset( $var['result'] ) ? $var['result'] : null;
unset( $var1, $var2 );
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
if ( array_key_exists( $currentNamespace, $vars ) && array_key_exists( 'children_count', $vars[$currentNamespace] ) )
{
    $vars[$currentNamespace]['children_count'] = $var;
    unset( $var );
}
$text .= '
                <div class="content-view-children">
                    ';
// if begins
unset( $if_cond );
unset( $if_cond );
$if_cond = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'children_count', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['children_count'] : null;
if (! isset( $if_cond ) ) $if_cond = NULL;
while ( is_object( $if_cond ) and method_exists( $if_cond, 'templateValue' ) )
    $if_cond = $if_cond->templateValue();

if ( $if_cond )
{
$text .= '                        ';
// foreach begins
$skipDelimiter = true;
if ( !isset( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_5 ) ) $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_5 = array();
$fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_5[] = compact( 'fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5', 'fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_5', 'fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_5', 'fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_5', 'fe_i_c1a23d0a76888f9330f32ba8917d1d7e_5', 'fe_key_c1a23d0a76888f9330f32ba8917d1d7e_5', 'fe_val_c1a23d0a76888f9330f32ba8917d1d7e_5', 'fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5', 'fe_max_c1a23d0a76888f9330f32ba8917d1d7e_5', 'fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_5', 'fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_5', 'fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_5' );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5 );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51 );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52 = compiledFetchAttribute( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51, 'node_id' );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52;
if (! isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51 ) ) $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51 = NULL;
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51->templateValue();
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51->templateValue();
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52 );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53 = compiledFetchAttribute( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52, 'sort_array' );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53;
if (! isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52 ) ) $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52 = NULL;
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52->templateValue();
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52->templateValue();
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53 );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'view_parameters', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['view_parameters'] : null;
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54 = compiledFetchAttribute( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53, 'offset' );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54;
if (! isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53 ) ) $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53 = NULL;
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53->templateValue();
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53->templateValue();
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54 );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'page_limit', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['page_limit'] : null;
if (! isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54 ) ) $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54 = NULL;
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54->templateValue();
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54->templateValue();
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55 );
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55 );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'classes', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['classes'] : null;
if (! isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55 ) ) $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55 = NULL;
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55->templateValue();
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55->templateValue();
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5 = call_user_func_array( array( new eZContentFunctionCollection(), 'fetchObjectTree' ),
  array(     'parent_node_id' => $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51,
    'sort_by' => $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52,
    'only_translated' => false,
    'language' => false,
    'offset' => $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53,
    'limit' => $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54,
    'depth' => 1,
    'depth_operator' => "le",
    'class_id' => false,
    'attribute_filter' => false,
    'extended_attribute_filter' => false,
    'class_filter_type' => "exclude",
    'class_filter_array' => $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55,
    'group_by' => false,
    'main_node_only' => false,
    'ignore_visibility' => false,
    'limitation' => null,
    'as_object' => null,
    'objectname_filter' => null,
    'load_data_map' => null ) );
$fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5 = isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5['result'] ) ? $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5['result'] : null;
unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_51, $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_52, $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_53, $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_54, $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_55 );
if (! isset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5 ) ) $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5 = NULL;
while ( is_object( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5 ) and method_exists( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5, 'templateValue' ) )
    $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5->templateValue();

$fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_5 = is_array( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5 ) ? array_keys( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5 ) : array();
$fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_5 = count( $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_5 );
$fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_5 = 0;
$fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5 = 0;
$fe_max_c1a23d0a76888f9330f32ba8917d1d7e_5 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_5 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5;
$fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_5 = false;
if ( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5 < 0 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5 >= $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_5 )
{
    $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5 = ( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5 < 0 ) ? 0 : $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_5;
    if ( $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_5 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5 < 0 )
 {
        eZDebug::writeWarning("Invalid 'offset' parameter specified: '$fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5'. Array count: $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_5");   
}
}
if ( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_5 < 0 || $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5 + $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_5 > $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_5 )
{
    if ( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_5 < 0 )
 eZDebug::writeWarning("Invalid 'max' parameter specified: $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_5");
    $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_5 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_5 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5;
}
if ( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_5 )
{
    $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_5 = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_5 - 1 - $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5;
    $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_5  = 0;
}
else
{
    $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_5 = $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5;
    $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_5  = $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_5 - 1;
}
// foreach
for ( $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_5 = $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_5; $fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_5 < $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_5 && ( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_5 ? $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_5 >= $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_5 : $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_5 <= $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_5 ); $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_5 ? $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_5-- : $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_5++ )
{
$fe_key_c1a23d0a76888f9330f32ba8917d1d7e_5 = $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_5[$fe_i_c1a23d0a76888f9330f32ba8917d1d7e_5];
$fe_val_c1a23d0a76888f9330f32ba8917d1d7e_5 = $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5[$fe_key_c1a23d0a76888f9330f32ba8917d1d7e_5];
$vars[$rootNamespace]['child'] = $fe_val_c1a23d0a76888f9330f32ba8917d1d7e_5;
$text .= '                            ';
unset( $var );
unset( $var );
$var = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'child', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['child'] : null;
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
$vars[$namespace]['node'] = $var;
unset( $var );
if ( !isset( $dKeys ) )
{
    $resH = $tpl->resourceHandler( 'design' );
    $dKeys = $resH->keys();
}
if ( !isset( $dKeysStack ) )
{
    $dKeysStack = array();
}
$dKeysStack[] = $dKeys;
unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'node_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["node"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'contentobject_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["object"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'contentclass_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["class"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'section_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["section"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'class_identifier' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["class_identifier"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'match_ingroup_id_list' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["class_group"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'state_id_array' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["state"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'state_identifier_array' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["state_identifier"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'parent_node_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["parent_node"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'depth' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["depth"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'url_alias' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["url_alias"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'remote_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["remote_id"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'remote_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["node_remote_id"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'parent' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'class_identifier' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["parent_class_identifier"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'parent' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'remote_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["parent_node_remote_id"] = $dKey;
unset( $dKey );

unset( $dKey );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $dKey );
$dKey = ( array_key_exists( $namespace, $vars ) and array_key_exists( 'node', $vars[$namespace] ) ) ? $vars[$namespace]['node'] : null;
$dKey1 = compiledFetchAttribute( $dKey, 'parent' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'object' );
unset( $dKey );
$dKey = $dKey1;
$dKey1 = compiledFetchAttribute( $dKey, 'remote_id' );
unset( $dKey );
$dKey = $dKey1;
if (! isset( $dKey ) ) $dKey = NULL;
while ( is_object( $dKey ) and method_exists( $dKey, 'templateValue' ) )
    $dKey = $dKey->templateValue();

$dKeys["parent_object_remote_id"] = $dKey;
unset( $dKey );

$resourceFound = false;
if ( file_exists( 'var/ezwebin_site/cache/template/compiled/line-a3f211d55d3ceccba88d733e6c17d7e7.php' ) )
{
$resourceFound = true;
$namespaceStack[] = array( $rootNamespace, $currentNamespace );
$currentNamespace = $rootNamespace = !$currentNamespace ? 'NodeView' : ( $currentNamespace . ':' . 'NodeView' );
$tpl->createLocalVariablesList();
$tpl->appendTemplateFetch( 'extension/ezwebin/design/ezwebin/templates/node/view/line.tpl' );
include( '' . 'var/ezwebin_site/cache/template/compiled/line-a3f211d55d3ceccba88d733e6c17d7e7.php' );
$tpl->unsetLocalVariables();
$tpl->destroyLocalVariablesList();
list( $rootNamespace, $currentNamespace ) = array_pop( $namespaceStack );
}
else
{
    $resourceFound = true;
$resourceFound = true;
$namespaceStack[] = array( $rootNamespace, $currentNamespace );
$currentNamespace = $rootNamespace = !$currentNamespace ? 'NodeView' : ( $currentNamespace . ':' . 'NodeView' );
$textElements = array();
$extraParameters = array();
$tpl->processURI( 'extension/ezwebin/design/ezwebin/templates/node/view/line.tpl', true, $extraParameters, $textElements, $rootNamespace, $currentNamespace );
$text .= implode( '', $textElements );
list( $rootNamespace, $currentNamespace ) = array_pop( $namespaceStack );
}

$dKeys = array_pop( $dKeysStack );
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "NodeView";
else
    $namespace .= ':NodeView';
unset( $vars[$namespace]['node'] );
$text .= '                        ';
$fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_5++;
} // foreach
$skipDelimiter = false;
if ( count( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_5 ) ) extract( array_pop( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_5 ) );

else
{

unset( $fe_array_c1a23d0a76888f9330f32ba8917d1d7e_5 );

unset( $fe_array_keys_c1a23d0a76888f9330f32ba8917d1d7e_5 );

unset( $fe_n_items_c1a23d0a76888f9330f32ba8917d1d7e_5 );

unset( $fe_n_items_processed_c1a23d0a76888f9330f32ba8917d1d7e_5 );

unset( $fe_i_c1a23d0a76888f9330f32ba8917d1d7e_5 );

unset( $fe_key_c1a23d0a76888f9330f32ba8917d1d7e_5 );

unset( $fe_val_c1a23d0a76888f9330f32ba8917d1d7e_5 );

unset( $fe_offset_c1a23d0a76888f9330f32ba8917d1d7e_5 );

unset( $fe_max_c1a23d0a76888f9330f32ba8917d1d7e_5 );

unset( $fe_reverse_c1a23d0a76888f9330f32ba8917d1d7e_5 );

unset( $fe_first_val_c1a23d0a76888f9330f32ba8917d1d7e_5 );

unset( $fe_last_val_c1a23d0a76888f9330f32ba8917d1d7e_5 );

unset( $fe_variable_stack_c1a23d0a76888f9330f32ba8917d1d7e_5 );

}

// foreach ends
$text .= '                    ';
}
unset( $if_cond );
// if ends

$text .= '                </div>

                ';
$oldRestoreIncludeArray_ff83c817f96bca8bd9320d7330a6676c = isset( $restoreIncludeArray ) ? $restoreIncludeArray : array();
$restoreIncludeArray = array();

if ( isset( $namespace ) and isset( $vars[$namespace]['page_uri'] ) )
    $restoreIncludeArray[] = array( $namespace, 'page_uri', $vars[$namespace]['page_uri'] );
elseif ( !isset( $vars[( isset( $namespace ) ? $namespace : '' )]['page_uri'] ) ) 
    $restoreIncludeArray[] = array( ( isset( $namespace ) ? $namespace : '' ), 'page_uri', 'unset' );

unset( $var );
unset( $var );
$var = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$var1 = compiledFetchAttribute( $var, 'url_alias' );
unset( $var );
$var = $var1;
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "navigator";
else
    $namespace .= ':navigator';
$vars[$namespace]['page_uri'] = $var;
unset( $var );
if ( isset( $namespace ) and isset( $vars[$namespace]['item_count'] ) )
    $restoreIncludeArray[] = array( $namespace, 'item_count', $vars[$namespace]['item_count'] );
elseif ( !isset( $vars[( isset( $namespace ) ? $namespace : '' )]['item_count'] ) ) 
    $restoreIncludeArray[] = array( ( isset( $namespace ) ? $namespace : '' ), 'item_count', 'unset' );

unset( $var );
unset( $var );
$var = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'children_count', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['children_count'] : null;
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "navigator";
else
    $namespace .= ':navigator';
$vars[$namespace]['item_count'] = $var;
unset( $var );
if ( isset( $namespace ) and isset( $vars[$namespace]['view_parameters'] ) )
    $restoreIncludeArray[] = array( $namespace, 'view_parameters', $vars[$namespace]['view_parameters'] );
elseif ( !isset( $vars[( isset( $namespace ) ? $namespace : '' )]['view_parameters'] ) ) 
    $restoreIncludeArray[] = array( ( isset( $namespace ) ? $namespace : '' ), 'view_parameters', 'unset' );

unset( $var );
unset( $var );
$var = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'view_parameters', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['view_parameters'] : null;
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "navigator";
else
    $namespace .= ':navigator';
$vars[$namespace]['view_parameters'] = $var;
unset( $var );
if ( isset( $namespace ) and isset( $vars[$namespace]['item_limit'] ) )
    $restoreIncludeArray[] = array( $namespace, 'item_limit', $vars[$namespace]['item_limit'] );
elseif ( !isset( $vars[( isset( $namespace ) ? $namespace : '' )]['item_limit'] ) ) 
    $restoreIncludeArray[] = array( ( isset( $namespace ) ? $namespace : '' ), 'item_limit', 'unset' );

unset( $var );
unset( $var );
$var = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'page_limit', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['page_limit'] : null;
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$namespace = $currentNamespace;
if ( $namespace == '' )
    $namespace = "navigator";
else
    $namespace .= ':navigator';
$vars[$namespace]['item_limit'] = $var;
unset( $var );
if ( !isset( $dKeys ) )
{
    $resH = $tpl->resourceHandler( "design" );
    $dKeys = $resH->keys();
}

$resourceFound = false;
if ( file_exists( 'var/ezwebin_site/cache/template/compiled/google-ef372f14ee9f6e0738c956baac6b6e96.php' ) )
{
$resourceFound = true;
$namespaceStack[] = array( $rootNamespace, $currentNamespace );
$currentNamespace = $rootNamespace = !$currentNamespace ? 'navigator' : ( $currentNamespace . ':' . 'navigator' );
$tpl->createLocalVariablesList();
$tpl->appendTemplateFetch( 'design/standard/templates/navigator/google.tpl' );
include( '' . 'var/ezwebin_site/cache/template/compiled/google-ef372f14ee9f6e0738c956baac6b6e96.php' );
$tpl->unsetLocalVariables();
$tpl->destroyLocalVariablesList();
list( $rootNamespace, $currentNamespace ) = array_pop( $namespaceStack );
}
else
{
    $resourceFound = true;
$resourceFound = true;
$namespaceStack[] = array( $rootNamespace, $currentNamespace );
$currentNamespace = $rootNamespace = !$currentNamespace ? 'navigator' : ( $currentNamespace . ':' . 'navigator' );
$textElements = array();
$extraParameters = array();
$tpl->processURI( 'design/standard/templates/navigator/google.tpl', true, $extraParameters, $textElements, $rootNamespace, $currentNamespace );
$text .= implode( '', $textElements );
list( $rootNamespace, $currentNamespace ) = array_pop( $namespaceStack );
}

foreach ( $restoreIncludeArray as $element )
{
    if ( $element[2] === 'unset' )
    {
        unset( $vars[$element[0]][$element[1]] );
        continue;
    }
    $vars[$element[0]][$element[1]] = $element[2];
}
$restoreIncludeArray = $oldRestoreIncludeArray_ff83c817f96bca8bd9320d7330a6676c;

$text .= '
        ';
}
unset( $if_cond );
// if ends

unset( $elseif_cond_c1a23d0a76888f9330f32ba8917d1d7e_6 );

$text .= '    ';
}
unset( $if_cond );
// if ends

$text .= '
    ';
// if begins
unset( $if_cond );
unset( $if_cond1 );
unset( $if_cond2 );
unset( $if_cond2 );
$if_cond2 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'versionview_mode', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['versionview_mode'] : null;
if (! isset( $if_cond2 ) ) $if_cond2 = NULL;
while ( is_object( $if_cond2 ) and method_exists( $if_cond2, 'templateValue' ) )
    $if_cond2 = $if_cond2->templateValue();
$if_cond1 = !isset( $if_cond2 );unset( $if_cond2 );
if (! isset( $if_cond1 ) ) $if_cond1 = NULL;
while ( is_object( $if_cond1 ) and method_exists( $if_cond1, 'templateValue' ) )
    $if_cond1 = $if_cond1->templateValue();
unset( $if_cond2 );
unset( $if_cond3 );
unset( $if_cond3 );
$if_cond3 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$if_cond4 = compiledFetchAttribute( $if_cond3, 'data_map' );
unset( $if_cond3 );
$if_cond3 = $if_cond4;
$if_cond4 = compiledFetchAttribute( $if_cond3, 'enable_tipafriend' );
unset( $if_cond3 );
$if_cond3 = $if_cond4;
if (! isset( $if_cond3 ) ) $if_cond3 = NULL;
while ( is_object( $if_cond3 ) and method_exists( $if_cond3, 'templateValue' ) )
    $if_cond3 = $if_cond3->templateValue();
$if_cond2 = isset( $if_cond3 );unset( $if_cond3 );
if (! isset( $if_cond2 ) ) $if_cond2 = NULL;
while ( is_object( $if_cond2 ) and method_exists( $if_cond2, 'templateValue' ) )
    $if_cond2 = $if_cond2->templateValue();
unset( $if_cond3 );
unset( $if_cond3 );
$if_cond3 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$if_cond4 = compiledFetchAttribute( $if_cond3, 'data_map' );
unset( $if_cond3 );
$if_cond3 = $if_cond4;
$if_cond4 = compiledFetchAttribute( $if_cond3, 'enable_tipafriend' );
unset( $if_cond3 );
$if_cond3 = $if_cond4;
$if_cond4 = compiledFetchAttribute( $if_cond3, 'data_int' );
unset( $if_cond3 );
$if_cond3 = $if_cond4;
if (! isset( $if_cond3 ) ) $if_cond3 = NULL;
while ( is_object( $if_cond3 ) and method_exists( $if_cond3, 'templateValue' ) )
    $if_cond3 = $if_cond3->templateValue();
if ( !$if_cond1 )
    $if_cond = false;
else if ( !$if_cond2 )
    $if_cond = false;
else if ( !$if_cond3 )
    $if_cond = false;
else
    $if_cond = $if_cond3;
unset( $if_cond1, $if_cond2, $if_cond3 );
if (! isset( $if_cond ) ) $if_cond = NULL;
while ( is_object( $if_cond ) and method_exists( $if_cond, 'templateValue' ) )
    $if_cond = $if_cond->templateValue();

if ( $if_cond )
{
$text .= '        ';
// def $tipafriend_access
unset( $var );
$var = call_user_func_array( array( new eZUserFunctionCollection(), 'hasAccessTo' ),
  array(     'module' => "content",
    'function' => "tipafriend",
    'user_id' => null ) );
$var = isset( $var['result'] ) ? $var['result'] : null;
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
if ( $tpl->hasVariable( 'tipafriend_access', $rootNamespace ) )
{
    $tpl->warning( 'def', "Variable 'tipafriend_access' is already defined.", array (
  0 => 
  array (
    0 => 110,
    1 => 8,
    2 => 6595,
  ),
  1 => 
  array (
    0 => 111,
    1 => 98,
    2 => 6775,
  ),
  2 => 'extension/ezwebin/design/ezwebin/templates/node/view/full.tpl',
) );
    $tpl->setVariable( 'tipafriend_access', $var, $rootNamespace );
}
else
{
    $tpl->setLocalVariable( 'tipafriend_access', $var, $rootNamespace );
}

$text .= '        ';
// if begins
unset( $if_cond );
unset( $if_cond1 );
if (! isset( $if_cond1 ) ) $if_cond1 = NULL;
while ( is_object( $if_cond1 ) and method_exists( $if_cond1, 'templateValue' ) )
    $if_cond1 = $if_cond1->templateValue();
$if_cond1Data = array( 'value' => $if_cond1 );
$tpl->processOperator( 'ezmodule',
                       array (
  0 => 
  array (
    0 => 
    array (
      0 => 1,
      1 => 'content/tipafriend',
      2 => false,
    ),
  ),
),
                       $rootNamespace, $currentNamespace, $if_cond1Data, false, false );
$if_cond1 = $if_cond1Data['value'];
unset( $if_cond1Data );
if (! isset( $if_cond1 ) ) $if_cond1 = NULL;
while ( is_object( $if_cond1 ) and method_exists( $if_cond1, 'templateValue' ) )
    $if_cond1 = $if_cond1->templateValue();
unset( $if_cond2 );
unset( $if_cond2 );
$if_cond2 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'tipafriend_access', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['tipafriend_access'] : null;
if (! isset( $if_cond2 ) ) $if_cond2 = NULL;
while ( is_object( $if_cond2 ) and method_exists( $if_cond2, 'templateValue' ) )
    $if_cond2 = $if_cond2->templateValue();
if ( !$if_cond1 )
    $if_cond = false;
else if ( !$if_cond2 )
    $if_cond = false;
else
    $if_cond = $if_cond2;
unset( $if_cond1, $if_cond2 );
if (! isset( $if_cond ) ) $if_cond = NULL;
while ( is_object( $if_cond ) and method_exists( $if_cond, 'templateValue' ) )
    $if_cond = $if_cond->templateValue();

if ( $if_cond )
{
$text .= '        <div class="attribute-tipafriend">
            <p><a href=';
unset( $var );
unset( $var1 );
unset( $var3 );
unset( $var3 );
$var3 = ( array_key_exists( $rootNamespace, $vars ) and array_key_exists( 'node', $vars[$rootNamespace] ) ) ? $vars[$rootNamespace]['node'] : null;
$var4 = compiledFetchAttribute( $var3, 'node_id' );
unset( $var3 );
$var3 = $var4;
if (! isset( $var3 ) ) $var3 = NULL;
while ( is_object( $var3 ) and method_exists( $var3, 'templateValue' ) )
    $var3 = $var3->templateValue();
while ( is_object( $var3 ) and method_exists( $var3, 'templateValue' ) )
    $var3 = $var3->templateValue();
$var1 = ( '/content/tipafriend/' . $var3 );
unset( $var3 );
if (! isset( $var1 ) ) $var1 = NULL;
while ( is_object( $var1 ) and method_exists( $var1, 'templateValue' ) )
    $var1 = $var1->templateValue();

eZURI::transformURI( $var1, false, eZURI::getTransformURIMode() );
$var1 = '"' . $var1 . '"';
$var = $var1;
unset( $var1 );
if (! isset( $var ) ) $var = NULL;
while ( is_object( $var ) and method_exists( $var, 'templateValue' ) )
    $var = $var->templateValue();
$text .= $var;
unset( $var );

$text .= ' title="友達に教える">友達に教える</a></p>
        </div>
        ';
}
unset( $if_cond );
// if ends

$text .= '    ';
}
unset( $if_cond );
// if ends

$text .= '
    </div>
</div>

</div></div></div>
<div class="border-bl"><div class="border-br"><div class="border-bc"></div></div></div>
</div>
';

$setArray = $oldSetArray_4f56b8828a471e1d70202e41d0e69e8f;
$tpl->Level--;
?>
