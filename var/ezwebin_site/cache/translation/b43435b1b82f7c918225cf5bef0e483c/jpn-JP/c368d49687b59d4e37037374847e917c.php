<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/browse_export',
);

$TranslationRoot = array (
  '3a98e452a8740a052292df8a548db2c3' => 
  array (
    'context' => 'design/admin/content/browse_export',
    'source' => 'Choose export node',
    'comment' => NULL,
    'translation' => 'エクスポートするノードの選択',
    'key' => '3a98e452a8740a052292df8a548db2c3',
  ),
  '557def1353731244874bc4a6724b7268' => 
  array (
    'context' => 'design/admin/content/browse_export',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => '557def1353731244874bc4a6724b7268',
  ),
  '41093769ccf71876811d97a0a19b58bf' => 
  array (
    'context' => 'design/admin/content/browse_export',
    'source' => 'Select the item that you want to export using the checkboxes then click "Select".',
    'comment' => NULL,
    'translation' => 'エクスポートしたいアイテムをチェックボックスで選択し、"選択"をクリックしてください。',
    'key' => '41093769ccf71876811d97a0a19b58bf',
  ),
);
?>
