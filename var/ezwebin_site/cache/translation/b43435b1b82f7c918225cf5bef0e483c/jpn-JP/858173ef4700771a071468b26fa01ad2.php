<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/user/forgotpassword',
);

$TranslationRoot = array (
  '76e1bd8c75a682f1e73a0364a7645211' => 
  array (
    'context' => 'design/ezwebin/user/forgotpassword',
    'source' => 'Password was successfully generated and sent to: %1',
    'comment' => NULL,
    'translation' => '新しいパスワードは %1 に送信されました.',
    'key' => '76e1bd8c75a682f1e73a0364a7645211',
  ),
  '800a0b48e98999ada29a7559950d6913' => 
  array (
    'context' => 'design/ezwebin/user/forgotpassword',
    'source' => 'The key is invalid or has been used. ',
    'comment' => NULL,
    'translation' => 'パスワードが無効か、またはすでに使用されています。',
    'key' => '800a0b48e98999ada29a7559950d6913',
  ),
  '52d73466a21a2220a9af918b5b60a3c1' => 
  array (
    'context' => 'design/ezwebin/user/forgotpassword',
    'source' => 'Have you forgotten your password?',
    'comment' => NULL,
    'translation' => 'パスワードを忘れましたか?',
    'key' => '52d73466a21a2220a9af918b5b60a3c1',
  ),
  '9833b97a6e92de6be41e7fbf3a40d39f' => 
  array (
    'context' => 'design/ezwebin/user/forgotpassword',
    'source' => 'Generate new password',
    'comment' => NULL,
    'translation' => '新規パスワードの作成',
    'key' => '9833b97a6e92de6be41e7fbf3a40d39f',
  ),
  'be86394d1eb75e27b1bc410aa41e447f' => 
  array (
    'context' => 'design/ezwebin/user/forgotpassword',
    'source' => 'An email has been sent to the following address: %1. It contains a link you need to click so that we can confirm that the correct user has received the new password.',
    'comment' => NULL,
    'translation' => '%1にメールが送信されました。正確なユーザーへパスワードが届いたかを確認するため、メールに指定されてあるリンクをクリックしてください。',
    'key' => 'be86394d1eb75e27b1bc410aa41e447f',
  ),
  'fc242b1f96c12f7df2edc5f1ae044fca' => 
  array (
    'context' => 'design/ezwebin/user/forgotpassword',
    'source' => 'There is no registered user with that email address.',
    'comment' => NULL,
    'translation' => 'このメールアドレスを持つユーザは登録されていません。',
    'key' => 'fc242b1f96c12f7df2edc5f1ae044fca',
  ),
  '2e300cbb2285dffa4366aefc1c79559a' => 
  array (
    'context' => 'design/ezwebin/user/forgotpassword',
    'source' => 'If you have forgotten your password, enter your email address and we will create a new password and send it to you.',
    'comment' => NULL,
    'translation' => 'パスワードを忘れた場合は、メールアドレスを入力してください。新しいパスワードを登録したメールアドレスに送信します。',
    'key' => '2e300cbb2285dffa4366aefc1c79559a',
  ),
  '830d8084b1abc1d4ecaba69efd05a90a' => 
  array (
    'context' => 'design/ezwebin/user/forgotpassword',
    'source' => 'Email',
    'comment' => NULL,
    'translation' => 'e-mail',
    'key' => '830d8084b1abc1d4ecaba69efd05a90a',
  ),
);
?>
