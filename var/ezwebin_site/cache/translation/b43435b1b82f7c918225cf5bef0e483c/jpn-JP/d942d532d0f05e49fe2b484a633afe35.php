<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/view/ezmedia',
);

$TranslationRoot = array (
  '9e25b2adc29e1d714ed52d5e1a0456fd' => 
  array (
    'context' => 'design/ezwebin/view/ezmedia',
    'source' => 'No %link_startFlash player%link_end avaliable!',
    'comment' => NULL,
    'translation' => '%link_startフラッシュプレイヤー%link_endがインストールされていません。',
    'key' => '9e25b2adc29e1d714ed52d5e1a0456fd',
  ),
  '8dc5aebc88cefa8ef8625572bef9b909' => 
  array (
    'context' => 'design/ezwebin/view/ezmedia',
    'source' => 'No media file is available.',
    'comment' => NULL,
    'translation' => '利用できるメディアファイルはありません。',
    'key' => '8dc5aebc88cefa8ef8625572bef9b909',
  ),
  'b2067dd1bb219c6f5a6232cef8931329' => 
  array (
    'context' => 'design/ezwebin/view/ezmedia',
    'source' => 'Your browser does not support html5 video.',
    'comment' => NULL,
    'translation' => '利用しているブラウザはHTML5ビデオを対応しません。',
    'key' => 'b2067dd1bb219c6f5a6232cef8931329',
  ),
  'd86a02ac47c6d5fae25ca51f13313086' => 
  array (
    'context' => 'design/ezwebin/view/ezmedia',
    'source' => 'Your browser does not support html5 audio.',
    'comment' => NULL,
    'translation' => '利用しているブラウザはHTML5オーディオを対応しません。',
    'key' => 'd86a02ac47c6d5fae25ca51f13313086',
  ),
);
?>
