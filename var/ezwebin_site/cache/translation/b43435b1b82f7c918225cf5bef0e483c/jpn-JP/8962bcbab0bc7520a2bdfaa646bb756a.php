<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/role/createpolicystep2',
);

$TranslationRoot = array (
  'c12242f9b7bdd28e4f8425a06262f479' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Step one: select module [completed]',
    'comment' => NULL,
    'translation' => 'ステップ1: モジュールの選択 [完了]',
    'key' => 'c12242f9b7bdd28e4f8425a06262f479',
  ),
  '82d2788cea67eeeb4f5f40a830dc8593' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Selected module',
    'comment' => NULL,
    'translation' => '選択したモジュール',
    'key' => '82d2788cea67eeeb4f5f40a830dc8593',
  ),
  'ca54bc8a416fea872116a8dc6f6e62ba' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'All modules',
    'comment' => NULL,
    'translation' => 'すべてのモジュール',
    'key' => 'ca54bc8a416fea872116a8dc6f6e62ba',
  ),
  '456787f12a56c955cb6cecdbad94edac' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Selected access method',
    'comment' => NULL,
    'translation' => '選択したアクセス権',
    'key' => '456787f12a56c955cb6cecdbad94edac',
  ),
  '0827dd02fe77f98dcd44e410e40b4f7a' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Limited',
    'comment' => NULL,
    'translation' => '制限付き',
    'key' => '0827dd02fe77f98dcd44e410e40b4f7a',
  ),
  'a86a8840af26e533486be90a8d41b2b0' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Step two: select function',
    'comment' => NULL,
    'translation' => 'ステップ2: ファンクションの選択',
    'key' => 'a86a8840af26e533486be90a8d41b2b0',
  ),
  '20784d0db1146feb8964fa6d2f4350a6' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Instructions',
    'comment' => NULL,
    'translation' => '説明',
    'key' => '20784d0db1146feb8964fa6d2f4350a6',
  ),
  '1ef10df6594b3d8997cce659a4724711' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Function',
    'comment' => NULL,
    'translation' => 'ファンクション',
    'key' => '1ef10df6594b3d8997cce659a4724711',
  ),
  '735bb3040fc6e68dcab4fec05ee4c65d' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Grant full access',
    'comment' => NULL,
    'translation' => 'すべてのアクセス権を設定',
    'key' => '735bb3040fc6e68dcab4fec05ee4c65d',
  ),
  '04a281f2d672ca71c7928bd47117d14f' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Grant limited access',
    'comment' => NULL,
    'translation' => '制限付きのアクセス権を設定',
    'key' => '04a281f2d672ca71c7928bd47117d14f',
  ),
  'afe54b070611572e743eef3885e36d40' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'The selected module (%module_name) does not support limitations on the function level. Please go back to step one and use the "Grant access to all functions" option instead.',
    'comment' => NULL,
    'translation' => '選択したモジュール（%module_name）はファンクションレベルでの制限をサポートしていません。ステップ1に戻り“全ファンクションのアクセス権を設定”を選択してください。',
    'key' => 'afe54b070611572e743eef3885e36d40',
  ),
  '8c93bd4dd94c530c2d416ee3f989eddf' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Go back to step one',
    'comment' => NULL,
    'translation' => 'ステップ1に戻る',
    'key' => '8c93bd4dd94c530c2d416ee3f989eddf',
  ),
  'c920af1740a5fcc178394f6ff3e5471b' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => 'c920af1740a5fcc178394f6ff3e5471b',
  ),
  '62c3cb93612b29048fe53507ad769701' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '62c3cb93612b29048fe53507ad769701',
  ),
  '2bc4e8b3f652ff500c75d33ccc9ed7cf' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Create a new policy for the <%role_name> role',
    'comment' => NULL,
    'translation' => '<%role_name> ロールに新しいポリシーを作成',
    'key' => '2bc4e8b3f652ff500c75d33ccc9ed7cf',
  ),
  '97e2af0bc335f778237116201d99b3b9' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Click on one of the "Grant.." buttons (explained below) in order to go to the next step.',
    'comment' => NULL,
    'translation' => '次のステップに進むには、下記の"アクセス権を設定"ボタンの一つをクリックしてください。',
    'key' => '97e2af0bc335f778237116201d99b3b9',
  ),
  '57c9750d68ee46cafe89be06510c90b7' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Welcome to the policy wizard. This three-step wizard will help you set up a new policy. The policy will be added to the role that is currently being edited. The wizard can be aborted at any stage by using the "Cancel" button.',
    'comment' => NULL,
    'translation' => 'ポリシーウィザードへようこそ。この３ステップウィザードは新しいポリシーのセットアップをお手伝いします。現在編集されているロールにポリシーを追加します。”キャンセル”ボタンをクリックすると、いつでもウィザードを取り消すことが出来ます。',
    'key' => '57c9750d68ee46cafe89be06510c90b7',
  ),
  '66107c17d3eb71f8c50a99eebd6d5258' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'Use the drop-down menu to select the function that you want to grant access to.',
    'comment' => NULL,
    'translation' => 'ドロップダウンメニューを使って、アクセス権限を与えるファンクションを選択してください。',
    'key' => '66107c17d3eb71f8c50a99eebd6d5258',
  ),
  'dcd0e04a628feb90568a6dd271def8c7' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'The "Grant full access" button will create a policy that grants unlimited access to the selected function within the module that was specified in step one. If you want to limit the access method, click the "Grant limited access" button. Function limitation is only supported by some functions. If unsupported, eZ Publish will set up a policy with unlimited access to the selected function.',
    'comment' => NULL,
    'translation' => '“全ファンクションのアクセス権を設定”ボタンは選択したモジュールの全ファンクションに対するアクセス権を持つポリシーを作成します。制限されたアクセス権が必要な場合は“指定ファンクションのアクセス権を設定”ボタンを使用します。 このファンクション制限はすべてのモジュールでサポートされているものではないので注意してください。サポートされていない場合は、制限なしのアクセスが設定されます。',
    'key' => 'dcd0e04a628feb90568a6dd271def8c7',
  ),
  '6835ffd780fa5aa90773171e692b80b5' => 
  array (
    'context' => 'design/admin/role/createpolicystep2',
    'source' => 'It is not possible to grant limited access to all modules at once. To grant unlimited access to all modules and their functions, go back to step one and select "Grant access to all functions". To grant limited access to different functions within different modules, you must set up a collection of policies.',
    'comment' => NULL,
    'translation' => '一回ですべてのモジュールに制限付きのアクセス権限を与えることはできません。モジュールとファンクションに無制限のアクセスを与えるには、前のステップに戻って、”全ファンクションのアクセス権を設定”を選択してください。違うモジュール内の異なるファンクションへのアクセス権を設定するには、ポリシーを複数設定することが必要です。',
    'key' => '6835ffd780fa5aa90773171e692b80b5',
  ),
);
?>
