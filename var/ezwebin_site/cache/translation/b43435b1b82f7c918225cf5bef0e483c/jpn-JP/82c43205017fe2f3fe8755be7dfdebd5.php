<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/collectedinfo/form',
);

$TranslationRoot = array (
  'b0cc37f5babcc770db1baeee9c403d8e' => 
  array (
    'context' => 'design/ezwebin/collectedinfo/form',
    'source' => 'Form %formname',
    'comment' => NULL,
    'translation' => 'フォーム%formname',
    'key' => 'b0cc37f5babcc770db1baeee9c403d8e',
  ),
  '974627ff35f506940d8a0d4b8b9fd34f' => 
  array (
    'context' => 'design/ezwebin/collectedinfo/form',
    'source' => 'Thank you for your feedback.',
    'comment' => NULL,
    'translation' => 'ご意見・ご感想有難うございました。',
    'key' => '974627ff35f506940d8a0d4b8b9fd34f',
  ),
  '43395e4ee2e6e08f5f6598123bee33ea' => 
  array (
    'context' => 'design/ezwebin/collectedinfo/form',
    'source' => 'Return to site',
    'comment' => NULL,
    'translation' => 'サイトへ戻る',
    'key' => '43395e4ee2e6e08f5f6598123bee33ea',
  ),
  'ebb620470f67315da221ee36cb15d6af' => 
  array (
    'context' => 'design/ezwebin/collectedinfo/form',
    'source' => 'You have already submitted this form. The data you entered was:',
    'comment' => NULL,
    'translation' => 'このフォームに既にご記入されています。以前送信されたデータは:',
    'key' => 'ebb620470f67315da221ee36cb15d6af',
  ),
);
?>
