<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/full/article',
);

$TranslationRoot = array (
  '728799cf65f9f2fb9e0f9186e19733ba' => 
  array (
    'context' => 'design/ezwebin/full/article',
    'source' => 'Comments',
    'comment' => NULL,
    'translation' => 'コメント',
    'key' => '728799cf65f9f2fb9e0f9186e19733ba',
  ),
  'cf18f7eb9acc424592a5059350dad42d' => 
  array (
    'context' => 'design/ezwebin/full/article',
    'source' => 'New comment',
    'comment' => NULL,
    'translation' => '新しいコメント',
    'key' => 'cf18f7eb9acc424592a5059350dad42d',
  ),
  'fbd99b972ec05fa219baf2107c50f034' => 
  array (
    'context' => 'design/ezwebin/full/article',
    'source' => '%login_link_startLog in%login_link_end or %create_link_startcreate a user account%create_link_end to comment.',
    'comment' => NULL,
    'translation' => 'コメントを投稿するには、%login_link_startログインする%login_link_end か %create_link_startユーザアカウントを作成してください%create_link_end',
    'key' => 'fbd99b972ec05fa219baf2107c50f034',
  ),
  '3bd0fc8f7f1f06698040620a8c29eef3' => 
  array (
    'context' => 'design/ezwebin/full/article',
    'source' => 'Tip a friend',
    'comment' => NULL,
    'translation' => '友達に教える',
    'key' => '3bd0fc8f7f1f06698040620a8c29eef3',
  ),
  'd3aaf39acc7d6dbcbaf8dc69ed964220' => 
  array (
    'context' => 'design/ezwebin/full/article',
    'source' => 'Related content',
    'comment' => NULL,
    'translation' => '関連コンテンツ',
    'key' => 'd3aaf39acc7d6dbcbaf8dc69ed964220',
  ),
);
?>
