<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/section',
);

$TranslationRoot = array (
  '6788697df12ad2b8dfff7dc39dec4ebc' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Assign section to node',
    'comment' => NULL,
    'translation' => 'ノードへのセクション割り当て',
    'key' => '6788697df12ad2b8dfff7dc39dec4ebc',
  ),
  '827f54dc44c61c3818c800880df5931e' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Are you sure you want to remove these sections?',
    'comment' => NULL,
    'translation' => 'セクションを削除してもよろしいですか?',
    'key' => '827f54dc44c61c3818c800880df5931e',
  ),
  'd8d5e049dd5eb8a215440e72dfd00497' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Confirm',
    'comment' => NULL,
    'translation' => '確認',
    'key' => 'd8d5e049dd5eb8a215440e72dfd00497',
  ),
  '260160267ee6c4f6e91d7819078d0d68' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '260160267ee6c4f6e91d7819078d0d68',
  ),
  'b08d7e9053f981b7be7ae34464a70601' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Section edit',
    'comment' => NULL,
    'translation' => 'セクションの編集',
    'key' => 'b08d7e9053f981b7be7ae34464a70601',
  ),
  '6af3caee577ff230079a58ea5a2b863b' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Store',
    'comment' => NULL,
    'translation' => '保存',
    'key' => '6af3caee577ff230079a58ea5a2b863b',
  ),
  'd2d7a3a0c81e112300ccac60f64252d2' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Section list',
    'comment' => NULL,
    'translation' => 'セクション一覧',
    'key' => 'd2d7a3a0c81e112300ccac60f64252d2',
  ),
  '2d55af5595edf2e11c2cf0a89ad6f937' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'New',
    'comment' => NULL,
    'translation' => '新規',
    'key' => '2d55af5595edf2e11c2cf0a89ad6f937',
  ),
  '43b3054d0c3cdc6195d56c2a1a699e7c' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => '43b3054d0c3cdc6195d56c2a1a699e7c',
  ),
  'a35339b7edf549b4e61af47b2e4d4f1a' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Choose section assignment',
    'comment' => NULL,
    'translation' => 'セクション割り当ての選択',
    'key' => 'a35339b7edf549b4e61af47b2e4d4f1a',
  ),
  '11b4dd8cdaa76bded7f33f0517d39e60' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '11b4dd8cdaa76bded7f33f0517d39e60',
  ),
  '156126652ee74c05378f910f4530dda4' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'ID',
    'comment' => NULL,
    'translation' => 'ID',
    'key' => '156126652ee74c05378f910f4530dda4',
  ),
  '9a1324b59b5fec49cdf5fbb22a91cf0a' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => '9a1324b59b5fec49cdf5fbb22a91cf0a',
  ),
  '7313f84fd098ea8137222b36629fb7b4' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Assign',
    'comment' => NULL,
    'translation' => '割り当て',
    'key' => '7313f84fd098ea8137222b36629fb7b4',
  ),
  '8c665db4f4b1731ba01a8158c97a4fbc' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Assign section - %section',
    'comment' => NULL,
    'translation' => '%section のセクション割り当て',
    'key' => '8c665db4f4b1731ba01a8158c97a4fbc',
  ),
  '98b419f8b667da5bae590a0f0d038c45' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Remove selected sections',
    'comment' => NULL,
    'translation' => '選択したセクションを削除',
    'key' => '98b419f8b667da5bae590a0f0d038c45',
  ),
  'f69dafd9a5f0d672b6f38d01f6f00636' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Please choose where you want to start the section assignment for section %sectionname.

    Select the placements then click the %buttonname button.
    Using the recent and bookmark items for quick placement is also possible.
    Click on placement names to change the browse listing.',
    'comment' => NULL,
    'translation' => '%sectionnameセクションの開始点を選択してください

￼￼%buttonnameボタンをクリックして、開始点を指定してください。￼￼￼最近使われたオブジェクトとブックマークアイテムを使うこともできます。￼￼￼リストを変更するには、オブジェクト名をクリックしてください。',
    'key' => 'f69dafd9a5f0d672b6f38d01f6f00636',
  ),
  'a7293bc244df2624b73868ea45703cc9' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Removing these sections can corrupt permissions, site designs, and other things in the system. Do not do this unless you know exactly what you are doing.',
    'comment' => NULL,
    'translation' => 'セクションを削除すると、パーミッション、サイトデザインなどシステムの一部を破壊する可能性があります。正確に影響の範囲を把握している場合のみ実行して下さい。',
    'key' => 'a7293bc244df2624b73868ea45703cc9',
  ),
  '5c431975526553529bd51608d4dc5b58' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Navigation part',
    'comment' => NULL,
    'translation' => 'ナビゲーションパート',
    'key' => '5c431975526553529bd51608d4dc5b58',
  ),
  '500d4995551534c007c26e4e3856710e' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'About navigation parts',
    'comment' => NULL,
    'translation' => 'ナビゲーションパートについて',
    'key' => '500d4995551534c007c26e4e3856710e',
  ),
  'f5c45d67b9a29ab9baaea1b2552a59f9' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'The eZ Publish Administration Interface is divided into navigation parts. This is a way to group different areas of the site administration. Select the navigation part that should be active when this section is browsed.',
    'comment' => NULL,
    'translation' => 'eZ Publishの管理画面はナビゲーションパートにわかれています。このセクションが閲覧されている時に有効になるパートを選択してください。',
    'key' => 'f5c45d67b9a29ab9baaea1b2552a59f9',
  ),
  'b5cf4aeb16dbd17b43bdb7babcf4b7ea' => 
  array (
    'context' => 'design/standard/section',
    'source' => 'Denied',
    'comment' => NULL,
    'translation' => '否認',
    'key' => 'b5cf4aeb16dbd17b43bdb7babcf4b7ea',
  ),
);
?>
