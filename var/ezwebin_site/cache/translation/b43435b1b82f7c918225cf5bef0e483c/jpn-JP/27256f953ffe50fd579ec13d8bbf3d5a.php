<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/shop/confirmorder',
);

$TranslationRoot = array (
  'e3936741bc230b453d0ea7a1aff09e29' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Order confirmation',
    'comment' => NULL,
    'translation' => '注文内容',
    'key' => 'e3936741bc230b453d0ea7a1aff09e29',
  ),
  '06b9dcbdb4b9dfa6b50ece42b4a0b18f' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Please confirm that the information below is correct. Click "Confirm order" to confirm the order.',
    'comment' => NULL,
    'translation' => '以下の内容を確認して“注文内容の確認”をクリックしてください。',
    'key' => '06b9dcbdb4b9dfa6b50ece42b4a0b18f',
  ),
  '72c5bd403d11bcb0ff53fa1ba3027bda' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Customer',
    'comment' => NULL,
    'translation' => 'お客様',
    'key' => '72c5bd403d11bcb0ff53fa1ba3027bda',
  ),
  '2e84eb4474b4620ef76ccef566983773' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Items to be purchased',
    'comment' => NULL,
    'translation' => '購入する製品',
    'key' => '2e84eb4474b4620ef76ccef566983773',
  ),
  'e4d65f80b45086bee0d717b539181984' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Product',
    'comment' => NULL,
    'translation' => '商品',
    'key' => 'e4d65f80b45086bee0d717b539181984',
  ),
  'c044aec6d9197b9bbe06aa800d5a8b06' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Quantity',
    'comment' => NULL,
    'translation' => '数量',
    'key' => 'c044aec6d9197b9bbe06aa800d5a8b06',
  ),
  '77672a9ce6966d881b428ec875e75e76' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'VAT',
    'comment' => NULL,
    'translation' => '消費税',
    'key' => '77672a9ce6966d881b428ec875e75e76',
  ),
  'cc4759fe2d84f2d3a8a07747606865d3' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Price (ex. VAT)',
    'comment' => NULL,
    'translation' => '価格（税抜）',
    'key' => 'cc4759fe2d84f2d3a8a07747606865d3',
  ),
  'a0350d6b0ce0663a5a8f52a368599a2e' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Price (inc. VAT)',
    'comment' => NULL,
    'translation' => '価格（税込）',
    'key' => 'a0350d6b0ce0663a5a8f52a368599a2e',
  ),
  '58ac6b122b1c7cb6dd69b915b82d06f2' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Discount',
    'comment' => NULL,
    'translation' => '割引率',
    'key' => '58ac6b122b1c7cb6dd69b915b82d06f2',
  ),
  '1af16b01602b31c41ed5341615eedad3' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Total price (ex. VAT)',
    'comment' => NULL,
    'translation' => '合計（税抜）',
    'key' => '1af16b01602b31c41ed5341615eedad3',
  ),
  'b294c51b8e697c0d62514f7d7dae2078' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Total price (inc. VAT)',
    'comment' => NULL,
    'translation' => '合計（税込）',
    'key' => 'b294c51b8e697c0d62514f7d7dae2078',
  ),
  '0340ffabad991576b5fb36d262653f2e' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Selected options',
    'comment' => NULL,
    'translation' => '選択したオプション',
    'key' => '0340ffabad991576b5fb36d262653f2e',
  ),
  '91e4440828b572f7361c2d40db5c298f' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Order summary',
    'comment' => NULL,
    'translation' => 'ご注文一覧',
    'key' => '91e4440828b572f7361c2d40db5c298f',
  ),
  'cf42f8f601f9b43e6caea840701dfee4' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Subtotal of items',
    'comment' => NULL,
    'translation' => '商品の小計',
    'key' => 'cf42f8f601f9b43e6caea840701dfee4',
  ),
  'b97e93d3aa91c01ee179df7e32266174' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Order total',
    'comment' => NULL,
    'translation' => 'お買い上げ合計',
    'key' => 'b97e93d3aa91c01ee179df7e32266174',
  ),
  '87ff7de2957db104cea120472c488361' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Confirm order',
    'comment' => NULL,
    'translation' => '注文内容の確認',
    'key' => '87ff7de2957db104cea120472c488361',
  ),
  '2f3a4d29daf7aaae9fc142d53bfbf838' => 
  array (
    'context' => 'design/admin/shop/confirmorder',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '2f3a4d29daf7aaae9fc142d53bfbf838',
  ),
);
?>
