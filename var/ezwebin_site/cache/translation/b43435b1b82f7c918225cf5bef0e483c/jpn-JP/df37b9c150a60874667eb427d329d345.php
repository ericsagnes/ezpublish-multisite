<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/view/versionview',
);

$TranslationRoot = array (
  'ad2e453a36a0d19ffa184b3bb61ce4a7' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Object information',
    'comment' => NULL,
    'translation' => 'オブジェクトプロパティ',
    'key' => 'ad2e453a36a0d19ffa184b3bb61ce4a7',
  ),
  'be0f1ea6469681a92672842025e34560' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'ID',
    'comment' => NULL,
    'translation' => 'ID',
    'key' => 'be0f1ea6469681a92672842025e34560',
  ),
  'a5ee394b8bfe9ca2e673e6c8c6401ef0' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Created',
    'comment' => NULL,
    'translation' => '作成日時',
    'key' => 'a5ee394b8bfe9ca2e673e6c8c6401ef0',
  ),
  '23d9fcdd2a6f530925c2bd5f385ed998' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Not yet published',
    'comment' => NULL,
    'translation' => '未公開',
    'key' => '23d9fcdd2a6f530925c2bd5f385ed998',
  ),
  '5d3ced71767c153b376d5da678f2a79d' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '5d3ced71767c153b376d5da678f2a79d',
  ),
  '8d33493b5e6173bf99c45594cb28899b' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Published version',
    'comment' => NULL,
    'translation' => '公開バージョン',
    'key' => '8d33493b5e6173bf99c45594cb28899b',
  ),
  '225da3ac67e95829bc634b293bec9fa2' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Manage versions',
    'comment' => NULL,
    'translation' => 'バージョン管理',
    'key' => '225da3ac67e95829bc634b293bec9fa2',
  ),
  '2b4c1db49896ad8fbb0d40a7c4369067' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'View and manage (copy, delete, etc.) the versions of this object.',
    'comment' => NULL,
    'translation' => 'このオブジェクトのバージョン管理（コピー, 削除, その他）と参照',
    'key' => '2b4c1db49896ad8fbb0d40a7c4369067',
  ),
  'ac28bc290970a92d12bbd6957144f343' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Version information',
    'comment' => NULL,
    'translation' => 'バージョンプロパティ',
    'key' => 'ac28bc290970a92d12bbd6957144f343',
  ),
  '2f4e812a495d52a2501df2acdcb47131' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Last modified',
    'comment' => NULL,
    'translation' => '最新の修正',
    'key' => '2f4e812a495d52a2501df2acdcb47131',
  ),
  '2c628a4ca08c9400aba1ec37b25c0ee7' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Status',
    'comment' => NULL,
    'translation' => 'ステータス',
    'key' => '2c628a4ca08c9400aba1ec37b25c0ee7',
  ),
  '5e63f34de5133352bbf855c747d1da73' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Draft',
    'comment' => NULL,
    'translation' => '下書き',
    'key' => '5e63f34de5133352bbf855c747d1da73',
  ),
  '3fcc23fa449a6f1e25778fbe14150e85' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Published / current',
    'comment' => NULL,
    'translation' => '公開中 / 現在',
    'key' => '3fcc23fa449a6f1e25778fbe14150e85',
  ),
  'ac57328d2259e4d6c197924b4027312c' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Pending',
    'comment' => NULL,
    'translation' => '保留中',
    'key' => 'ac57328d2259e4d6c197924b4027312c',
  ),
  'bea21882409ec21db3407eb445045567' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Archived',
    'comment' => NULL,
    'translation' => '保管',
    'key' => 'bea21882409ec21db3407eb445045567',
  ),
  'aa0b8bab6e272c215e597d58004c5894' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Rejected',
    'comment' => NULL,
    'translation' => '非承認',
    'key' => 'aa0b8bab6e272c215e597d58004c5894',
  ),
  'ced88f0608bc46019e83326a871b564a' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => 'ced88f0608bc46019e83326a871b564a',
  ),
  'a8e432c4c2215fe9811dd8aa0763e12b' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'View control',
    'comment' => NULL,
    'translation' => '表示設定',
    'key' => 'a8e432c4c2215fe9811dd8aa0763e12b',
  ),
  '7f327891f093f622217aa2c9c97c3365' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Translation',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => '7f327891f093f622217aa2c9c97c3365',
  ),
  'bc6591b08b810da991eb7bd69d95491d' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => '%1 (No locale information available)',
    'comment' => NULL,
    'translation' => '%1（使用可能なロケールプロパティなし）',
    'key' => 'bc6591b08b810da991eb7bd69d95491d',
  ),
  'bbabb22610b3fa40437be5d5466da7bf' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Location',
    'comment' => NULL,
    'translation' => '配置先',
    'key' => 'bbabb22610b3fa40437be5d5466da7bf',
  ),
  '9e87f9006db34c0f58078b05bac11cb9' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Update view',
    'comment' => NULL,
    'translation' => '表示の更新',
    'key' => '9e87f9006db34c0f58078b05bac11cb9',
  ),
  '1d34563bb530ef6e46ac2d95e53f8690' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'View the version that is currently being displayed using the selected language, location and design.',
    'comment' => NULL,
    'translation' => '現在表示中のバージョンを、選択した言語、配置、デザインで表示',
    'key' => '1d34563bb530ef6e46ac2d95e53f8690',
  ),
  '4a21b43493e30d54129fc4db98e689d3' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => '4a21b43493e30d54129fc4db98e689d3',
  ),
  '79bd62bf8dcdc6ba4feb6c5fffc8ba48' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Edit the draft that is being displayed.',
    'comment' => NULL,
    'translation' => '表示中の下書きを編集する',
    'key' => '79bd62bf8dcdc6ba4feb6c5fffc8ba48',
  ),
  'a863cc05e172f298df3ef2a70a0fc9a6' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Siteaccess',
    'comment' => NULL,
    'translation' => 'サイトアクセス',
    'key' => 'a863cc05e172f298df3ef2a70a0fc9a6',
  ),
  '4698cf495a3a8211d1a23587e64037f0' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Publish',
    'comment' => NULL,
    'translation' => '公開',
    'key' => '4698cf495a3a8211d1a23587e64037f0',
  ),
  '39d83293672f7dbbb7755b1916cf8a58' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Publish the draft that is being displayed.',
    'comment' => NULL,
    'translation' => '表示中の下書きを公開する',
    'key' => '39d83293672f7dbbb7755b1916cf8a58',
  ),
  '5eaed917c657742a865901db2f05bdcf' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'You cannot manage the versions of this object because there is only one version available (the one that is being displayed).',
    'comment' => NULL,
    'translation' => 'バージョンが一つしかないため（現在編集中のバージョン）、このオブジェクトのバージョンを管理することはできません。',
    'key' => '5eaed917c657742a865901db2f05bdcf',
  ),
  '15fce0c660c6fc7e80b4befb83789447' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'This version is not a draft and therefore cannot be edited.',
    'comment' => NULL,
    'translation' => 'このバージョンは下書きではないため、編集をすることはできません。',
    'key' => '15fce0c660c6fc7e80b4befb83789447',
  ),
  '30e619f48de9212cb869c3e82d7d4121' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Translation mismatch',
    'comment' => NULL,
    'translation' => '翻訳の不具合',
    'key' => '30e619f48de9212cb869c3e82d7d4121',
  ),
  '84e19ae27a3721793fa314c5f678edfc' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Your selected translation does not match the language of your selected siteaccess. This may lead to unexpected results in the preview, however it may also be what you intended.',
    'comment' => NULL,
    'translation' => '選択した翻訳は選択されたサイトアクセスと不一致します。プレビューの際に予想外な結果が表示される可能性があります。',
    'key' => '84e19ae27a3721793fa314c5f678edfc',
  ),
  '2f463452d9b14a5cd82f29811730b627' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Back to edit',
    'comment' => NULL,
    'translation' => '編集へ戻る',
    'key' => '2f463452d9b14a5cd82f29811730b627',
  ),
  'f93a5c02c08d6bc84cbd335b6c546e3c' => 
  array (
    'context' => 'design/admin/content/view/versionview',
    'source' => 'Back',
    'comment' => NULL,
    'translation' => '戻る',
    'key' => 'f93a5c02c08d6bc84cbd335b6c546e3c',
  ),
);
?>
