<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/rss/browse_source',
);

$TranslationRoot = array (
  'eca4fe4654b175a8f3c9c069eaf61321' => 
  array (
    'context' => 'design/admin/rss/browse_source',
    'source' => 'Choose source for RSS export',
    'comment' => NULL,
    'translation' => 'RSSエクスポートソースの選択',
    'key' => 'eca4fe4654b175a8f3c9c069eaf61321',
  ),
  'f6656ab5883f792fdafe7e81662444ed' => 
  array (
    'context' => 'design/admin/rss/browse_source',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => 'f6656ab5883f792fdafe7e81662444ed',
  ),
  '1eda8f70ba7e09309d828e935c1d4762' => 
  array (
    'context' => 'design/admin/rss/browse_source',
    'source' => 'Use the radio buttons to choose the item that you want to export using RSS then click "OK".',
    'comment' => NULL,
    'translation' => 'ラジオボタンを使って、RSSでエクスポートするアイテムを選択し、"OK"をクリックしてください。',
    'key' => '1eda8f70ba7e09309d828e935c1d4762',
  ),
);
?>
