<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/user/login',
);

$TranslationRoot = array (
  'd4cf54ba98bfce913e7951e94d15c46b' => 
  array (
    'context' => 'design/ezwebin/user/login',
    'source' => 'Login',
    'comment' => NULL,
    'translation' => 'ログイン',
    'key' => 'd4cf54ba98bfce913e7951e94d15c46b',
  ),
  '6eb1fcb71d6928dcf0da7a88a0351473' => 
  array (
    'context' => 'design/ezwebin/user/login',
    'source' => 'Could not login',
    'comment' => NULL,
    'translation' => 'ログインできません',
    'key' => '6eb1fcb71d6928dcf0da7a88a0351473',
  ),
  'df89489aadcbb411ab094b8eab090bf5' => 
  array (
    'context' => 'design/ezwebin/user/login',
    'source' => 'A valid username and password is required to login.',
    'comment' => NULL,
    'translation' => '正しいユーザ名とパスワードでログインしてください。',
    'key' => 'df89489aadcbb411ab094b8eab090bf5',
  ),
  '536c869e17d60b0a6607c8cf43f857f1' => 
  array (
    'context' => 'design/ezwebin/user/login',
    'source' => 'Access not allowed',
    'comment' => NULL,
    'translation' => 'アクセスできません',
    'key' => '536c869e17d60b0a6607c8cf43f857f1',
  ),
  '64b87f06e941517e27a62cf0e4784738' => 
  array (
    'context' => 'design/ezwebin/user/login',
    'source' => 'You are not allowed to access %1.',
    'comment' => NULL,
    'translation' => '%1 へのアクセスはできません。',
    'key' => '64b87f06e941517e27a62cf0e4784738',
  ),
  'ea5431fa02254ef3d9cd147360745885' => 
  array (
    'context' => 'design/ezwebin/user/login',
    'source' => 'Username',
    'comment' => 'User name',
    'translation' => 'ユーザ名',
    'key' => 'ea5431fa02254ef3d9cd147360745885',
  ),
  '73109530e48a35064d4e6faa2ae4437b' => 
  array (
    'context' => 'design/ezwebin/user/login',
    'source' => 'Password',
    'comment' => NULL,
    'translation' => 'パスワード',
    'key' => '73109530e48a35064d4e6faa2ae4437b',
  ),
  '3cb1d1b2cf03c1e384e3741831ddce7d' => 
  array (
    'context' => 'design/ezwebin/user/login',
    'source' => 'Remember me',
    'comment' => NULL,
    'translation' => '次回からログインを省略',
    'key' => '3cb1d1b2cf03c1e384e3741831ddce7d',
  ),
  '351708e1cd36d43711ad7753be49a79a' => 
  array (
    'context' => 'design/ezwebin/user/login',
    'source' => 'Login',
    'comment' => 'Button',
    'translation' => 'ログイン',
    'key' => '351708e1cd36d43711ad7753be49a79a',
  ),
  '93c3cf7803d29c6220f59cb3ce39f192' => 
  array (
    'context' => 'design/ezwebin/user/login',
    'source' => 'Forgot your password?',
    'comment' => NULL,
    'translation' => 'パスワードを忘れましたか?',
    'key' => '93c3cf7803d29c6220f59cb3ce39f192',
  ),
  'ab00f0ed457dcd860b579d87c251b66e' => 
  array (
    'context' => 'design/ezwebin/user/login',
    'source' => 'Log in to the eZ Publish Administration Interface',
    'comment' => NULL,
    'translation' => 'eZ Publishの管理画面にログインする',
    'key' => 'ab00f0ed457dcd860b579d87c251b66e',
  ),
  '2faed2d497ecc5aedde51574a1a72983' => 
  array (
    'context' => 'design/ezwebin/user/login',
    'source' => 'Sign up',
    'comment' => 'Button',
    'translation' => '新規登録',
    'key' => '2faed2d497ecc5aedde51574a1a72983',
  ),
);
?>
