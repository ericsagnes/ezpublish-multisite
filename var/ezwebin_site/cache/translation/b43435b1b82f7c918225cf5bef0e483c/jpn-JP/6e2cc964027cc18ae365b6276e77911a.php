<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/shop/vattype',
);

$TranslationRoot = array (
  '52bbe31a57ec008ae73ab8392d78023c' => 
  array (
    'context' => 'kernel/shop/vattype',
    'source' => 'Empty VAT type names are not allowed (corrected).',
    'comment' => NULL,
    'translation' => '課税方法の名前を空にすることは出来ません。',
    'key' => '52bbe31a57ec008ae73ab8392d78023c',
  ),
  'f10a9844cb71931433f218730399c245' => 
  array (
    'context' => 'kernel/shop/vattype',
    'source' => 'Wrong VAT percentage (corrected).',
    'comment' => NULL,
    'translation' => '誤った税率です。',
    'key' => 'f10a9844cb71931433f218730399c245',
  ),
);
?>
