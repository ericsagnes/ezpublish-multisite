<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/notification/handler/ezsubtree/settings/edit',
);

$TranslationRoot = array (
  'f63810d4901baff44394a63f2345988b' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezsubtree/settings/edit',
    'source' => 'My item notifications [%notification_count]',
    'comment' => NULL,
    'translation' => '通知アイテム [%notification_count]',
    'key' => 'f63810d4901baff44394a63f2345988b',
  ),
  'd9e63b2976e3bd3c34f8b7cb457b7830' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Invert selection.',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => 'd9e63b2976e3bd3c34f8b7cb457b7830',
  ),
  'fa7d7fe7b2897eed604f783867b506ba' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => 'fa7d7fe7b2897eed604f783867b506ba',
  ),
  'f9b8564b568ca04be3c38bd2727489c3' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Type',
    'comment' => NULL,
    'translation' => 'タイプ',
    'key' => 'f9b8564b568ca04be3c38bd2727489c3',
  ),
  '3df79c8bfae0ec7de1efd75616138071' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Section',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => '3df79c8bfae0ec7de1efd75616138071',
  ),
  '2c85fbd84a1258416906a752fd91f055' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Unknown',
    'comment' => NULL,
    'translation' => '不明',
    'key' => '2c85fbd84a1258416906a752fd91f055',
  ),
  'ef1aafc463718d5a5b036994025f017d' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezsubtree/settings/edit',
    'source' => 'You have not subscribed to receive notifications about any items.',
    'comment' => NULL,
    'translation' => '登録した通知アイテムはありません。',
    'key' => 'ef1aafc463718d5a5b036994025f017d',
  ),
  '6e1f1aaeb166ad8c0733e7aa17888f42' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択項目の削除',
    'key' => '6e1f1aaeb166ad8c0733e7aa17888f42',
  ),
  'ebd95fb197119d725e4c98c3a3d21033' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Add items',
    'comment' => NULL,
    'translation' => 'アイテムの追加',
    'key' => 'ebd95fb197119d725e4c98c3a3d21033',
  ),
);
?>
