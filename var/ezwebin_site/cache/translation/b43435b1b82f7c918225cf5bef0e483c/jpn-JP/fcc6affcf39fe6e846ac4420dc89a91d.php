<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/translationview',
);

$TranslationRoot = array (
  '423cad5700af6741a396d2ea4fe37f9b' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => '%translation [Translation]',
    'comment' => NULL,
    'translation' => '%translation [翻訳]',
    'key' => '423cad5700af6741a396d2ea4fe37f9b',
  ),
  '113f6f5cc03759ae9d6c1c84b4f60743' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'ID',
    'comment' => NULL,
    'translation' => 'ID',
    'key' => '113f6f5cc03759ae9d6c1c84b4f60743',
  ),
  '74cd644975d3241127217b18d365b9ec' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Locale',
    'comment' => NULL,
    'translation' => 'ロケール',
    'key' => '74cd644975d3241127217b18d365b9ec',
  ),
  '225ef4d76b0e73924958f7e1c80487d3' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => '225ef4d76b0e73924958f7e1c80487d3',
  ),
  '8569433d7944a27fcbad72060d8405c0' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => '%locale [Locale]',
    'comment' => NULL,
    'translation' => '%locale [ロケール]',
    'key' => '8569433d7944a27fcbad72060d8405c0',
  ),
  'd7054c2e0ec83f2fa59eb684768c9a89' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Charset',
    'comment' => NULL,
    'translation' => '文字セット',
    'key' => 'd7054c2e0ec83f2fa59eb684768c9a89',
  ),
  '17d0242b284a436528a568bac38fbef2' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Not set',
    'comment' => NULL,
    'translation' => '設定なし',
    'key' => '17d0242b284a436528a568bac38fbef2',
  ),
  '983d7c9e0bb36cbb3e89a24a6dc62cd4' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Allowed charsets',
    'comment' => NULL,
    'translation' => '許容文字セット',
    'key' => '983d7c9e0bb36cbb3e89a24a6dc62cd4',
  ),
  '513193ad0e06dc8b49fcbee241ae04f2' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Language name',
    'comment' => NULL,
    'translation' => '言語名',
    'key' => '513193ad0e06dc8b49fcbee241ae04f2',
  ),
  '8250d8843b10bb27d0d0e275f0e67634' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'International language name',
    'comment' => NULL,
    'translation' => '国際言語名',
    'key' => '8250d8843b10bb27d0d0e275f0e67634',
  ),
  '31094b20888c0d1180d335ae948c7880' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Language code',
    'comment' => NULL,
    'translation' => '言語コード',
    'key' => '31094b20888c0d1180d335ae948c7880',
  ),
  '3612aacf5c495828807e86af0cbae8e4' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Language comment',
    'comment' => NULL,
    'translation' => '言語コメント',
    'key' => '3612aacf5c495828807e86af0cbae8e4',
  ),
  '0b9aa505ee42631bf3b04dffc1ef7b88' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Locale code',
    'comment' => NULL,
    'translation' => 'ロケールコード',
    'key' => '0b9aa505ee42631bf3b04dffc1ef7b88',
  ),
  '9afb124d01ff2e69af8bd0db6a81fc40' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Full locale code',
    'comment' => NULL,
    'translation' => '完全なロケールコード',
    'key' => '9afb124d01ff2e69af8bd0db6a81fc40',
  ),
  '53a45d9865485fb505bf4423e891f31b' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'HTTP locale code',
    'comment' => NULL,
    'translation' => 'HTTPロケールコード',
    'key' => '53a45d9865485fb505bf4423e891f31b',
  ),
  '8aabb7387d1583cde05f28cf8808f963' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Decimal symbol',
    'comment' => NULL,
    'translation' => '小数点記号',
    'key' => '8aabb7387d1583cde05f28cf8808f963',
  ),
  '1d5d66c3f8f898f0dc23b92f94d6b756' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Thousands separator',
    'comment' => NULL,
    'translation' => '千桁区切り',
    'key' => '1d5d66c3f8f898f0dc23b92f94d6b756',
  ),
  'e12b3ae48af80c509f99fe7a926ba0ac' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Decimal count',
    'comment' => NULL,
    'translation' => '小数桁数',
    'key' => 'e12b3ae48af80c509f99fe7a926ba0ac',
  ),
  '24c896cf097a3d73e4e37fca2def0475' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Negative symbol',
    'comment' => NULL,
    'translation' => '負号',
    'key' => '24c896cf097a3d73e4e37fca2def0475',
  ),
  'd70877bc7eb43e098433ec74c2cde923' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Positive symbol',
    'comment' => NULL,
    'translation' => '正号',
    'key' => 'd70877bc7eb43e098433ec74c2cde923',
  ),
  '90b94a17d7b2d4598a4c24e778ec22f3' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Currency decimal symbol',
    'comment' => NULL,
    'translation' => '通貨小数点記号',
    'key' => '90b94a17d7b2d4598a4c24e778ec22f3',
  ),
  '4206547a3d0dcac1230c99552f002d98' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Currency thousands separator',
    'comment' => NULL,
    'translation' => '通貨千桁区切り',
    'key' => '4206547a3d0dcac1230c99552f002d98',
  ),
  'a451b4c23a8f842b76374aab1d850c7e' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Currency decimal count',
    'comment' => NULL,
    'translation' => '通貨小数桁数',
    'key' => 'a451b4c23a8f842b76374aab1d850c7e',
  ),
  'f985aa2893aa9ca5ae64b7f1804f2185' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Currency negative symbol',
    'comment' => NULL,
    'translation' => '通貨負号',
    'key' => 'f985aa2893aa9ca5ae64b7f1804f2185',
  ),
  '0195080ec26f0dfeb0bd6a5d4c21cee5' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Currency positive symbol',
    'comment' => NULL,
    'translation' => '通貨正号',
    'key' => '0195080ec26f0dfeb0bd6a5d4c21cee5',
  ),
  '3557ad4ea63fd519eca930a9536aebee' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Currency symbol',
    'comment' => NULL,
    'translation' => '通貨記号',
    'key' => '3557ad4ea63fd519eca930a9536aebee',
  ),
  'bf4c7e2c32d03975bf459249dd0efe3e' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Currency name',
    'comment' => NULL,
    'translation' => '通貨名',
    'key' => 'bf4c7e2c32d03975bf459249dd0efe3e',
  ),
  'ca121c3814a5af3950875fb8363905ae' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Currency short name',
    'comment' => NULL,
    'translation' => '通貨短縮名',
    'key' => 'ca121c3814a5af3950875fb8363905ae',
  ),
  '5b1340d263703a8451137c19bb78a7d6' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'First day of week',
    'comment' => NULL,
    'translation' => '週の開始曜日',
    'key' => '5b1340d263703a8451137c19bb78a7d6',
  ),
  '53d8b287fc0fa89d572fa7bd2fb0a297' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Monday',
    'comment' => NULL,
    'translation' => '月曜日',
    'key' => '53d8b287fc0fa89d572fa7bd2fb0a297',
  ),
  '227dfe27cd8e5471686645da083eccec' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Weekday names',
    'comment' => NULL,
    'translation' => '曜日名',
    'key' => '227dfe27cd8e5471686645da083eccec',
  ),
  '14f04c2805450ddf8fc910b350bc7378' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Month names',
    'comment' => NULL,
    'translation' => '月名',
    'key' => '14f04c2805450ddf8fc910b350bc7378',
  ),
  'c0d403cf7fa24986ed29bb9b9c1ced73' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Sunday',
    'comment' => NULL,
    'translation' => '日曜日',
    'key' => 'c0d403cf7fa24986ed29bb9b9c1ced73',
  ),
  'd178404b34f81d2e5d540a269ebd6a53' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Country/region name',
    'comment' => NULL,
    'translation' => '国/県名',
    'key' => 'd178404b34f81d2e5d540a269ebd6a53',
  ),
  '4df3caaf3676c8fd8b1b99a40568c80c' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Country/region comment',
    'comment' => NULL,
    'translation' => '国/県 コメント',
    'key' => '4df3caaf3676c8fd8b1b99a40568c80c',
  ),
  '090f34997334d279e56a6898d9e486f3' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Country/region code',
    'comment' => NULL,
    'translation' => '国/県 コード',
    'key' => '090f34997334d279e56a6898d9e486f3',
  ),
  '931e3ce2af05d1208bdd0fdeae3d4000' => 
  array (
    'context' => 'design/admin/content/translationview',
    'source' => 'Country/region variation',
    'comment' => NULL,
    'translation' => '国/県 バリエーション',
    'key' => '931e3ce2af05d1208bdd0fdeae3d4000',
  ),
);
?>
