<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/setup',
);

$TranslationRoot = array (
  'e06e38426a8b7a84a75acd67042f1bf7' => 
  array (
    'context' => 'kernel/setup',
    'source' => 'Cache admin',
    'comment' => NULL,
    'translation' => 'キャッシュ管理',
    'key' => 'e06e38426a8b7a84a75acd67042f1bf7',
  ),
  '6b34b1ad71df43b2e042c8dff2582380' => 
  array (
    'context' => 'kernel/setup',
    'source' => 'Datatype wizard',
    'comment' => NULL,
    'translation' => 'データタイプウイザード',
    'key' => '6b34b1ad71df43b2e042c8dff2582380',
  ),
  '576bce89190d29b8c2d77c1dc3cfcc5d' => 
  array (
    'context' => 'kernel/setup',
    'source' => 'Extension configuration',
    'comment' => NULL,
    'translation' => 'エクステンション設定',
    'key' => '576bce89190d29b8c2d77c1dc3cfcc5d',
  ),
  '2aa52dc87bbd268db6ed3085fb29cbd5' => 
  array (
    'context' => 'kernel/setup',
    'source' => 'System information',
    'comment' => NULL,
    'translation' => 'システム情報',
    'key' => '2aa52dc87bbd268db6ed3085fb29cbd5',
  ),
  '8f13047f049dee5f08212eb6d7939d34' => 
  array (
    'context' => 'kernel/setup',
    'source' => 'Rapid Application Development',
    'comment' => NULL,
    'translation' => '高速アプリケーション開発ツール',
    'key' => '8f13047f049dee5f08212eb6d7939d34',
  ),
  '767568aab53bbce4597eaec8577c58ee' => 
  array (
    'context' => 'kernel/setup',
    'source' => 'Session admin',
    'comment' => NULL,
    'translation' => 'ユーザセッション管理',
    'key' => '767568aab53bbce4597eaec8577c58ee',
  ),
  '00526dddb4617adccdfdfde9fd08f953' => 
  array (
    'context' => 'kernel/setup',
    'source' => 'Setup menu',
    'comment' => NULL,
    'translation' => 'セットアップ・メニュー',
    'key' => '00526dddb4617adccdfdfde9fd08f953',
  ),
  'ee4532b239215cc15e40e1d65532437c' => 
  array (
    'context' => 'kernel/setup',
    'source' => 'File %1 does not exist. You should copy it from the recent eZ Publish distribution.',
    'comment' => NULL,
    'translation' => 'ファイル%1が見つかりません。最新のeZ Publishパッケージからコピーして下さい。',
    'key' => 'ee4532b239215cc15e40e1d65532437c',
  ),
  'c68c894157a29b669d528fe093d59715' => 
  array (
    'context' => 'kernel/setup',
    'source' => 'System Upgrade',
    'comment' => NULL,
    'translation' => 'システムアップグレード',
    'key' => 'c68c894157a29b669d528fe093d59715',
  ),
  'e03cd2120f28f46698ec2a79031d4c7c' => 
  array (
    'context' => 'kernel/setup',
    'source' => 'Template operator wizard',
    'comment' => NULL,
    'translation' => 'テンプレートオペレータウイザード',
    'key' => 'e03cd2120f28f46698ec2a79031d4c7c',
  ),
);
?>
