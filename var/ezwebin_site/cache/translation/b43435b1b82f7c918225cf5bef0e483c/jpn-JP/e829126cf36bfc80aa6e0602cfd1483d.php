<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/websitetoolbar/objectstates',
);

$TranslationRoot = array (
  '17e058e97a23a2a9fd662e79855bd4c6' => 
  array (
    'context' => 'design/ezwebin/websitetoolbar/objectstates',
    'source' => 'Object states for object',
    'comment' => NULL,
    'translation' => 'オブジェクトのオブジェクトステート',
    'key' => '17e058e97a23a2a9fd662e79855bd4c6',
  ),
  'e139d497f19c4721e15a1fd661f81611' => 
  array (
    'context' => 'design/ezwebin/websitetoolbar/objectstates',
    'source' => 'Content object state group',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクトステートグループ',
    'key' => 'e139d497f19c4721e15a1fd661f81611',
  ),
  '9e1bc5be7a72a08935410420e7f5c5c7' => 
  array (
    'context' => 'design/ezwebin/websitetoolbar/objectstates',
    'source' => 'Available states',
    'comment' => NULL,
    'translation' => '利用可能なステート',
    'key' => '9e1bc5be7a72a08935410420e7f5c5c7',
  ),
  'fa64879f6a5550b2fde99a1a97a0951c' => 
  array (
    'context' => 'design/ezwebin/websitetoolbar/objectstates',
    'source' => 'No content object state is configured. This can be done %urlstart here %urlend.',
    'comment' => NULL,
    'translation' => '設定されているコンテンツオブジェクトステートはありません。%urlstartこちら%urlendから設定できます。',
    'key' => 'fa64879f6a5550b2fde99a1a97a0951c',
  ),
  '417eab0868be4ff753dea893ce301d68' => 
  array (
    'context' => 'design/ezwebin/websitetoolbar/objectstates',
    'source' => 'Set states',
    'comment' => NULL,
    'translation' => 'ステートを設定',
    'key' => '417eab0868be4ff753dea893ce301d68',
  ),
  '2214b7e63670f04ccaa1d9215b38fb18' => 
  array (
    'context' => 'design/ezwebin/websitetoolbar/objectstates',
    'source' => 'Apply states from the list above.',
    'comment' => NULL,
    'translation' => '上記のリストからステートを適応。',
    'key' => '2214b7e63670f04ccaa1d9215b38fb18',
  ),
  '3df26b0140957f8f75af8f8b88b8991c' => 
  array (
    'context' => 'design/ezwebin/websitetoolbar/objectstates',
    'source' => 'No state to be applied to this content object. You might need to be assigned a more permissive access policy.',
    'comment' => NULL,
    'translation' => 'このコンテンツオブジェクトに適応出来るステートはありません。より高いアクセス権限が必要かもしれません。',
    'key' => '3df26b0140957f8f75af8f8b88b8991c',
  ),
);
?>
