<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/shop/vatrules',
);

$TranslationRoot = array (
  '12c66250c49d5a6a062e2ddc5039baf3' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'VAT charging rules [%rules]',
    'comment' => NULL,
    'translation' => '課税ルール [%rules]',
    'key' => '12c66250c49d5a6a062e2ddc5039baf3',
  ),
  '3ced8cebf805bb7bccab66525f709407' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'Invert selection.',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => '3ced8cebf805bb7bccab66525f709407',
  ),
  '053ea75839bca7149db40d798e3f2c81' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'Product categories',
    'comment' => NULL,
    'translation' => '商品区分',
    'key' => '053ea75839bca7149db40d798e3f2c81',
  ),
  '2914b4bb9e5c971e3d1e7e41b17f773e' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'VAT type',
    'comment' => NULL,
    'translation' => '課税方式',
    'key' => '2914b4bb9e5c971e3d1e7e41b17f773e',
  ),
  '8bb75c067ee7cdb356de09cca573cc70' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'Select rule for removal.',
    'comment' => NULL,
    'translation' => '削除するルールの選択',
    'key' => '8bb75c067ee7cdb356de09cca573cc70',
  ),
  '77c538c1a7a0d52f333ad5e061e4dafe' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => '77c538c1a7a0d52f333ad5e061e4dafe',
  ),
  '9062c9dfd9a92b8a1b33ae1bc7f74235' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'Edit rule.',
    'comment' => NULL,
    'translation' => '編集ルール',
    'key' => '9062c9dfd9a92b8a1b33ae1bc7f74235',
  ),
  '50ebd7cebcf634751d88a1b932a7b286' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'Any',
    'comment' => NULL,
    'translation' => '指定なし',
    'key' => '50ebd7cebcf634751d88a1b932a7b286',
  ),
  'ea192feb68710ed040d1a6821a98e044' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'There are no VAT charging rules.',
    'comment' => NULL,
    'translation' => '課税方式がありません',
    'key' => 'ea192feb68710ed040d1a6821a98e044',
  ),
  '7f0b430e405d61187e990fc1d65d57a8' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => '7f0b430e405d61187e990fc1d65d57a8',
  ),
  'ddb369e08e5aada7c5e6c1ff5cf95b8b' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'Remove selected VAT charging rules.',
    'comment' => NULL,
    'translation' => '選択した課税方式を削除',
    'key' => 'ddb369e08e5aada7c5e6c1ff5cf95b8b',
  ),
  '25ec3dd6d1023ccd50e1232086e413ae' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'New rule',
    'comment' => NULL,
    'translation' => '新規のルール',
    'key' => '25ec3dd6d1023ccd50e1232086e413ae',
  ),
  'b942b8998629a2911ca5676208df9684' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'Create a new VAT charging rule.',
    'comment' => NULL,
    'translation' => '新規の課税方式作成',
    'key' => 'b942b8998629a2911ca5676208df9684',
  ),
  '0feedc7858421adcbb8e2eb3133c94ec' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'Wrong or missing rules.',
    'comment' => NULL,
    'translation' => '無効なルールあるいは、未入力ルール',
    'key' => '0feedc7858421adcbb8e2eb3133c94ec',
  ),
  '3117a635ca44cf9185ea2396706b142e' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'Errors in VAT rules configuration may lead to charging wrong VAT for your products. Please fix them.',
    'comment' => NULL,
    'translation' => '課税ルールの構成においてのエラーは、商品に誤った税を課す可能性があります。 エラーを修正してください。',
    'key' => '3117a635ca44cf9185ea2396706b142e',
  ),
  'b103664f34dffbe6fcc2460fedabf2d3' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'Country/region',
    'comment' => NULL,
    'translation' => '国/県',
    'key' => 'b103664f34dffbe6fcc2460fedabf2d3',
  ),
  '1e968bcbfaa6004fa680295cbbe5b9f2' => 
  array (
    'context' => 'design/admin/shop/vatrules',
    'source' => 'VAT charging rules (%rules)',
    'comment' => NULL,
    'translation' => '課税ルール (%rules)',
    'key' => '1e968bcbfaa6004fa680295cbbe5b9f2',
  ),
);
?>
