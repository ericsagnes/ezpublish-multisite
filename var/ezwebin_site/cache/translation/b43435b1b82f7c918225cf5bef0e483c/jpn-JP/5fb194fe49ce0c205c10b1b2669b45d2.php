<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/content/edit',
);

$TranslationRoot = array (
  'b3e368fba2f9c562888e592925ea489a' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => 'b3e368fba2f9c562888e592925ea489a',
  ),
  '17cda57aa4d481012c5dfae189111143' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Preview',
    'comment' => NULL,
    'translation' => 'プレビュー',
    'key' => '17cda57aa4d481012c5dfae189111143',
  ),
  'b1392e73f4cce1291ccbc7db23a2a9b4' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Send for publishing',
    'comment' => NULL,
    'translation' => '送信して公開',
    'key' => 'b1392e73f4cce1291ccbc7db23a2a9b4',
  ),
  '7c926386fd3a1dff580c8a8608fd20ab' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Discard',
    'comment' => NULL,
    'translation' => '破棄',
    'key' => '7c926386fd3a1dff580c8a8608fd20ab',
  ),
  '535fe09bbbec5d68fa07d9103633ea1b' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Location',
    'comment' => NULL,
    'translation' => '配置先',
    'key' => '535fe09bbbec5d68fa07d9103633ea1b',
  ),
  'd6517a24b2848f659b43ff98e648eb9f' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Sort by',
    'comment' => NULL,
    'translation' => 'ソートキー',
    'key' => 'd6517a24b2848f659b43ff98e648eb9f',
  ),
  'dbfe3a7765fd2a9f3f27fd60dbfeddbe' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Ordering',
    'comment' => NULL,
    'translation' => 'ソート順',
    'key' => 'dbfe3a7765fd2a9f3f27fd60dbfeddbe',
  ),
  'a7bb7f2588fc412f89757318f24d96a8' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Main',
    'comment' => NULL,
    'translation' => '主要ノード',
    'key' => 'a7bb7f2588fc412f89757318f24d96a8',
  ),
  '510ffe8ee01caeddc507ec8bc2304589' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Move',
    'comment' => NULL,
    'translation' => '移動',
    'key' => '510ffe8ee01caeddc507ec8bc2304589',
  ),
  'd7a664b57a999942fc71f2fb31760d87' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => 'd7a664b57a999942fc71f2fb31760d87',
  ),
  '835b098f40a7bf2df3359ad25ef16095' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '835b098f40a7bf2df3359ad25ef16095',
  ),
  'e0b67cf639c893a024f1a9c46a42c541' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Published',
    'comment' => NULL,
    'translation' => '公開日時',
    'key' => 'e0b67cf639c893a024f1a9c46a42c541',
  ),
  '5423e3abbb056f12941350fc8fdca119' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '5423e3abbb056f12941350fc8fdca119',
  ),
  'bd6b736e2b96c7115a3693132e9b0b32' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Section',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => 'bd6b736e2b96c7115a3693132e9b0b32',
  ),
  'cbf36625d6a801ce97bf988b5a342874' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Depth',
    'comment' => NULL,
    'translation' => '階層',
    'key' => 'cbf36625d6a801ce97bf988b5a342874',
  ),
  '16e69d37cca482a4143144f50a50a90b' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Priority',
    'comment' => NULL,
    'translation' => '優先度',
    'key' => '16e69d37cca482a4143144f50a50a90b',
  ),
  'fa41d19172c5c999d9634318f7c5a839' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Add locations',
    'comment' => NULL,
    'translation' => '配置先の追加',
    'key' => 'fa41d19172c5c999d9634318f7c5a839',
  ),
  '255e0f38c93b4a7c7d4f8912f051f6de' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Object info',
    'comment' => NULL,
    'translation' => 'オブジェクト情報',
    'key' => '255e0f38c93b4a7c7d4f8912f051f6de',
  ),
  '6f8714027fffb3c7d2f4373a566dbaff' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Not yet published',
    'comment' => NULL,
    'translation' => '未公開',
    'key' => '6f8714027fffb3c7d2f4373a566dbaff',
  ),
  'c743287c40f1a23dfbab4756089a2e14' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Current',
    'comment' => NULL,
    'translation' => '現在',
    'key' => 'c743287c40f1a23dfbab4756089a2e14',
  ),
  '87e862e6e733007ffee2b47383a07894' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Manage',
    'comment' => NULL,
    'translation' => '管理',
    'key' => '87e862e6e733007ffee2b47383a07894',
  ),
  '909af2a70421b0ac9a40fe248bdb1caf' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Translations',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => '909af2a70421b0ac9a40fe248bdb1caf',
  ),
  '1bfe7524bc17dcd2bada7de20d17f21b' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Related objects',
    'comment' => NULL,
    'translation' => '関連したオブジェクト',
    'key' => '1bfe7524bc17dcd2bada7de20d17f21b',
  ),
  '30597cb6bf11e4e097fb90730ea7067b' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Find',
    'comment' => NULL,
    'translation' => '検索',
    'key' => '30597cb6bf11e4e097fb90730ea7067b',
  ),
  '709d96efb5ea58b3e94b1e6081dd17b8' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'New',
    'comment' => NULL,
    'translation' => '新規',
    'key' => '709d96efb5ea58b3e94b1e6081dd17b8',
  ),
  'd544aa391fb54bc911ff7babbe9da5a0' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Validation failed',
    'comment' => NULL,
    'translation' => 'データチェック・エラー',
    'key' => 'd544aa391fb54bc911ff7babbe9da5a0',
  ),
  '1a74a737d03f9efb6076628cd216ef11' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Input did not validate',
    'comment' => NULL,
    'translation' => '無効な入力です',
    'key' => '1a74a737d03f9efb6076628cd216ef11',
  ),
  'e09027ff10383f67517c274f5f3cde78' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Location did not validate',
    'comment' => NULL,
    'translation' => '配置先は無効です',
    'key' => 'e09027ff10383f67517c274f5f3cde78',
  ),
  'bacb8ce29df5bf926923bf75284df507' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Input was stored successfully',
    'comment' => NULL,
    'translation' => '入力は保存されました',
    'key' => 'bacb8ce29df5bf926923bf75284df507',
  ),
  'db575707f28266347028dd3c0e405982' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Confirm',
    'comment' => NULL,
    'translation' => '確認',
    'key' => 'db575707f28266347028dd3c0e405982',
  ),
  '2fa8a4a3f6b4fc19b41b855c4a916ea5' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '2fa8a4a3f6b4fc19b41b855c4a916ea5',
  ),
  '11399811a1bd67c83d291067f9d8df75' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Collected information from %1',
    'comment' => NULL,
    'translation' => '%1 から情報を収集しました',
    'key' => '11399811a1bd67c83d291067f9d8df75',
  ),
  '28adeedd31b77bd89b3e6ee95554486a' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Edit %1 - %2',
    'comment' => NULL,
    'translation' => '編集  %1 - %2',
    'key' => '28adeedd31b77bd89b3e6ee95554486a',
  ),
  '8a757136c6e7be742f7e88c23f282024' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Store draft',
    'comment' => NULL,
    'translation' => '下書きを保存',
    'key' => '8a757136c6e7be742f7e88c23f282024',
  ),
  '246b1f8abfa9802f37cbb1273b8c5294' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'New draft',
    'comment' => NULL,
    'translation' => '新規の下書き',
    'key' => '246b1f8abfa9802f37cbb1273b8c5294',
  ),
  'cd3cec7826703383d6554ef0b16c6ce6' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Created',
    'comment' => NULL,
    'translation' => '作成日時',
    'key' => 'cd3cec7826703383d6554ef0b16c6ce6',
  ),
  'a023ed604920c8993a2821c4f7d6b2dd' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Versions',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => 'a023ed604920c8993a2821c4f7d6b2dd',
  ),
  '89619a51a9ec798f5b7ab45a227eaa21' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Editing',
    'comment' => NULL,
    'translation' => '編集中',
    'key' => '89619a51a9ec798f5b7ab45a227eaa21',
  ),
  '0651d2fa331d23522850654e7bc0e346' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Feedback from %1',
    'comment' => NULL,
    'translation' => '%1 からのお問合せ',
    'key' => '0651d2fa331d23522850654e7bc0e346',
  ),
  'b8c7933c1ac985b230d02b0d903e7480' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'The currently published version is %version and was published at %time.',
    'comment' => NULL,
    'translation' => '現在の公開バージョンは %version で、 %timeに公開されました。',
    'key' => 'b8c7933c1ac985b230d02b0d903e7480',
  ),
  '53d4e18d664fb8ffb6ecdabbc426fd46' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'The last modification was done at %modified.',
    'comment' => NULL,
    'translation' => '最終修正日時: %modified',
    'key' => '53d4e18d664fb8ffb6ecdabbc426fd46',
  ),
  'b37dde676ef2ae5868177f67a555466b' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'The object is owned by %owner.',
    'comment' => NULL,
    'translation' => 'オブジェクトは ユーザ %owner の所有です。',
    'key' => 'b37dde676ef2ae5868177f67a555466b',
  ),
  'db21b5a2833da5501716598cb5808a13' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'This object is already being edited by you.
        You can either continue editing one of your drafts or you can create a new draft.',
    'comment' => NULL,
    'translation' => 'オブジェクトは、あなたにより編集状態となっています。下書きの編集を継続するか、新規に下書きを作成するかを選択できます。',
    'key' => 'db21b5a2833da5501716598cb5808a13',
  ),
  'ed2663b83524a4618fd35f8232f3f7a3' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'This object is already being edited by someone else.
        You should either contact the person about the draft or create a new draft for personal editing.',
    'comment' => NULL,
    'translation' => 'このオブジェクトは、他のユーザにより編集状態にあります。このユーザに連絡を取るか、新規下書きを作成してください。',
    'key' => 'ed2663b83524a4618fd35f8232f3f7a3',
  ),
  'c4a48283e813f727fc2b5eba0f1eff29' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Current drafts',
    'comment' => NULL,
    'translation' => '現在の下書き',
    'key' => 'c4a48283e813f727fc2b5eba0f1eff29',
  ),
  'eda5d34ba3437b1e2c6c119d56d3e186' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => 'eda5d34ba3437b1e2c6c119d56d3e186',
  ),
  '680756798a64d52ad9a80570c1c04de4' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Owner',
    'comment' => NULL,
    'translation' => '所有者',
    'key' => '680756798a64d52ad9a80570c1c04de4',
  ),
  '40fca32346920d717bd6eff693671027' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Last modified',
    'comment' => NULL,
    'translation' => '最終修正日時',
    'key' => '40fca32346920d717bd6eff693671027',
  ),
  'dd906b3eec0678c688bda7ad18c2b31c' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Input was partially stored',
    'comment' => NULL,
    'translation' => '入力の一部は、保存されました',
    'key' => 'dd906b3eec0678c688bda7ad18c2b31c',
  ),
  '03f77228fa5a1f07e1c0d385983713f4' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Are you sure you want to discard the draft %versionname?',
    'comment' => NULL,
    'translation' => '下書き %versionname を破棄してよろしいですか?',
    'key' => '03f77228fa5a1f07e1c0d385983713f4',
  ),
  '300b195eebe4dca8aeb64b6397cff1ee' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Last Modified',
    'comment' => NULL,
    'translation' => '最終修正日時',
    'key' => '300b195eebe4dca8aeb64b6397cff1ee',
  ),
  'bbaab9c3d7636001cca3a35f29984e87' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'The following feedback was collected',
    'comment' => NULL,
    'translation' => '以下のお問合せデータを収集しました',
    'key' => 'bbaab9c3d7636001cca3a35f29984e87',
  ),
  'a0426d7658cf1cb2fee4365d8c639ae8' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'The following information was collected',
    'comment' => NULL,
    'translation' => '以下の情報を収集しました',
    'key' => 'a0426d7658cf1cb2fee4365d8c639ae8',
  ),
  '0cfb7999048eb80ead20c5ca8c643093' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'No translation',
    'comment' => NULL,
    'translation' => '未翻訳',
    'key' => '0cfb7999048eb80ead20c5ca8c643093',
  ),
  '5a570708088d7e174fcc43a3b3bbfef1' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Translate',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => '5a570708088d7e174fcc43a3b3bbfef1',
  ),
  '900577d384b800dcfcfe65083efac9ae' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'This object is already being edited by yourself or someone else.
    You can either continue editing one of your drafts or you can create a new draft.',
    'comment' => NULL,
    'translation' => 'このオブジェクトはすでに編集されています。
作成した下書きを続けて編集するか、新しい下書きを作成することができます。',
    'key' => '900577d384b800dcfcfe65083efac9ae',
  ),
  '5dd6e71de0e88889ae9787437c9cfa51' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Class identifier',
    'comment' => NULL,
    'translation' => 'クラス識別子',
    'key' => '5dd6e71de0e88889ae9787437c9cfa51',
  ),
  '3667a82e3cafb33c545b2ebc9ba519f5' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Class name',
    'comment' => NULL,
    'translation' => 'クラス名',
    'key' => '3667a82e3cafb33c545b2ebc9ba519f5',
  ),
  '4aee633bd46a006be5863ae9aeef6503' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Manage versions',
    'comment' => NULL,
    'translation' => 'バージョン管理',
    'key' => '4aee633bd46a006be5863ae9aeef6503',
  ),
  '562079e319ec1c89f10a0d7b3c7e538c' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Translate from',
    'comment' => NULL,
    'translation' => 'から翻訳',
    'key' => '562079e319ec1c89f10a0d7b3c7e538c',
  ),
  'a3e30e3fbb7c216c97698cf064813f44' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Publish the contents of the draft that is being edited. The draft will become the published version of the object.',
    'comment' => NULL,
    'translation' => '編集されているドラフトを公開する。ドラフトはオブジェクトの公開バージョンとなります。',
    'key' => 'a3e30e3fbb7c216c97698cf064813f44',
  ),
  '22c162a6b0bba692f33556cb2b6b1beb' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Store the contents of the draft that is being edited and continue editing. Use this button to periodically save your work while editing.',
    'comment' => NULL,
    'translation' => '編集されているドラフトのコンテンツを保存し、編集を続ける。このボタンを使って、編集の際に定期的にドラフトを保存保存できます。',
    'key' => '22c162a6b0bba692f33556cb2b6b1beb',
  ),
  '2a7a7f22e92a9aa9b1dda3d229ad20dc' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Store draft and exit',
    'comment' => NULL,
    'translation' => 'ドラフトを保存して、編集の終了',
    'key' => '2a7a7f22e92a9aa9b1dda3d229ad20dc',
  ),
  'abdb3a3d605c8d6d55b0d61594a52544' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Store the draft that is being edited and exit from edit mode. Use when you need to exit your work and return later to continue.',
    'comment' => NULL,
    'translation' => '編集されているドラフトを保存して、編集を終了します。後から編集を続く時に使ってください。',
    'key' => 'abdb3a3d605c8d6d55b0d61594a52544',
  ),
  '071f1444cb97fba26bfb6b21f8354d41' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Discard draft',
    'comment' => NULL,
    'translation' => 'ドラフトの削除',
    'key' => '071f1444cb97fba26bfb6b21f8354d41',
  ),
  'bee6c0eae6d490c408cd1e8241616b3b' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Are you sure you want to discard the draft?',
    'comment' => NULL,
    'translation' => 'ドラフトを削除してもよろしいですか？',
    'key' => 'bee6c0eae6d490c408cd1e8241616b3b',
  ),
  '80ae4cd27406933ac07c58b41da28fe0' => 
  array (
    'context' => 'design/standard/content/edit',
    'source' => 'Discard the draft that is being edited. This will also remove the translations that belong to the draft (if any).',
    'comment' => NULL,
    'translation' => '編集しているドラフトを削除する。ドラフトに翻訳がある場合、その翻訳も削除されます。',
    'key' => '80ae4cd27406933ac07c58b41da28fe0',
  ),
);
?>
