<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/rss/browse_destination',
);

$TranslationRoot = array (
  '9e68511cca742d4848743487492c3978' => 
  array (
    'context' => 'design/admin/rss/browse_destination',
    'source' => 'Choose a destination for RSS import',
    'comment' => NULL,
    'translation' => 'RSSインポートオブジェクトの配置先の選択',
    'key' => '9e68511cca742d4848743487492c3978',
  ),
  'dab84b67f2bac15548ab5278ea8d3517' => 
  array (
    'context' => 'design/admin/rss/browse_destination',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => 'dab84b67f2bac15548ab5278ea8d3517',
  ),
  '684a48cb9ef46d0566dcd7b0634b1c6b' => 
  array (
    'context' => 'design/admin/rss/browse_destination',
    'source' => 'Use the radio buttons to choose a destination location for RSS import then click "OK".',
    'comment' => NULL,
    'translation' => 'ラジオボタンを使って、RSSインポートする配置先を選択し、"OK"をクリックしてください。',
    'key' => '684a48cb9ef46d0566dcd7b0634b1c6b',
  ),
);
?>
