<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/workflow/eventtype/edit',
);

$TranslationRoot = array (
  '1fa5bc46c75722ee1491dbdc0ee4124c' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Affected sections',
    'comment' => NULL,
    'translation' => '対象セクション',
    'key' => '1fa5bc46c75722ee1491dbdc0ee4124c',
  ),
  'f090b61775b4e7834daadffb622fc4a2' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'All sections',
    'comment' => NULL,
    'translation' => 'すべてのセクション',
    'key' => 'f090b61775b4e7834daadffb622fc4a2',
  ),
  'd2dd6b03a72274b5b110376e7674b093' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Classes to run workflow',
    'comment' => NULL,
    'translation' => 'ワークフローを実行するクラス',
    'key' => 'd2dd6b03a72274b5b110376e7674b093',
  ),
  '0d2a017b4f396d5d3c0fdfbe73902fc2' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'All classes',
    'comment' => NULL,
    'translation' => 'すべてのクラス',
    'key' => '0d2a017b4f396d5d3c0fdfbe73902fc2',
  ),
  'e1c7e44b4fda77eb24f7d3829033ba9f' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Users without workflow IDs',
    'comment' => NULL,
    'translation' => 'ワークフローIDなしのユーザ',
    'key' => 'e1c7e44b4fda77eb24f7d3829033ba9f',
  ),
  '3ac1d7a472b15383839d8689042a74d4' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Workflow to run',
    'comment' => NULL,
    'translation' => '実行するワークフロー',
    'key' => '3ac1d7a472b15383839d8689042a74d4',
  ),
  '683be980b1d7721cea26968a4f719de9' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Type',
    'comment' => NULL,
    'translation' => '種類',
    'key' => '683be980b1d7721cea26968a4f719de9',
  ),
  '21a8f605bff05d2ef2a724ae30da1e90' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'All',
    'comment' => NULL,
    'translation' => 'すべて',
    'key' => '21a8f605bff05d2ef2a724ae30da1e90',
  ),
  'bb2b68a7bf35dbfd261553172d364004' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => 'bb2b68a7bf35dbfd261553172d364004',
  ),
  'ee97811fc498b987fc44b4ecb96d9fbb' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Update attributes',
    'comment' => NULL,
    'translation' => '属性の更新',
    'key' => 'ee97811fc498b987fc44b4ecb96d9fbb',
  ),
  'cad381e5540d688ea37d2e51f0fb7f16' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Attribute',
    'comment' => NULL,
    'translation' => '属性',
    'key' => 'cad381e5540d688ea37d2e51f0fb7f16',
  ),
  '0966fff54186661b834632edd41ae961' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Select attribute',
    'comment' => NULL,
    'translation' => '属性の選択',
    'key' => '0966fff54186661b834632edd41ae961',
  ),
  '57a58a5b9508f5add87e22fa8078774d' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Class/attribute combinations [%count]',
    'comment' => NULL,
    'translation' => 'クラスと属性の組み合わせ数  [%count]',
    'key' => '57a58a5b9508f5add87e22fa8078774d',
  ),
  'b56a83e65f75d11161a8f76e7f093f54' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'There are no combinations',
    'comment' => NULL,
    'translation' => '組み合わせ設定はありません',
    'key' => 'b56a83e65f75d11161a8f76e7f093f54',
  ),
  '5a3d384253a4b7d32851c48b3d99f54e' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => '5a3d384253a4b7d32851c48b3d99f54e',
  ),
  'd6bc6b61099b510681d6baff94303898' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Modify the objects\' publishing dates',
    'comment' => NULL,
    'translation' => 'オブジェクト公開日の修正',
    'key' => 'd6bc6b61099b510681d6baff94303898',
  ),
  '7631c6401199279cbcf2474f6c3025c3' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Please install a payment extension first.',
    'comment' => NULL,
    'translation' => '決済用のエクテンションを先にインストールしてください。',
    'key' => '7631c6401199279cbcf2474f6c3025c3',
  ),
  'ca3a09753c088f7af515e38c86f3bb42' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'User',
    'comment' => NULL,
    'translation' => 'ユーザ',
    'key' => 'ca3a09753c088f7af515e38c86f3bb42',
  ),
  'bedc4dd43f9407de98006ae4985779d5' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'No users selected.',
    'comment' => NULL,
    'translation' => 'ユーザが選択されていません。',
    'key' => 'bedc4dd43f9407de98006ae4985779d5',
  ),
  'ffe2f8d456a06c5a1b8db0bf76feb2db' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Add users',
    'comment' => NULL,
    'translation' => 'ユーザを追加',
    'key' => 'ffe2f8d456a06c5a1b8db0bf76feb2db',
  ),
  'b538cc17c13bfad42c366c438da8c35f' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Group',
    'comment' => NULL,
    'translation' => 'グループ',
    'key' => 'b538cc17c13bfad42c366c438da8c35f',
  ),
  '231192a04ffa70763ee7db835d506f80' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'No groups selected.',
    'comment' => NULL,
    'translation' => '選択されたグループはありません。',
    'key' => '231192a04ffa70763ee7db835d506f80',
  ),
  '1bfc43637bb7d6a8b08f8b6ef2dd779a' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Add groups',
    'comment' => NULL,
    'translation' => 'ユーザグループを追加',
    'key' => '1bfc43637bb7d6a8b08f8b6ef2dd779a',
  ),
  'ec6a3d878d8ceb06e2553866a5a5277f' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Excluded user groups ( users in these groups do not need to have their content approved )',
    'comment' => NULL,
    'translation' => '対象外のユーザグループ（グループの中のユーザはコンテンツ承認をする必要がありません）',
    'key' => 'ec6a3d878d8ceb06e2553866a5a5277f',
  ),
  '411d978b36c75ff6e08dc49efdd6d5f4' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'User and user groups',
    'comment' => NULL,
    'translation' => 'ユーザとユーザグループ',
    'key' => '411d978b36c75ff6e08dc49efdd6d5f4',
  ),
  '809ea46e251df5d06db0c7286d1b6f7b' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Affected languages',
    'comment' => NULL,
    'translation' => '適用される言語',
    'key' => '809ea46e251df5d06db0c7286d1b6f7b',
  ),
  'b0b42280959aee27b81e0fa9c6684e4d' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'All languages',
    'comment' => NULL,
    'translation' => 'すべての言語',
    'key' => 'b0b42280959aee27b81e0fa9c6684e4d',
  ),
  'eecedce59a6893f40a25b240cda115ad' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'You have to create a workflow before using this event.',
    'comment' => NULL,
    'translation' => 'このイベントを使用する前にワークフローを作成して下さい。',
    'key' => 'eecedce59a6893f40a25b240cda115ad',
  ),
  'ca21df6268d30e255d388155b682edc0' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'All versions',
    'comment' => NULL,
    'translation' => 'すべてのバージョン',
    'key' => 'ca21df6268d30e255d388155b682edc0',
  ),
  'c13cbeb45b5edec7363c5b64c5ab8167' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Publishing new object',
    'comment' => NULL,
    'translation' => '新規オブジェクトの公開',
    'key' => 'c13cbeb45b5edec7363c5b64c5ab8167',
  ),
  'e1fd162f89a636235bfab53da3b323bd' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Updating existing object',
    'comment' => NULL,
    'translation' => '既存オブジェクトの更新',
    'key' => 'e1fd162f89a636235bfab53da3b323bd',
  ),
  'f5fd1456467861381d64e96ed34fe07a' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Affected versions',
    'comment' => NULL,
    'translation' => '対象バージョン',
    'key' => 'f5fd1456467861381d64e96ed34fe07a',
  ),
  'ed633884d6fc3d1f6b26dcf6873a80d8' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Users who approve content',
    'comment' => NULL,
    'translation' => '承認者ユーザ',
    'key' => 'ed633884d6fc3d1f6b26dcf6873a80d8',
  ),
  '1f993dce1f625605c2bf65dc672f818d' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Groups who approve content',
    'comment' => NULL,
    'translation' => '承認グループ',
    'key' => '1f993dce1f625605c2bf65dc672f818d',
  ),
  '748a5323027c050dee6efddb3e9b4681' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'There are no payment gateway extensions installed.',
    'comment' => NULL,
    'translation' => '決済用のエクステンションを先にインストールして下さい。',
    'key' => '748a5323027c050dee6efddb3e9b4681',
  ),
  '5a4edef6e91ebca925091fbe2aa822c9' => 
  array (
    'context' => 'design/admin/workflow/eventtype/edit',
    'source' => 'Class/attribute combinations (%count)',
    'comment' => NULL,
    'translation' => 'クラスと属性の組み合わせ数  (%count)',
    'key' => '5a4edef6e91ebca925091fbe2aa822c9',
  ),
);
?>
