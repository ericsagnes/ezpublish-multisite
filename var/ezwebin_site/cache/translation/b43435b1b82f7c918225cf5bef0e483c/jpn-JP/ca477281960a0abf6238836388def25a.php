<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/setup/extensions',
);

$TranslationRoot = array (
  '6e96b74419ed64700e912f23282c7ecf' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Available extensions [%extension_count]',
    'comment' => NULL,
    'translation' => '利用可能なエクステンション [%extension_count]',
    'key' => '6e96b74419ed64700e912f23282c7ecf',
  ),
  '0e4070ee3bdef63757c2abc8482d4a08' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Active',
    'comment' => NULL,
    'translation' => '有効',
    'key' => '0e4070ee3bdef63757c2abc8482d4a08',
  ),
  '5f5668a9f6b7dab7e0a33df7d67ba05e' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '5f5668a9f6b7dab7e0a33df7d67ba05e',
  ),
  '65b47d7e3a716ff49549851ffc9b3807' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Apply changes',
    'comment' => NULL,
    'translation' => '変更の適用',
    'key' => '65b47d7e3a716ff49549851ffc9b3807',
  ),
  'f13222d04247efcc3da2227b58d76b3f' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Activate or deactivate extension. Use the "Apply changes" button to apply the changes.',
    'comment' => NULL,
    'translation' => 'エクステンションの有効/無効を選択します。“変更の適用”ボタンを使用して変更を適用します。',
    'key' => 'f13222d04247efcc3da2227b58d76b3f',
  ),
  'a59d338119b2055b87a47d258ab35ffe' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'There are no available extensions.',
    'comment' => NULL,
    'translation' => '利用可能なエクステンションはありません。',
    'key' => 'a59d338119b2055b87a47d258ab35ffe',
  ),
  '25ade0841fdb1cad05776122ea947881' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Click this button to store changes if you have modified the status of the checkboxes above.',
    'comment' => NULL,
    'translation' => 'チェックボックスの状態を変更した項目があれば、このボタンをクリックすると変更点が保存されます。',
    'key' => '25ade0841fdb1cad05776122ea947881',
  ),
  '2ddfaf8414bd174f2ab67f6437f59510' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Regenerate autoload arrays for extensions',
    'comment' => NULL,
    'translation' => 'エクステンション用のオートーロード配列の再生成',
    'key' => '2ddfaf8414bd174f2ab67f6437f59510',
  ),
  'd9e1572e9e569fba55c9a987b0f64c98' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Click this button to regenerate the autoload arrays used by the system for extensions.',
    'comment' => NULL,
    'translation' => 'システムが使っているエクステンション用のオートーロード配列を再生成するには、このボタンをクリックして下さい。',
    'key' => 'd9e1572e9e569fba55c9a987b0f64c98',
  ),
  'b116d07e2a251c245a27be380abb403c' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Problems detected during autoload generation:',
    'comment' => NULL,
    'translation' => 'オートロード中に見つかった問題: ',
    'key' => 'b116d07e2a251c245a27be380abb403c',
  ),
  'db8a4f0891e3a61ad41bd94286e47897' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Available extensions (%extension_count)',
    'comment' => NULL,
    'translation' => '利用可能なエクステンション (%extension_count)',
    'key' => 'db8a4f0891e3a61ad41bd94286e47897',
  ),
  '5d82c8f7a805b205ec9d96a022a83994' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Invert selection.',
    'comment' => NULL,
    'translation' => '選択の反転。',
    'key' => '5d82c8f7a805b205ec9d96a022a83994',
  ),
  '57c8759eef24fd244ec50221a3d47f65' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Activate',
    'comment' => NULL,
    'translation' => '有効にする',
    'key' => '57c8759eef24fd244ec50221a3d47f65',
  ),
  'ad3707ca2e3bda0dd0b7d31ec4f3fce2' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Activate or deactivate extension. Use the "Update" button to apply the changes.',
    'comment' => NULL,
    'translation' => 'エクステンションを有効か無効にできます。変更を保存するために、「更新」ボタンを押してください。',
    'key' => 'ad3707ca2e3bda0dd0b7d31ec4f3fce2',
  ),
  '547cfc75fa2495ffab6d28025c7469fb' => 
  array (
    'context' => 'design/admin/setup/extensions',
    'source' => 'Update',
    'comment' => NULL,
    'translation' => '更新',
    'key' => '547cfc75fa2495ffab6d28025c7469fb',
  ),
);
?>
