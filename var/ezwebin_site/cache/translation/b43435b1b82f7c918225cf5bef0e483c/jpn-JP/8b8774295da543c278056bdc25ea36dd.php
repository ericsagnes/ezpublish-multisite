<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/content/pendinglist',
);

$TranslationRoot = array (
  '4651ce7a1e49d99ff20deb3346accfe9' => 
  array (
    'context' => 'design/ezwebin/content/pendinglist',
    'source' => 'My pending items [%pending_count]',
    'comment' => NULL,
    'translation' => '保留中のアイテム [%pending_count]',
    'key' => '4651ce7a1e49d99ff20deb3346accfe9',
  ),
  '51fa8d7c7ef3bdff653ab0308ccedbfe' => 
  array (
    'context' => 'design/ezwebin/content/pendinglist',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '51fa8d7c7ef3bdff653ab0308ccedbfe',
  ),
  '0305e61657e051ada0dfb28ef6b3e711' => 
  array (
    'context' => 'design/ezwebin/content/pendinglist',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => '0305e61657e051ada0dfb28ef6b3e711',
  ),
  '1fbbf85298053648ec7ef351df9c0f35' => 
  array (
    'context' => 'design/ezwebin/content/pendinglist',
    'source' => 'Section',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => '1fbbf85298053648ec7ef351df9c0f35',
  ),
  '4af5c1d759712dae4fb54f6f80ecc568' => 
  array (
    'context' => 'design/ezwebin/content/pendinglist',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => '4af5c1d759712dae4fb54f6f80ecc568',
  ),
  'd46046652b66f2c7da372e968fd5d696' => 
  array (
    'context' => 'design/ezwebin/content/pendinglist',
    'source' => 'Last modified',
    'comment' => NULL,
    'translation' => '最新の修正',
    'key' => 'd46046652b66f2c7da372e968fd5d696',
  ),
  '7ff6fadc5997124b1382841ad34e8bd4' => 
  array (
    'context' => 'design/ezwebin/content/pendinglist',
    'source' => 'Unknown',
    'comment' => NULL,
    'translation' => '不明',
    'key' => '7ff6fadc5997124b1382841ad34e8bd4',
  ),
  'ea6fb3e6f975c92e4ab616ad1d327499' => 
  array (
    'context' => 'design/ezwebin/content/pendinglist',
    'source' => 'Your pending list is empty',
    'comment' => NULL,
    'translation' => '保留中のアイテムはありません。',
    'key' => 'ea6fb3e6f975c92e4ab616ad1d327499',
  ),
);
?>
