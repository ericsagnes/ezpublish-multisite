<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/classes/datatypes',
);

$TranslationRoot = array (
  'd01cadcbda33fc3dda3143fe0664ddad' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Missing date input.',
    'comment' => NULL,
    'translation' => '日付が未入力です。',
    'key' => 'd01cadcbda33fc3dda3143fe0664ddad',
  ),
  '0a5703e162a1fb33f729a5e467341ec4' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Missing datetime input.',
    'comment' => NULL,
    'translation' => '日付と時刻が未入力です。',
    'key' => '0a5703e162a1fb33f729a5e467341ec4',
  ),
  'c50b438cad21f589845163bbd191cc1b' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'At least one author is required.',
    'comment' => NULL,
    'translation' => '作成者が一名以上必要です。',
    'key' => 'c50b438cad21f589845163bbd191cc1b',
  ),
  '99f1ae5362f707fe77d7efe6bef6c68f' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'A valid file is required.',
    'comment' => NULL,
    'translation' => '有効なファイルが必要です。',
    'key' => '99f1ae5362f707fe77d7efe6bef6c68f',
  ),
  '9573cc089c670a3be746a6ee4ccc826f' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Checkbox',
    'comment' => 'Datatype name',
    'translation' => 'チェックボックス',
    'key' => '9573cc089c670a3be746a6ee4ccc826f',
  ),
  'b75836cd872c04f1bfbda15a1e9e71ec' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Enum',
    'comment' => 'Datatype name',
    'translation' => 'リスト',
    'key' => 'b75836cd872c04f1bfbda15a1e9e71ec',
  ),
  '12f012ae4a9d482d99a8d8999b57a223' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'At least one field should be chosen.',
    'comment' => NULL,
    'translation' => '一つ以上フィールドを選択してください。',
    'key' => '12f012ae4a9d482d99a8d8999b57a223',
  ),
  'd7710cdb69e2e851d216870c16cae2cc' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Float',
    'comment' => 'Datatype name',
    'translation' => '浮動小数点数',
    'key' => 'd7710cdb69e2e851d216870c16cae2cc',
  ),
  '97a93f342457d1680df62ad06a8871f0' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Image',
    'comment' => 'Datatype name',
    'translation' => '画像',
    'key' => '97a93f342457d1680df62ad06a8871f0',
  ),
  'd9a9cb1d89690a47b856182d3b6e8a16' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Integer',
    'comment' => 'Datatype name',
    'translation' => '整数',
    'key' => 'd9a9cb1d89690a47b856182d3b6e8a16',
  ),
  '732b90618eede0d096728f1c7c0d05d4' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'ISBN',
    'comment' => 'Datatype name',
    'translation' => 'ISBN',
    'key' => '732b90618eede0d096728f1c7c0d05d4',
  ),
  '17f0a84457d80852c6f278c662817cf3' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Matrix',
    'comment' => 'Datatype name',
    'translation' => '行列',
    'key' => '17f0a84457d80852c6f278c662817cf3',
  ),
  '5e10c6bf082d3fdde0d13552e9b8f1df' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Media',
    'comment' => 'Datatype name',
    'translation' => 'メディア',
    'key' => '5e10c6bf082d3fdde0d13552e9b8f1df',
  ),
  '68192ba071a11736d8aa63084b6fead3' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Object relation',
    'comment' => 'Datatype name',
    'translation' => '関連オブジェクト',
    'key' => '68192ba071a11736d8aa63084b6fead3',
  ),
  'd0211f48d88f55cabb2ad25b9d3ea79a' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Option',
    'comment' => 'Datatype name',
    'translation' => 'オプションリスト',
    'key' => 'd0211f48d88f55cabb2ad25b9d3ea79a',
  ),
  'b9b562906c08f9e84684a538d3c5ad1c' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'At least one option is required.',
    'comment' => NULL,
    'translation' => '一つ以上のオプションが必要です。',
    'key' => 'b9b562906c08f9e84684a538d3c5ad1c',
  ),
  '3506f5cbba8f25d1256c924a456f0e7d' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Price',
    'comment' => 'Datatype name',
    'translation' => '価格',
    'key' => '3506f5cbba8f25d1256c924a456f0e7d',
  ),
  '84a6bf4ad2220f6b9435fe2e9684beac' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Add to basket',
    'comment' => NULL,
    'translation' => '買い物かごに追加',
    'key' => '84a6bf4ad2220f6b9435fe2e9684beac',
  ),
  'ba70ce8a17f2fba80f26f0fb9e7b5868' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Add to wish list',
    'comment' => NULL,
    'translation' => 'ウイッシュリストに追加',
    'key' => 'ba70ce8a17f2fba80f26f0fb9e7b5868',
  ),
  'f7c05e20241f315102a3d23090da9543' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Range option',
    'comment' => 'Datatype name',
    'translation' => '数値範囲',
    'key' => 'f7c05e20241f315102a3d23090da9543',
  ),
  '58af42ae8982bfb0f0bd9b03cfe85864' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Selection',
    'comment' => 'Datatype name',
    'translation' => '選択リスト',
    'key' => '58af42ae8982bfb0f0bd9b03cfe85864',
  ),
  '8e599c29a4aeb39360cc3953369b54b2' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Text line',
    'comment' => 'Datatype name',
    'translation' => 'テキスト',
    'key' => '8e599c29a4aeb39360cc3953369b54b2',
  ),
  '6de37e3bcfe8948746d826790d8c17ac' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Subtree subscription',
    'comment' => 'Datatype name',
    'translation' => 'サブツリーサブクスクリプション',
    'key' => '6de37e3bcfe8948746d826790d8c17ac',
  ),
  'dfdb2dc3cb5ab093a705d1bbeb970521' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'URL',
    'comment' => 'Datatype name',
    'translation' => 'URL',
    'key' => 'dfdb2dc3cb5ab093a705d1bbeb970521',
  ),
  'f922288737e4b5a01163016e3c76a44b' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'User account',
    'comment' => 'Datatype name',
    'translation' => 'ユーザアカウント',
    'key' => 'f922288737e4b5a01163016e3c76a44b',
  ),
  '96f4eb2c57f729dd2cd136864d2d5672' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'A user with this email already exists.',
    'comment' => NULL,
    'translation' => 'このメールアドレスはすでに登録されています。',
    'key' => '96f4eb2c57f729dd2cd136864d2d5672',
  ),
  'e9a439ce4a7ca7bd82a838885306a160' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Identifier',
    'comment' => 'Datatype name',
    'translation' => '識別子',
    'key' => 'e9a439ce4a7ca7bd82a838885306a160',
  ),
  '521d014134a80ef9c7bcce4ff89e4d55' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'image',
    'comment' => 'Default image name',
    'translation' => '画像',
    'key' => '521d014134a80ef9c7bcce4ff89e4d55',
  ),
  'a903f877f0cf8c003eb7d21e26f34634' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Ini Setting',
    'comment' => 'Datatype name',
    'translation' => 'INI設定',
    'key' => 'a903f877f0cf8c003eb7d21e26f34634',
  ),
  '1370b809078ac1bd10eb6fda5aa0d119' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Package',
    'comment' => 'Datatype name',
    'translation' => 'パッケージ',
    'key' => '1370b809078ac1bd10eb6fda5aa0d119',
  ),
  '58c984f66cf61f9ae0025c875d79278c' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Send',
    'comment' => 'Datatype information collector action',
    'translation' => '送信',
    'key' => '58c984f66cf61f9ae0025c875d79278c',
  ),
  '54a56e786bea49154fa03c820a705d6b' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Content required',
    'comment' => NULL,
    'translation' => 'コンテンツが必要です',
    'key' => '54a56e786bea49154fa03c820a705d6b',
  ),
  '0871bc1249d72d497bce251301f77686' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Missing objectrelation input.',
    'comment' => NULL,
    'translation' => 'オブジェクトリレーションが未入力です。',
    'key' => '0871bc1249d72d497bce251301f77686',
  ),
  '5f296c38fb85e53ac45701610462a3ee' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Invalid time.',
    'comment' => NULL,
    'translation' => '時刻が無効です。',
    'key' => '5f296c38fb85e53ac45701610462a3ee',
  ),
  '5e9fd1b1e2807ea71e15dbee1c7068b2' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The author name must be provided.',
    'comment' => NULL,
    'translation' => '作成者名が必要です。',
    'key' => '5e9fd1b1e2807ea71e15dbee1c7068b2',
  ),
  'ee4dc6b7ac6a0d46655e331cadfabe0f' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The email address is not valid.',
    'comment' => NULL,
    'translation' => 'メールアドレスが正しくありません。',
    'key' => 'ee4dc6b7ac6a0d46655e331cadfabe0f',
  ),
  '00cecf8ff8ccb40d4ec4b2ffb3dc6638' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'File uploading is not enabled. Please contact the site administrator to enable it.',
    'comment' => NULL,
    'translation' => 'ファイルアップロードができません。サイト管理者に連絡してください。',
    'key' => '00cecf8ff8ccb40d4ec4b2ffb3dc6638',
  ),
  '3bfb735a1c35c42bce52a847e5103492' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The size of the uploaded file exceeds the limit set by the upload_max_filesize directive in php.ini.',
    'comment' => NULL,
    'translation' => 'アップロードした画像のサイズは、php.iniのupload_max_filesizeディレクティブで設定された制限を越えています。',
    'key' => '3bfb735a1c35c42bce52a847e5103492',
  ),
  '1c19891756e7644591f314023abeae4e' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The size of the uploaded file exceeds the maximum upload size: %1 bytes.',
    'comment' => NULL,
    'translation' => 'アップロードしたサイズはこのサイトの制限である %1 バイトを超えています。',
    'key' => '1c19891756e7644591f314023abeae4e',
  ),
  'f7c1fe207971a242e246a0c90697a915' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The email address is empty.',
    'comment' => NULL,
    'translation' => 'メールアドレスが未入力です。',
    'key' => 'f7c1fe207971a242e246a0c90697a915',
  ),
  '5e77ff4c40951ca84a43f274bfd65660' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The given input is not a floating point number.',
    'comment' => NULL,
    'translation' => '入力は浮動小数点数ではありません。',
    'key' => '5e77ff4c40951ca84a43f274bfd65660',
  ),
  '0523d35d733e1301f2fb118f555a6e5f' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The input must be greater than %1',
    'comment' => NULL,
    'translation' => '入力値は %1 より大きい数値が必要です',
    'key' => '0523d35d733e1301f2fb118f555a6e5f',
  ),
  'f17a23df7b19836a6fc5d00008b1bf37' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The input must be less than %1',
    'comment' => NULL,
    'translation' => '入力値は %1 より小さい数値が必要です',
    'key' => 'f17a23df7b19836a6fc5d00008b1bf37',
  ),
  '1a6405e0bda5bb89686a53e44aff0bdf' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The input is not in defined range %1 - %2',
    'comment' => NULL,
    'translation' => '入力値が定義範囲（ %1 - %2 ）外です',
    'key' => '1a6405e0bda5bb89686a53e44aff0bdf',
  ),
  'ae4cf8a8bfadeb5450c875e86ead293a' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'A valid image file is required.',
    'comment' => NULL,
    'translation' => '有効な画像ファイルが必要です。',
    'key' => 'ae4cf8a8bfadeb5450c875e86ead293a',
  ),
  '0fff9ebaa7c7cd2e7afe7d4853d3e84d' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The size of the uploaded image exceeds limit set by upload_max_filesize directive in php.ini. Please contact the site administrator.',
    'comment' => NULL,
    'translation' => 'アップロードした画像のサイズは、php.ini の upload_max_filesize ディレクティブで設定された制限を超えています。サイト管理者に連絡してください。',
    'key' => '0fff9ebaa7c7cd2e7afe7d4853d3e84d',
  ),
  '704bc2a98a9c5f9aea7e351d6a4a9608' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The size of the uploaded file exceeds the limit set for this site: %1 bytes.',
    'comment' => NULL,
    'translation' => 'アップロードしたファイルはこのサイトの制限である %1 バイトを超えています。',
    'key' => '704bc2a98a9c5f9aea7e351d6a4a9608',
  ),
  'b3e30b61299bb50ae5b5e1a474d940da' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Could not locate the ini file.',
    'comment' => NULL,
    'translation' => 'INIファイルを見つかりません。',
    'key' => 'b3e30b61299bb50ae5b5e1a474d940da',
  ),
  '31915e32bd8de54294f2689c9ce94278' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The input is not a valid integer.',
    'comment' => NULL,
    'translation' => '入力は有効な整数ではありません。',
    'key' => '31915e32bd8de54294f2689c9ce94278',
  ),
  '732449dbd9fc0cb435c010cfdbcdd0a9' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The number must be greater than %1',
    'comment' => NULL,
    'translation' => '数値は %1 より大きい必要があります',
    'key' => '732449dbd9fc0cb435c010cfdbcdd0a9',
  ),
  '1679ed0790bf4afd17b903cbdce2780c' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The number must be less than %1',
    'comment' => NULL,
    'translation' => '数値は %1 より小さい必要があります',
    'key' => '1679ed0790bf4afd17b903cbdce2780c',
  ),
  '97c8684178d211efc830b7a1135cd9cc' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The number is not within the required range %1 - %2',
    'comment' => NULL,
    'translation' => '数値が定義範囲（ %1 - %2 ）外です',
    'key' => '97c8684178d211efc830b7a1135cd9cc',
  ),
  '73c403681aa8a42a79ae8567e4c63a88' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The ISBN number is not correct. Please check the input for mistakes.',
    'comment' => NULL,
    'translation' => 'ISBN番号が正しくありません。誤りを確認してください。',
    'key' => '73c403681aa8a42a79ae8567e4c63a88',
  ),
  '9f7e0df17768b7184620992e5f79b0d1' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'A valid media file is required.',
    'comment' => NULL,
    'translation' => '有効なメディアファイルが必要です。',
    'key' => '9f7e0df17768b7184620992e5f79b0d1',
  ),
  '7651377c5dd1aaee0fb7cd99c5f5630e' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The size of the uploaded file exceeds the limit set by upload_max_filesize directive in php.ini. Please contact the site administrator.',
    'comment' => NULL,
    'translation' => 'アップロードした画像のサイズは、php.ini の upload_max_filesize ディレクティブで設定された制限を超えています。サイト管理者に連絡してください。',
    'key' => '7651377c5dd1aaee0fb7cd99c5f5630e',
  ),
  '254ba66bde6540ed02686e44b3c25606' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The size of the uploaded file exceeds site maximum: %1 bytes.',
    'comment' => NULL,
    'translation' => 'アップロードしたファイルは最大サイズ%1バイトを超えています。',
    'key' => '254ba66bde6540ed02686e44b3c25606',
  ),
  '838e1b2323be7fa58b5dba2c8dc431f2' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The option value must be provided.',
    'comment' => NULL,
    'translation' => 'オプションの値は必須です。',
    'key' => '838e1b2323be7fa58b5dba2c8dc431f2',
  ),
  '9e81de44e2da9d8872c1b536526c6fdc' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The additional price for the multioption value is not valid.',
    'comment' => NULL,
    'translation' => 'マルチオプションの価格設定が無効です。',
    'key' => '9e81de44e2da9d8872c1b536526c6fdc',
  ),
  '4645a145230a348ffa0dd1e4ac5f6412' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The Additional price value is not valid.',
    'comment' => NULL,
    'translation' => '価格設定が無効です。',
    'key' => '4645a145230a348ffa0dd1e4ac5f6412',
  ),
  '53f290ae66dafbea1dd804ef1674c38f' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Input required.',
    'comment' => NULL,
    'translation' => '入力必須項目です。',
    'key' => '53f290ae66dafbea1dd804ef1674c38f',
  ),
  '52987b947f7a47937e39c11acd2f757c' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The input text is too long. The maximum number of characters allowed is %1.',
    'comment' => NULL,
    'translation' => '入力したテキストが長すぎます。最大文字数は%1文字です。',
    'key' => '52987b947f7a47937e39c11acd2f757c',
  ),
  '34e019a0b9e201bba2b3d58760d3b820' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Time input required.',
    'comment' => NULL,
    'translation' => '時刻は入力必須です。',
    'key' => '34e019a0b9e201bba2b3d58760d3b820',
  ),
  'c9afee5fcadb8fcbdbf8a91c683414d1' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The username must be specified.',
    'comment' => NULL,
    'translation' => 'ユーザ名を必ず指定してください。',
    'key' => 'c9afee5fcadb8fcbdbf8a91c683414d1',
  ),
  'af455d9ededbfda27846e26e85032933' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The username already exists, please choose another one.',
    'comment' => NULL,
    'translation' => 'このユーザ名は既に使用されています。異なるユーザ名を選んで下さい。',
    'key' => 'af455d9ededbfda27846e26e85032933',
  ),
  '1684f500f544e8ef5a5a61482c7901a2' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The passwords do not match.',
    'comment' => 'eZUserType',
    'translation' => 'パスワードが一致しません。',
    'key' => '1684f500f544e8ef5a5a61482c7901a2',
  ),
  'f2a62dd1e117b9970f139a4fa950789c' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Cannot remove the account:',
    'comment' => NULL,
    'translation' => 'ユーザを削除できません:',
    'key' => 'f2a62dd1e117b9970f139a4fa950789c',
  ),
  '1cd8d752e9b9b2f46fc2f34d5bb1820e' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The account owner is currently logged in.',
    'comment' => NULL,
    'translation' => '所有者は現在ログイン中です。',
    'key' => '1cd8d752e9b9b2f46fc2f34d5bb1820e',
  ),
  '82c81f5cfff8014173d889bb8ee04de5' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The account is currently used by the anonymous user.',
    'comment' => NULL,
    'translation' => '現在匿名ユーザにより使用されています。',
    'key' => '82c81f5cfff8014173d889bb8ee04de5',
  ),
  '01914151aa7f9585cd7f054b69c6e8e8' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Multi-option',
    'comment' => 'Datatype name',
    'translation' => 'マルチオプションリスト',
    'key' => '01914151aa7f9585cd7f054b69c6e8e8',
  ),
  '681b5dd565eb34f4008b86d479367cd9' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Authors',
    'comment' => 'Datatype name',
    'translation' => '作成者',
    'key' => '681b5dd565eb34f4008b86d479367cd9',
  ),
  '9bbed1606832f7dca53c84194313a455' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'File',
    'comment' => 'Datatype name',
    'translation' => 'ファイル',
    'key' => '9bbed1606832f7dca53c84194313a455',
  ),
  '0e6d37ca07b120b05341a05e0d6c8aa1' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Date',
    'comment' => 'Datatype name',
    'translation' => '日付',
    'key' => '0e6d37ca07b120b05341a05e0d6c8aa1',
  ),
  'da946ac61fb7fbe504c05bdbde7bc35d' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Date and time',
    'comment' => 'Datatype name',
    'translation' => '日付と時刻',
    'key' => 'da946ac61fb7fbe504c05bdbde7bc35d',
  ),
  '9037756c79cfcf4d19b63ba433343c32' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Keywords',
    'comment' => 'Datatype name',
    'translation' => 'キーワード',
    'key' => '9037756c79cfcf4d19b63ba433343c32',
  ),
  '94ca85a254ae6841729d87cf9fa398df' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Object relations',
    'comment' => 'Datatype name',
    'translation' => '関連オブジェクト（複数）',
    'key' => '94ca85a254ae6841729d87cf9fa398df',
  ),
  '21964d43f84df39c5946b7824f42f818' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Text block',
    'comment' => 'Datatype name',
    'translation' => 'テキストブロック',
    'key' => '21964d43f84df39c5946b7824f42f818',
  ),
  '531a412b15cd4f33f29e7c1d4475782e' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Time',
    'comment' => 'Datatype name',
    'translation' => '時刻',
    'key' => '531a412b15cd4f33f29e7c1d4475782e',
  ),
  '230290dac5f542eaed11e1836cccd638' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'XML block',
    'comment' => 'Datatype name',
    'translation' => 'XMLブロック',
    'key' => '230290dac5f542eaed11e1836cccd638',
  ),
  '9124bc08211d7ec3cbc2e34eebe49361' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Object %1 can not be embeded to itself.',
    'comment' => NULL,
    'translation' => 'オブジェクト %1 を自身に埋め込むことはできません。',
    'key' => '9124bc08211d7ec3cbc2e34eebe49361',
  ),
  '36cc4ccfedb93350fe64655b0d92e7f4' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Country',
    'comment' => 'Datatype name',
    'translation' => '国名',
    'key' => '36cc4ccfedb93350fe64655b0d92e7f4',
  ),
  '780962ece0360c582976ba1814dbb80b' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Date is not valid.',
    'comment' => NULL,
    'translation' => '日付が無効です。',
    'key' => '780962ece0360c582976ba1814dbb80b',
  ),
  'b085dcbb90ded9868d4f2cd8a4bdb2be' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Time is not valid.',
    'comment' => NULL,
    'translation' => '時刻が無効です。',
    'key' => 'b085dcbb90ded9868d4f2cd8a4bdb2be',
  ),
  '27d799d0aeb21e31c2a6ed8e45fa7906' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The image file must have non-zero size.',
    'comment' => NULL,
    'translation' => '画像ファイルサイズは0であることができません。',
    'key' => '27d799d0aeb21e31c2a6ed8e45fa7906',
  ),
  'f983b7c8b321d4c6f83a69d7118a2f4e' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Wrong text field value.',
    'comment' => NULL,
    'translation' => 'テキストボックス値に誤りがあります。',
    'key' => 'f983b7c8b321d4c6f83a69d7118a2f4e',
  ),
  '6275a62c93906bfe28d1f6db843fa4a0' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Missing matrix input.',
    'comment' => NULL,
    'translation' => 'マトリックス入力が未入力です。',
    'key' => '6275a62c93906bfe28d1f6db843fa4a0',
  ),
  '82bd69164cd8825fe4b70076d515c7c0' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Multi-price',
    'comment' => 'Datatype name',
    'translation' => '複数価格',
    'key' => '82bd69164cd8825fe4b70076d515c7c0',
  ),
  'c2b26d97c9b7e659e151cd1b533dd9cc' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Invalid price for \'%currencyCode\' currency ',
    'comment' => NULL,
    'translation' => '\'%currencyCode\'通貨で無効な価格です',
    'key' => 'c2b26d97c9b7e659e151cd1b533dd9cc',
  ),
  '052890e9393772a0dd4d8feda79a2a3f' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Missing objectrelation list input.',
    'comment' => NULL,
    'translation' => 'オブジェクトリレーションが未入力です。',
    'key' => '052890e9393772a0dd4d8feda79a2a3f',
  ),
  '59a1f83df340e86cc80bb3c5993971de' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'NAME is required.',
    'comment' => NULL,
    'translation' => '名前は入力必須です。',
    'key' => '59a1f83df340e86cc80bb3c5993971de',
  ),
  '185b159627dee5da7fa897442fed915c' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Invalid price.',
    'comment' => NULL,
    'translation' => '不正な価格です。',
    'key' => '185b159627dee5da7fa897442fed915c',
  ),
  '1be86470ecabdd493069bfea5a716bfd' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Product category',
    'comment' => 'Datatype name',
    'translation' => '商品カテゴリ',
    'key' => '1be86470ecabdd493069bfea5a716bfd',
  ),
  'a06db0d65c6eafa9916d52186a163e2a' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Dynamic VAT cannot be included.',
    'comment' => NULL,
    'translation' => 'ダイナミック課税方法を含めることはできません。',
    'key' => 'a06db0d65c6eafa9916d52186a163e2a',
  ),
  '2fee2cc0afce354860f509e3a696cc0b' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Missing range option input.',
    'comment' => NULL,
    'translation' => '数値範囲が未入力です。',
    'key' => '2fee2cc0afce354860f509e3a696cc0b',
  ),
  '6f246d5b4690fb2568d972bf9b03e939' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Option set name is required.',
    'comment' => NULL,
    'translation' => 'オプションセット名が必要です。',
    'key' => '6f246d5b4690fb2568d972bf9b03e939',
  ),
  'dc8f7c302301b69ae8e9bea9e9d1892c' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The password must be at least %1 characters long.',
    'comment' => NULL,
    'translation' => 'パスワードは最低%1文字必要です。',
    'key' => 'dc8f7c302301b69ae8e9bea9e9d1892c',
  ),
  'c0ffb367073beec99dc7f4b0f84b3a2e' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Invalid reference in &lt;embed&gt; tag. Note that <embed> tag supports only \'eznode\' and \'ezobject\' protocols.',
    'comment' => NULL,
    'translation' => '&lt;embed&gt; タグの参照先が無効です。<embed> タグは \'eznode\' と \'ezobject\' プロトコルのみをサポートします。',
    'key' => 'c0ffb367073beec99dc7f4b0f84b3a2e',
  ),
  '0279ed347a3336bc113db344afed1714' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The ISBN number is not correct. ',
    'comment' => NULL,
    'translation' => 'ISBN番号が正しくありません。',
    'key' => '0279ed347a3336bc113db344afed1714',
  ),
  '2ff496f1048e26c26c7299ad823534cf' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => '13 digit ISBN must start with 978 or 979',
    'comment' => NULL,
    'translation' => '13桁のISBN番号は978または979で始まるはずです',
    'key' => '2ff496f1048e26c26c7299ad823534cf',
  ),
  '935b3eebd2af8e31bfd458384b550b48' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'ISBN length is invalid',
    'comment' => NULL,
    'translation' => 'ISBN番号の長さが正しくありません',
    'key' => '935b3eebd2af8e31bfd458384b550b48',
  ),
  'c2c2cb9767277bec06a57b9d9c42744d' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Email',
    'comment' => 'Datatype name',
    'translation' => 'メール',
    'key' => 'c2c2cb9767277bec06a57b9d9c42744d',
  ),
  '3f1186d8e4ca80f8e6c18b9b40839424' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The registrant element of the ISBN number does not exist.',
    'comment' => NULL,
    'translation' => 'ISBN番号の登録要素は存在しません。',
    'key' => '3f1186d8e4ca80f8e6c18b9b40839424',
  ),
  '9b426e850292da7aff6214ad3bec3e10' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The ISBN number has a incorrect registration group number.',
    'comment' => NULL,
    'translation' => 'ISBN番号の登録グループナンバーが正しく有りません。',
    'key' => '9b426e850292da7aff6214ad3bec3e10',
  ),
  '412b82eefbf5b9548b4699a9104afa86' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The group element of the ISBN number does not exist.',
    'comment' => NULL,
    'translation' => 'ISBN番号のグループ要素は存在しません。',
    'key' => '412b82eefbf5b9548b4699a9104afa86',
  ),
  'db4a1d89e9da4bc5bbb185d470ba0064' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => '%1 is not a valid prefix of the ISBN number.',
    'comment' => NULL,
    'translation' => '%1は有効なISBN番号の数字ではありません。',
    'key' => 'db4a1d89e9da4bc5bbb185d470ba0064',
  ),
  '95773ee7b02be3786c640f6b956e5a72' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'All ISBN 13 characters need to be numeric',
    'comment' => NULL,
    'translation' => 'すべてのISBN13文字は数字である必要があります',
    'key' => '95773ee7b02be3786c640f6b956e5a72',
  ),
  'a7cc260a89d9fbc7426f3a43b24cd204' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Bad checksum, last digit should be %1',
    'comment' => NULL,
    'translation' => 'チェックサムが正しくありません。最後の数字は%1であるはずです',
    'key' => 'a7cc260a89d9fbc7426f3a43b24cd204',
  ),
  '57a10a2a90a5f50492c0d7383e66245c' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The ISBN number should be ISBN13, but seems to be ISBN10.',
    'comment' => NULL,
    'translation' => 'ISBN番号はISBN13である必要がありますが、入力されたものはISBN10のようです。',
    'key' => '57a10a2a90a5f50492c0d7383e66245c',
  ),
  'd0f58ed4f5a1b26def0a75d94660d80f' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Multi-option2',
    'comment' => 'Datatype name',
    'translation' => 'マルチオプションリスト2',
    'key' => 'd0f58ed4f5a1b26def0a75d94660d80f',
  ),
  'f6feeb57c2afb0e56c35348aa0265568' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'You cannot choose option value "%1" from "%2" because it is unselectable ',
    'comment' => NULL,
    'translation' => '選択不可能であるため、"%2"から"%1"オプションを選択することはできません',
    'key' => 'f6feeb57c2afb0e56c35348aa0265568',
  ),
  '222cfd6cad4afb7b295a6bf179730b7f' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'You cannot choose option value "%1" from "%2"  
 if you selected option "%3" from "%4" ',
    'comment' => NULL,
    'translation' => '"%4"からオプション"%3"を選択した場合、"%2"からオプション"%1"を選択することはできません  ',
    'key' => '222cfd6cad4afb7b295a6bf179730b7f',
  ),
  'aa0d31796b608b6951b3de14c64ddfe8' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The password must not be "password".',
    'comment' => NULL,
    'translation' => '”password”をパスワードには設定出来ません。',
    'key' => 'aa0d31796b608b6951b3de14c64ddfe8',
  ),
  '300c7a773f8fb0da03eb14330534acff' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The account is currently used the administrator user.',
    'comment' => NULL,
    'translation' => 'このアカウントは現在、管理者ユーザに使われてます。',
    'key' => '300c7a773f8fb0da03eb14330534acff',
  ),
  'febae2c5ae882b718a50e2868136672d' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'You cannot remove the last class holding user accounts.',
    'comment' => NULL,
    'translation' => 'ユーザアカウントを持っている最後のクラスは削除できません。',
    'key' => 'febae2c5ae882b718a50e2868136672d',
  ),
  'c205ec43a27d16d084a869581ad6b3a3' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Missing email input.',
    'comment' => NULL,
    'translation' => 'メールアドレスが未入力です。',
    'key' => 'c205ec43a27d16d084a869581ad6b3a3',
  ),
  'a52c1d80ad76fa4a8b49eeef0060d8c3' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'Input required',
    'comment' => NULL,
    'translation' => '入力必須項目',
    'key' => 'a52c1d80ad76fa4a8b49eeef0060d8c3',
  ),
  '682b66c900304eb37f4bcc7cf87ddc34' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The password cannot be empty.',
    'comment' => 'eZUserType',
    'translation' => 'パスワードは空にすることができません。',
    'key' => '682b66c900304eb37f4bcc7cf87ddc34',
  ),
  '8f84d362b57bb62d4ef23894e49bbd5b' => 
  array (
    'context' => 'kernel/classes/datatypes',
    'source' => 'The emails do not match.',
    'comment' => 'eZUserType',
    'translation' => 'メールは一致しません。',
    'key' => '8f84d362b57bb62d4ef23894e49bbd5b',
  ),
);
?>
