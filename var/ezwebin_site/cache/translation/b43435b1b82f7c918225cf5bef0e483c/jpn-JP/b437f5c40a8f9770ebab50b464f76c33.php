<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/edit/file',
);

$TranslationRoot = array (
  'a44602405e78ef0b267e57639ab5c02a' => 
  array (
    'context' => 'design/ezwebin/edit/file',
    'source' => 'Edit %1 - %2',
    'comment' => NULL,
    'translation' => '%1 - %2の編集',
    'key' => 'a44602405e78ef0b267e57639ab5c02a',
  ),
  '1649ef7c15526b4ed2f0c5f3c7614a81' => 
  array (
    'context' => 'design/ezwebin/edit/file',
    'source' => 'Send for publishing',
    'comment' => NULL,
    'translation' => '送信して公開',
    'key' => '1649ef7c15526b4ed2f0c5f3c7614a81',
  ),
  'ca02a0e8fb4f8b26e31a2ceab7bb2cf5' => 
  array (
    'context' => 'design/ezwebin/edit/file',
    'source' => 'Discard',
    'comment' => NULL,
    'translation' => '破棄',
    'key' => 'ca02a0e8fb4f8b26e31a2ceab7bb2cf5',
  ),
);
?>
