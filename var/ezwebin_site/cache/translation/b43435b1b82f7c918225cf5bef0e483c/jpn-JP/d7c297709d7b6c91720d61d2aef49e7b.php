<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/browse_move_placement',
);

$TranslationRoot = array (
  'b51ccaa4a53e7318347188d7c791a99a' => 
  array (
    'context' => 'design/admin/content/browse_move_placement',
    'source' => 'Choose a new location for <%version_name>',
    'comment' => NULL,
    'translation' => '<%version_name> の新しい配置先の選択',
    'key' => 'b51ccaa4a53e7318347188d7c791a99a',
  ),
  '45d770b97fb1077722429e20bd77b249' => 
  array (
    'context' => 'design/admin/content/browse_move_placement',
    'source' => 'The previous location was <%previous_location>.',
    'comment' => NULL,
    'translation' => '従来の配置は <%previous_location> でした。',
    'key' => '45d770b97fb1077722429e20bd77b249',
  ),
  'd03e5592dcc0904248a2ea7f165645bb' => 
  array (
    'context' => 'design/admin/content/browse_move_placement',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => 'd03e5592dcc0904248a2ea7f165645bb',
  ),
  'cdd7ae2b3d109ad2cc7030eea78ef8f2' => 
  array (
    'context' => 'design/admin/content/browse_move_placement',
    'source' => 'Choose a new location for <%version_name> using the radio buttons then click "Select".',
    'comment' => NULL,
    'translation' => '<%version_name>の新しい配置先をラジオボタンで選択し、"選択"をクリックしてください。',
    'key' => 'cdd7ae2b3d109ad2cc7030eea78ef8f2',
  ),
);
?>
