<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/translations',
);

$TranslationRoot = array (
  '92a3f0e9b6cbfff16ca01c3ad9a546db' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Language',
    'comment' => NULL,
    'translation' => '言語',
    'key' => '92a3f0e9b6cbfff16ca01c3ad9a546db',
  ),
  '7dba08654a21e5018936e5fef0cee7cf' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Locale',
    'comment' => NULL,
    'translation' => 'ロケール',
    'key' => '7dba08654a21e5018936e5fef0cee7cf',
  ),
  '4349e1e9c6f007bbc4ea5d815d39e7f1' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => '4349e1e9c6f007bbc4ea5d815d39e7f1',
  ),
  'f2f44b6d29dc30cbe914b9469c0c433b' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Add language',
    'comment' => NULL,
    'translation' => '言語の追加',
    'key' => 'f2f44b6d29dc30cbe914b9469c0c433b',
  ),
  '81fad4d3553cfda0c4426f6893640580' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Available languages for translation of content [%translations_count]',
    'comment' => NULL,
    'translation' => 'コンテンツ翻訳に利用可能な言語 [%translations_count]',
    'key' => '81fad4d3553cfda0c4426f6893640580',
  ),
  '6d65ff3b19fd0d985662b1432fb242db' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Invert selection.',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => '6d65ff3b19fd0d985662b1432fb242db',
  ),
  '67b5cea707ee20b34d13b9c1e67c2a9a' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Translations',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => '67b5cea707ee20b34d13b9c1e67c2a9a',
  ),
  'f606580ea53440bd5e38e21978298ec4' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Select language for removal.',
    'comment' => NULL,
    'translation' => '削除する言語を選択',
    'key' => 'f606580ea53440bd5e38e21978298ec4',
  ),
  'ec43ddffdd8219bd72852eb258734371' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Remove selected languages.',
    'comment' => NULL,
    'translation' => '選択した言語を削除',
    'key' => 'ec43ddffdd8219bd72852eb258734371',
  ),
  'b221929b918c5e231e76d9f4e2bc4f10' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Add a new language. The new language can then be used when translating content.',
    'comment' => NULL,
    'translation' => '新しい言語を追加します。新しい言語はコンテンツ翻訳に使用できるようになります。',
    'key' => 'b221929b918c5e231e76d9f4e2bc4f10',
  ),
  '56fb9e4b3b95df4e658f6371c05b8b8a' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Classes translations',
    'comment' => NULL,
    'translation' => 'クラス翻訳',
    'key' => '56fb9e4b3b95df4e658f6371c05b8b8a',
  ),
  '7c4b02d9f735c5f5c5dc200688099d0b' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Country/region',
    'comment' => NULL,
    'translation' => '国/県',
    'key' => '7c4b02d9f735c5f5c5dc200688099d0b',
  ),
  '65f141c20fee8d62f7c50feded50b1e0' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'The language cannot be removed because it is in use.',
    'comment' => NULL,
    'translation' => '現在使用されているため、言語を削除することはできません。',
    'key' => '65f141c20fee8d62f7c50feded50b1e0',
  ),
  '08436deb80ffdafb1c2d8c08a7f90fdb' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Available languages for translation of content (%translations_count)',
    'comment' => NULL,
    'translation' => 'コンテンツ翻訳に利用可能な言語 (%translations_count)',
    'key' => '08436deb80ffdafb1c2d8c08a7f90fdb',
  ),
  'bddb6cb912ad27839173f530b800726a' => 
  array (
    'context' => 'design/admin/content/translations',
    'source' => 'Toggle all.',
    'comment' => NULL,
    'translation' => 'すべて反転。',
    'key' => 'bddb6cb912ad27839173f530b800726a',
  ),
);
?>
