<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/shop/userregister',
);

$TranslationRoot = array (
  'b6fc8b98c846adf8bbb2688d0ba60586' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Shopping basket',
    'comment' => NULL,
    'translation' => '買い物カゴ',
    'key' => 'b6fc8b98c846adf8bbb2688d0ba60586',
  ),
  'bc9d62372bc7a8b69f01a654c7fb0c71' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Account information',
    'comment' => NULL,
    'translation' => 'お客様情報',
    'key' => 'bc9d62372bc7a8b69f01a654c7fb0c71',
  ),
  '125fb5f2c86b0298daf3fcf41ffc8ab0' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Confirm order',
    'comment' => NULL,
    'translation' => 'オーダーの確認',
    'key' => '125fb5f2c86b0298daf3fcf41ffc8ab0',
  ),
  '2d806e2e2aae53c36ba5f70bbb6f358a' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Your account information',
    'comment' => NULL,
    'translation' => 'お客様情報',
    'key' => '2d806e2e2aae53c36ba5f70bbb6f358a',
  ),
  '21e49e3fe09b2ac59c4cf5ebf71ebc51' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'First name',
    'comment' => NULL,
    'translation' => '名',
    'key' => '21e49e3fe09b2ac59c4cf5ebf71ebc51',
  ),
  'eabe423dcf73fd17ff08688bdce9a13b' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Last name',
    'comment' => NULL,
    'translation' => '姓',
    'key' => 'eabe423dcf73fd17ff08688bdce9a13b',
  ),
  '20b050bfabfae1d812facbf2238f80b8' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Company',
    'comment' => NULL,
    'translation' => '会社・法人名',
    'key' => '20b050bfabfae1d812facbf2238f80b8',
  ),
  'c321ce420eeb704fba5c4aca95effe70' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Street',
    'comment' => NULL,
    'translation' => 'その他(番地、建物名など)',
    'key' => 'c321ce420eeb704fba5c4aca95effe70',
  ),
  '5ae64f094f0340543ed31bfcd15f4fb8' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Zip',
    'comment' => NULL,
    'translation' => '郵便番号',
    'key' => '5ae64f094f0340543ed31bfcd15f4fb8',
  ),
  '2231cb0e81c890d8f96bfc689c630d65' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Place',
    'comment' => NULL,
    'translation' => '市区町村',
    'key' => '2231cb0e81c890d8f96bfc689c630d65',
  ),
  '828529ca830b1af8be8e0da3a338b127' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'State',
    'comment' => NULL,
    'translation' => '都道府県・州',
    'key' => '828529ca830b1af8be8e0da3a338b127',
  ),
  '2e752e98e9131ef8393b64f7d75e406e' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Country',
    'comment' => NULL,
    'translation' => '国名',
    'key' => '2e752e98e9131ef8393b64f7d75e406e',
  ),
  '2dedfc06a3feeb4ac0624b3d64158006' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Comment',
    'comment' => NULL,
    'translation' => 'コメント',
    'key' => '2dedfc06a3feeb4ac0624b3d64158006',
  ),
  '5bab2925f95d2bc81991777b9a6bd699' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '5bab2925f95d2bc81991777b9a6bd699',
  ),
  '2b0a50c5145853c2bcc19f01922a27fb' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Continue',
    'comment' => NULL,
    'translation' => '次へ',
    'key' => '2b0a50c5145853c2bcc19f01922a27fb',
  ),
  '5383c34be8fbe05fe33809131fb8f051' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'All fields marked with * must be filled in.',
    'comment' => NULL,
    'translation' => '* 印の付いた項目は入力必須です。',
    'key' => '5383c34be8fbe05fe33809131fb8f051',
  ),
  '8ba525dad2f10702a3f7b42d1c61f853' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Input did not validate. All fields marked with * must be filled in.',
    'comment' => NULL,
    'translation' => '* 印の付いた項目は入力必須です。全ての項目に入力してください。',
    'key' => '8ba525dad2f10702a3f7b42d1c61f853',
  ),
  'b9a51ae86b71c220ed09a6c390f75cc7' => 
  array (
    'context' => 'design/ezwebin/shop/userregister',
    'source' => 'Email',
    'comment' => NULL,
    'translation' => 'e-mail',
    'key' => 'b9a51ae86b71c220ed09a6c390f75cc7',
  ),
);
?>
