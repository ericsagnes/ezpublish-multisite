<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/websitetoolbar/sort',
);

$TranslationRoot = array (
  'f18ee5ac76fb4ce7b3cb12a16ed59254' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Sub items [%children_count]',
    'comment' => NULL,
    'translation' => 'サブアイテム  [%children_count]',
    'key' => 'f18ee5ac76fb4ce7b3cb12a16ed59254',
  ),
  '553504c41defc24fbefc357e2fc26fe6' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Show 10 items per page.',
    'comment' => NULL,
    'translation' => '表示: 10 アイテム/ページ',
    'key' => '553504c41defc24fbefc357e2fc26fe6',
  ),
  '1ae95526c3e68ed76f23fcfd171e2813' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Show 50 items per page.',
    'comment' => NULL,
    'translation' => '表示: 50 アイテム/ページ',
    'key' => '1ae95526c3e68ed76f23fcfd171e2813',
  ),
  'c6369a028d816e060249296957f0af2c' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Show 25 items per page.',
    'comment' => NULL,
    'translation' => '表示: 25 アイテム/ページ',
    'key' => 'c6369a028d816e060249296957f0af2c',
  ),
  'b380a0a321e952f19d5d2b3cea049d66' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Priority',
    'comment' => NULL,
    'translation' => '優先度',
    'key' => 'b380a0a321e952f19d5d2b3cea049d66',
  ),
  '9022830e74114dc7c8bd7516ae3df246' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '9022830e74114dc7c8bd7516ae3df246',
  ),
  '815ff08b9b2ac95d575ce593a4335469' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Type',
    'comment' => NULL,
    'translation' => 'タイプ',
    'key' => '815ff08b9b2ac95d575ce593a4335469',
  ),
  'ea324f61c96e491ecb549cee29d99509' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => 'ea324f61c96e491ecb549cee29d99509',
  ),
  '2630b4f4c981e2530907bb506a5909a9' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Section',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => '2630b4f4c981e2530907bb506a5909a9',
  ),
  'f05445d6d82b83dde9224fd77ec873a5' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Use the priority fields to control the order in which the items appear. You can use both positive and negative integers. Click the "Update priorities" button to apply the changes.',
    'comment' => NULL,
    'translation' => 'アイテムの表示順をコントロールするには、優先度を利用して下さい。正と負両方の正数を利用出来ます。変更を適用するには「優先度の更新」ボタンをクリックして下さい。',
    'key' => 'f05445d6d82b83dde9224fd77ec873a5',
  ),
  'ff14da553f17a03f6a9dc8106028779a' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'You are not allowed to update the priorities because you do not have permission to edit <%node_name>.',
    'comment' => NULL,
    'translation' => '<%node_name>を編集する権限を持っていないため、優先度を更新することが出来ません。',
    'key' => 'ff14da553f17a03f6a9dc8106028779a',
  ),
  'a6a6b088547494643d5ea228e2c5e516' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'The current item does not contain any sub items.',
    'comment' => NULL,
    'translation' => 'このアイテムはサブアイテムを持っていません。',
    'key' => 'a6a6b088547494643d5ea228e2c5e516',
  ),
  '777d9441f83088938673e1988c77b609' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Update priorities',
    'comment' => NULL,
    'translation' => '優先度の更新',
    'key' => '777d9441f83088938673e1988c77b609',
  ),
  '438fc8ba80d6a48fd772067bb2ec2361' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Apply changes to the priorities of the items in the list above.',
    'comment' => NULL,
    'translation' => '子アイテムリストに優先度の変更を適用します。',
    'key' => '438fc8ba80d6a48fd772067bb2ec2361',
  ),
  '7cc8cfbf02276e261fda0d2745643b4f' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'You cannot update the priorities because you do not have permission to edit the current item or because a non-priority sorting method is used.',
    'comment' => NULL,
    'translation' => 'このアイテムの編集権限を持っていないか、優先度以外のソート方法が設定されているため、優先度の更新はできません。',
    'key' => '7cc8cfbf02276e261fda0d2745643b4f',
  ),
  '31823da7ae582591dfc83c149414ac72' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Sorting',
    'comment' => NULL,
    'translation' => 'ソート',
    'key' => '31823da7ae582591dfc83c149414ac72',
  ),
  '84a10220f47f628961ed4caa4050a1bc' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Class identifier',
    'comment' => NULL,
    'translation' => 'クラス識別子',
    'key' => '84a10220f47f628961ed4caa4050a1bc',
  ),
  'cc8d95f30b68bbcbd3891d8c97002516' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Class name',
    'comment' => NULL,
    'translation' => 'クラス名',
    'key' => 'cc8d95f30b68bbcbd3891d8c97002516',
  ),
  '3088022d87d1f19e8dd4e1a94c9099d7' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Depth',
    'comment' => NULL,
    'translation' => '深度',
    'key' => '3088022d87d1f19e8dd4e1a94c9099d7',
  ),
  'a97a3cb191892fa237b7e2dd2a6142e1' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Published',
    'comment' => NULL,
    'translation' => '公開中',
    'key' => 'a97a3cb191892fa237b7e2dd2a6142e1',
  ),
  '2b26e94b91751632b7856ad5f43e5e9c' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'You cannot set the sorting method for the current location because you do not have permission to edit the current item.',
    'comment' => NULL,
    'translation' => 'このアイテムの編集権限を持っていないため、ソート方法を設定することができません。',
    'key' => '2b26e94b91751632b7856ad5f43e5e9c',
  ),
  'be7360c9b1bcea84f436366f54f0c23d' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Use these controls to set the sorting method for the sub items of the current location.',
    'comment' => NULL,
    'translation' => 'このメニューで子アイテムのソート方法を指定します。',
    'key' => 'be7360c9b1bcea84f436366f54f0c23d',
  ),
  '89e291c5da0fc17a245f1c314ea36eed' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Descending',
    'comment' => NULL,
    'translation' => '降順',
    'key' => '89e291c5da0fc17a245f1c314ea36eed',
  ),
  'e5a0fa72ab19033c5833a658df6c5088' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Ascending',
    'comment' => NULL,
    'translation' => '昇順',
    'key' => 'e5a0fa72ab19033c5833a658df6c5088',
  ),
  '9f5c47942d58fac1bd7e380227218bde' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Set',
    'comment' => NULL,
    'translation' => '設定',
    'key' => '9f5c47942d58fac1bd7e380227218bde',
  ),
  '16a687e217100ded849f2454fc43da46' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Invalid or missing parameter: %parameter',
    'comment' => NULL,
    'translation' => '無効なパラメータか、パラメータが見つかりません: %parameter',
    'key' => '16a687e217100ded849f2454fc43da46',
  ),
  '9151387e688243efb12b80d0c80a5c74' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Use these checkboxes to select items for removal. Click the "Remove selected" button to  remove the selected items.',
    'comment' => NULL,
    'translation' => 'このチェックボックスを使って、削除するアイテムを選択してください。削除するに、「選択項目の削除」ボタンを押してください。',
    'key' => '9151387e688243efb12b80d0c80a5c74',
  ),
  'fe93dfc944f1d1c704db7b5bfda08bb6' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'You do not have permission to remove this item.',
    'comment' => NULL,
    'translation' => 'このアイテムを削除する権限がありません。',
    'key' => 'fe93dfc944f1d1c704db7b5bfda08bb6',
  ),
  '57607acfa7915e0aeb243b737d6c8be9' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Remove the selected items from the list above.',
    'comment' => NULL,
    'translation' => '上記のリストから選択した項目の削除。',
    'key' => '57607acfa7915e0aeb243b737d6c8be9',
  ),
  '47f6520efa17a3c2677826850fbf8ab9' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'You do not have permission to remove any of the items from the list above.',
    'comment' => NULL,
    'translation' => '上記のリストのアイテムを削除する権限はありません。',
    'key' => '47f6520efa17a3c2677826850fbf8ab9',
  ),
  '2330f61e4e3451bdb33df4dd51b14634' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Automatic update',
    'comment' => NULL,
    'translation' => '自動アップデート',
    'key' => '2330f61e4e3451bdb33df4dd51b14634',
  ),
  'eb1f31da3b5b87a48c96ce88895e4286' => 
  array (
    'context' => 'design/standard/websitetoolbar/sort',
    'source' => 'Path String',
    'comment' => NULL,
    'translation' => 'Path String',
    'key' => 'eb1f31da3b5b87a48c96ce88895e4286',
  ),
);
?>
