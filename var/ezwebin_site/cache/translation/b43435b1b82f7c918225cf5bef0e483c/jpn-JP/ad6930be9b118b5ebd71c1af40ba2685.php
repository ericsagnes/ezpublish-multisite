<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/collaboration/approval',
);

$TranslationRoot = array (
  'bb61d6d024425874795993d9086a979d' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'Approval',
    'comment' => NULL,
    'translation' => '承認',
    'key' => 'bb61d6d024425874795993d9086a979d',
  ),
  '852f5bd2bc1dc1a76485af85a00183da' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'The content object %1 awaits approval before it can be published.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクト %1 は公開前の承認を待っています。',
    'key' => '852f5bd2bc1dc1a76485af85a00183da',
  ),
  'aeabd79c0b42ca2f7aaf5429b214158d' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'The content object %1 needs your approval before it can be published.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクト %1 は公開前にあなたの承認が必要です。',
    'key' => 'aeabd79c0b42ca2f7aaf5429b214158d',
  ),
  '3de5af77c0bac085fae36fb233dacfa5' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'Do you approve of the content object being published?',
    'comment' => NULL,
    'translation' => 'このコンテンツオブジェクトの公開を承認しますか？',
    'key' => '3de5af77c0bac085fae36fb233dacfa5',
  ),
  'efec9ab747543e7f28f282e83bdd2eab' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'Comment',
    'comment' => NULL,
    'translation' => 'コメント',
    'key' => 'efec9ab747543e7f28f282e83bdd2eab',
  ),
  '263320804ac61732b2bd62c97cd877d0' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'Add Comment',
    'comment' => NULL,
    'translation' => 'コメントの追加',
    'key' => '263320804ac61732b2bd62c97cd877d0',
  ),
  '795ec2b3491bcadc93962dccd6c130ab' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'Approve',
    'comment' => NULL,
    'translation' => '承認',
    'key' => '795ec2b3491bcadc93962dccd6c130ab',
  ),
  'bb310325b1ae5939493079b5e328a9e7' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'Deny',
    'comment' => NULL,
    'translation' => '否認',
    'key' => 'bb310325b1ae5939493079b5e328a9e7',
  ),
  '95070ff02a1feec5c1c15e5ce29f4431' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'Participants',
    'comment' => NULL,
    'translation' => '関係ユーザ',
    'key' => '95070ff02a1feec5c1c15e5ce29f4431',
  ),
  '979344b5bb4bb71aa65db0732ec75391' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'Messages',
    'comment' => NULL,
    'translation' => 'メッセージ',
    'key' => '979344b5bb4bb71aa65db0732ec75391',
  ),
  '7aa11fe1b93a741e9010417a6301dde2' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'Edit the object',
    'comment' => NULL,
    'translation' => 'オブジェクトの編集',
    'key' => '7aa11fe1b93a741e9010417a6301dde2',
  ),
  'b0e6a2adcd6767901b20125a15b891de' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'The content object %1 was not accepted but will be available as a draft for the author.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクト %1 の公開は承認されませんでしたが、作成者は下書きとして利用可能です。',
    'key' => 'b0e6a2adcd6767901b20125a15b891de',
  ),
  'c20b702d95477ca20e5c16f5df08ecc6' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => '[%sitename] Approval of "%objectname" awaits your attention',
    'comment' => NULL,
    'translation' => '[%sitename] "%objectname" があなたの承認を待っています。',
    'key' => 'c20b702d95477ca20e5c16f5df08ecc6',
  ),
  '96e9842c71d5e1f8268e5937d408efcf' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => '[%sitename] "%objectname" awaits approval',
    'comment' => NULL,
    'translation' => '[%sitename] "%objectname" は承認を待っています。',
    'key' => '96e9842c71d5e1f8268e5937d408efcf',
  ),
  'a25f6c9490ef5a040d8dad20f5f28390' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'You may re-edit the draft and publish it, in which case an approval is required again.',
    'comment' => NULL,
    'translation' => '下書きを編集後、再び公開の承認を要求することが出来ます。',
    'key' => 'a25f6c9490ef5a040d8dad20f5f28390',
  ),
  '3b095d0ce7f60e01e2e0fe9529ce5845' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'This email is to inform you that "%objectname" awaits your attention at %sitename.
The publishing process has been halted and it is up to you to decide if it should continue or stop.
The approval can be viewed by using the URL below.',
    'comment' => NULL,
    'translation' => 'このメールは、%sitenameに公開される"%objectname"があなたの承認を待っていることを通知するメールです。
公開プロセスは承認待ちのために保留されています。
承認待ちのコンテンツは下記のURLから確認できます。',
    'key' => '3b095d0ce7f60e01e2e0fe9529ce5845',
  ),
  'cdc4369e9f8397d331a36589526d6b00' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'This email is to inform you that "%objectname" awaits approval at %sitename before it can be published.
If you want to send comments to the approver or view the status use the URL below.',
    'comment' => NULL,
    'translation' => 'このメールは、%sitenameに公開される"%objectname"は公開前に承認が必要であることを通知するメールです。承認者にコメントを送るか、ステータスを確認する場合は下記のURLで行ってください。',
    'key' => 'cdc4369e9f8397d331a36589526d6b00',
  ),
  '225491286c39ceb5b2f42cdfcf9dae69' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'Do you want to send a message to the person approving it?',
    'comment' => NULL,
    'translation' => '承認者にメールを送りますか?',
    'key' => '225491286c39ceb5b2f42cdfcf9dae69',
  ),
  '1c97163d46d888c61a8655fd0750b74a' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'The content object %1 was approved and will be published when the publishing workflow continues.',
    'comment' => NULL,
    'translation' => '%1コンテンツオブジェクトは承認されました。公開ワークフローが実行されたら、ウエブサイトに公開されます。',
    'key' => '1c97163d46d888c61a8655fd0750b74a',
  ),
  '8b2f8606dee97b4400fc86d49fcc1328' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'The content object %1 was not accepted but is still available as a draft.',
    'comment' => NULL,
    'translation' => '%1コンテンツオブジェクトは承認されませんでした。現在は下書きとして利用できます。',
    'key' => '8b2f8606dee97b4400fc86d49fcc1328',
  ),
  '4e4ddb9d3de21d778fa074cbd1c2fa97' => 
  array (
    'context' => 'design/standard/collaboration/approval',
    'source' => 'The author can re-edit the draft and publish it again, in which case a new approval item is made.',
    'comment' => NULL,
    'translation' => '作成者は下書きを再編集し、もう一度承認プロセスへ送ることができます。その場合新しい承認アイテムが作成されます。',
    'key' => '4e4ddb9d3de21d778fa074cbd1c2fa97',
  ),
);
?>
