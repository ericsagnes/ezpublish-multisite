<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/content/tipafriend',
);

$TranslationRoot = array (
  '4245580ac21ff8f05fbb33fd99285112' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'Tip a friend',
    'comment' => NULL,
    'translation' => '友達に教える',
    'key' => '4245580ac21ff8f05fbb33fd99285112',
  ),
  'df50facb098da09a8097a0a2e43dcdb2' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'The message was sent.',
    'comment' => NULL,
    'translation' => 'メッセージを送信しました。',
    'key' => 'df50facb098da09a8097a0a2e43dcdb2',
  ),
  '28f9eccc868abb0e2aadc7dc6badcf8c' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'Click here to return to the original page.',
    'comment' => NULL,
    'translation' => '元のページに戻るには、ここをクリックしてください。',
    'key' => '28f9eccc868abb0e2aadc7dc6badcf8c',
  ),
  'afd6aba3a7cbbbfd10bd7709a5d659cc' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'The message was not sent.',
    'comment' => NULL,
    'translation' => 'メッセージは、送信されませんでした。',
    'key' => 'afd6aba3a7cbbbfd10bd7709a5d659cc',
  ),
  'c2d29c3a1e2351bba34f8da004c0beeb' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'The message was not sent due to an unknown error. Please notify the site administrator about this error.',
    'comment' => NULL,
    'translation' => 'メッセージは、予期せぬエラーが発生したために送信されませんでした。 このエラーをサイト管理者にご連絡ください。',
    'key' => 'c2d29c3a1e2351bba34f8da004c0beeb',
  ),
  'd317b5122b7047f7d10f6411641e54c8' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'Your name',
    'comment' => NULL,
    'translation' => 'お名前',
    'key' => 'd317b5122b7047f7d10f6411641e54c8',
  ),
  'bada950fd0739d73546de21956876079' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'Your email address',
    'comment' => NULL,
    'translation' => 'e-mail',
    'key' => 'bada950fd0739d73546de21956876079',
  ),
  '1a12c9cbd5926adb28d1253babd8f002' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'Receivers name',
    'comment' => NULL,
    'translation' => '受信者名',
    'key' => '1a12c9cbd5926adb28d1253babd8f002',
  ),
  '64d9c6154a4b9792b48daba34a1154a9' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'Receivers email address',
    'comment' => NULL,
    'translation' => '受信者のe-mail',
    'key' => '64d9c6154a4b9792b48daba34a1154a9',
  ),
  '9b29e7492eea83a540f36a17ceca15c9' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'Subject',
    'comment' => NULL,
    'translation' => '件名',
    'key' => '9b29e7492eea83a540f36a17ceca15c9',
  ),
  'd3adf7cf99973794badf2ecc191e441e' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'Comment',
    'comment' => NULL,
    'translation' => 'コメント',
    'key' => 'd3adf7cf99973794badf2ecc191e441e',
  ),
  '5ae348ae9f80b34cf6da5b6758438c65' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'Send',
    'comment' => NULL,
    'translation' => '送信',
    'key' => '5ae348ae9f80b34cf6da5b6758438c65',
  ),
  '4b8da2856f1b9c0f9b00a7fb0da70f46' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '4b8da2856f1b9c0f9b00a7fb0da70f46',
  ),
  'c06565b65f0ff28758f1c435486e97e3' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'This message was sent to you because "%1 <%2>" thought you might find the page "%3" at %4 interesting.',
    'comment' => NULL,
    'translation' => 'このメッセージは"%1 <%2>"さんが、%4 サイトの "%3" ページをあなたに紹介するために送信されました。',
    'key' => 'c06565b65f0ff28758f1c435486e97e3',
  ),
  'eb6963bde2eca380f25647ff0dd02a46' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'Please correct the following errors',
    'comment' => NULL,
    'translation' => 'エラーを確認してください',
    'key' => 'eb6963bde2eca380f25647ff0dd02a46',
  ),
  'a99a848b1d767db50abc5985c7a2f9a9' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'This is the link to the page',
    'comment' => NULL,
    'translation' => 'このページへのリンク',
    'key' => 'a99a848b1d767db50abc5985c7a2f9a9',
  ),
  '895d22d2405c91fedd99fe8bcc98f165' => 
  array (
    'context' => 'design/standard/content/tipafriend',
    'source' => 'Comment by "%1 <%2>"',
    'comment' => NULL,
    'translation' => '"%1 <%2>"さんからのコメント',
    'key' => '895d22d2405c91fedd99fe8bcc98f165',
  ),
);
?>
