<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/notification/handler/ezgeneraldigest/settings/edit',
);

$TranslationRoot = array (
  '49c32315ff15129e8141a7af89ed0d05' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezgeneraldigest/settings/edit',
    'source' => 'Receive all messages combined in one digest',
    'comment' => NULL,
    'translation' => 'すべての通知をダイジェスト形式で受け取る',
    'key' => '49c32315ff15129e8141a7af89ed0d05',
  ),
  '6c65caf455ac65aa621eb01396e0c785' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezgeneraldigest/settings/edit',
    'source' => 'Receive digests',
    'comment' => NULL,
    'translation' => 'ダイジェストの受信',
    'key' => '6c65caf455ac65aa621eb01396e0c785',
  ),
  'e5ca45d98172b95dcc369a48c33bc669' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezgeneraldigest/settings/edit',
    'source' => 'Daily, at',
    'comment' => NULL,
    'translation' => '毎日 (指定時刻)',
    'key' => 'e5ca45d98172b95dcc369a48c33bc669',
  ),
  'c35d0b0bdb5081da79bedc8d43df103e' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezgeneraldigest/settings/edit',
    'source' => 'Once per week, on ',
    'comment' => NULL,
    'translation' => '週に一回 (曜日指定)',
    'key' => 'c35d0b0bdb5081da79bedc8d43df103e',
  ),
  '4909c5c013621da85e22db09f8806866' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezgeneraldigest/settings/edit',
    'source' => 'Once per month, on day number',
    'comment' => NULL,
    'translation' => '月に一回 (指定)',
    'key' => '4909c5c013621da85e22db09f8806866',
  ),
  '6f9f4425fe9edc7c3285c8ccb8ba74eb' => 
  array (
    'context' => 'design/ezwebin/notification/handler/ezgeneraldigest/settings/edit',
    'source' => 'If day number is larger than the number of days within the current month, the last day of the current month will be used.',
    'comment' => NULL,
    'translation' => '指定日が該当月に存在しない場合、月の最終日がそれに代わります。',
    'key' => '6f9f4425fe9edc7c3285c8ccb8ba74eb',
  ),
);
?>
