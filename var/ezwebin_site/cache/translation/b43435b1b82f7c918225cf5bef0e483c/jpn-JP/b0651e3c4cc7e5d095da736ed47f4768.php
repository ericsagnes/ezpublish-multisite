<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/shop/discountgroup',
);

$TranslationRoot = array (
  'bdb8fee4256ae3f7df0926615f227cac' => 
  array (
    'context' => 'design/admin/shop/discountgroup',
    'source' => 'Discount groups [%discount_groups]',
    'comment' => NULL,
    'translation' => 'ディスカウント・グループ [%discount_groups]',
    'key' => 'bdb8fee4256ae3f7df0926615f227cac',
  ),
  '66693be17b20445c0a8f46a582069bd6' => 
  array (
    'context' => 'design/admin/shop/discountgroup',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '66693be17b20445c0a8f46a582069bd6',
  ),
  'ef1cd1c7b0fbaeb4001ba5ef1e1d45a0' => 
  array (
    'context' => 'design/admin/shop/discountgroup',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => 'ef1cd1c7b0fbaeb4001ba5ef1e1d45a0',
  ),
  '2ec09a29def54c2e4d6046b7d3354d95' => 
  array (
    'context' => 'design/admin/shop/discountgroup',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => '2ec09a29def54c2e4d6046b7d3354d95',
  ),
  'a6a29bc264887933125b46e1bcfe6f86' => 
  array (
    'context' => 'design/admin/shop/discountgroup',
    'source' => 'New discount group',
    'comment' => NULL,
    'translation' => '新規のディスカウント・グループ',
    'key' => 'a6a29bc264887933125b46e1bcfe6f86',
  ),
  '1d2f6e89ea43083d30d801a12749fb9c' => 
  array (
    'context' => 'design/admin/shop/discountgroup',
    'source' => 'Invert selection.',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => '1d2f6e89ea43083d30d801a12749fb9c',
  ),
  '61bba70b9b4f553bf1aa789b41abfb02' => 
  array (
    'context' => 'design/admin/shop/discountgroup',
    'source' => 'Select discount group for removal.',
    'comment' => NULL,
    'translation' => '削除するディスカウント・グループの選択',
    'key' => '61bba70b9b4f553bf1aa789b41abfb02',
  ),
  'cc77011c2c01fd82028f41f92ca34165' => 
  array (
    'context' => 'design/admin/shop/discountgroup',
    'source' => 'Edit the <%discountgroup_name> discount group.',
    'comment' => NULL,
    'translation' => '<%discountgroup_name> ディスカウント・グループを編集',
    'key' => 'cc77011c2c01fd82028f41f92ca34165',
  ),
  'efa856791097f45c85ebcf3343df7faa' => 
  array (
    'context' => 'design/admin/shop/discountgroup',
    'source' => 'There are no discount groups.',
    'comment' => NULL,
    'translation' => 'ディスカウント・グループがありません',
    'key' => 'efa856791097f45c85ebcf3343df7faa',
  ),
  'd74ef45494e84dfdde776621560c6f7c' => 
  array (
    'context' => 'design/admin/shop/discountgroup',
    'source' => 'Remove selected discount groups.',
    'comment' => NULL,
    'translation' => '選択したディスカウント・グループの削除',
    'key' => 'd74ef45494e84dfdde776621560c6f7c',
  ),
  '1f867c8251fc4245e18a03346b524592' => 
  array (
    'context' => 'design/admin/shop/discountgroup',
    'source' => 'Create a new discount group.',
    'comment' => NULL,
    'translation' => '新規のディスカウント・グループの作成',
    'key' => '1f867c8251fc4245e18a03346b524592',
  ),
  '8567ebc8a3d1e9fef36a854fbc6bdfdd' => 
  array (
    'context' => 'design/admin/shop/discountgroup',
    'source' => 'Discount groups (%discount_groups)',
    'comment' => NULL,
    'translation' => 'ディスカウント・グループ (%discount_groups)',
    'key' => '8567ebc8a3d1e9fef36a854fbc6bdfdd',
  ),
);
?>
