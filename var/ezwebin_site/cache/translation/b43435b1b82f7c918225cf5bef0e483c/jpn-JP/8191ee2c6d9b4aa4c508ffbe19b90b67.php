<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/rss/edit_import',
);

$TranslationRoot = array (
  '4ba383c1dd79b5ebb6f9517af61cfc8d' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '4ba383c1dd79b5ebb6f9517af61cfc8d',
  ),
  '00ee525c340dcd0b0e1c6b0b71af25d9' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Source URL',
    'comment' => NULL,
    'translation' => 'ソースURL',
    'key' => '00ee525c340dcd0b0e1c6b0b71af25d9',
  ),
  '8a11e62a3c4198a5c97e32724ee9dacd' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Destination path',
    'comment' => NULL,
    'translation' => '配置パス',
    'key' => '8a11e62a3c4198a5c97e32724ee9dacd',
  ),
  '40e857710b736ea43ef32046bea13d71' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Browse',
    'comment' => NULL,
    'translation' => 'ブラウズ',
    'key' => '40e857710b736ea43ef32046bea13d71',
  ),
  '2e1af8dff65797f6dff5e049138f9690' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Imported objects will be owned by',
    'comment' => NULL,
    'translation' => 'インポートしたオブジェクトの所有者',
    'key' => '2e1af8dff65797f6dff5e049138f9690',
  ),
  '9f111adf7ff32a2c2e22c2581b5037cc' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Change user',
    'comment' => NULL,
    'translation' => 'ユーザの変更',
    'key' => '9f111adf7ff32a2c2e22c2581b5037cc',
  ),
  '1a7e3c4a95c2edee546244ef4389b1a8' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'コンテンツのクラス',
    'key' => '1a7e3c4a95c2edee546244ef4389b1a8',
  ),
  'b382f09f4c808392e463b63b76ae975f' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Set',
    'comment' => NULL,
    'translation' => '設定',
    'key' => 'b382f09f4c808392e463b63b76ae975f',
  ),
  '2a16609b557126f04406d69dcad35fb7' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Ignore',
    'comment' => NULL,
    'translation' => '無視する',
    'key' => '2a16609b557126f04406d69dcad35fb7',
  ),
  '66a38245d82396a8589f4a8faa3662fd' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Active',
    'comment' => NULL,
    'translation' => '有効',
    'key' => '66a38245d82396a8589f4a8faa3662fd',
  ),
  'cd9a8741c0e229f1c75a3ed564c803db' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => 'cd9a8741c0e229f1c75a3ed564c803db',
  ),
  'e6dd43250eee63dd75805dc5668bddd5' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => 'e6dd43250eee63dd75805dc5668bddd5',
  ),
  '4095530f87efde12d1b499d6b35a2a17' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Edit <%rss_import_name> [RSS Import]',
    'comment' => NULL,
    'translation' => '<%rss_import_name> [RSSインポート] の編集',
    'key' => '4095530f87efde12d1b499d6b35a2a17',
  ),
  'cbfa7d2c80c5316104eec9cf8a587d23' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Use this field to enter the source URL of the RSS feed to import.',
    'comment' => NULL,
    'translation' => 'インポートするRSSフィードのソースURLを入力します。',
    'key' => 'cbfa7d2c80c5316104eec9cf8a587d23',
  ),
  '5cfc0c7dfd75c2960eaf2a45e6dcd2f1' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Click this button to select the destination node where objects created by the import are located.',
    'comment' => NULL,
    'translation' => 'インポートにより生成されたオブジェクトの配置先ノードを選択します。',
    'key' => '5cfc0c7dfd75c2960eaf2a45e6dcd2f1',
  ),
  '83ed222599653f36e7d36c1bfa18f076' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Click this button to select the user who should own the objects created by the import.',
    'comment' => NULL,
    'translation' => 'インポートにより生成されたオブジェクトの所有者を選択します。',
    'key' => '83ed222599653f36e7d36c1bfa18f076',
  ),
  '05f6c70873bb01991d2ee3f3b4a9748d' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Use this checkbox to control if the RSS feed is active or not. An inactive feed will not be automatically updated.',
    'comment' => NULL,
    'translation' => 'RSSインポートの有効/無効を設定します。無効時は自動更新が行われません。',
    'key' => '05f6c70873bb01991d2ee3f3b4a9748d',
  ),
  '685d6025ab7923a7700bb2fd3a6733eb' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Apply the changes and return to the RSS overview.',
    'comment' => NULL,
    'translation' => '変更を適用しRSS一覧に戻ります。',
    'key' => '685d6025ab7923a7700bb2fd3a6733eb',
  ),
  'e21393bbff07aa8c3cabb63b8875dd6c' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Cancel the changes and return to the RSS overview.',
    'comment' => NULL,
    'translation' => '変更をキャンセルしRSS一覧に戻ります。',
    'key' => 'e21393bbff07aa8c3cabb63b8875dd6c',
  ),
  '204a71b082825f338381332d7650b406' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Update',
    'comment' => NULL,
    'translation' => '更新',
    'key' => '204a71b082825f338381332d7650b406',
  ),
  '2bfc985b61fb5f81b312627f610644fe' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'RSS Version',
    'comment' => NULL,
    'translation' => 'RSSバージョン',
    'key' => '2bfc985b61fb5f81b312627f610644fe',
  ),
  '0b02e98305eebacfed118cc84ab09045' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Use this drop-down menu to select the attribute that should bet set as information from the RSS stream.',
    'comment' => NULL,
    'translation' => 'ドロップダウンメニューより、RSSストリームからの情報として設定する属性を選択してください。',
    'key' => '0b02e98305eebacfed118cc84ab09045',
  ),
  '36a86cfceb2161f9f9e7a5efd5f33403' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Object attributes',
    'comment' => NULL,
    'translation' => 'オブジェクトの属性',
    'key' => '36a86cfceb2161f9f9e7a5efd5f33403',
  ),
  '18c8e98de98b9136bc85c0dfda9e4095' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Name of the RSS import. This name is used in the Administration Interface only, to distinguish the different imports from each other.',
    'comment' => NULL,
    'translation' => 'RSSエクスポートの名前。この名前は管理画面でそれぞれのRSSエクスポートを区別するために利用されます。',
    'key' => '18c8e98de98b9136bc85c0dfda9e4095',
  ),
  '4f33f5b7d3ae85e168ef984a8f077490' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Click this button to proceed and analyze the import feed.',
    'comment' => NULL,
    'translation' => 'インポートフィードを処理、分析するにはこのボタンをクリックしてください。',
    'key' => '4f33f5b7d3ae85e168ef984a8f077490',
  ),
  '3ccb2188aa2f35f7443d98d425c2be71' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Use this drop-down to select the type of object the import should create. Click the "Set" button to load the attribute types for the remaining fields.',
    'comment' => NULL,
    'translation' => 'このドロップダウンメニューを使って、インポートが作成するオブジェクトのタイプを選択してください。残りのフィールドに属性のタイプをロードするには”設定”ボタンをクリックしてください。',
    'key' => '3ccb2188aa2f35f7443d98d425c2be71',
  ),
  'daac5c8a5fdcb91cdce78d7b56d3faac' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Click this button to load the correct values into the drop-down fields below. Use the drop-down menu on the left to select the class.',
    'comment' => NULL,
    'translation' => '下記のドロップダウンフィールドに正しい値をロードするには、このボタンをクリックしてください。左のドロップダウンメニューを使って、クラスを選択してください。',
    'key' => 'daac5c8a5fdcb91cdce78d7b56d3faac',
  ),
  'b772bb35e0e6f21feec7cf38d3b3a959' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Class attributes',
    'comment' => NULL,
    'translation' => 'クラス属性',
    'key' => 'b772bb35e0e6f21feec7cf38d3b3a959',
  ),
  '944eb80862cb046359280b15d73c6c1d' => 
  array (
    'context' => 'design/admin/rss/edit_import',
    'source' => 'Name of the RSS import. This name is used in the Administration Interface only to distinguish the different imports from each other.',
    'comment' => NULL,
    'translation' => 'RSSエクスポートの名前。この名前は管理画面でそれぞれのRSSエクスポートを区別するために利用されます。',
    'key' => '944eb80862cb046359280b15d73c6c1d',
  ),
);
?>
