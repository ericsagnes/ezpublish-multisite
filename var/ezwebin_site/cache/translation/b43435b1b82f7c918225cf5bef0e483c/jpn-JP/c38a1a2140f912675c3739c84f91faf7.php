<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/user/register',
);

$TranslationRoot = array (
  '5f5b21848516df8cee392c67c4fb8e86' => 
  array (
    'context' => 'kernel/user/register',
    'source' => 'Registration info',
    'comment' => NULL,
    'translation' => '登録情報',
    'key' => '5f5b21848516df8cee392c67c4fb8e86',
  ),
  'fae3c1957cac1e39063539b7cfc51572' => 
  array (
    'context' => 'kernel/user/register',
    'source' => 'New user registered',
    'comment' => NULL,
    'translation' => '新規に登録されたユーザ',
    'key' => 'fae3c1957cac1e39063539b7cfc51572',
  ),
  'c6b35ea07c48954604f186ef530d66ce' => 
  array (
    'context' => 'kernel/user/register',
    'source' => 'User registration approved',
    'comment' => NULL,
    'translation' => 'ユーザの登録は承認されました',
    'key' => 'c6b35ea07c48954604f186ef530d66ce',
  ),
);
?>
