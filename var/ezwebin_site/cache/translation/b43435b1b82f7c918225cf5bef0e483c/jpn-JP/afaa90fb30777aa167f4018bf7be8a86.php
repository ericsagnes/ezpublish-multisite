<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/search',
);

$TranslationRoot = array (
  'eb4b6f2b88c733c49fe1cbd9ff8701e3' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Update attributes',
    'comment' => NULL,
    'translation' => '属性の更新',
    'key' => 'eb4b6f2b88c733c49fe1cbd9ff8701e3',
  ),
  '3bc3503aad2ff96f8dd9f284bc4452a4' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Search',
    'comment' => NULL,
    'translation' => '検索',
    'key' => '3bc3503aad2ff96f8dd9f284bc4452a4',
  ),
  '810b4d6326c7d21f3cb4a40b7970fe81' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'All content',
    'comment' => NULL,
    'translation' => 'すべてのコンテンツ',
    'key' => '810b4d6326c7d21f3cb4a40b7970fe81',
  ),
  'd7368580460bd097121e958cc0af0c03' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'The same location',
    'comment' => NULL,
    'translation' => '同じ配置先',
    'key' => 'd7368580460bd097121e958cc0af0c03',
  ),
  'ad8e5791380f27bb2bbf78381262cc64' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'For more options try the %1fd search%2.',
    'comment' => 'The parameters are link start and end tags.',
    'translation' => 'その他のオプションは %1検索オプション%2を試して見て下さい。',
    'key' => 'ad8e5791380f27bb2bbf78381262cc64',
  ),
  '99ef8c15461fc0e75ab37b414132b3e6' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'No results were found while searching for <%1>',
    'comment' => NULL,
    'translation' => '<%1> に該当する検索結果はありません',
    'key' => '99ef8c15461fc0e75ab37b414132b3e6',
  ),
  'b4f33a484b6db949482fe36b5fbab441' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Search tips',
    'comment' => NULL,
    'translation' => '検索のコツ',
    'key' => 'b4f33a484b6db949482fe36b5fbab441',
  ),
  'a8382153c77163361cab6aafb9072750' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Check spelling of keywords.',
    'comment' => NULL,
    'translation' => 'キーワードを確認して下さい。',
    'key' => 'a8382153c77163361cab6aafb9072750',
  ),
  '28d33e7ae00ba6f6576c257d3ac1d665' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Try more general keywords.',
    'comment' => NULL,
    'translation' => '一般的なキーワードを試して下さい。',
    'key' => '28d33e7ae00ba6f6576c257d3ac1d665',
  ),
  '82eacea7c57bdd87285550cef8f9919b' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Search for <%1> returned %2 matches',
    'comment' => NULL,
    'translation' => '<%1> の検索結果は %2 件です',
    'key' => '82eacea7c57bdd87285550cef8f9919b',
  ),
  '4a3b2e21802ebfcb28bca3018f7bda35' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '4a3b2e21802ebfcb28bca3018f7bda35',
  ),
  'c61fb578016e818d78e551a8321e7c45' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Type',
    'comment' => NULL,
    'translation' => 'タイプ',
    'key' => 'c61fb578016e818d78e551a8321e7c45',
  ),
  '53d6d8f9c6c099511607e875581b8ed4' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Advanced search',
    'comment' => NULL,
    'translation' => '検索オプション',
    'key' => '53d6d8f9c6c099511607e875581b8ed4',
  ),
  '702d3ee0c8abf808e94ef486ec9d9947' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Search for all of the following words',
    'comment' => NULL,
    'translation' => '検索に含めるワード',
    'key' => '702d3ee0c8abf808e94ef486ec9d9947',
  ),
  '17545805693e85afa1968961e332f2c2' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Search for an exact phrase',
    'comment' => NULL,
    'translation' => '一致する句',
    'key' => '17545805693e85afa1968961e332f2c2',
  ),
  '88517dc42729e833b20865e9c0223cac' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'コンテンツのクラス',
    'key' => '88517dc42729e833b20865e9c0223cac',
  ),
  'ca313e022da10857c5a6ee36a89c9ae9' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Any class',
    'comment' => NULL,
    'translation' => '指定なし',
    'key' => 'ca313e022da10857c5a6ee36a89c9ae9',
  ),
  '4d267e429c97a3e63b13531afd99f5c6' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Class attribute',
    'comment' => NULL,
    'translation' => 'クラスの属性',
    'key' => '4d267e429c97a3e63b13531afd99f5c6',
  ),
  '91802f346890d9631b0bd3228b03a870' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Any attribute',
    'comment' => NULL,
    'translation' => '指定なし',
    'key' => '91802f346890d9631b0bd3228b03a870',
  ),
  '23bb3298438bea10d85d8ef3f1039bcd' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'In',
    'comment' => NULL,
    'translation' => '対象セクション',
    'key' => '23bb3298438bea10d85d8ef3f1039bcd',
  ),
  'aced05d3922c361df338689dff38fc56' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Any section',
    'comment' => NULL,
    'translation' => '指定なし',
    'key' => 'aced05d3922c361df338689dff38fc56',
  ),
  'e57e402f711b0ee55da6b43aa63406d2' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Published',
    'comment' => NULL,
    'translation' => '公開日',
    'key' => 'e57e402f711b0ee55da6b43aa63406d2',
  ),
  '5392bdb339af8937e1a3e99b008e6334' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Any time',
    'comment' => NULL,
    'translation' => '指定なし',
    'key' => '5392bdb339af8937e1a3e99b008e6334',
  ),
  '34c93dc14241fdaee707aa47df07d611' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Last day',
    'comment' => NULL,
    'translation' => '昨日',
    'key' => '34c93dc14241fdaee707aa47df07d611',
  ),
  '384047efaecb571fa89233a10fdd1dad' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Last week',
    'comment' => NULL,
    'translation' => '過去一週間',
    'key' => '384047efaecb571fa89233a10fdd1dad',
  ),
  '91837e111034fd9004c5619c29292c9e' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Last month',
    'comment' => NULL,
    'translation' => '過去一ヶ月',
    'key' => '91837e111034fd9004c5619c29292c9e',
  ),
  '13b0bcc5ac018abdad6928013f22f74e' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Last three months',
    'comment' => NULL,
    'translation' => '過去三ヶ月',
    'key' => '13b0bcc5ac018abdad6928013f22f74e',
  ),
  '3409e6c64ec0d5d71af1d667bc8e9fc9' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Last year',
    'comment' => NULL,
    'translation' => '過去一年',
    'key' => '3409e6c64ec0d5d71af1d667bc8e9fc9',
  ),
  'ab7de7729e5af092efd5a3b24c09525a' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Display per page',
    'comment' => NULL,
    'translation' => '過去一年',
    'key' => 'ab7de7729e5af092efd5a3b24c09525a',
  ),
  '946a5ce92263c1369f1dbb7534a0515f' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => '5 items',
    'comment' => NULL,
    'translation' => '5件',
    'key' => '946a5ce92263c1369f1dbb7534a0515f',
  ),
  'e514604dc46faff361dfedf3df38cb90' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => '10 items',
    'comment' => NULL,
    'translation' => '10件',
    'key' => 'e514604dc46faff361dfedf3df38cb90',
  ),
  'fa196478cd9d7f69e09f9e673ac819d4' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => '20 items',
    'comment' => NULL,
    'translation' => '20件',
    'key' => 'fa196478cd9d7f69e09f9e673ac819d4',
  ),
  'dcd5adab10788f658016526e4f9063ee' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => '30 items',
    'comment' => NULL,
    'translation' => '30件',
    'key' => 'dcd5adab10788f658016526e4f9063ee',
  ),
  'd5c5145949a486815c8b025a2ded4fca' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => '50 items',
    'comment' => NULL,
    'translation' => '50件',
    'key' => 'd5c5145949a486815c8b025a2ded4fca',
  ),
  '6f365dc5d6d24403e529dcbd11176993' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'No results were found when searching for <%1>',
    'comment' => NULL,
    'translation' => '"%1" に該当する検索結果はありません',
    'key' => '6f365dc5d6d24403e529dcbd11176993',
  ),
  '26253e0a1ad10f739914e555e2aa1393' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'The following words were excluded from the search',
    'comment' => NULL,
    'translation' => '以下の単語は検索対象とはなりません',
    'key' => '26253e0a1ad10f739914e555e2aa1393',
  ),
  '500133059713e6b39ff7bf03275fed4e' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Try changing some keywords e.g. &quot;car&quot; instead of &quot;cars&quot;.',
    'comment' => NULL,
    'translation' => 'キーワードを変更してみてください。例えば:&quot;car&quot;の代わりに&quot;cars&quot;',
    'key' => '500133059713e6b39ff7bf03275fed4e',
  ),
  '03066b4eb33e70f8283d9ec495295979' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'Fewer keywords result in more matches. Try reducing keywords until you get a result.',
    'comment' => NULL,
    'translation' => 'キーワードを少なくすると、検索結果が増えます。検索結果が出るまでキーワードを少なくしてみて下さい。',
    'key' => '03066b4eb33e70f8283d9ec495295979',
  ),
  'c89db33a1cb0d7dc13e2374272d16de0' => 
  array (
    'context' => 'design/admin/content/search',
    'source' => 'For more options try the %1Advanced search%2.',
    'comment' => 'The parameters are link start and end tags.',
    'translation' => 'その他のオプションは %1検索オプション%2を試してみてください。',
    'key' => 'c89db33a1cb0d7dc13e2374272d16de0',
  ),
);
?>
