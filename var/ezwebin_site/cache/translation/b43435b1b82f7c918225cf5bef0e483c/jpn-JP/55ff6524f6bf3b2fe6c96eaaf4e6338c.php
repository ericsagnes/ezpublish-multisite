<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/state/edit',
);

$TranslationRoot = array (
  '8a38bfa07e563975db7fa99b57339c56' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'The content object state was successfully stored.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクトステートの保存に成功しました。',
    'key' => '8a38bfa07e563975db7fa99b57339c56',
  ),
  'e522d8a082e5e58e9c8da9b9f2cdd2fb' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'The content object state could not be stored.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクトステートを保存出来ませんでした。',
    'key' => 'e522d8a082e5e58e9c8da9b9f2cdd2fb',
  ),
  '520c4746feb1e13da54acf3c03c6b4d8' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'Required data is either missing or is invalid',
    'comment' => NULL,
    'translation' => '入力必須項目が未入力または不正です',
    'key' => '520c4746feb1e13da54acf3c03c6b4d8',
  ),
  'f326a56450c3bf774ec8c103d724694b' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'Edit content object state "%state_name"',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクトステート"%state_name"を編集する',
    'key' => 'f326a56450c3bf774ec8c103d724694b',
  ),
  '961270c749603b04f18f69e520990cf4' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'New content object state',
    'comment' => NULL,
    'translation' => '新規コンテンツオブジェクトステート',
    'key' => '961270c749603b04f18f69e520990cf4',
  ),
  'e07fc16a5d109947f37e7071a820e567' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'Save changes',
    'comment' => NULL,
    'translation' => '変更の保存',
    'key' => 'e07fc16a5d109947f37e7071a820e567',
  ),
  '3400e24e0ce3069ac2a2bc75ff96ddb4' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'Save changes to this states.',
    'comment' => NULL,
    'translation' => 'このステートへの変更を保存する。',
    'key' => '3400e24e0ce3069ac2a2bc75ff96ddb4',
  ),
  '6c3977fdffe2139ed6b7b94a71eeeaef' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '6c3977fdffe2139ed6b7b94a71eeeaef',
  ),
  '66097196460583a9a1070bd34de54fc4' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'Cancel saving any changes.',
    'comment' => NULL,
    'translation' => '変更の保存をキャンセルする。',
    'key' => '66097196460583a9a1070bd34de54fc4',
  ),
  '580512154e2b29b20de0b9932e55af35' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'Identifier:',
    'comment' => NULL,
    'translation' => '識別子:',
    'key' => '580512154e2b29b20de0b9932e55af35',
  ),
  '4a91b12be5ead5b93641b82e5d41253b' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'Default language:',
    'comment' => NULL,
    'translation' => 'デフォルト言語:',
    'key' => '4a91b12be5ead5b93641b82e5d41253b',
  ),
  'c26e7f5789396faf37b52f7b1e18c757' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'Name:',
    'comment' => NULL,
    'translation' => 'ステート名:',
    'key' => 'c26e7f5789396faf37b52f7b1e18c757',
  ),
  'e11c748be6f79fbf094c395e8902baaa' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'Description:',
    'comment' => NULL,
    'translation' => '概要:',
    'key' => 'e11c748be6f79fbf094c395e8902baaa',
  ),
  'e136fa10523f012ce487c59f43648bbf' => 
  array (
    'context' => 'design/admin/state/edit',
    'source' => 'Save changes to this state.',
    'comment' => NULL,
    'translation' => 'このステートの変更を保存します。',
    'key' => 'e136fa10523f012ce487c59f43648bbf',
  ),
);
?>
