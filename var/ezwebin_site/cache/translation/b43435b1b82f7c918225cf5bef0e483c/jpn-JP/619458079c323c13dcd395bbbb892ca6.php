<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/rss/edit',
);

$TranslationRoot = array (
  '895238a38e0825e8a5c3702a59ef1113' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Display frontpage',
    'comment' => NULL,
    'translation' => 'フロントページを表示する',
    'key' => '895238a38e0825e8a5c3702a59ef1113',
  ),
  'cc52f26a994e683743276cd63c0ac48d' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'RSS Export',
    'comment' => NULL,
    'translation' => 'RSSエクスポート',
    'key' => 'cc52f26a994e683743276cd63c0ac48d',
  ),
  'e8afedf0a4c6d5a7c77d5e6fce655947' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Title',
    'comment' => NULL,
    'translation' => 'タイトル',
    'key' => 'e8afedf0a4c6d5a7c77d5e6fce655947',
  ),
  '6c37d1a7e3fc1d585cc7d224e2e54487' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Description',
    'comment' => NULL,
    'translation' => '説明',
    'key' => '6c37d1a7e3fc1d585cc7d224e2e54487',
  ),
  '7d8c5f511874fd46b5101e28bc39e588' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Site URL',
    'comment' => NULL,
    'translation' => 'サイトURL',
    'key' => '7d8c5f511874fd46b5101e28bc39e588',
  ),
  '47beb64de79bfe1c4fcfa55f9151a879' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Image',
    'comment' => NULL,
    'translation' => '画像',
    'key' => '47beb64de79bfe1c4fcfa55f9151a879',
  ),
  'fc5af16cdbc334c1c9be50f17592f0fd' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Browse',
    'comment' => NULL,
    'translation' => 'ブラウズ',
    'key' => 'fc5af16cdbc334c1c9be50f17592f0fd',
  ),
  '84f59978c61449e8352a779208fcfaa4' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Site Access',
    'comment' => NULL,
    'translation' => 'サイトアクセス',
    'key' => '84f59978c61449e8352a779208fcfaa4',
  ),
  '370e8ded4dcbf98ea2252341ab855c0d' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'RSS version',
    'comment' => NULL,
    'translation' => 'RSSバージョン',
    'key' => '370e8ded4dcbf98ea2252341ab855c0d',
  ),
  '0bce684fbbf2b19cdbb7b3b3cb18e8cc' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Active',
    'comment' => NULL,
    'translation' => '有効',
    'key' => '0bce684fbbf2b19cdbb7b3b3cb18e8cc',
  ),
  '725f299f22d0d6bd6c2874c8971227b7' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Access URL',
    'comment' => NULL,
    'translation' => 'アクセスURL',
    'key' => '725f299f22d0d6bd6c2874c8971227b7',
  ),
  'ea8ae2a74152f530ff6467bdb9ee82e4' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Source path',
    'comment' => NULL,
    'translation' => 'ソースのパス',
    'key' => 'ea8ae2a74152f530ff6467bdb9ee82e4',
  ),
  '7027cc14ac1b4804ec04b14843c33bf8' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => '7027cc14ac1b4804ec04b14843c33bf8',
  ),
  '37420c1ce3bc6784bee1ee1c01498be0' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Update',
    'comment' => NULL,
    'translation' => '更新',
    'key' => '37420c1ce3bc6784bee1ee1c01498be0',
  ),
  '85bc99472f7970516ee5fc30a88ae925' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Store',
    'comment' => NULL,
    'translation' => '保存',
    'key' => '85bc99472f7970516ee5fc30a88ae925',
  ),
  'df2018c16157e5e976d5cc4a5115ad11' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Add Source',
    'comment' => NULL,
    'translation' => 'ソースの追加',
    'key' => 'df2018c16157e5e976d5cc4a5115ad11',
  ),
  '2c5ffedc6798d8dd578c175d890ad269' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => '2c5ffedc6798d8dd578c175d890ad269',
  ),
  '7e840a9b4ff96bf690e9ae0cad2ede26' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Remove Source',
    'comment' => NULL,
    'translation' => 'ソースを削除',
    'key' => '7e840a9b4ff96bf690e9ae0cad2ede26',
  ),
  'f93427c1e171381307e6d2072c4fd56c' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'PDF Export',
    'comment' => NULL,
    'translation' => 'PDF出力',
    'key' => 'f93427c1e171381307e6d2072c4fd56c',
  ),
  'b9caa2b49f286d814d6ea2ebfcf11a57' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Intro text',
    'comment' => NULL,
    'translation' => '紹介テキスト',
    'key' => 'b9caa2b49f286d814d6ea2ebfcf11a57',
  ),
  '24a1930df5036fedaa30f6801619c545' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Sub text',
    'comment' => NULL,
    'translation' => 'サブテキスト',
    'key' => '24a1930df5036fedaa30f6801619c545',
  ),
  '5401a107983e7e6569a245afac094879' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Source node',
    'comment' => NULL,
    'translation' => 'ソースノード',
    'key' => '5401a107983e7e6569a245afac094879',
  ),
  '8819cd7da85c7fbffda21c4caef17636' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Export structure',
    'comment' => NULL,
    'translation' => '出力範囲',
    'key' => '8819cd7da85c7fbffda21c4caef17636',
  ),
  '93e39c958cba92a66d72075f228e6b32' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Tree',
    'comment' => NULL,
    'translation' => 'ツリー',
    'key' => '93e39c958cba92a66d72075f228e6b32',
  ),
  '939b438b3646968c42bf22c9f2370b59' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Node',
    'comment' => NULL,
    'translation' => 'ノード',
    'key' => '939b438b3646968c42bf22c9f2370b59',
  ),
  '7b3d36d74b023b1ce0c39ae9ce55bc29' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Export classes',
    'comment' => NULL,
    'translation' => 'クラスのエクスポート',
    'key' => '7b3d36d74b023b1ce0c39ae9ce55bc29',
  ),
  '51f85857dba5969807d005a06b342774' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Export destination',
    'comment' => NULL,
    'translation' => 'エクスポート先',
    'key' => '51f85857dba5969807d005a06b342774',
  ),
  '0032caa38b8f9442a515241707baab5f' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Export to URL',
    'comment' => NULL,
    'translation' => 'エクスポート先',
    'key' => '0032caa38b8f9442a515241707baab5f',
  ),
  'dff870f54881e28299bc89c2ee9e1c8e' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Export for direct download',
    'comment' => NULL,
    'translation' => '直接ダウンロード用のエクスポート',
    'key' => 'dff870f54881e28299bc89c2ee9e1c8e',
  ),
  'af1b244e9ccc533efba783a2238cfd9c' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Export',
    'comment' => NULL,
    'translation' => 'エクスポート',
    'key' => 'af1b244e9ccc533efba783a2238cfd9c',
  ),
  'bae917e32add09864cf1b28e91c6e32e' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Number of objects',
    'comment' => NULL,
    'translation' => 'オブジェクト数',
    'key' => 'bae917e32add09864cf1b28e91c6e32e',
  ),
  '6440f61ee0009a8b6509eb783aa38395' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Main node only',
    'comment' => NULL,
    'translation' => '主要ノードのみ',
    'key' => '6440f61ee0009a8b6509eb783aa38395',
  ),
  '94093282e26edb7cc01e6aa7c7763aa5' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Check if you want to only feed the object from the main node.',
    'comment' => NULL,
    'translation' => '主要ノードからのフィードのみを有効にする場合はチェックします。',
    'key' => '94093282e26edb7cc01e6aa7c7763aa5',
  ),
  'a295327bd2f06ef4fd63827ddecb3fd5' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Subnodes',
    'comment' => NULL,
    'translation' => '子アイテム',
    'key' => 'a295327bd2f06ef4fd63827ddecb3fd5',
  ),
  '985054f5ab468bac3dc9898ac4045a10' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Use this field to enter the base URL of your site. It is used to produce the URLs in the export, composed by the Site URL (e.g. "http://www.example.com/index.php") and the path to the object (e.g. "/articles/my_article"). The Site URL depends on your Web server and eZ Publish configuration.',
    'comment' => NULL,
    'translation' => 'このフィールドにサイトのベースURLを入力してください。サイトURL(e.g. "http://www.example.com/index.php")とオブジェクトのパス(e.g. "/articles/my_article")を使ってエクスポートURLの作成を行います。サイトURLはサーバとeZ Publishの設定次第です。',
    'key' => '985054f5ab468bac3dc9898ac4045a10',
  ),
  '11fb39b0507b710c91660a752c4603da' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Use this drop-down to select the maximum number of objects included in the RSS feed.',
    'comment' => NULL,
    'translation' => 'このドロップダウンメニューを使って、RSSフィードに含めるオブジェクトの最大数を設定してください。',
    'key' => '11fb39b0507b710c91660a752c4603da',
  ),
  'fd0e8de077c1491141720099ffdf53f7' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Activate this checkbox if objects from the subnodes of the source should also be fed.',
    'comment' => NULL,
    'translation' => 'ソースの子ノードをフィードしたい場合はこのチェックボックスをチェックして下さい。',
    'key' => 'fd0e8de077c1491141720099ffdf53f7',
  ),
  '00a4c809c069413ba690d80a1835a833' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Category',
    'comment' => NULL,
    'translation' => 'カテゴリ',
    'key' => '00a4c809c069413ba690d80a1835a833',
  ),
  '168db9a887e5aebbca1a9c65cdc15348' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'optional',
    'comment' => NULL,
    'translation' => 'オプション',
    'key' => '168db9a887e5aebbca1a9c65cdc15348',
  ),
  '2a694f61b629503b86608ae35c10d9b1' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Skip',
    'comment' => NULL,
    'translation' => 'スキップする',
    'key' => '2a694f61b629503b86608ae35c10d9b1',
  ),
  'b1d2a49a9efa2794225dc51cc7940c57' => 
  array (
    'context' => 'design/standard/rss/edit',
    'source' => 'Enclosure (media)',
    'comment' => NULL,
    'translation' => 'メディアのダイレクトリンク',
    'key' => 'b1d2a49a9efa2794225dc51cc7940c57',
  ),
);
?>
