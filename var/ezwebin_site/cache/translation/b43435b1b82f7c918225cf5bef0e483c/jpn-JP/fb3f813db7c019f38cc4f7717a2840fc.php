<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/content/edit_draft',
);

$TranslationRoot = array (
  '1475d72b51e1c6dc4c20cdaf9b0c170f' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'The currently published version is %version and was published at %time.',
    'comment' => NULL,
    'translation' => '現在公開中のバージョンは%versionで、%timeに公開されています。',
    'key' => '1475d72b51e1c6dc4c20cdaf9b0c170f',
  ),
  'c77924b685f5d6f53af79ea10abc2ca1' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'The last modification was done at %modified.',
    'comment' => NULL,
    'translation' => '最新の修正日時は%modifiedです。',
    'key' => 'c77924b685f5d6f53af79ea10abc2ca1',
  ),
  '2f43bc375975a94febb1731abe0e3061' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'The object is owned by %owner.',
    'comment' => NULL,
    'translation' => 'オブジェクトは ユーザ %owner の所有です。',
    'key' => '2f43bc375975a94febb1731abe0e3061',
  ),
  '7a0db05b07a07e96e4b67cfdee1e08a5' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'Current drafts',
    'comment' => NULL,
    'translation' => '現在のドラフト',
    'key' => '7a0db05b07a07e96e4b67cfdee1e08a5',
  ),
  '4b70e16f5863d4ab0eb75f43f29331ca' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => '4b70e16f5863d4ab0eb75f43f29331ca',
  ),
  '17479aba0a1d37b78e6b71398f0b4369' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '17479aba0a1d37b78e6b71398f0b4369',
  ),
  'dd39bb7c5b08ae50d3fd3eb2f7bc0300' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'Owner',
    'comment' => NULL,
    'translation' => '所有者',
    'key' => 'dd39bb7c5b08ae50d3fd3eb2f7bc0300',
  ),
  '32faac531431e98a3fd53fb5492548e4' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'Created',
    'comment' => NULL,
    'translation' => '作成日時',
    'key' => '32faac531431e98a3fd53fb5492548e4',
  ),
  '3789f8aa166b092fda69d1fd1e3482d5' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'Last modified',
    'comment' => NULL,
    'translation' => '最新の修正',
    'key' => '3789f8aa166b092fda69d1fd1e3482d5',
  ),
  '29855ddb5c31c23ad21cbc0751353696' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => '29855ddb5c31c23ad21cbc0751353696',
  ),
  '443117030e5da75e533968f58a6e9c56' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'New draft',
    'comment' => NULL,
    'translation' => '新規ドラフト',
    'key' => '443117030e5da75e533968f58a6e9c56',
  ),
  '1424b65fc2d1103382d486e178efac23' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'This object is already being edited by yourself and others.
    You can either continue editing one of your drafts or you can create a new draft.',
    'comment' => NULL,
    'translation' => 'このオブジェクトはすでに編集されています
ドラフトを編集するか、新しいドラフトを作ることができます。',
    'key' => '1424b65fc2d1103382d486e178efac23',
  ),
  'a3beaf281acf39f844ce3de2fddd1b82' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'This object is already being edited by you.
        You can either continue editing one of your drafts or you can create a new draft.',
    'comment' => NULL,
    'translation' => 'このオブジェクトをすでに編集しています。
ドラフトを編集するか、新しいドラフトを作ることができます。',
    'key' => 'a3beaf281acf39f844ce3de2fddd1b82',
  ),
  '11d1b15ff893a1e4b74a443000ad0e27' => 
  array (
    'context' => 'design/ezwebin/content/edit_draft',
    'source' => 'This object is already being edited by someone else.
        You should either contact the person about their draft or create a new draft for your own use.',
    'comment' => NULL,
    'translation' => '他人がすでにこのオブジェクトを編集しています。
編集者に連絡をするか、新しいドラフトを作成することができます。',
    'key' => '11d1b15ff893a1e4b74a443000ad0e27',
  ),
);
?>
