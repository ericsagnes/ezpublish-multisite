<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/shop/orderstatistics',
);

$TranslationRoot = array (
  '59603ef76ef862f6088132b3f0018bd9' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'Product statistics [%count]',
    'comment' => NULL,
    'translation' => '注文商品の集計 [%count]',
    'key' => '59603ef76ef862f6088132b3f0018bd9',
  ),
  'bd020c06a1de84d2f079ed6b7d17f6bd' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'Product',
    'comment' => NULL,
    'translation' => '商品',
    'key' => 'bd020c06a1de84d2f079ed6b7d17f6bd',
  ),
  'c2d7f1adc7513763717cbbd25bd100be' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'Quantity',
    'comment' => NULL,
    'translation' => '数量',
    'key' => 'c2d7f1adc7513763717cbbd25bd100be',
  ),
  '8e001e7bf8c77a94f65eec8039f45b7f' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'Total (ex. VAT)',
    'comment' => NULL,
    'translation' => '売上合計（税抜）',
    'key' => '8e001e7bf8c77a94f65eec8039f45b7f',
  ),
  'a8d89ff295ecb4f39dd175ea0f495548' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'Total (inc. VAT)',
    'comment' => NULL,
    'translation' => '売上合計（税込）',
    'key' => 'a8d89ff295ecb4f39dd175ea0f495548',
  ),
  '96d69bba0b2d2350b266833a337a070c' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'All years',
    'comment' => NULL,
    'translation' => '年の指定なし',
    'key' => '96d69bba0b2d2350b266833a337a070c',
  ),
  'bd40f5ede7e9834bad29f9c9bafc2d13' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'All months',
    'comment' => NULL,
    'translation' => '月の指定なし',
    'key' => 'bd40f5ede7e9834bad29f9c9bafc2d13',
  ),
  '77595d8d271af9ae8bf05675dfd8f047' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'Show',
    'comment' => NULL,
    'translation' => '集計',
    'key' => '77595d8d271af9ae8bf05675dfd8f047',
  ),
  'b769e87590481b5223314a34f69f0761' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'SUM',
    'comment' => NULL,
    'translation' => '合計',
    'key' => 'b769e87590481b5223314a34f69f0761',
  ),
  'd9e657c001caef2ffd3f6c5365fd3694' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'The list is empty.',
    'comment' => NULL,
    'translation' => 'リストは空です。',
    'key' => 'd9e657c001caef2ffd3f6c5365fd3694',
  ),
  'f3e013c44277d850e463f2daf6012e5a' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'Select the year for which you want to view statistics.',
    'comment' => NULL,
    'translation' => '統計を見たい年を選択してください。',
    'key' => 'f3e013c44277d850e463f2daf6012e5a',
  ),
  'c40b2bb9ef5bf413a02090ddb7e79fbe' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'Select the month for which you want to view statistics.',
    'comment' => NULL,
    'translation' => '統計を見たい月を選択してください。',
    'key' => 'c40b2bb9ef5bf413a02090ddb7e79fbe',
  ),
  'bf0371e7c201c67195b96daf9cd1a2a9' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'Update the list using the values specified by the menus to the left.',
    'comment' => NULL,
    'translation' => '左のメニューで表示される値を使って、リストを更新してください。',
    'key' => 'bf0371e7c201c67195b96daf9cd1a2a9',
  ),
  '10b19e56c95e8f997c4ea1993690d552' => 
  array (
    'context' => 'design/admin/shop/orderstatistics',
    'source' => 'Product statistics (%count)',
    'comment' => NULL,
    'translation' => '注文商品の集計 (%count)',
    'key' => '10b19e56c95e8f997c4ea1993690d552',
  ),
);
?>
