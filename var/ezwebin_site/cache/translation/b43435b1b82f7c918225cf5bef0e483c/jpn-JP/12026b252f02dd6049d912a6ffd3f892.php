<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/class/edit_locked',
);

$TranslationRoot = array (
  '4f7f3a1055b014d84a041362f86e1a38' => 
  array (
    'context' => 'design/standard/class/edit_locked',
    'source' => 'Class locked',
    'comment' => NULL,
    'translation' => 'クラスはロックされています',
    'key' => '4f7f3a1055b014d84a041362f86e1a38',
  ),
  '8efcad0835d9183395395aa0d2b90f90' => 
  array (
    'context' => 'design/standard/class/edit_locked',
    'source' => 'This class has pending modifications defered to cronjob and thus it cannot be edited.',
    'comment' => NULL,
    'translation' => 'このクラスにペンディングな変更がありますので、編集ができません。',
    'key' => '8efcad0835d9183395395aa0d2b90f90',
  ),
  'a59201d78d8d8e000c633dc056cfd53f' => 
  array (
    'context' => 'design/standard/class/edit_locked',
    'source' => 'Wait until the script is finished. You might see the status in the %urlstart script monitor%urlend</a>.',
    'comment' => NULL,
    'translation' => 'スクリプトが完了するまでに待ってください。%urlstartスクリプトモニター%urlendでステータスを見る事ができます。',
    'key' => 'a59201d78d8d8e000c633dc056cfd53f',
  ),
  '9c312400c7409f419ddc2b1193795de6' => 
  array (
    'context' => 'design/standard/class/edit_locked',
    'source' => 'To force the modification of the class you may run the following command',
    'comment' => NULL,
    'translation' => 'クラスの変更を強制で行いたい場合は下記のコマンドを実行してください',
    'key' => '9c312400c7409f419ddc2b1193795de6',
  ),
  'f7638290549037cbf6364d175e2dcd8d' => 
  array (
    'context' => 'design/standard/class/edit_locked',
    'source' => 'Edit <%class_name> [Class]',
    'comment' => NULL,
    'translation' => '<%class_name> [クラス] の編集',
    'key' => 'f7638290549037cbf6364d175e2dcd8d',
  ),
  'af6a32a4deee497c5e9464b4443e3b10' => 
  array (
    'context' => 'design/standard/class/edit_locked',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => 'af6a32a4deee497c5e9464b4443e3b10',
  ),
  '81d38a7cf4af687f723aaf1ac786affa' => 
  array (
    'context' => 'design/standard/class/edit_locked',
    'source' => 'Last modifier',
    'comment' => NULL,
    'translation' => '最終編集者',
    'key' => '81d38a7cf4af687f723aaf1ac786affa',
  ),
  'a36b032eeab824e766b90581d8c664d3' => 
  array (
    'context' => 'design/standard/class/edit_locked',
    'source' => 'Last modified on',
    'comment' => NULL,
    'translation' => '最終変更日時',
    'key' => 'a36b032eeab824e766b90581d8c664d3',
  ),
  '7c41d35c3ae63ff1e897070f2d0bdbc7' => 
  array (
    'context' => 'design/standard/class/edit_locked',
    'source' => 'The class will be available for editing after the script has been run by the cronjob.',
    'comment' => NULL,
    'translation' => 'クロンジョブスクリプトが実行されて後にクラスが編集できます。',
    'key' => '7c41d35c3ae63ff1e897070f2d0bdbc7',
  ),
  'b8e9a22ab86373be2f909d5f94b63288' => 
  array (
    'context' => 'design/standard/class/edit_locked',
    'source' => 'Retry',
    'comment' => NULL,
    'translation' => '再試行',
    'key' => 'b8e9a22ab86373be2f909d5f94b63288',
  ),
  '22c517649133c59fbf2f5e9b252de2e2' => 
  array (
    'context' => 'design/standard/class/edit_locked',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '22c517649133c59fbf2f5e9b252de2e2',
  ),
);
?>
