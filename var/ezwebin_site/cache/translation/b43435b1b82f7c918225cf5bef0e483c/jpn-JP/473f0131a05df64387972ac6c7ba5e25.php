<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/contentstructuremenu',
);

$TranslationRoot = array (
  '177c33091f10a4a29f5954b27a5b4ac9' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Fold/Unfold',
    'comment' => NULL,
    'translation' => '開く/たたむ',
    'key' => '177c33091f10a4a29f5954b27a5b4ac9',
  ),
  'cc8859f50df3888af717e53619ed9b9e' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Click on the icon to display a context-sensitive menu.',
    'comment' => NULL,
    'translation' => 'コンテキストメニューを表示するにはアイコンをクリックしてください。',
    'key' => 'cc8859f50df3888af717e53619ed9b9e',
  ),
  '29c39b11657dc660768c9aba6c0e8069' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Node ID',
    'comment' => NULL,
    'translation' => 'ノードID',
    'key' => '29c39b11657dc660768c9aba6c0e8069',
  ),
  'bb45a4f5c94125f0d20910e4213e3e2a' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Visibility',
    'comment' => NULL,
    'translation' => '表示設定',
    'key' => 'bb45a4f5c94125f0d20910e4213e3e2a',
  ),
  'e96bfea7470021418e4965d62d08eed0' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Hidden',
    'comment' => NULL,
    'translation' => '非表示',
    'key' => 'e96bfea7470021418e4965d62d08eed0',
  ),
  'ba506b41acf6fbf90d9454d60737dabf' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Hidden by superior',
    'comment' => NULL,
    'translation' => '権限を持つユーザによる非表示',
    'key' => 'ba506b41acf6fbf90d9454d60737dabf',
  ),
  '3b2aecc7a3b7881d97a17cf7e478f8d8' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Visible',
    'comment' => NULL,
    'translation' => '表示',
    'key' => '3b2aecc7a3b7881d97a17cf7e478f8d8',
  ),
  '658e7cb7b858bf859a5a3563cbbed1e9' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Dynamic tree not allowed for this siteaccess',
    'comment' => NULL,
    'translation' => 'このサイトアクセスではダイナミックツリーを利用出来ません。',
    'key' => '658e7cb7b858bf859a5a3563cbbed1e9',
  ),
  '216f4db89360296659a8dbb37d2f4560' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Node does not exist',
    'comment' => NULL,
    'translation' => 'ノードが存在しません',
    'key' => '216f4db89360296659a8dbb37d2f4560',
  ),
  'd6176e7c24ecacccd58f2ffd7fb1ce93' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Internal error',
    'comment' => NULL,
    'translation' => '内部エラー',
    'key' => 'd6176e7c24ecacccd58f2ffd7fb1ce93',
  ),
  '675c44a4485051ee55aa0eea1e55bee0' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => '[%classname] Click on the icon to display a context-sensitive menu.',
    'comment' => NULL,
    'translation' => '[%classname] コンテキストメニューを表示するにはアイコンをクリックしてください。',
    'key' => '675c44a4485051ee55aa0eea1e55bee0',
  ),
  '72bfe37e6407ea84d8e808d0c4faa23c' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Object ID',
    'comment' => NULL,
    'translation' => 'オブジェクトID',
    'key' => '72bfe37e6407ea84d8e808d0c4faa23c',
  ),
  '54b0820b433b9f8c5f0db98787296d9d' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Dynamic tree menu is disabled for this siteaccess!',
    'comment' => NULL,
    'translation' => 'このサイトアクセスではダイナミックツリーを利用出来ません!',
    'key' => '54b0820b433b9f8c5f0db98787296d9d',
  ),
  '1a3ccc344b73656ee756743460108b07' => 
  array (
    'context' => 'design/admin/contentstructuremenu',
    'source' => 'Internal errorD',
    'comment' => NULL,
    'translation' => '内部エラー',
    'key' => '1a3ccc344b73656ee756743460108b07',
  ),
);
?>
