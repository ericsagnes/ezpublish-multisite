<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/setup',
);

$TranslationRoot = array (
  '46cb7cd0c0490ef1c4800f008eca1bd6' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'File consistency check OK.',
    'comment' => NULL,
    'translation' => 'ファイル整合性チェック OK',
    'key' => '46cb7cd0c0490ef1c4800f008eca1bd6',
  ),
  'fc6f95becd2785d5d4f811f6f0a51e9f' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'Database check OK.',
    'comment' => NULL,
    'translation' => 'データベースチェック OK',
    'key' => 'fc6f95becd2785d5d4f811f6f0a51e9f',
  ),
  'f35e9ca4236defa1271364aff6fcf92b' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'The database is not consistent with the distribution database.',
    'comment' => NULL,
    'translation' => 'データベースがディストリビューションデータベースと整合しません',
    'key' => 'f35e9ca4236defa1271364aff6fcf92b',
  ),
  'e72337ce016de0628aeddcec14c2ac6f' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'To synchronize your database with the distribution setup, run the following SQL commands',
    'comment' => NULL,
    'translation' => 'データベースをディストリビューション設定に同期するには、以下のSQLコマンドを実行してください',
    'key' => 'e72337ce016de0628aeddcec14c2ac6f',
  ),
  '1681d3a64a2022a1c8d49eed3d1a7768' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'System upgrade check',
    'comment' => NULL,
    'translation' => 'アップグレードの確認',
    'key' => '1681d3a64a2022a1c8d49eed3d1a7768',
  ),
  '5db82883576a39be98904f932c8c188c' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'File consistency check',
    'comment' => NULL,
    'translation' => 'ファイル整合性の確認',
    'key' => '5db82883576a39be98904f932c8c188c',
  ),
  '311ca0084470b43856255a81b500a842' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'Database consistency check',
    'comment' => NULL,
    'translation' => 'データベース整合性の確認',
    'key' => '311ca0084470b43856255a81b500a842',
  ),
  '8f0fd9b6bb6d452d6a874120822adf34' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'Check file consistency',
    'comment' => NULL,
    'translation' => 'ファイル整合性の確認',
    'key' => '8f0fd9b6bb6d452d6a874120822adf34',
  ),
  '7dab614cef577a87eca1d901aeaec779' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'Check database consistency',
    'comment' => NULL,
    'translation' => 'データベース整合性の確認',
    'key' => '7dab614cef577a87eca1d901aeaec779',
  ),
  '127f9578d9dc0218e08b2f683652b490' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'Warning: it is not safe to upgrade without checking the modifications done to the following files',
    'comment' => NULL,
    'translation' => '警告: 以下のファイルに対して行った編集を確認していない場合、アップグレードするのは危険です',
    'key' => '127f9578d9dc0218e08b2f683652b490',
  ),
  '753cbd327bca6fc2e68c7ca82040a020' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'Before upgrading eZ Publish to a newer version, it is important to check that the current installation is ready for upgrading.',
    'comment' => NULL,
    'translation' => 'eZ Publishのアップグレードを行う前に、現在のインストレーションにおいてアップグレードの準備ができているかを必ず確認して下さい。',
    'key' => '753cbd327bca6fc2e68c7ca82040a020',
  ),
  '6abf87e87a5f03f5823627e3b33bdaaf' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'Remember to make a backup of the eZ Publish directory and the database before you upgrade.',
    'comment' => NULL,
    'translation' => 'アップグレードを行う前に、eZ Publishディレクトリーとデータベースのバックアップを忘れないでください。',
    'key' => '6abf87e87a5f03f5823627e3b33bdaaf',
  ),
  '8bf45671f58f1d4c1f485dd167cd304c' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'The file consistency tool checks if you have altered any of the files that came with the current installation. Altered files may be replaced by new versions that contain bugfixes, new features, etc. Make sure that you backup and then merge your changes into the new versions of the files.',
    'comment' => NULL,
    'translation' => 'ファイル整合性検査ツールは現在のインストレーションのファイルを変更したかどうかを確認します。変更したファイルは新しい機能、バグフィックスが含まれている新しいファイルに上書きされる可能性があります。バックアップを必ず行い、後に新しいファイルに変更箇所を追加してください。',
    'key' => '8bf45671f58f1d4c1f485dd167cd304c',
  ),
  '8ad90237c6366a448ebc2024c3c1bd23' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'The database consistency tool checks if the current database is consistent with the database schema that came with the eZ Publish distribution. If there are any inconsistencies, the tool will suggest the necessary SQL statements that should be run in order to bring the database into a consistent state. Please run the suggested SQL statements before upgrading.',
    'comment' => NULL,
    'translation' => 'データベース整合性検査ツールは、使用されているデータベースとeZ Publishと一緒に提供されるデータベーススキーマとに矛盾がないかを確認します。矛盾がある場合、ツールはデータベースを矛盾のない状態にするために必要なSQLクエリーを提案します。アップグレードをする前に提案されたクエリーを実行してください。',
    'key' => '8ad90237c6366a448ebc2024c3c1bd23',
  ),
  '35b9fd66dd180311c246864eccd5d897' => 
  array (
    'context' => 'design/admin/setup',
    'source' => 'The upgrade checking tools require a lot of system resources. They may take some time to run.',
    'comment' => NULL,
    'translation' => 'アップグレードツールはシステムのリソースを多く使用します。完了するまでに時間がかかる場合があります。',
    'key' => '35b9fd66dd180311c246864eccd5d897',
  ),
);
?>
