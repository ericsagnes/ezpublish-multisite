<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/shop/confirmorder',
);

$TranslationRoot = array (
  '383bf535e8ea815851e38259cb9ef13f' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Shopping basket',
    'comment' => NULL,
    'translation' => '買い物カゴ',
    'key' => '383bf535e8ea815851e38259cb9ef13f',
  ),
  '49c7cd246674375b6ef7ff7a23030bd8' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Account information',
    'comment' => NULL,
    'translation' => 'お客様情報',
    'key' => '49c7cd246674375b6ef7ff7a23030bd8',
  ),
  '28fb69eed7bde70ffb9c35c113611eee' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Confirm order',
    'comment' => NULL,
    'translation' => 'オーダーの確認',
    'key' => '28fb69eed7bde70ffb9c35c113611eee',
  ),
  '0e026cc64e04d449f6eecc4de07f3ecc' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Product items',
    'comment' => NULL,
    'translation' => '商品',
    'key' => '0e026cc64e04d449f6eecc4de07f3ecc',
  ),
  'c7fbd34f842d5a742ecdfb2eff971c2d' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Count',
    'comment' => NULL,
    'translation' => '数量',
    'key' => 'c7fbd34f842d5a742ecdfb2eff971c2d',
  ),
  'dd8d37a1ed5738b532c914be9811ee28' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'VAT',
    'comment' => NULL,
    'translation' => '消費税',
    'key' => 'dd8d37a1ed5738b532c914be9811ee28',
  ),
  '84df14c1982489896bf5a6efd1e00862' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Price inc. VAT',
    'comment' => NULL,
    'translation' => '価格（税込）',
    'key' => '84df14c1982489896bf5a6efd1e00862',
  ),
  'b16e57b28cf4f583c0ff1b93a595e41d' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Discount',
    'comment' => NULL,
    'translation' => '割引',
    'key' => 'b16e57b28cf4f583c0ff1b93a595e41d',
  ),
  '69c8bc8d90f2faa176eb12123c006055' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Selected options',
    'comment' => NULL,
    'translation' => '選択したオプション',
    'key' => '69c8bc8d90f2faa176eb12123c006055',
  ),
  '18e29e870fc34f31f3434f4a0832065f' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Order summary',
    'comment' => NULL,
    'translation' => 'ご注文のサマリ',
    'key' => '18e29e870fc34f31f3434f4a0832065f',
  ),
  '5ad7e2f70a7d39fb66f2e66b2ea91ef6' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Subtotal of items',
    'comment' => NULL,
    'translation' => '商品小計',
    'key' => '5ad7e2f70a7d39fb66f2e66b2ea91ef6',
  ),
  '47851027a54df96bb3cde6b2717e009b' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Order total',
    'comment' => NULL,
    'translation' => 'お買い上げ合計',
    'key' => '47851027a54df96bb3cde6b2717e009b',
  ),
  'd2d36df721cf04e52898e1b561efd461' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => 'd2d36df721cf04e52898e1b561efd461',
  ),
  'a662e23f35e5ceef797c87819eae4650' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Confirm',
    'comment' => NULL,
    'translation' => '確認',
    'key' => 'a662e23f35e5ceef797c87819eae4650',
  ),
  'ebd15ab5e7727aec9637617b11ac7dcc' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Total price ex. VAT',
    'comment' => NULL,
    'translation' => '合計（税別）',
    'key' => 'ebd15ab5e7727aec9637617b11ac7dcc',
  ),
  '817bb05d592d21238efd151a88125e71' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Total price inc. VAT',
    'comment' => NULL,
    'translation' => '合計（税込）',
    'key' => '817bb05d592d21238efd151a88125e71',
  ),
  'f4784aaf0916fa5c5379c4d56c0de243' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Summary',
    'comment' => NULL,
    'translation' => 'サマリ',
    'key' => 'f4784aaf0916fa5c5379c4d56c0de243',
  ),
  'ec27d54d8bdb807f52c358f837b0d515' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Total ex. VAT',
    'comment' => NULL,
    'translation' => '合計（税別）',
    'key' => 'ec27d54d8bdb807f52c358f837b0d515',
  ),
  '365916ab34fbf004d3882183b2450443' => 
  array (
    'context' => 'design/ezwebin/shop/confirmorder',
    'source' => 'Total inc. VAT',
    'comment' => NULL,
    'translation' => '合計（税込）',
    'key' => '365916ab34fbf004d3882183b2450443',
  ),
);
?>
