<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/urlalias',
);

$TranslationRoot = array (
  '3504fdfdb39e7c2d124a71fe2286f3c9' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'The selected aliases were successfully removed.',
    'comment' => NULL,
    'translation' => '選択したエイリアスは削除されました。',
    'key' => '3504fdfdb39e7c2d124a71fe2286f3c9',
  ),
  '7b91a950b29d3f19cb660a73b948f6b9' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'All aliases for this node were successfully removed.',
    'comment' => NULL,
    'translation' => 'このノードのすべてのURLエイリアスは削除されました。',
    'key' => '7b91a950b29d3f19cb660a73b948f6b9',
  ),
  '8e04f62502196d9ffcbea454465ff651' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'The specified language code <%language> is not valid.',
    'comment' => NULL,
    'translation' => '指定された<%language>言語コードは無効です。',
    'key' => '8e04f62502196d9ffcbea454465ff651',
  ),
  'a7750a44ca6cf9e958a5ff6db68effd8' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Text is missing for the URL alias',
    'comment' => NULL,
    'translation' => 'URLエイリアスの入力が必要です',
    'key' => 'a7750a44ca6cf9e958a5ff6db68effd8',
  ),
  '34fa05da1e8876e33b45af1ebd738017' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Enter text in the input box to create a new alias.',
    'comment' => NULL,
    'translation' => '新しいエイリアスを作成するにはインプットボックスにテキストを入力してください。',
    'key' => '34fa05da1e8876e33b45af1ebd738017',
  ),
  '3d29e091170948040a24a4b5d2a0797c' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'The URL alias was successfully created, but was modified by the system to <%new_alias>',
    'comment' => NULL,
    'translation' => 'URLエイリアスの作成は成功しましたが、システムから<%new_alias>に変更されました。',
    'key' => '3d29e091170948040a24a4b5d2a0797c',
  ),
  '74a7dc011940fdbae2f6c52f6cbb6cab' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Invalid characters will be removed or transformed to valid characters.',
    'comment' => NULL,
    'translation' => '無効な文字は削除されるか、有効な文字に変換されます。',
    'key' => '74a7dc011940fdbae2f6c52f6cbb6cab',
  ),
  'f39493b2de85a9dff25bff963782d445' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Existing objects or functionality with the same name take precedence on the name.',
    'comment' => NULL,
    'translation' => 'すでに存在しているオブジェクトかファンクションと同じ名前を指定した場合、既存のオブエジェクト又はファンクションが優先されます。',
    'key' => 'f39493b2de85a9dff25bff963782d445',
  ),
  '575efbb97411f5cb93296caf6d7b7d7c' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'The URL alias <%new_alias> was successfully created',
    'comment' => NULL,
    'translation' => '<%new_alias>URLエイリアスの作成に成功しました',
    'key' => '575efbb97411f5cb93296caf6d7b7d7c',
  ),
  'b435aa64ebc6c35d343ea603e8fc81dd' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'The URL alias &lt;%new_alias&gt; already exists, and it points to &lt;%action_url&gt;',
    'comment' => NULL,
    'translation' => '&lt;%new_alias&gt;URLエイリアスはすでに存在しています。&lt;%action_url&gt;に利用されています。',
    'key' => 'b435aa64ebc6c35d343ea603e8fc81dd',
  ),
  'b7edf91d1f76ec5e031e6d8802aa364a' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'URL aliases for <%node_name> [%alias_count]',
    'comment' => NULL,
    'translation' => '<%node_name> [%alias_count]のエイリアス',
    'key' => 'b7edf91d1f76ec5e031e6d8802aa364a',
  ),
  '87956dee138e56552edd93606d773212' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'The current item does not have any aliases associated with it.',
    'comment' => NULL,
    'translation' => '現在のアイテムにはエイリアスは設定されていません。',
    'key' => '87956dee138e56552edd93606d773212',
  ),
  '4b30c82500b2434dce848fd1f8d8f2b9' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Invert selection.',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => '4b30c82500b2434dce848fd1f8d8f2b9',
  ),
  '458e2c39a80b6d170ca6550ed5cc2b3a' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'URL alias',
    'comment' => NULL,
    'translation' => 'URLエイリアス',
    'key' => '458e2c39a80b6d170ca6550ed5cc2b3a',
  ),
  'b8ebfef2eccd2c1674bf2032029b61b1' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Language',
    'comment' => NULL,
    'translation' => '言語',
    'key' => 'b8ebfef2eccd2c1674bf2032029b61b1',
  ),
  '14ca71ab54f4fd2851e0b769b95097c4' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => '14ca71ab54f4fd2851e0b769b95097c4',
  ),
  '692d75fc4d65fe7422a006ca9c07ecac' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Remove selected alias from the list above.',
    'comment' => NULL,
    'translation' => '上記のリストから選択したエイリアスを削除します。',
    'key' => '692d75fc4d65fe7422a006ca9c07ecac',
  ),
  '618b4e8bdd6931d31cdcb1303261902f' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Are you sure you want to remove the selected aliases?',
    'comment' => NULL,
    'translation' => '選択したエイリアスを削除してもよろしいですか?',
    'key' => '618b4e8bdd6931d31cdcb1303261902f',
  ),
  '44b20ba8e9fd661e364d15c50c1a84e8' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Remove all',
    'comment' => NULL,
    'translation' => 'すべて削除',
    'key' => '44b20ba8e9fd661e364d15c50c1a84e8',
  ),
  '754d49b5584a54186273f77c4768171c' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Remove all aliases for this node.',
    'comment' => NULL,
    'translation' => 'このノードのすべてのエイリアスを削除する。',
    'key' => '754d49b5584a54186273f77c4768171c',
  ),
  'b40ce099d6a06710cb9cfa83c7d9d47e' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Are you sure you want to remove all aliases for this node?',
    'comment' => NULL,
    'translation' => 'このノードのすべてのエイリアスを削除してもよろしいですか?',
    'key' => 'b40ce099d6a06710cb9cfa83c7d9d47e',
  ),
  '6b2819df20c873a83e1c9de66d5567f8' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'There are no removable aliases.',
    'comment' => NULL,
    'translation' => '削除可能なエイリアスはありません。',
    'key' => '6b2819df20c873a83e1c9de66d5567f8',
  ),
  'df4564e9b18d8880f9c3acbc1304661e' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'You cannot remove any aliases because you do not have permission to edit the current item.',
    'comment' => NULL,
    'translation' => '現在のアイテムを編集する権限がないため、エイリアスを削除することはできません。',
    'key' => 'df4564e9b18d8880f9c3acbc1304661e',
  ),
  '5dde2ce1a724530d44cbda251c3babea' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Choose the language for the new URL alias.',
    'comment' => NULL,
    'translation' => '新しいURLエイリアスの言語を選んでください。',
    'key' => '5dde2ce1a724530d44cbda251c3babea',
  ),
  'bca0ee612f948207d51ab8a09cc40252' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Not available',
    'comment' => NULL,
    'translation' => '利用不可能',
    'key' => 'bca0ee612f948207d51ab8a09cc40252',
  ),
  'd1ff3e09a52433f99725ac3750837f5c' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Enter the URL for the new alias. Use forward slashes (/) to create subentries.',
    'comment' => NULL,
    'translation' => '新しいエイリアスのURLを入力してください。スラッシュ(/)を使って、サブエントリーを作ってください。',
    'key' => 'd1ff3e09a52433f99725ac3750837f5c',
  ),
  '7834ff034795890f990ac19835687c5f' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Generated aliases [%count]',
    'comment' => NULL,
    'translation' => '[%count]エイリアスを生成しました',
    'key' => '7834ff034795890f990ac19835687c5f',
  ),
  'f96204c8272ddfa99c58f1aceb1e4a2e' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Note that these entries are automatically generated from the name of the object. To change these names you must edit the object in the specific language and publish the changes.',
    'comment' => NULL,
    'translation' => 'この項目はオブジェクト名を元にして自動で生成されています。名前を変更するには特定の言語でオブジェクトを編集し、変更を公開してください。',
    'key' => 'f96204c8272ddfa99c58f1aceb1e4a2e',
  ),
  '79de11bb8f6c2cf33defa8df5fde91f7' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Edit the contents for language %language.',
    'comment' => NULL,
    'translation' => '%languageのコンテンツを編集する。',
    'key' => '79de11bb8f6c2cf33defa8df5fde91f7',
  ),
  'e532464142ebfeca87b986fbb0aaac1f' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'You cannot edit the contents for language %language because you do not have permission to edit the object.',
    'comment' => NULL,
    'translation' => 'オブジェクトを編集する権限を持っていないため、%languageのコンテンツを編集することはできません。',
    'key' => 'e532464142ebfeca87b986fbb0aaac1f',
  ),
  '089bdc64ad616ae5941f77c6c013486b' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Create new alias',
    'comment' => NULL,
    'translation' => '新しいエイリアスを作成',
    'key' => '089bdc64ad616ae5941f77c6c013486b',
  ),
  'f926e36fa3b458bfb496ee5330830ac2' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Include in other languages',
    'comment' => NULL,
    'translation' => '他の言語を含める',
    'key' => 'f926e36fa3b458bfb496ee5330830ac2',
  ),
  'e6e7d39db30c6f68873a72aa4c9569c3' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Create new URL forwarding with wildcard',
    'comment' => NULL,
    'translation' => 'ワイルドカードを使って、新しいURLリダイレクトを作成する',
    'key' => 'e6e7d39db30c6f68873a72aa4c9569c3',
  ),
  'c123bc5be9d7e7a83bb0601cda4d6e6f' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Redirecting URL',
    'comment' => NULL,
    'translation' => 'URLリダイレクト',
    'key' => 'c123bc5be9d7e7a83bb0601cda4d6e6f',
  ),
  'babf956ce4a1d3f6672d67771654d710' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Type',
    'comment' => NULL,
    'translation' => 'タイプ',
    'key' => 'babf956ce4a1d3f6672d67771654d710',
  ),
  'c6ecc9070bc324a28f621ca569471c38' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Redirect',
    'comment' => NULL,
    'translation' => 'リダイレクト',
    'key' => 'c6ecc9070bc324a28f621ca569471c38',
  ),
  '653a1c1da12ec8f5338074db6ed2ab15' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Direct',
    'comment' => NULL,
    'translation' => 'ダイレクト',
    'key' => '653a1c1da12ec8f5338074db6ed2ab15',
  ),
  '5f6918aef8e4170d220966e6a662debc' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'URL alias name:',
    'comment' => NULL,
    'translation' => 'URLエイリアス名:',
    'key' => '5f6918aef8e4170d220966e6a662debc',
  ),
  'bfa4dfef31563723ccb20eb33e5cf794' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Destination:',
    'comment' => NULL,
    'translation' => '対象:',
    'key' => 'bfa4dfef31563723ccb20eb33e5cf794',
  ),
  '37ef3e08e71ee8f9882cab1bb4770e79' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Destination.',
    'comment' => NULL,
    'translation' => '対象',
    'key' => '37ef3e08e71ee8f9882cab1bb4770e79',
  ),
  '3196b22a5c49d1db3f99e9b852b06efa' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Language:',
    'comment' => NULL,
    'translation' => '言語:',
    'key' => '3196b22a5c49d1db3f99e9b852b06efa',
  ),
  'deed180034bea7837303702304b30033' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Alias should redirect to its destination',
    'comment' => NULL,
    'translation' => 'エイリアスを対象へとリダイレクトする',
    'key' => 'deed180034bea7837303702304b30033',
  ),
  'ae90af0d731b79641e688244756a38f3' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'With <em>Alias should redirect to its destination</em> checked eZ Publish will redirect to the destination using a HTTP 301 response. Un-check it and the URL will stay the same &#8212; no redirection will be performed.',
    'comment' => NULL,
    'translation' => '<em>エイリアスを対象へとリダイレクトする</em>がチェックされている場合、eZ PublishはHTTP 301レスポンスを使って、リダイレクトします。チェックを外すとURLは変わらず、リダイレクトは実行されません。',
    'key' => 'ae90af0d731b79641e688244756a38f3',
  ),
  '14fcb4ae7dff368a15049db7803a94e9' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'If checked the alias will start from the parent of the current node. If un-checked the aliases will start from the root of the site.',
    'comment' => NULL,
    'translation' => 'チェックすると、新しいエイリアスは親ノードから始まります。チェックを外すとエイリアスはサイトルートから始まります。',
    'key' => '14fcb4ae7dff368a15049db7803a94e9',
  ),
  '0a5ff2a4a4585c97fefbf8c2cca8e311' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Place alias on the site root',
    'comment' => NULL,
    'translation' => 'サイトルートにエイリアスを配置します',
    'key' => '0a5ff2a4a4585c97fefbf8c2cca8e311',
  ),
  'd430c06b475a48e010add257abbb28f6' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'The new alias be placed under %link',
    'comment' => NULL,
    'translation' => '新しいエイリアスは%linkの下に配置されます',
    'key' => 'd430c06b475a48e010add257abbb28f6',
  ),
  '6520979c3c2056446c1f2c240bce603b' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => '<em>Un-check</em> to create the new alias under %link. Leave it checked and the new alias will be created on <em><a href=\'/\'>%siteroot</a></em>.',
    'comment' => NULL,
    'translation' => '%linkで新しいエイリアスを作成するには<em>チェックを外して下さい</em>。チェックされた状態では、新しいエイリアスは<em><a href=\'/\'>%siteroot</a></em>に作成されます。',
    'key' => '6520979c3c2056446c1f2c240bce603b',
  ),
  '511dac826369d4ea7f232499fb64d557' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'URL aliases for <%node_name> (%alias_count)',
    'comment' => NULL,
    'translation' => '<%node_name> (%alias_count)のエイリアス',
    'key' => '511dac826369d4ea7f232499fb64d557',
  ),
  '6f414be0aa0562b6228bc28e17b4bf6f' => 
  array (
    'context' => 'design/admin/content/urlalias',
    'source' => 'Generated aliases (%count)',
    'comment' => NULL,
    'translation' => '(%count)エイリアスを生成しました',
    'key' => '6f414be0aa0562b6228bc28e17b4bf6f',
  ),
);
?>
