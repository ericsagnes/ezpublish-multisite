<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/user/setting',
);

$TranslationRoot = array (
  '197aa4b576e5dad28823820d4e30dd2d' => 
  array (
    'context' => 'design/admin/user/setting',
    'source' => 'Maximum concurrent logins',
    'comment' => NULL,
    'translation' => '最大同時ログイン数',
    'key' => '197aa4b576e5dad28823820d4e30dd2d',
  ),
  '32d8370a7e5fb1dd55d31a488f8ed9b3' => 
  array (
    'context' => 'design/admin/user/setting',
    'source' => 'Enable user account',
    'comment' => NULL,
    'translation' => 'ユーザアカウント有効化',
    'key' => '32d8370a7e5fb1dd55d31a488f8ed9b3',
  ),
  'ff05cd1dbd0e5f512e125e0cfd00a310' => 
  array (
    'context' => 'design/admin/user/setting',
    'source' => 'Use this checkbox to enable or disable the user account.',
    'comment' => NULL,
    'translation' => 'ユーザ情報を有効あるいは、無効にするには、チェックボックを使用して下さい。',
    'key' => 'ff05cd1dbd0e5f512e125e0cfd00a310',
  ),
  'aa6175a7070f7b9ad1165070681c4d28' => 
  array (
    'context' => 'design/admin/user/setting',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => 'aa6175a7070f7b9ad1165070681c4d28',
  ),
  '0061feb0a487d145511a8bbf91a5a1dc' => 
  array (
    'context' => 'design/admin/user/setting',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '0061feb0a487d145511a8bbf91a5a1dc',
  ),
  'c3f0f3e093b0fdc2e0f362f780247a50' => 
  array (
    'context' => 'design/admin/user/setting',
    'source' => 'This functionality is not currently not available. [Use this field to specify the maximum allowed number of concurrent logins.]',
    'comment' => NULL,
    'translation' => 'この機能は現在利用できません。 （この項目は、最大同時ログインの設定に使用します）',
    'key' => 'c3f0f3e093b0fdc2e0f362f780247a50',
  ),
  '713ee6e48c5cdc437f29312752f21304' => 
  array (
    'context' => 'design/admin/user/setting',
    'source' => 'User settings for <%user_name>',
    'comment' => NULL,
    'translation' => '<%user_name> のユーザ設定',
    'key' => '713ee6e48c5cdc437f29312752f21304',
  ),
  '61ccd932eee1759705751c18d4a2545e' => 
  array (
    'context' => 'design/admin/user/setting',
    'source' => 'Maximum number of failed login attempts',
    'comment' => NULL,
    'translation' => '最大ログイン試行回数',
    'key' => '61ccd932eee1759705751c18d4a2545e',
  ),
  '77e6786b2aff964e2ce6043b36b2e99a' => 
  array (
    'context' => 'design/admin/user/setting',
    'source' => 'Number of failed login attempts for this user',
    'comment' => NULL,
    'translation' => 'ユーザ・ログイン失敗回数',
    'key' => '77e6786b2aff964e2ce6043b36b2e99a',
  ),
  '2a28f8e6f015d3ae494852add958da0d' => 
  array (
    'context' => 'design/admin/user/setting',
    'source' => 'Reset',
    'comment' => NULL,
    'translation' => 'リセット',
    'key' => '2a28f8e6f015d3ae494852add958da0d',
  ),
  'cb08f84f86ddf735a25eac6f55dc31b3' => 
  array (
    'context' => 'design/admin/user/setting',
    'source' => 'Account has been locked because the maximum number of failed login attempts was exceeded.',
    'comment' => NULL,
    'translation' => '不正ログイン数の制限を超えたため、アカウントがロックされました。',
    'key' => 'cb08f84f86ddf735a25eac6f55dc31b3',
  ),
);
?>
