<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/content/poll',
);

$TranslationRoot = array (
  '6a959572bd06d14975bf6f433e7f6f73' => 
  array (
    'context' => 'design/standard/content/poll',
    'source' => 'Poll %pollname',
    'comment' => NULL,
    'translation' => '%pollname の投票',
    'key' => '6a959572bd06d14975bf6f433e7f6f73',
  ),
  '26b1e02faeb899e8b096373d1e3526cf' => 
  array (
    'context' => 'design/standard/content/poll',
    'source' => '%count total votes',
    'comment' => NULL,
    'translation' => '投票総数: %count ',
    'key' => '26b1e02faeb899e8b096373d1e3526cf',
  ),
  '0961f2774fe52ff0ed0a19a1d086dbfa' => 
  array (
    'context' => 'design/standard/content/poll',
    'source' => 'Poll results',
    'comment' => NULL,
    'translation' => '投票結果',
    'key' => '0961f2774fe52ff0ed0a19a1d086dbfa',
  ),
  '95dd5c00e1999deea8c4640a7f2016cc' => 
  array (
    'context' => 'design/standard/content/poll',
    'source' => 'Anonymous users are not allowed to vote in this poll. Please log in.',
    'comment' => NULL,
    'translation' => '匿名ユーザはこの投票に投票することは出来ません。投票するにはログインしてください。',
    'key' => '95dd5c00e1999deea8c4640a7f2016cc',
  ),
  '84e325542d08f8298dd604422cf3758e' => 
  array (
    'context' => 'design/standard/content/poll',
    'source' => 'You have already voted in this poll.',
    'comment' => NULL,
    'translation' => 'この投票には既に参加しています。',
    'key' => '84e325542d08f8298dd604422cf3758e',
  ),
);
?>
