<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/browse_related',
);

$TranslationRoot = array (
  '893e198c19cdb3a5e3e7fa25a7d3f58a' => 
  array (
    'context' => 'design/admin/content/browse_related',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => '893e198c19cdb3a5e3e7fa25a7d3f58a',
  ),
  'f3ff5384e01eb52b41109f10ac000084' => 
  array (
    'context' => 'design/admin/content/browse_related',
    'source' => 'Choose objects that you want to relate to <%version_name>',
    'comment' => NULL,
    'translation' => '<%version_name>と関連づけたいオブジェクトを選択して下さい',
    'key' => 'f3ff5384e01eb52b41109f10ac000084',
  ),
  '575b1d925530be180f2ff26ab2e71abd' => 
  array (
    'context' => 'design/admin/content/browse_related',
    'source' => 'Use the checkboxes to choose the objects that you want to relate to <%version_name>.',
    'comment' => NULL,
    'translation' => '<%version_name>に関連づけたいオブジェクトをチェックボックスで選択して下さい。',
    'key' => '575b1d925530be180f2ff26ab2e71abd',
  ),
);
?>
