<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/ezodf/browse_place',
);

$TranslationRoot = array (
  '16b97ec6542b0dec457baa8c96b08fad' => 
  array (
    'context' => 'design/ezwebin/ezodf/browse_place',
    'source' => 'Choose document placement',
    'comment' => NULL,
    'translation' => 'ドキュメントのロケーションを選択してください',
    'key' => '16b97ec6542b0dec457baa8c96b08fad',
  ),
  '978551d8aaf052a35c7764411096ddb6' => 
  array (
    'context' => 'design/ezwebin/ezodf/browse_place',
    'source' => 'Select',
    'comment' => NULL,
    'translation' => '選択',
    'key' => '978551d8aaf052a35c7764411096ddb6',
  ),
  'f777a228d07753ea4f28b86c0fc55a41' => 
  array (
    'context' => 'design/ezwebin/ezodf/browse_place',
    'source' => 'Please choose the placement for the OpenOffice.org object.

    Select the placements and click the %buttonname button.
    Using the recent and bookmark items for quick placement is also possible.
    Click on placement names to change the browse listing.',
    'comment' => NULL,
    'translation' => 'OpenOffice.orgオブジェクトの配置先を選択してください。
    配置先を選んで、%buttonnameボタンをクリックしてください。
    お気に入りを利用する事もできます。
    配置先名にクリックして、ブラウズリストを変更できます。',
    'key' => 'f777a228d07753ea4f28b86c0fc55a41',
  ),
);
?>
