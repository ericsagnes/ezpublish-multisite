<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/full/forum',
);

$TranslationRoot = array (
  'aabb6feedc7b8548ff750c6cd53ed7b3' => 
  array (
    'context' => 'design/ezwebin/full/forum',
    'source' => 'New topic',
    'comment' => NULL,
    'translation' => '新規トピック',
    'key' => 'aabb6feedc7b8548ff750c6cd53ed7b3',
  ),
  'ed0393e81759f3c8adefa6cf2418119c' => 
  array (
    'context' => 'design/ezwebin/full/forum',
    'source' => 'Keep me updated',
    'comment' => NULL,
    'translation' => '更新を知らせる',
    'key' => 'ed0393e81759f3c8adefa6cf2418119c',
  ),
  '14ab21f83ed29ddde110f0a11294aa60' => 
  array (
    'context' => 'design/ezwebin/full/forum',
    'source' => 'You need to be logged in to get access to the forums. You can do so %login_link_start%here%login_link_end%',
    'comment' => NULL,
    'translation' => 'フォーラムへアクセスするには、以下からログインして下さい.%login_link_start%here%login_link_end%',
    'key' => '14ab21f83ed29ddde110f0a11294aa60',
  ),
  '7cffd3ea179f3893903887e0a276681c' => 
  array (
    'context' => 'design/ezwebin/full/forum',
    'source' => 'Topic',
    'comment' => NULL,
    'translation' => 'トピック',
    'key' => '7cffd3ea179f3893903887e0a276681c',
  ),
  'b3262e0a5d379ba6a84fea064fbee56d' => 
  array (
    'context' => 'design/ezwebin/full/forum',
    'source' => 'Replies',
    'comment' => NULL,
    'translation' => '返答数',
    'key' => 'b3262e0a5d379ba6a84fea064fbee56d',
  ),
  '551b6ee297fa6f3dbcadad8d976383ea' => 
  array (
    'context' => 'design/ezwebin/full/forum',
    'source' => 'Author',
    'comment' => NULL,
    'translation' => '投稿者',
    'key' => '551b6ee297fa6f3dbcadad8d976383ea',
  ),
  'c19afc4ee0a6cd9576a23c5ed673623d' => 
  array (
    'context' => 'design/ezwebin/full/forum',
    'source' => 'Last reply',
    'comment' => NULL,
    'translation' => '最新のコメント',
    'key' => 'c19afc4ee0a6cd9576a23c5ed673623d',
  ),
  '1d3b80fe5395e42792ee846a101e72ad' => 
  array (
    'context' => 'design/ezwebin/full/forum',
    'source' => 'Pages',
    'comment' => NULL,
    'translation' => 'ページ',
    'key' => '1d3b80fe5395e42792ee846a101e72ad',
  ),
);
?>
