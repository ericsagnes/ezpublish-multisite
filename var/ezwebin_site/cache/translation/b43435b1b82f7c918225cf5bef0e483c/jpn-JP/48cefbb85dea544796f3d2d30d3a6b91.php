<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/package/creators/ezcontentobject',
);

$TranslationRoot = array (
  '8ce513037315d4c9c751be11da0ab3aa' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Please choose the node(s) you wish to export.',
    'comment' => NULL,
    'translation' => 'エクスポートするノードを選択してください。',
    'key' => '8ce513037315d4c9c751be11da0ab3aa',
  ),
  'd7f3795b950d792a16722808f4b75f20' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Please choose the subtree(s) you wish to export.',
    'comment' => NULL,
    'translation' => 'エクスポートするサブツリーを選択してください。',
    'key' => 'd7f3795b950d792a16722808f4b75f20',
  ),
  '83d7cd3488c67986ef0225f13cb20a07' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Choose node for export',
    'comment' => NULL,
    'translation' => 'エクスポートするノードの選択',
    'key' => '83d7cd3488c67986ef0225f13cb20a07',
  ),
  'a28053f899381d858085db37fe4d3295' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Choose subtree for export',
    'comment' => NULL,
    'translation' => 'エクスポートするサブツリーの選択',
    'key' => 'a28053f899381d858085db37fe4d3295',
  ),
  '7c35fb71c00dbdea34d221b30f795be4' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Miscellaneous',
    'comment' => NULL,
    'translation' => '一般',
    'key' => '7c35fb71c00dbdea34d221b30f795be4',
  ),
  'af9bc249d5702430dc131f4a2506a807' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Include class definitions.',
    'comment' => NULL,
    'translation' => 'クラス定義を含める',
    'key' => 'af9bc249d5702430dc131f4a2506a807',
  ),
  'eb715055e9247078ce9f85f2879ffbd0' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Select templates from the following siteaccesses',
    'comment' => NULL,
    'translation' => 'テンプレートのサイトアクセスを選択',
    'key' => 'eb715055e9247078ce9f85f2879ffbd0',
  ),
  'd3cd5d1eea9b71e4bc27850788fab1ce' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Versions',
    'comment' => NULL,
    'translation' => 'バージョン数',
    'key' => 'd3cd5d1eea9b71e4bc27850788fab1ce',
  ),
  'e31c52a1b2c03d2da50da5090383ab15' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Published version',
    'comment' => NULL,
    'translation' => '公開バージョン',
    'key' => 'e31c52a1b2c03d2da50da5090383ab15',
  ),
  '8e5b909615e4e697ee64d63a1cce10bd' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'All versions',
    'comment' => NULL,
    'translation' => 'すべてのバージョン',
    'key' => '8e5b909615e4e697ee64d63a1cce10bd',
  ),
  '8dece8bbc1c24031cc4457f0863cd145' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Languages',
    'comment' => NULL,
    'translation' => '言語',
    'key' => '8dece8bbc1c24031cc4457f0863cd145',
  ),
  '5826d21781faaf96deed087e6a127839' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Select languages to export',
    'comment' => NULL,
    'translation' => 'エクスポートする言語の選択',
    'key' => '5826d21781faaf96deed087e6a127839',
  ),
  '12999c3702d36ce090efd5560b6ba4ce' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Node assignments',
    'comment' => NULL,
    'translation' => 'ノード割り当て',
    'key' => '12999c3702d36ce090efd5560b6ba4ce',
  ),
  '8ca9b1ab2f1f784456e2f1e661f6513e' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Keep all in selected nodes',
    'comment' => NULL,
    'translation' => '選択したノードのすべてを保持',
    'key' => '8ca9b1ab2f1f784456e2f1e661f6513e',
  ),
  '1fee6b2c7423d2e413b3e7af79665a1a' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Main only',
    'comment' => NULL,
    'translation' => '主要ノードのみ',
    'key' => '1fee6b2c7423d2e413b3e7af79665a1a',
  ),
  '9f3258980b73e794def2d42153f9cdde' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Related objects',
    'comment' => NULL,
    'translation' => '関連付けしたオブジェクト',
    'key' => '9f3258980b73e794def2d42153f9cdde',
  ),
  '7f5d06ea41766aa080916ff63d695d02' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'None',
    'comment' => NULL,
    'translation' => 'なし',
    'key' => '7f5d06ea41766aa080916ff63d695d02',
  ),
  'b618f8ef378a906b246560f2d22c5994' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Specify export properties. Default settings will probably be suitable for your needs.',
    'comment' => NULL,
    'translation' => 'エクスポート設定を定義します。デフォルト設定は一般的な用途に最適化されています。',
    'key' => 'b618f8ef378a906b246560f2d22c5994',
  ),
  '613daecd8bfbf62d09e3d8b33083198b' => 
  array (
    'context' => 'design/standard/package/creators/ezcontentobject',
    'source' => 'Include templates related to exported objects.',
    'comment' => NULL,
    'translation' => 'エクスポートオブジェクトと関係のあるテンプレートを含める。',
    'key' => '613daecd8bfbf62d09e3d8b33083198b',
  ),
);
?>
