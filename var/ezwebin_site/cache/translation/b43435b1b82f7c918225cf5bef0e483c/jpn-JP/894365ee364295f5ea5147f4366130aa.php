<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/user/success',
);

$TranslationRoot = array (
  '3cdbcfa813c0182cf2cac0eac43f08c2' => 
  array (
    'context' => 'design/ezwebin/user/success',
    'source' => 'User registered',
    'comment' => NULL,
    'translation' => '登録されました',
    'key' => '3cdbcfa813c0182cf2cac0eac43f08c2',
  ),
  'd30db4008d85f0922b0d6833d72e387d' => 
  array (
    'context' => 'design/ezwebin/user/success',
    'source' => 'Your account was successfully created.',
    'comment' => NULL,
    'translation' => 'アカウントが作成されました。',
    'key' => 'd30db4008d85f0922b0d6833d72e387d',
  ),
  '647dabf9937b80188ffcee3a468a71fc' => 
  array (
    'context' => 'design/ezwebin/user/success',
    'source' => 'Your account was successfully created. An email will be sent to the specified address. Follow the instructions in that email to activate your account.',
    'comment' => NULL,
    'translation' => 'アカウントが作成されました。設定したメールアドレスにメールが送られます。アカウントを有効にするにはそのメールに書いてある指示にしたがってください。',
    'key' => '647dabf9937b80188ffcee3a468a71fc',
  ),
);
?>
