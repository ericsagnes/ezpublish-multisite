<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/content/trash',
);

$TranslationRoot = array (
  '334129ab6aed52d8ab1f40fc9f9ab38e' => 
  array (
    'context' => 'design/standard/content/trash',
    'source' => 'Trash',
    'comment' => NULL,
    'translation' => 'ごみ箱',
    'key' => '334129ab6aed52d8ab1f40fc9f9ab38e',
  ),
  '4eb9148c0c6f257c1f6859f75e330aaa' => 
  array (
    'context' => 'design/standard/content/trash',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => '4eb9148c0c6f257c1f6859f75e330aaa',
  ),
  '710f7412a83920023b462b33bc0d3ab6' => 
  array (
    'context' => 'design/standard/content/trash',
    'source' => 'Trash is empty',
    'comment' => NULL,
    'translation' => 'ごみ箱は空です',
    'key' => '710f7412a83920023b462b33bc0d3ab6',
  ),
  '1c6e2a8c9cc54d02872b7e5257599780' => 
  array (
    'context' => 'design/standard/content/trash',
    'source' => 'Empty Trash',
    'comment' => NULL,
    'translation' => 'ごみ箱を空にする',
    'key' => '1c6e2a8c9cc54d02872b7e5257599780',
  ),
  'b008d59aa331fc9b1c67873a65fc267f' => 
  array (
    'context' => 'design/standard/content/trash',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => 'b008d59aa331fc9b1c67873a65fc267f',
  ),
  '2c4ba409adb3eb5a3444d9b23e288859' => 
  array (
    'context' => 'design/standard/content/trash',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => '2c4ba409adb3eb5a3444d9b23e288859',
  ),
  '0914c58a486d14f617771ecb5e3cd601' => 
  array (
    'context' => 'design/standard/content/trash',
    'source' => 'Section',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => '0914c58a486d14f617771ecb5e3cd601',
  ),
  '827e7373533d64c076a90f6b94b55721' => 
  array (
    'context' => 'design/standard/content/trash',
    'source' => 'Current version',
    'comment' => NULL,
    'translation' => '現在のバージョン',
    'key' => '827e7373533d64c076a90f6b94b55721',
  ),
  'fe81b088a80e656a75ebf95a75136f7f' => 
  array (
    'context' => 'design/standard/content/trash',
    'source' => 'Restore',
    'comment' => NULL,
    'translation' => '元に戻す',
    'key' => 'fe81b088a80e656a75ebf95a75136f7f',
  ),
  'f43d0346bd991d28e906a8b6c0817a9b' => 
  array (
    'context' => 'design/standard/content/trash',
    'source' => 'Select all',
    'comment' => NULL,
    'translation' => 'すべて選択',
    'key' => 'f43d0346bd991d28e906a8b6c0817a9b',
  ),
  '8f94944efe6676c7f95136a6a96f372a' => 
  array (
    'context' => 'design/standard/content/trash',
    'source' => 'Deselect all',
    'comment' => NULL,
    'translation' => '選択をすべて解除',
    'key' => '8f94944efe6676c7f95136a6a96f372a',
  ),
);
?>
