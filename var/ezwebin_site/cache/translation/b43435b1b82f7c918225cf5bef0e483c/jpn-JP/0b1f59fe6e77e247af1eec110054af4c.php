<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/rss/edit_export',
);

$TranslationRoot = array (
  '212a0e3047cf860a297a2a6b5b488245' => 
  array (
    'context' => 'kernel/rss/edit_export',
    'source' => 'Selected class does not exist',
    'comment' => NULL,
    'translation' => '選択されたクラスは存在しません',
    'key' => '212a0e3047cf860a297a2a6b5b488245',
  ),
  '057bc65b23de8638874b66de39e7155c' => 
  array (
    'context' => 'kernel/rss/edit_export',
    'source' => 'Invalid selection for title class %1 does not have attribute "%2"',
    'comment' => NULL,
    'translation' => 'タイトルの選択が不正です、クラス%1は属性"%2"を持ちません',
    'key' => '057bc65b23de8638874b66de39e7155c',
  ),
  'd00b8f45fcd8ae462661786d4e86d027' => 
  array (
    'context' => 'kernel/rss/edit_export',
    'source' => 'Invalid selection for description class %1 does not have attribute "%2"',
    'comment' => NULL,
    'translation' => '説明の選択が不正です、クラス%1は属性"%2"を持ちません',
    'key' => 'd00b8f45fcd8ae462661786d4e86d027',
  ),
  '32f9b749d665e22171ba3e5ee9b1a72d' => 
  array (
    'context' => 'kernel/rss/edit_export',
    'source' => 'Invalid selection for category class %1 does not have attribute "%2"',
    'comment' => NULL,
    'translation' => 'カテゴリーの選択が不正です。クラス%1は属性"%2"を持ちません',
    'key' => '32f9b749d665e22171ba3e5ee9b1a72d',
  ),
);
?>
