<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/browse_placement',
);

$TranslationRoot = array (
  '42121e4459d16c9a97555e8f1d84bd8a' => 
  array (
    'context' => 'design/admin/content/browse_placement',
    'source' => 'Choose locations for <%version_name>',
    'comment' => NULL,
    'translation' => '<%version_name> の配置先の選択',
    'key' => '42121e4459d16c9a97555e8f1d84bd8a',
  ),
  '14a95f23b0b5c1e9243729089bf39440' => 
  array (
    'context' => 'design/admin/content/browse_placement',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => '14a95f23b0b5c1e9243729089bf39440',
  ),
  'c1b1998b8df72ad338502e6dd1330841' => 
  array (
    'context' => 'design/admin/content/browse_placement',
    'source' => 'Choose locations for <%version_name> using the checkboxes then click "Select".',
    'comment' => NULL,
    'translation' => '<%version_name>の配置先をチェックボックスで選択し、"選択"をクリックしてください。',
    'key' => 'c1b1998b8df72ad338502e6dd1330841',
  ),
);
?>
