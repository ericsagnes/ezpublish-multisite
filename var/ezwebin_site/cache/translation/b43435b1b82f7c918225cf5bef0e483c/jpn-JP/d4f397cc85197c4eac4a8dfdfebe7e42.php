<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/versions',
);

$TranslationRoot = array (
  '643417c28259f74323732bba9995c045' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Unable to create new version',
    'comment' => NULL,
    'translation' => '新しいバージョンを作成できません',
    'key' => '643417c28259f74323732bba9995c045',
  ),
  '123ea9d18a69c530b686a1b45567b728' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Version history limit has been exceeded and no archived version can be removed by the system.',
    'comment' => NULL,
    'translation' => 'バージョンの履歴制限を超えました。未保管のバージョンはシステムにより削除されます。',
    'key' => '123ea9d18a69c530b686a1b45567b728',
  ),
  'a613c329ab4458fa9d2fd65ca7443bc6' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'You can change your version history settings in content.ini, remove draft versions or edit existing drafts.',
    'comment' => NULL,
    'translation' => 'バージョン履歴設定はcontent.ini設定により変更可能です。下書きバージョンを削除するか編集してください。',
    'key' => 'a613c329ab4458fa9d2fd65ca7443bc6',
  ),
  'c785fd2b034df9ba0f8efc61ac32ab36' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Versions for <%object_name> [%version_count]',
    'comment' => NULL,
    'translation' => '<%object_name> のバージョン [%version_count]',
    'key' => 'c785fd2b034df9ba0f8efc61ac32ab36',
  ),
  'ad5a7753db16a975e4f9ef7e89bd8376' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => 'ad5a7753db16a975e4f9ef7e89bd8376',
  ),
  '464a7881651f45312ecfea2b9dddbb5c' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Status',
    'comment' => NULL,
    'translation' => 'ステータス',
    'key' => '464a7881651f45312ecfea2b9dddbb5c',
  ),
  'e341007c1e24deefe6a562cc39e7a706' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Creator',
    'comment' => NULL,
    'translation' => '作成者',
    'key' => 'e341007c1e24deefe6a562cc39e7a706',
  ),
  '2c6eb6935e6d277548edabfd82e85765' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Created',
    'comment' => NULL,
    'translation' => '作成日時',
    'key' => '2c6eb6935e6d277548edabfd82e85765',
  ),
  '295b78b64cebc97c0d255abaad7861ca' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Draft',
    'comment' => NULL,
    'translation' => '下書き',
    'key' => '295b78b64cebc97c0d255abaad7861ca',
  ),
  'f1a949ff6ea3ae53641a861249a26e4f' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Published',
    'comment' => NULL,
    'translation' => '公開中',
    'key' => 'f1a949ff6ea3ae53641a861249a26e4f',
  ),
  '586fb0fb9b102f91850eec5a6cbe9fa8' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Pending',
    'comment' => NULL,
    'translation' => '保留中',
    'key' => '586fb0fb9b102f91850eec5a6cbe9fa8',
  ),
  '26307e9135a95eb77211dd5e46c8e671' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Archived',
    'comment' => NULL,
    'translation' => '保管',
    'key' => '26307e9135a95eb77211dd5e46c8e671',
  ),
  'd367daf73ee04b8603f1d6ca25c94b74' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Rejected',
    'comment' => NULL,
    'translation' => '非承認',
    'key' => 'd367daf73ee04b8603f1d6ca25c94b74',
  ),
  '5014a531096b8c002474d5da2acae508' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => '5014a531096b8c002474d5da2acae508',
  ),
  'f540b7a615b5121d8ad400ea7576c23c' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => 'f540b7a615b5121d8ad400ea7576c23c',
  ),
  '8a5c1d076ce21447cbda042b3a1e142f' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Object information',
    'comment' => NULL,
    'translation' => 'オブジェクトプロパティ',
    'key' => '8a5c1d076ce21447cbda042b3a1e142f',
  ),
  '51c4b431b9c596a51ebd1b0863c67603' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'ID',
    'comment' => NULL,
    'translation' => 'ID',
    'key' => '51c4b431b9c596a51ebd1b0863c67603',
  ),
  '51f35cb5d625d00ed45035df5eb64d65' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Not yet published',
    'comment' => NULL,
    'translation' => '未公開',
    'key' => '51f35cb5d625d00ed45035df5eb64d65',
  ),
  '516227760fe7b226fdccea1d627f6ae5' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '516227760fe7b226fdccea1d627f6ae5',
  ),
  'e1d7fca194ed94a2e37b7c01d9f31533' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Published version',
    'comment' => NULL,
    'translation' => '公開バージョン',
    'key' => 'e1d7fca194ed94a2e37b7c01d9f31533',
  ),
  '4f667e1c34b0e708f4232a08dc41c5a8' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Select version #%version_number for removal.',
    'comment' => NULL,
    'translation' => '削除するバージョン #%version_number を選択',
    'key' => '4f667e1c34b0e708f4232a08dc41c5a8',
  ),
  '8489c7f0e0c1851a0e68bc2cdc3a0c35' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'View the contents of version #%version_number. Translation: %translation.',
    'comment' => NULL,
    'translation' => 'バージョン #%version_number を表示。 翻訳: %translation',
    'key' => '8489c7f0e0c1851a0e68bc2cdc3a0c35',
  ),
  'ba648f05dbef6cc83edf3822fbb0838d' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Copy',
    'comment' => NULL,
    'translation' => '複製',
    'key' => 'ba648f05dbef6cc83edf3822fbb0838d',
  ),
  '95f3c664296e291d349e7466b54cf822' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Create a copy of version #%version_number.',
    'comment' => NULL,
    'translation' => 'バージョン #%version_number の複製を作成',
    'key' => '95f3c664296e291d349e7466b54cf822',
  ),
  'd3d38ec5d3634999856c28221efc1057' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Edit the contents of version #%version_number.',
    'comment' => NULL,
    'translation' => 'コンテンツバージョン #%version_number を編集',
    'key' => 'd3d38ec5d3634999856c28221efc1057',
  ),
  '1ec56974d3338febbb24c1b3969629e3' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'This object does not have any versions.',
    'comment' => NULL,
    'translation' => 'このオブジェクトにバージョンはありません。',
    'key' => '1ec56974d3338febbb24c1b3969629e3',
  ),
  '056fb811804aae0288628f39fa2de0fb' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Remove the selected versions from the object.',
    'comment' => NULL,
    'translation' => '選択したバージョンを削除',
    'key' => '056fb811804aae0288628f39fa2de0fb',
  ),
  'cbf6b3050d452e8265283e24fa5832a9' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Untouched draft',
    'comment' => NULL,
    'translation' => '未変更の下書き',
    'key' => 'cbf6b3050d452e8265283e24fa5832a9',
  ),
  '66c36ab823ef865b0a2f2fd12ff001e1' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Back',
    'comment' => NULL,
    'translation' => '戻る',
    'key' => '66c36ab823ef865b0a2f2fd12ff001e1',
  ),
  'e115db0edc05c09d9ac3504585464645' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Version is not a draft',
    'comment' => NULL,
    'translation' => 'バージョンは下書きではありません',
    'key' => 'e115db0edc05c09d9ac3504585464645',
  ),
  'e42ffafccac34d8c17b6027decd8ac54' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Version %1 is not available for editing anymore. Only drafts can be edited.',
    'comment' => NULL,
    'translation' => 'バージョン%1は編集することができません。下書きのみ編集できます。',
    'key' => 'e42ffafccac34d8c17b6027decd8ac54',
  ),
  '9284f1d61fa6be814c1ab1227c948c3b' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'To edit this version, first create a copy of it.',
    'comment' => NULL,
    'translation' => 'このバージョンを編集するには、先に複製を作成して下さい。',
    'key' => '9284f1d61fa6be814c1ab1227c948c3b',
  ),
  '5fd3415cdc735214f6888b12f63a7d1b' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Version is not yours',
    'comment' => NULL,
    'translation' => 'バージョンの所有者ではありません',
    'key' => '5fd3415cdc735214f6888b12f63a7d1b',
  ),
  'b5ecebe5f944e0b0d3b5d8c04dd76658' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Version %1 was not created by you. You can only edit your own drafts.',
    'comment' => NULL,
    'translation' => 'バージョン%1の作成者ではありません。自分が作成した下書きしか編集できません。',
    'key' => 'b5ecebe5f944e0b0d3b5d8c04dd76658',
  ),
  '394625a84b25529c140d7d74b3bd6a5f' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Modified translation',
    'comment' => NULL,
    'translation' => '変更された翻訳',
    'key' => '394625a84b25529c140d7d74b3bd6a5f',
  ),
  'b61abf457afc418586ced5c22e7aa2bf' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Version #%version_number cannot be removed because it is either the published version of the object or because you do not have permission to remove it.',
    'comment' => NULL,
    'translation' => '公開されているバージョンか、削除する権限を持っていないため、バージョン#%version_numberを削除することはできません。',
    'key' => 'b61abf457afc418586ced5c22e7aa2bf',
  ),
  '3011d058caaf47299f683dda9bf59a87' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'There is no need to make copies of untouched drafts.',
    'comment' => NULL,
    'translation' => '変更されていない下書きを複製することはできません。',
    'key' => '3011d058caaf47299f683dda9bf59a87',
  ),
  'b17b6d8995028361619832bdc1c91800' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'You cannot make copies of versions because you do not have permission to edit the object.',
    'comment' => NULL,
    'translation' => 'オブジェクトの編集権限がないため、バージョンの複製を作成することはできません。',
    'key' => 'b17b6d8995028361619832bdc1c91800',
  ),
  '4e3597f4e5179db8a1c6c396242c8326' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'You cannot edit the contents of version #%version_number either because it is not a draft or because you do not have permission to edit the object.',
    'comment' => NULL,
    'translation' => '下書きではないか、編集権限がないため、バージョン#%version_numberの編集はできません。',
    'key' => '4e3597f4e5179db8a1c6c396242c8326',
  ),
  'b582640011fc5eaa5aa1e2e38edf5b46' => 
  array (
    'context' => 'design/admin/content/versions',
    'source' => 'Versions for <%object_name> (%version_count)',
    'comment' => NULL,
    'translation' => '<%object_name> のバージョン (%version_count)',
    'key' => 'b582640011fc5eaa5aa1e2e38edf5b46',
  ),
);
?>
