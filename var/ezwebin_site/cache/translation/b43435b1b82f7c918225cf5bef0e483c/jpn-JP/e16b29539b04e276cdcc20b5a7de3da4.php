<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/shop/vatrules',
);

$TranslationRoot = array (
  'b32fb2e0e13ebf2bedc761012d965194' => 
  array (
    'context' => 'kernel/shop/vatrules',
    'source' => 'No default rule found. Please add rule having "Any" country and "Any" category.',
    'comment' => NULL,
    'translation' => 'デフォルトのルールが見つかりません。国名 \'指定なし\' やカテゴリー \'指定なし\' を選択出来るルールを追加してください。',
    'key' => 'b32fb2e0e13ebf2bedc761012d965194',
  ),
  'ed133239f41f83beaf94f9346fe98a26' => 
  array (
    'context' => 'kernel/shop/vatrules',
    'source' => 'VAT rules',
    'comment' => NULL,
    'translation' => '課税ルール',
    'key' => 'ed133239f41f83beaf94f9346fe98a26',
  ),
);
?>
