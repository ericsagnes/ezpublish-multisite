<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/role/createpolicystep1',
);

$TranslationRoot = array (
  'c975a4ea54cda1b737bdca2928baf75c' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'Create a new policy for the <%role_name> role',
    'comment' => NULL,
    'translation' => '<%role_name> ロールの新しいポリシーの作成',
    'key' => 'c975a4ea54cda1b737bdca2928baf75c',
  ),
  '8b0d21e0c6c57ae2677e09acaae7a305' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'Step one: select module',
    'comment' => NULL,
    'translation' => 'ステップ1: モジュールの選択',
    'key' => '8b0d21e0c6c57ae2677e09acaae7a305',
  ),
  'c13fbdcd19822c61c0eeb9d876d13906' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'Instructions',
    'comment' => NULL,
    'translation' => '説明',
    'key' => 'c13fbdcd19822c61c0eeb9d876d13906',
  ),
  'c748d5b56363da75d0ed55334dd6ba6c' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'Click one of the "Grant.." buttons (explained below) in order to go to the next step.',
    'comment' => NULL,
    'translation' => '二つの“アクセス権を設定”ボタンから適切な方をクリックし次のステップに進みます。',
    'key' => 'c748d5b56363da75d0ed55334dd6ba6c',
  ),
  'd66074da2d8ee5447fbe33d367a37f99' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'The "Grant access to all functions" button will create a policy that grants unlimited access to all functions of the selected module. If you wish to limit the access method to a specific function, use the "Grant access to a function" button. Please note that function limitation is only supported by some modules (the next step will reveal if it works or not).',
    'comment' => NULL,
    'translation' => '“全ファンクションのアクセス権を設定”ボタンは選択したモジュールの全ファンクションに対するアクセス権を持つポリシーを作成します。制限されたアクセス権が必要な場合は“指定ファンクションのアクセス権を設定”ボタンを使用します。 このファンクション制限はすべてのモジュールでサポートされているものではないので注意してください（次のステップで設定がサポートされているか確認できます）。',
    'key' => 'd66074da2d8ee5447fbe33d367a37f99',
  ),
  '5590f56f24951f1a43e9fd1622e927af' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'Module',
    'comment' => NULL,
    'translation' => 'モジュール',
    'key' => '5590f56f24951f1a43e9fd1622e927af',
  ),
  'f149d41573eb33e4ba054506535cc92a' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'Every module',
    'comment' => NULL,
    'translation' => 'すべてのモジュール',
    'key' => 'f149d41573eb33e4ba054506535cc92a',
  ),
  '67ed421a42a9f223ee4a3656e02a43d9' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'Grant access to all functions',
    'comment' => NULL,
    'translation' => '全ファンクションのアクセス権を設定',
    'key' => '67ed421a42a9f223ee4a3656e02a43d9',
  ),
  '964b0af17ced4148e0688dda2dd6bae8' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'Grant access to one function',
    'comment' => NULL,
    'translation' => '指定ファンクションのアクセス権を設定',
    'key' => '964b0af17ced4148e0688dda2dd6bae8',
  ),
  '05645a768e0513b95d85b1f9169b3165' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => '05645a768e0513b95d85b1f9169b3165',
  ),
  'a0b28c1adc1a684d9ad4a8d408790b70' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => 'a0b28c1adc1a684d9ad4a8d408790b70',
  ),
  '72ace5365fffdf646175cbd4bcf44e02' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'Welcome to the policy wizard. This three-step wizard will help you create a new policy that will be added to the role that is currently being edited. The wizard can be aborted at any stage by using the "Cancel" button.',
    'comment' => NULL,
    'translation' => 'ポリシーウィザードへようこそ。この３ステップウィザードは新しいポリシーのセットアップをお手伝いします。現在編集されているロールにポリシーを追加します。”キャンセル”ボタンをクリックすると、いつでもウィザードを取り消すことが出来ます。',
    'key' => '72ace5365fffdf646175cbd4bcf44e02',
  ),
  'b64083f3c6ee2bd19f1594e7694ef56d' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'Use the drop-down menu to select the module that you want to grant access to.',
    'comment' => NULL,
    'translation' => 'ドロップダウンメニューを使って、アクセスを与えたいモジュールを選択',
    'key' => 'b64083f3c6ee2bd19f1594e7694ef56d',
  ),
  'f104fc25809482a36b1278577117cf44' => 
  array (
    'context' => 'design/admin/role/createpolicystep1',
    'source' => 'Every function',
    'comment' => NULL,
    'translation' => 'すべてのファンクション',
    'key' => 'f104fc25809482a36b1278577117cf44',
  ),
);
?>
