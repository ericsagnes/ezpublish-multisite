<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/shop/browse_discountgroup',
);

$TranslationRoot = array (
  '1a032d9cd420144ace5654645af8795e' => 
  array (
    'context' => 'design/admin/shop/browse_discountgroup',
    'source' => 'Choose products for discount group',
    'comment' => NULL,
    'translation' => 'ディスカウント対象商品の選択',
    'key' => '1a032d9cd420144ace5654645af8795e',
  ),
  '9798a2c97230b4398d81f0a4facd0cbe' => 
  array (
    'context' => 'design/admin/shop/browse_discountgroup',
    'source' => 'Use the checkboxes to select products to be included in the discount group.',
    'comment' => NULL,
    'translation' => 'チェックボックスを使用して、ディスカウント対象に含める商品を選択して下さい。',
    'key' => '9798a2c97230b4398d81f0a4facd0cbe',
  ),
  '04c3a4d6033051006a2fd1dce9fc29e4' => 
  array (
    'context' => 'design/admin/shop/browse_discountgroup',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => '04c3a4d6033051006a2fd1dce9fc29e4',
  ),
);
?>
