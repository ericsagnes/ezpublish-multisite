<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/setup/db',
);

$TranslationRoot = array (
  'aef54baeeb91c36ce32281f30e2281ed' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'If you are having problems connecting to your database you should take a look at',
    'comment' => NULL,
    'translation' => 'データベース接続に支障がある場合はこちらを確認下さい: ',
    'key' => 'aef54baeeb91c36ce32281f30e2281ed',
  ),
  '3479d0caded27299dba2f10fdcf12a97' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'at',
    'comment' => NULL,
    'translation' => '次のリンク (英文) から参照してください',
    'key' => '3479d0caded27299dba2f10fdcf12a97',
  ),
  '2239fd33a8b770f97b563fc2bfc5c8dc' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'MySQL',
    'comment' => NULL,
    'translation' => 'MySQL',
    'key' => '2239fd33a8b770f97b563fc2bfc5c8dc',
  ),
  '8c84f38baeaa516c1e214eebda9809bc' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'Introduction',
    'comment' => NULL,
    'translation' => 'ご紹介',
    'key' => '8c84f38baeaa516c1e214eebda9809bc',
  ),
  '7a701763b4ef4f84de9d3044a4c43154' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'MySQL is a database management system created by MySQL AB.',
    'comment' => NULL,
    'translation' => 'MySQL は MySQL AB が開発したデータベース管理システムです。',
    'key' => '7a701763b4ef4f84de9d3044a4c43154',
  ),
  'a0719a6893dae876cfe6d059cb3a0586' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'MySQL is the world\'s most popular Open Source Database, designed for speed, power and precision in mission critical, heavy load use.',
    'comment' => NULL,
    'translation' => 'MySQL は世界で最も普及しているオープンソースデータベースであり、高負荷利用に耐えうるスピードやパワー、正確さを追求した設計をされています。',
    'key' => 'a0719a6893dae876cfe6d059cb3a0586',
  ),
  '827c942d15054b9d739c17356be2642c' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'More information can be found on',
    'comment' => NULL,
    'translation' => '詳細は次のリンク (英文) を参照してください',
    'key' => '827c942d15054b9d739c17356be2642c',
  ),
  '7ab8bc1fbf01213c265cc9ce6efe3274' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'Details',
    'comment' => NULL,
    'translation' => '補足',
    'key' => '7ab8bc1fbf01213c265cc9ce6efe3274',
  ),
  '2eee4ada9e4fcb9f1e72ec996ec780c2' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'Installation',
    'comment' => NULL,
    'translation' => 'インストール方法',
    'key' => '2eee4ada9e4fcb9f1e72ec996ec780c2',
  ),
  'ab57f4c196a457a8da2dfd235f811c12' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'By using the',
    'comment' => NULL,
    'translation' => '次の Configure オプション',
    'key' => 'ab57f4c196a457a8da2dfd235f811c12',
  ),
  '842b2489baa0c56afbecac3a978393da' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'configuration option you enable PHP to access MySQL databases. If you use this option without specifying the path to MySQL, PHP will use the built-in MySQL client libraries.',
    'comment' => NULL,
    'translation' => 'で PHP が使用する MySQL を指定します。MySQL のパスの記述なしにこのオプションを指定した場合、PHP は内蔵の MySQL ライブラリを使用します。',
    'key' => '842b2489baa0c56afbecac3a978393da',
  ),
  'e78074bba379a2ac5be31ff9af43c58d' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'More information on the MySQL extension can be found at',
    'comment' => NULL,
    'translation' => 'MySQLエクステンションの詳細は次のリンク (英文) を参照してください: ',
    'key' => 'e78074bba379a2ac5be31ff9af43c58d',
  ),
  '2536dee0c3311e1b7d81ca13769768d3' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'PostgreSQL',
    'comment' => NULL,
    'translation' => 'PostgreSQL',
    'key' => '2536dee0c3311e1b7d81ca13769768d3',
  ),
  '9796d7b39bc80901681cd35bf9ee2b0c' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'PostgreSQL is a database management system developed at the University of California at Berkeley Computer Science Department.',
    'comment' => NULL,
    'translation' => 'PostgreSQL はカリフォルニア大学バークレー校コンピュータサイエンス学部で開発されたデータベース管理システムです。',
    'key' => '9796d7b39bc80901681cd35bf9ee2b0c',
  ),
  '45126cb01b36cf9d81813b2c94b48656' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'PostgreSQL is a sophisticated Object-Relational DBMS, supporting almost all SQL constructs, including subselects, transactions, and user-defined types and functions. It is the most advanced open-source database available anywhere.',
    'comment' => NULL,
    'translation' => 'PostgreSQL は、ほぼ全ての SQL 標準規格 (サブSELECT、トランザクション、ユーザ定義型、ユーザ定義関数を含む) をサポートする、洗練されたオブジェクトリレーショナルデータベース管理システムです。 現在入手可能なオープンソースデータベースの中では、最も進化しているものと言えます。',
    'key' => '45126cb01b36cf9d81813b2c94b48656',
  ),
  '8c9a3fe3a574b20c21cf8087c967315e' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'PostgreSQL is a good choice for handling most languages, including Unicode, but may require some configuration to get good speed.',
    'comment' => NULL,
    'translation' => 'PostgreSQL はユニコードを含むほとんどの言語を扱うのに適しています。ただし、高パフォーマンスを得るには多少のチューニングが必要な場合があります。',
    'key' => '8c9a3fe3a574b20c21cf8087c967315e',
  ),
  '4e7e4633daa4e057297e5cea9ae19156' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'In order to enable PostgreSQL support,',
    'comment' => NULL,
    'translation' => 'PHP の PostgreSQL サポートを有効にするには',
    'key' => '4e7e4633daa4e057297e5cea9ae19156',
  ),
  '9ec50d9acc7ce587a22b031695ede7dc' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'is required when you compile PHP.',
    'comment' => NULL,
    'translation' => 'の指定が PHP コンパイル時に必要です。',
    'key' => '9ec50d9acc7ce587a22b031695ede7dc',
  ),
  'c69317ca0ee09088766d10aecd89af19' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'More information on the PostgreSQL extension can be found at',
    'comment' => NULL,
    'translation' => 'PostgreSQLエクステンションの詳細は次のリンク (英文) を参照してください',
    'key' => 'c69317ca0ee09088766d10aecd89af19',
  ),
  '28d3642ecea84ecca0758f9f8570c7e9' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'From their homepage',
    'comment' => NULL,
    'translation' => 'Web サイトからの引用',
    'key' => '28d3642ecea84ecca0758f9f8570c7e9',
  ),
  '7d2808482c6f241ccf9e5c1bc7069406' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'MySQL is a good choice for handling most western languages, and as of version 4.1 it also supports Unicode.',
    'comment' => NULL,
    'translation' => 'MySQL は西欧言語の利用に適しています。バージョン4.1からはユニコードもサポートしています。',
    'key' => '7d2808482c6f241ccf9e5c1bc7069406',
  ),
  '1126ecc97d0f5373a54d37273a3c41f4' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'It is one of the most popular databases in the Open Source community and most often on by default in PHP.',
    'comment' => NULL,
    'translation' => 'オープンソースコミュニティーで一番人気があるデータベースです。PHPとデフォルトでよく使われています。',
    'key' => '1126ecc97d0f5373a54d37273a3c41f4',
  ),
  'eca5cdd44cce755cf497810846fbb3ed' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'MySQL Improved',
    'comment' => NULL,
    'translation' => 'MySQL Improved',
    'key' => 'eca5cdd44cce755cf497810846fbb3ed',
  ),
  'ebee55949f81855c45c7bad9df1f884f' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'configuration option you enable PHP to access MySQL databases through the MySQL Improved extension. If you use this option without specifying the path to MySQL, PHP will use the built-in MySQL client libraries.',
    'comment' => NULL,
    'translation' => 'オプション設定はMySQL Improvedエクステンションを通して、PHPをMySQLデータベースにアクセスさせます。MySQLへのパスを設定せずにこのオプションを使うと、PHPはビルトインのMySQLライブラリを使用します。',
    'key' => 'ebee55949f81855c45c7bad9df1f884f',
  ),
  '061cfd81391d2e1b94a12006bc05e0a6' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'More information on the MySQLi extension can be found at',
    'comment' => NULL,
    'translation' => 'MySQLiエクステンションの詳しい情報は次を参照にしてください: ',
    'key' => '061cfd81391d2e1b94a12006bc05e0a6',
  ),
  '9ea6411e41cba56bec26172b561b3ec4' => 
  array (
    'context' => 'design/standard/setup/db',
    'source' => 'It is a very popular database in the Open Source community and provides highly advanced database functionality.',
    'comment' => NULL,
    'translation' => 'オープンソースコミュニティーでとても人気のあるデータベースであり、高機能なデーターベースを提供します。',
    'key' => '9ea6411e41cba56bec26172b561b3ec4',
  ),
);
?>
