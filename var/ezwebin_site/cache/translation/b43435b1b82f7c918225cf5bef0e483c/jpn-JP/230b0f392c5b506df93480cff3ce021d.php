<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/rss/browse_user',
);

$TranslationRoot = array (
  '573bf852d2b999e8160c08b9f36ce7e7' => 
  array (
    'context' => 'design/admin/rss/browse_user',
    'source' => 'Choose owner for RSS imported objects',
    'comment' => NULL,
    'translation' => 'RSSインポートオブジェクトの所有者の選択',
    'key' => '573bf852d2b999e8160c08b9f36ce7e7',
  ),
  'b42ecaa1d0ac93a59706edbfed06a913' => 
  array (
    'context' => 'design/admin/rss/browse_user',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => 'b42ecaa1d0ac93a59706edbfed06a913',
  ),
  'afb7ef27bf292e106e1c22e56367d5cb' => 
  array (
    'context' => 'design/admin/rss/browse_user',
    'source' => 'Use the radio buttons to choose a user then click "OK". The user will become the owner of the objects that were imported using RSS.',
    'comment' => NULL,
    'translation' => 'ラジオボタンを使って、ユーザを選択し、"OK"をクリックしてください。そのユーザはRSSでインポートされたオブジェクトの所有者になります。',
    'key' => 'afb7ef27bf292e106e1c22e56367d5cb',
  ),
);
?>
