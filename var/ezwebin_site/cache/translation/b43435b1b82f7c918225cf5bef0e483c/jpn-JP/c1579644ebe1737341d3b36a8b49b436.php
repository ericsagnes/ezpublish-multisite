<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/edit_draft',
);

$TranslationRoot = array (
  '26e60d3bd234455415c8c96d1e4a0c0f' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Object information',
    'comment' => NULL,
    'translation' => 'オブジェクトプロパティ',
    'key' => '26e60d3bd234455415c8c96d1e4a0c0f',
  ),
  '1b23a20edf8b1c309b5050954b6ff9d2' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'ID',
    'comment' => NULL,
    'translation' => 'ID',
    'key' => '1b23a20edf8b1c309b5050954b6ff9d2',
  ),
  'f036076908d8c7ae488f532b744688e8' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Created',
    'comment' => NULL,
    'translation' => '作成日時',
    'key' => 'f036076908d8c7ae488f532b744688e8',
  ),
  'ac9f56a348d3008cee0806ce1c4ac56a' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Not yet published',
    'comment' => NULL,
    'translation' => '未公開',
    'key' => 'ac9f56a348d3008cee0806ce1c4ac56a',
  ),
  '9e10eb38720a7e93310c68476f091d29' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '9e10eb38720a7e93310c68476f091d29',
  ),
  'f0e52ca38b7e84ca18a5c4d84cb6393f' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Published version',
    'comment' => NULL,
    'translation' => '公開バージョン',
    'key' => 'f0e52ca38b7e84ca18a5c4d84cb6393f',
  ),
  '990372bcf446b18e7c15d62439955536' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Possible edit conflict',
    'comment' => NULL,
    'translation' => '同時編集の可能性',
    'key' => '990372bcf446b18e7c15d62439955536',
  ),
  '8d410bbd0a581f30d235002de344ad24' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'This object is already being edited by someone else. In addition, it is already being edited by you.',
    'comment' => NULL,
    'translation' => '指定のオブジェクトは別ユーザにより編集状態になっています。また あなた自身による編集中のバージョンも存在しています。',
    'key' => '8d410bbd0a581f30d235002de344ad24',
  ),
  'e99bd1c3cbbb33c4888980faecfdf4ba' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'You should contact the other user(s) to make sure that you are not stepping on anyone\'s toes.',
    'comment' => NULL,
    'translation' => '確認のため、 編集中のユーザに連絡して下さい。',
    'key' => 'e99bd1c3cbbb33c4888980faecfdf4ba',
  ),
  '8b1a510f3aed05827a65bc38b339ebd2' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'The most recently modified draft is version #%version, created by %creator, last changed: %modified.',
    'comment' => NULL,
    'translation' => '最も新しく修正された下書きはバージョン #%version で、 %creator が作成し、 %modified に保存されています。',
    'key' => '8b1a510f3aed05827a65bc38b339ebd2',
  ),
  '060d873892994cf374234fda57e0dad6' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'This object is already being edited by you.',
    'comment' => NULL,
    'translation' => '指定のオブジェクトはすでにあなた自身により編集状態にあります。',
    'key' => '060d873892994cf374234fda57e0dad6',
  ),
  'd029b2d6560427ed6301951660a54c1e' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Your most recently modified draft is version #%version, last changed: %modified.',
    'comment' => NULL,
    'translation' => 'あなたが修正した最新の下書きはバージョン #%version で、 %modified に保存されています。',
    'key' => 'd029b2d6560427ed6301951660a54c1e',
  ),
  '1269777b0d7616ede61de6cec2f14568' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'This object is already being edited by someone else.',
    'comment' => NULL,
    'translation' => '指定のオブジェクトは別ユーザにより編集状態になっています。',
    'key' => '1269777b0d7616ede61de6cec2f14568',
  ),
  '5f5d86178c2a308ddf1f837d3fa5965b' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Possible actions',
    'comment' => NULL,
    'translation' => '選択可能な動作',
    'key' => '5f5d86178c2a308ddf1f837d3fa5965b',
  ),
  'f32f0f27e3fb717ba825c14f9f91184b' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Continue editing one of your drafts.',
    'comment' => NULL,
    'translation' => '（保存されている）あなたの下書きを編集します。',
    'key' => 'f32f0f27e3fb717ba825c14f9f91184b',
  ),
  '9d2491e6e48f1d19d65ff5eb658e1ecd' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Create a new draft and start editing it.',
    'comment' => NULL,
    'translation' => '新たな（別の）下書きを作成し編集します。',
    'key' => '9d2491e6e48f1d19d65ff5eb658e1ecd',
  ),
  '54ed171f8c94bd801d5d4dff1b720377' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Cancel the edit operation.',
    'comment' => NULL,
    'translation' => '今回の編集操作をキャンセルします。',
    'key' => '54ed171f8c94bd801d5d4dff1b720377',
  ),
  '9c79256be4c4bb2d2e1a4608b34c51a0' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Current drafts [%draft_count]',
    'comment' => NULL,
    'translation' => '編集状態の下書き [%draft_count]',
    'key' => '9c79256be4c4bb2d2e1a4608b34c51a0',
  ),
  '5afa4f9c684a339dd54b657698a7194d' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => '5afa4f9c684a339dd54b657698a7194d',
  ),
  '60dbf9980514e90f795b9ee7fc6ed70a' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Translations',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => '60dbf9980514e90f795b9ee7fc6ed70a',
  ),
  '46b1517e63ba4dad32ea887bd9e0fef0' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Creator',
    'comment' => NULL,
    'translation' => '作成者',
    'key' => '46b1517e63ba4dad32ea887bd9e0fef0',
  ),
  'a34d458e27afa923f35100eb0c808804' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Select draft version #%version for editing.',
    'comment' => NULL,
    'translation' => '下書きバージョン #%version を編集対象に選択。',
    'key' => 'a34d458e27afa923f35100eb0c808804',
  ),
  '448c22426fe1ada5e0453c9b7999d0ce' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'View the contents of version #%version_number. Translation: %translation.',
    'comment' => NULL,
    'translation' => 'バージョン #%version_number を表示。 翻訳: %default_translation',
    'key' => '448c22426fe1ada5e0453c9b7999d0ce',
  ),
  'd181a31bd86b6731f7ee31e0368c0e85' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Edit selected',
    'comment' => NULL,
    'translation' => '選択したバージョンを編集',
    'key' => 'd181a31bd86b6731f7ee31e0368c0e85',
  ),
  '17d518b8423e1a66ad3bd2bdaa603873' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Edit the selected draft.',
    'comment' => NULL,
    'translation' => '選択した下書きを編集',
    'key' => '17d518b8423e1a66ad3bd2bdaa603873',
  ),
  '24b574c1b2887647b5a008d6e6018910' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'New draft',
    'comment' => NULL,
    'translation' => '新規下書き',
    'key' => '24b574c1b2887647b5a008d6e6018910',
  ),
  '729a6c1cc8666a29613d12632cc4c89a' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '729a6c1cc8666a29613d12632cc4c89a',
  ),
  '481e4a1534711e35d4efea6f07fd5831' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'View the contents of version #%version. Translation: %translation.',
    'comment' => NULL,
    'translation' => 'バージョン #%version のコンテンツを表示。 翻訳: %translation',
    'key' => '481e4a1534711e35d4efea6f07fd5831',
  ),
  '98423572ae44e3184437d459a9fc96b6' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'The object has already been published by someone else.',
    'comment' => NULL,
    'translation' => 'オブジェクトは他の人によってすでに公開されています。',
    'key' => '98423572ae44e3184437d459a9fc96b6',
  ),
  '41cb12eaf2a611dbdc095431399ea079' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Publish data as it is (and overwriting the published data).',
    'comment' => NULL,
    'translation' => 'そのままの状態で公開（公開中のデータを上書きする）',
    'key' => '41cb12eaf2a611dbdc095431399ea079',
  ),
  '0b31ca0f914fa926c6a0ba60c2f7a2ff' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Go back to editing and show the published data.',
    'comment' => NULL,
    'translation' => '編集に戻ると公開データが表示されます',
    'key' => '0b31ca0f914fa926c6a0ba60c2f7a2ff',
  ),
  '7aa751d0de2111435986de3089c283fd' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Status',
    'comment' => NULL,
    'translation' => 'ステータス',
    'key' => '7aa751d0de2111435986de3089c283fd',
  ),
  'd85c78d812501909862e4f6d39588e16' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Show the published data',
    'comment' => NULL,
    'translation' => '公開データの表示',
    'key' => 'd85c78d812501909862e4f6d39588e16',
  ),
  '2b5040904a9459d27f1e8751a1b8947e' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Conflicting versions [%draft_count]',
    'comment' => NULL,
    'translation' => '同時編集の可能性があるバージョン [%draft_count]',
    'key' => '2b5040904a9459d27f1e8751a1b8947e',
  ),
  '0c30fd940634262968b292bdb02570df' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Create a new draft. The contents of the new draft will be copied from the published version.',
    'comment' => NULL,
    'translation' => '新しい下書きの作成。新しい下書きは公開されているバージョンの複製になります。',
    'key' => '0c30fd940634262968b292bdb02570df',
  ),
  'f86a03cae0309d0d361bdf5172ccffed' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'You cannot select draft version #%version for editing because it belongs to another user. Please select a draft that belongs to you or create a new draft and then edit it.',
    'comment' => NULL,
    'translation' => '違うユーザが所有者のため、バージョン#%versionは編集できません。自分が所有している下書きを選択するか、新しい下書きを作成し、編集してください。',
    'key' => 'f86a03cae0309d0d361bdf5172ccffed',
  ),
  '3183814f0b17958e7b69e1c06e475582' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'You cannot edit any of the drafts because none of them belong to you. You can create a new draft, select it and then edit it.',
    'comment' => NULL,
    'translation' => '下書きの所有者ではないため、編集はできません。新しい下書きを作成し、編集して下さい。',
    'key' => '3183814f0b17958e7b69e1c06e475582',
  ),
  '4465bb6b719ce579efceaa6dda4cbdf4' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Conflicting versions (%draft_count)',
    'comment' => NULL,
    'translation' => '同時編集の可能性があるバージョン (%draft_count)',
    'key' => '4465bb6b719ce579efceaa6dda4cbdf4',
  ),
  '00bb15d8657de60364d2051414cdec02' => 
  array (
    'context' => 'design/admin/content/edit_draft',
    'source' => 'Current drafts (%draft_count)',
    'comment' => NULL,
    'translation' => '編集状態の下書き (%draft_count)',
    'key' => '00bb15d8657de60364d2051414cdec02',
  ),
);
?>
