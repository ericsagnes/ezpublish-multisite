<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/shop/classes/ezcurrencydata',
);

$TranslationRoot = array (
  '7a936545f492d1d854ee35c0a87f5c34' => 
  array (
    'context' => 'kernel/shop/classes/ezcurrencydata',
    'source' => 'Invalid characters in currency code.',
    'comment' => NULL,
    'translation' => '通貨記号に不正な文字が含まれています。',
    'key' => '7a936545f492d1d854ee35c0a87f5c34',
  ),
  'cf3712b055026630dc493552de686c44' => 
  array (
    'context' => 'kernel/shop/classes/ezcurrencydata',
    'source' => 'Currency already exists.',
    'comment' => NULL,
    'translation' => '通貨はすでに存在します。',
    'key' => 'cf3712b055026630dc493552de686c44',
  ),
  '03ed34098817b7203137c756f37a3cd1' => 
  array (
    'context' => 'kernel/shop/classes/ezcurrencydata',
    'source' => 'Unknown error.',
    'comment' => NULL,
    'translation' => '不明なエラー。',
    'key' => '03ed34098817b7203137c756f37a3cd1',
  ),
);
?>
