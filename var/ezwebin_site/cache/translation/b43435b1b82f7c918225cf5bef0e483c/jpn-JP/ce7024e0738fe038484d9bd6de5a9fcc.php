<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/infocollector/view',
);

$TranslationRoot = array (
  'a16923dcba1772cefac2855635fd7849' => 
  array (
    'context' => 'design/admin/infocollector/view',
    'source' => 'Collection #%collection_id for <%object_name>',
    'comment' => NULL,
    'translation' => '<%object_name> で収集したデータ #%collection_id',
    'key' => 'a16923dcba1772cefac2855635fd7849',
  ),
  'be99674aee6af7841c75bff398132d3f' => 
  array (
    'context' => 'design/admin/infocollector/view',
    'source' => 'Last modified',
    'comment' => NULL,
    'translation' => '最新の修正',
    'key' => 'be99674aee6af7841c75bff398132d3f',
  ),
  'c1e2039ab5ee6ab2da31d01177f2fa8d' => 
  array (
    'context' => 'design/admin/infocollector/view',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => 'c1e2039ab5ee6ab2da31d01177f2fa8d',
  ),
  '5ca931858139dd01b347e6979e2057e2' => 
  array (
    'context' => 'design/admin/infocollector/view',
    'source' => 'Remove collection.',
    'comment' => NULL,
    'translation' => 'このデータを削除',
    'key' => '5ca931858139dd01b347e6979e2057e2',
  ),
  '1a4fbc81d9ba9bd4be3d0d52ab725e13' => 
  array (
    'context' => 'design/admin/infocollector/view',
    'source' => 'Unknown user',
    'comment' => NULL,
    'translation' => '不明なユーザ',
    'key' => '1a4fbc81d9ba9bd4be3d0d52ab725e13',
  ),
);
?>
