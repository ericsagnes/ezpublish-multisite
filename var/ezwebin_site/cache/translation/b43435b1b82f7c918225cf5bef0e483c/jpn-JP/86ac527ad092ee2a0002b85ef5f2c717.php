<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/node/view/full',
);

$TranslationRoot = array (
  'dd6a89b737933fa041317fd8d37bf731' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hide preview of content.',
    'comment' => NULL,
    'translation' => 'プレビューを隠す',
    'key' => 'dd6a89b737933fa041317fd8d37bf731',
  ),
  '67f830c20a2be42db493ba51492bc630' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Preview',
    'comment' => NULL,
    'translation' => 'プレビュー',
    'key' => '67f830c20a2be42db493ba51492bc630',
  ),
  'cd540982e7a84cd393b3d2aaa37243cc' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show preview of content.',
    'comment' => NULL,
    'translation' => 'プレビューを表示',
    'key' => 'cd540982e7a84cd393b3d2aaa37243cc',
  ),
  'a89388a01642293d380cf26f6162c159' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hide available translations.',
    'comment' => NULL,
    'translation' => '翻訳一覧を隠す',
    'key' => 'a89388a01642293d380cf26f6162c159',
  ),
  'a32c774f27b70a7be8abff4b42c547f9' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show available translations.',
    'comment' => NULL,
    'translation' => '翻訳一覧の表示',
    'key' => 'a32c774f27b70a7be8abff4b42c547f9',
  ),
  'bdc504a9b5c9b4c6a24539878f232770' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hide location overview.',
    'comment' => NULL,
    'translation' => '配置先一覧を隠す',
    'key' => 'bdc504a9b5c9b4c6a24539878f232770',
  ),
  '4ac6a2d80b02e1dd8ac4e999f0bdb8b4' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Locations',
    'comment' => NULL,
    'translation' => '配置先',
    'key' => '4ac6a2d80b02e1dd8ac4e999f0bdb8b4',
  ),
  '68640265ac0b5601a2aa1894efd09848' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show location overview.',
    'comment' => NULL,
    'translation' => '配置先一覧の表示',
    'key' => '68640265ac0b5601a2aa1894efd09848',
  ),
  'a246da0db45738a1435e9bb18e12c92a' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hide relation overview.',
    'comment' => NULL,
    'translation' => '関連一覧を隠す',
    'key' => 'a246da0db45738a1435e9bb18e12c92a',
  ),
  'e3256ab675c725523dd6df97b8fd497c' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Relations',
    'comment' => NULL,
    'translation' => '関連',
    'key' => 'e3256ab675c725523dd6df97b8fd497c',
  ),
  'ac9a0f0f616f320b753654395a139c0c' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show relation overview.',
    'comment' => NULL,
    'translation' => '関連一覧の表示',
    'key' => 'ac9a0f0f616f320b753654395a139c0c',
  ),
  'eea2e43a5367ef0b03b2bd36eaf60645' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hide role overview.',
    'comment' => NULL,
    'translation' => 'ロール一覧を隠す',
    'key' => 'eea2e43a5367ef0b03b2bd36eaf60645',
  ),
  'c036ef551cdf6e13fafef24a4b63a282' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Roles',
    'comment' => NULL,
    'translation' => 'ロール',
    'key' => 'c036ef551cdf6e13fafef24a4b63a282',
  ),
  'a9ed84c961300fc07f33ab9f78ed2722' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show role overview.',
    'comment' => NULL,
    'translation' => 'ロール一覧を表示',
    'key' => 'a9ed84c961300fc07f33ab9f78ed2722',
  ),
  '9b7f2ac28dcba5f7078630b0d85b2074' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hide policy overview.',
    'comment' => NULL,
    'translation' => 'ポリシー一覧を隠す',
    'key' => '9b7f2ac28dcba5f7078630b0d85b2074',
  ),
  'a3615d6fa623067e4ad91325f970d027' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Policies',
    'comment' => NULL,
    'translation' => 'ポリシー',
    'key' => 'a3615d6fa623067e4ad91325f970d027',
  ),
  '8d0e34abb46f5a48d8cc1b45eb0870c2' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show policy overview.',
    'comment' => NULL,
    'translation' => 'ポリシー一覧を表示',
    'key' => '8d0e34abb46f5a48d8cc1b45eb0870c2',
  ),
  '14f8e1737f5ba2210cc6abf80bf7bea4' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Up one level',
    'comment' => NULL,
    'translation' => '上の階層へ移動',
    'key' => '14f8e1737f5ba2210cc6abf80bf7bea4',
  ),
  'cbe2110cf025ffce31d4cba7a38639b3' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Sub items [%children_count]',
    'comment' => NULL,
    'translation' => '子アイテム [%children_count]',
    'key' => 'cbe2110cf025ffce31d4cba7a38639b3',
  ),
  'a532071f95fcfc8bde705c7f495b2eca' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'The current item does not contain any sub items.',
    'comment' => NULL,
    'translation' => 'このアイテムに子アイテムはありません。',
    'key' => 'a532071f95fcfc8bde705c7f495b2eca',
  ),
  '0280bdaee7ff796edff91dc50026a496' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択項目の削除',
    'key' => '0280bdaee7ff796edff91dc50026a496',
  ),
  'fd63ea45fa0cdb7e828fed114f5fea01' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Remove the selected items from the list above.',
    'comment' => NULL,
    'translation' => '選択したアイテムを削除します。',
    'key' => 'fd63ea45fa0cdb7e828fed114f5fea01',
  ),
  'b9bfb031caeb29601318e203f03926e8' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Update priorities',
    'comment' => NULL,
    'translation' => '優先度の更新',
    'key' => 'b9bfb031caeb29601318e203f03926e8',
  ),
  'ed674ae7ae07f67ecfee1db2dcf906be' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Apply changes to the priorities of the items in the list above.',
    'comment' => NULL,
    'translation' => '子アイテムリストに設定した優先度を適用します。',
    'key' => 'ed674ae7ae07f67ecfee1db2dcf906be',
  ),
  'dbb74ee9fe1c48e75cdc81b3b61476bd' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Create here',
    'comment' => NULL,
    'translation' => '作成',
    'key' => 'dbb74ee9fe1c48e75cdc81b3b61476bd',
  ),
  '945823581d87399ab88171e72d2a3ae0' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Depth',
    'comment' => NULL,
    'translation' => '階層',
    'key' => '945823581d87399ab88171e72d2a3ae0',
  ),
  '9b1e33fc2041e614a815f78210394b8a' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '9b1e33fc2041e614a815f78210394b8a',
  ),
  'a085ee55c2dc40a9242a7d25feceb169' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => 'a085ee55c2dc40a9242a7d25feceb169',
  ),
  'dca5baa116ca5129446a5eecef9c3800' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Priority',
    'comment' => NULL,
    'translation' => '優先度',
    'key' => 'dca5baa116ca5129446a5eecef9c3800',
  ),
  '0c1aa5618cd41577e42dc276278a09c1' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Published',
    'comment' => NULL,
    'translation' => '公開日時',
    'key' => '0c1aa5618cd41577e42dc276278a09c1',
  ),
  '62e24a30c399a504f02c2dff5b2f09c0' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Section',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => '62e24a30c399a504f02c2dff5b2f09c0',
  ),
  'ff2146d63a44bb6c1cf8ffdab0d32856' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Set',
    'comment' => NULL,
    'translation' => '設定',
    'key' => 'ff2146d63a44bb6c1cf8ffdab0d32856',
  ),
  'b5e5d0206e9670c4d8bd8db0ecc7358c' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Invert selection.',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => 'b5e5d0206e9670c4d8bd8db0ecc7358c',
  ),
  '9772d79fd4669fadbbcdbce13470d74a' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Modifier',
    'comment' => NULL,
    'translation' => '修正者',
    'key' => '9772d79fd4669fadbbcdbce13470d74a',
  ),
  '196119fbd7df1dde678447fb568d3c14' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Copy',
    'comment' => NULL,
    'translation' => '複製',
    'key' => '196119fbd7df1dde678447fb568d3c14',
  ),
  'b4275613addc02efa828698d8f3b542c' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Create a copy of <%child_name>.',
    'comment' => NULL,
    'translation' => '<%child_name>を複製',
    'key' => 'b4275613addc02efa828698d8f3b542c',
  ),
  'ff1d4131a63573e78f2b765261e132ec' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => 'ff1d4131a63573e78f2b765261e132ec',
  ),
  'd5cac71804550b9ba66b9a0626718f61' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Edit <%child_name>.',
    'comment' => NULL,
    'translation' => '<%child_name>を編集',
    'key' => 'd5cac71804550b9ba66b9a0626718f61',
  ),
  'de5b563a0b0ee549ffdda18e157b7bf2' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Creator',
    'comment' => NULL,
    'translation' => '作成者',
    'key' => 'de5b563a0b0ee549ffdda18e157b7bf2',
  ),
  '02f31e1f276d5e95ee0b98e2c70d8987' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Created',
    'comment' => NULL,
    'translation' => '作成日時',
    'key' => '02f31e1f276d5e95ee0b98e2c70d8987',
  ),
  'b9329dfcd740364a0373bdb8d84cfe16' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Versions',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => 'b9329dfcd740364a0373bdb8d84cfe16',
  ),
  '069920997b2720fc9cfb77fa60b6166a' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Translations',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => '069920997b2720fc9cfb77fa60b6166a',
  ),
  '912c5a93b7405585fcdfdb0101fa08f3' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Node ID',
    'comment' => NULL,
    'translation' => 'ノードID',
    'key' => '912c5a93b7405585fcdfdb0101fa08f3',
  ),
  '8e1b574eebac33326673da2cf161c3e9' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Object ID',
    'comment' => NULL,
    'translation' => 'オブジェクトID',
    'key' => '8e1b574eebac33326673da2cf161c3e9',
  ),
  '474eb8b408b2966250179091c05b20b5' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Language',
    'comment' => NULL,
    'translation' => '言語',
    'key' => '474eb8b408b2966250179091c05b20b5',
  ),
  'baa15e0ef05d5c553b8db2a40ed91db1' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Locale',
    'comment' => NULL,
    'translation' => 'ロケール',
    'key' => 'baa15e0ef05d5c553b8db2a40ed91db1',
  ),
  'f3ddf2f7ca481f871af66230343553fe' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Location',
    'comment' => NULL,
    'translation' => '配置先',
    'key' => 'f3ddf2f7ca481f871af66230343553fe',
  ),
  'be0b14f0824c8a8670d390ca3307406f' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Sorting',
    'comment' => NULL,
    'translation' => 'ソート',
    'key' => 'be0b14f0824c8a8670d390ca3307406f',
  ),
  '51afd47bc076a34b9ca97ec9c04fbcf9' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Main',
    'comment' => NULL,
    'translation' => '主要ノード',
    'key' => '51afd47bc076a34b9ca97ec9c04fbcf9',
  ),
  'f14d4dca021d771d5c519fd8f44747bd' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'up',
    'comment' => NULL,
    'translation' => '一つ上へ',
    'key' => 'f14d4dca021d771d5c519fd8f44747bd',
  ),
  'a95a29a272b82fc1ffd1b67a08bf5b7c' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'down',
    'comment' => NULL,
    'translation' => '一つ下へ',
    'key' => 'a95a29a272b82fc1ffd1b67a08bf5b7c',
  ),
  '81ddcea1e95002eb1787e69bca4d6e86' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Remove selected locations from the list above.',
    'comment' => NULL,
    'translation' => '選択した配置先を削除します。',
    'key' => '81ddcea1e95002eb1787e69bca4d6e86',
  ),
  '28788f8376014782c3c8edf4b8456186' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Set main',
    'comment' => NULL,
    'translation' => '主要ノードに設定',
    'key' => '28788f8376014782c3c8edf4b8456186',
  ),
  '28c2d4ef387fdc1a515403bb79aa44c2' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Last modified',
    'comment' => NULL,
    'translation' => '最新の修正',
    'key' => '28c2d4ef387fdc1a515403bb79aa44c2',
  ),
  '85f30c4be008a73f2d507017bc3cef73' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Move',
    'comment' => NULL,
    'translation' => '移動',
    'key' => '85f30c4be008a73f2d507017bc3cef73',
  ),
  'c0f94712fd1b56af218ed00e307938ab' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Move this item to another location.',
    'comment' => NULL,
    'translation' => 'このアイテムを移動',
    'key' => 'c0f94712fd1b56af218ed00e307938ab',
  ),
  '4a8ccd9640bce4f2bf0577b145e0c8f9' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => '4a8ccd9640bce4f2bf0577b145e0c8f9',
  ),
  'b0e37c1cc0206f9c325601254a4ddc87' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Remove this item.',
    'comment' => NULL,
    'translation' => 'このアイテムを削除',
    'key' => 'b0e37c1cc0206f9c325601254a4ddc87',
  ),
  '6054b9df585828bffdc591805e0e9de0' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Function',
    'comment' => NULL,
    'translation' => 'ファンクション',
    'key' => '6054b9df585828bffdc591805e0e9de0',
  ),
  '49db349a81f9e12597041092288dbf11' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Limitation',
    'comment' => NULL,
    'translation' => '制限',
    'key' => '49db349a81f9e12597041092288dbf11',
  ),
  '0307175f0c7e2fa885e1cd304dbbf886' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'all modules',
    'comment' => NULL,
    'translation' => 'すべてのモジュール',
    'key' => '0307175f0c7e2fa885e1cd304dbbf886',
  ),
  'f91fab2ad23519e2fdc7be54a65f6b1a' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'all functions',
    'comment' => NULL,
    'translation' => 'すべてのファンクション',
    'key' => 'f91fab2ad23519e2fdc7be54a65f6b1a',
  ),
  '1cee940da6cd4c397ae7d57cbf44e461' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'There are no available policies.',
    'comment' => NULL,
    'translation' => '使用可能なポリシーはありません',
    'key' => '1cee940da6cd4c397ae7d57cbf44e461',
  ),
  'e9245753c95a8c56873be3e64d6f6c48' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Related objects [%related_objects_count]',
    'comment' => NULL,
    'translation' => '関連付けしたオブジェクト: [%related_objects_count]',
    'key' => 'e9245753c95a8c56873be3e64d6f6c48',
  ),
  'e5b0510cecd56036ccdfeb8c455317c9' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'The item being viewed does not make use of any other objects.',
    'comment' => NULL,
    'translation' => 'このアイテムから関連付けしたオブジェクトはありません',
    'key' => 'e5b0510cecd56036ccdfeb8c455317c9',
  ),
  'c328d5f0b760fd3200c235f146e5a151' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Assigned roles [%roles_count]',
    'comment' => NULL,
    'translation' => '割り当て済ロール [%roles_count]',
    'key' => 'c328d5f0b760fd3200c235f146e5a151',
  ),
  'a8e4b95420e98c35055dea09347e0db5' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'No limitation',
    'comment' => NULL,
    'translation' => '制限なし',
    'key' => 'a8e4b95420e98c35055dea09347e0db5',
  ),
  '1536d8a93460a383e7a32b0b1280f5b9' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Edit role.',
    'comment' => NULL,
    'translation' => 'ロールの編集',
    'key' => '1536d8a93460a383e7a32b0b1280f5b9',
  ),
  'dfafe07cbc7048a62d18e2d1e2e43de8' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'There are no assigned roles.',
    'comment' => NULL,
    'translation' => '割り当てられたロールはありません',
    'key' => 'dfafe07cbc7048a62d18e2d1e2e43de8',
  ),
  '8fc3c677eac72f289da8cd12967d8311' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hide details.',
    'comment' => NULL,
    'translation' => 'プロパティを隠す',
    'key' => '8fc3c677eac72f289da8cd12967d8311',
  ),
  '6de476bc6cb15c9b6abb99cd92b781d5' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Details',
    'comment' => NULL,
    'translation' => 'プロパティ',
    'key' => '6de476bc6cb15c9b6abb99cd92b781d5',
  ),
  '06cba8af8f9fa9b84e1c248566561110' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show details.',
    'comment' => NULL,
    'translation' => 'プロパティの表示',
    'key' => '06cba8af8f9fa9b84e1c248566561110',
  ),
  'eb3dc4e00aefb84553d7ee459a380fc7' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Up one level.',
    'comment' => NULL,
    'translation' => '上の階層へ移動',
    'key' => 'eb3dc4e00aefb84553d7ee459a380fc7',
  ),
  'def147829b4bc3baa7753cff1c5fbdf1' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show 10 items per page.',
    'comment' => NULL,
    'translation' => '10アイテム/ページ',
    'key' => 'def147829b4bc3baa7753cff1c5fbdf1',
  ),
  '9bec441ffe7546b4d5afc2f126082290' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show 50 items per page.',
    'comment' => NULL,
    'translation' => '50アイテム/ページ',
    'key' => '9bec441ffe7546b4d5afc2f126082290',
  ),
  'fe5ad89c87cb07b4613a7143ba33b208' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show 25 items per page.',
    'comment' => NULL,
    'translation' => '25アイテム/ページ',
    'key' => 'fe5ad89c87cb07b4613a7143ba33b208',
  ),
  '230ad1ae43ebc2e04a36916e14aa36b8' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Display sub items using a simple list.',
    'comment' => NULL,
    'translation' => '子アイテムを一覧形式で表示',
    'key' => '230ad1ae43ebc2e04a36916e14aa36b8',
  ),
  'bc19f8aa991cea210d8d79f14f446687' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'List',
    'comment' => NULL,
    'translation' => '一覧',
    'key' => 'bc19f8aa991cea210d8d79f14f446687',
  ),
  '491b8cd48f5c47e3915caeed994c00a3' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Thumbnail',
    'comment' => NULL,
    'translation' => 'サムネイル',
    'key' => '491b8cd48f5c47e3915caeed994c00a3',
  ),
  'e215d9ec62964b733d32bfa69b25652f' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Display sub items using a detailed list.',
    'comment' => NULL,
    'translation' => '子アイテムを詳細形式で表示',
    'key' => 'e215d9ec62964b733d32bfa69b25652f',
  ),
  'f7c0b6be45055b66c0237c7614833c38' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Detailed',
    'comment' => NULL,
    'translation' => 'プロパティ',
    'key' => 'f7c0b6be45055b66c0237c7614833c38',
  ),
  '6ff00d8f3cb8830bb0c1cae966b6039b' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Display sub items as thumbnails.',
    'comment' => NULL,
    'translation' => '子アイテムをアイコン形式で表示',
    'key' => '6ff00d8f3cb8830bb0c1cae966b6039b',
  ),
  'ca66e51b6a3590434ca5fce2c109ff1e' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Not available',
    'comment' => NULL,
    'translation' => '利用できません',
    'key' => 'ca66e51b6a3590434ca5fce2c109ff1e',
  ),
  '1d9a84ba3b62192eb8f7a186bc4574d0' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Class identifier',
    'comment' => NULL,
    'translation' => 'クラス識別子',
    'key' => '1d9a84ba3b62192eb8f7a186bc4574d0',
  ),
  'f8c955f259cb620775324c3578ca7c4b' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Class name',
    'comment' => NULL,
    'translation' => 'クラス名',
    'key' => 'f8c955f259cb620775324c3578ca7c4b',
  ),
  '842f10c6ffcbca6eec0fac307b08156b' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Use these controls to set the sorting method for the sub items of the current location.',
    'comment' => NULL,
    'translation' => 'このメニューで子アイテムのソート方法を指定します。',
    'key' => '842f10c6ffcbca6eec0fac307b08156b',
  ),
  '081c20756d5743a1fa8c240c655bb428' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Descending',
    'comment' => NULL,
    'translation' => '降順',
    'key' => '081c20756d5743a1fa8c240c655bb428',
  ),
  '9a0e61fc38607226ae5876d71990d677' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Ascending',
    'comment' => NULL,
    'translation' => '昇順',
    'key' => '9a0e61fc38607226ae5876d71990d677',
  ),
  '7be6e16c8c5a8e55afa53e5fdffb7ad3' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Visibility',
    'comment' => NULL,
    'translation' => '表示設定',
    'key' => '7be6e16c8c5a8e55afa53e5fdffb7ad3',
  ),
  '30bd4e0d1a52e8afff9bbc9172842606' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Type',
    'comment' => NULL,
    'translation' => 'タイプ',
    'key' => '30bd4e0d1a52e8afff9bbc9172842606',
  ),
  '769187c9d96dc75315d4fc0d15bfa065' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Move <%child_name> to another location.',
    'comment' => NULL,
    'translation' => '<%child_name>を移動',
    'key' => '769187c9d96dc75315d4fc0d15bfa065',
  ),
  'bdc28b1bb0beb2263ff2778891b8eee8' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Locations [%locations]',
    'comment' => NULL,
    'translation' => '配置先 [%locations]',
    'key' => 'bdc28b1bb0beb2263ff2778891b8eee8',
  ),
  'ca2e074058f958d82848ba933ee2abd5' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Sub items',
    'comment' => NULL,
    'translation' => '子アイテム数',
    'key' => 'ca2e074058f958d82848ba933ee2abd5',
  ),
  'b6df565e228a852a708f0a6af242e143' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Select location for removal.',
    'comment' => NULL,
    'translation' => '削除する配置先を選択',
    'key' => 'b6df565e228a852a708f0a6af242e143',
  ),
  'ebfe1dc5234a1fcca8dc7e6b9626ffb1' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hidden',
    'comment' => NULL,
    'translation' => '非表示',
    'key' => 'ebfe1dc5234a1fcca8dc7e6b9626ffb1',
  ),
  '41b04de612151df8688cecc167bc08c6' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Make location and all sub items visible.',
    'comment' => NULL,
    'translation' => 'この配置先（子アイテムを含めて）を表示可能にします。',
    'key' => '41b04de612151df8688cecc167bc08c6',
  ),
  'ef13483428c033b021751d7b8116b005' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Reveal',
    'comment' => NULL,
    'translation' => '表示可能にする',
    'key' => 'ef13483428c033b021751d7b8116b005',
  ),
  '49d20fa12b5c0ec4ebb9cc23333aaa8d' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hidden by superior',
    'comment' => NULL,
    'translation' => '権限を持つユーザによる非表示',
    'key' => '49d20fa12b5c0ec4ebb9cc23333aaa8d',
  ),
  '5428ee8210779418f87867dcd28c68ab' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hide location and all sub items.',
    'comment' => NULL,
    'translation' => 'この配置先（子アイテムを含めて）を非表示にします。',
    'key' => '5428ee8210779418f87867dcd28c68ab',
  ),
  '2dae620520548ee0328c651ff080ecde' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hide',
    'comment' => NULL,
    'translation' => '非表示にする',
    'key' => '2dae620520548ee0328c651ff080ecde',
  ),
  '8aa55b5576486c4c1e5f78429d45ad8a' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Visible',
    'comment' => NULL,
    'translation' => '表示可能にする',
    'key' => '8aa55b5576486c4c1e5f78429d45ad8a',
  ),
  '88543541d5f266232b76426ecfbdb5c9' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Use these radio buttons to select the desired main location.',
    'comment' => NULL,
    'translation' => 'このラジオボタンで主要ノードとなる配置先を選択します。',
    'key' => '88543541d5f266232b76426ecfbdb5c9',
  ),
  '63a862b14d072b065179d509b8715dea' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Add locations',
    'comment' => NULL,
    'translation' => '配置先の追加',
    'key' => '63a862b14d072b065179d509b8715dea',
  ),
  '5ffd351c640987c72c8e27ff5b41f83a' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Add one or more new locations.',
    'comment' => NULL,
    'translation' => '配置先を追加します',
    'key' => '5ffd351c640987c72c8e27ff5b41f83a',
  ),
  'f99428c3aba01231689a45d50058b188' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'It is not possible to add locations to a top level node.',
    'comment' => NULL,
    'translation' => '最上位のノードに配置先を追加することはできません。',
    'key' => 'f99428c3aba01231689a45d50058b188',
  ),
  '6b3144b8c4cf52a8a014d4cb4a84ae69' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'The <%class_name> class is not configured to contain any sub items.',
    'comment' => NULL,
    'translation' => '<%class_name>クラスは子アイテムを持つことができません。',
    'key' => '6b3144b8c4cf52a8a014d4cb4a84ae69',
  ),
  'e03b440c70d8f95f0e1963eaf6c05d85' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Edit the contents of this item.',
    'comment' => NULL,
    'translation' => 'このアイテムを編集',
    'key' => 'e03b440c70d8f95f0e1963eaf6c05d85',
  ),
  'f850e9993a0cbae22fa361fdfe88af8e' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Module',
    'comment' => NULL,
    'translation' => 'モジュール',
    'key' => 'f850e9993a0cbae22fa361fdfe88af8e',
  ),
  'f3db8bf2731f15515b7557313f0e6c05' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'No limitations',
    'comment' => NULL,
    'translation' => '制限なし',
    'key' => 'f3db8bf2731f15515b7557313f0e6c05',
  ),
  '7f0e0249b166dd76b10e5b9db2255861' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Relations [%relation_count]',
    'comment' => NULL,
    'translation' => '関連 [%relation_count]',
    'key' => '7f0e0249b166dd76b10e5b9db2255861',
  ),
  '7786a7bc4beea073870e3002ff29c429' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Role',
    'comment' => NULL,
    'translation' => 'ロール',
    'key' => '7786a7bc4beea073870e3002ff29c429',
  ),
  '3a449bc37daad67de5ccde20569f9f00' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Translations [%translations]',
    'comment' => NULL,
    'translation' => '翻訳 [%translations]',
    'key' => '3a449bc37daad67de5ccde20569f9f00',
  ),
  '11aabd439797dee6d4f86db20edd61ef' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'View translation.',
    'comment' => NULL,
    'translation' => '翻訳の表示',
    'key' => '11aabd439797dee6d4f86db20edd61ef',
  ),
  '5d2cb7fc048fab749b181d45e19b013d' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hide object relation overview.',
    'comment' => NULL,
    'translation' => 'オブジェクト関連一覧を隠す',
    'key' => '5d2cb7fc048fab749b181d45e19b013d',
  ),
  '6a0543bb209ee96be82a50b63e9b5076' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show object relation overview.',
    'comment' => NULL,
    'translation' => 'オブジェクト関連一覧の表示',
    'key' => '6a0543bb209ee96be82a50b63e9b5076',
  ),
  '050623cd936edcee903db2c93afdd11f' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Unknown',
    'comment' => NULL,
    'translation' => '不明',
    'key' => '050623cd936edcee903db2c93afdd11f',
  ),
  '1b6a320218d30f9b0d0fe93d428febf2' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'The information could not be collected.',
    'comment' => NULL,
    'translation' => '選択された情報は収集することが出来ません。',
    'key' => '1b6a320218d30f9b0d0fe93d428febf2',
  ),
  'c9ba5d0d9264c2867e3ff383eee77f82' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Required data is either missing or is invalid',
    'comment' => NULL,
    'translation' => '入力必須項目が未入力または不正です',
    'key' => 'c9ba5d0d9264c2867e3ff383eee77f82',
  ),
  'bb4eba5518d1b65340837b9a504a3d2d' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'There is no removable location.',
    'comment' => NULL,
    'translation' => '削除可能な配置先はありません',
    'key' => 'bb4eba5518d1b65340837b9a504a3d2d',
  ),
  '50690827142c4026f065dacd48ce93ba' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Available policies [%policy_count]',
    'comment' => NULL,
    'translation' => '使用可能ポリシー [%policy_count]',
    'key' => '50690827142c4026f065dacd48ce93ba',
  ),
  '6b2f3739e4b1f36be50e2ec61bdd7d34' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'limited to %limitation_identifier %limitation_value',
    'comment' => NULL,
    'translation' => '制限:  %limitation_identifier %limitation_value ',
    'key' => '6b2f3739e4b1f36be50e2ec61bdd7d34',
  ),
  '82c22a6570abebd69cf7c46575363591' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => '(disabled)',
    'comment' => NULL,
    'translation' => '（無効）',
    'key' => '82c22a6570abebd69cf7c46575363591',
  ),
  'b33fe6adebf71c94ddb93b97ccc1be9e' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => 'b33fe6adebf71c94ddb93b97ccc1be9e',
  ),
  '3668e27f27e904944b2b53f996605dba' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Reverse related objects [%related_objects_count]',
    'comment' => NULL,
    'translation' => '関連付けしたオブジェクト [%related_objects_count] を反転',
    'key' => '3668e27f27e904944b2b53f996605dba',
  ),
  '9e982416f841e4d40057e8fa63c15879' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Published at',
    'comment' => NULL,
    'translation' => '以下に作成 ',
    'key' => '9e982416f841e4d40057e8fa63c15879',
  ),
  '0d48bbd6cf97b862354568ad0b5f7e82' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Another language',
    'comment' => NULL,
    'translation' => 'その他の言語',
    'key' => '0d48bbd6cf97b862354568ad0b5f7e82',
  ),
  '3c8721e3fccc6021cc91c095ea006a22' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Existing languages',
    'comment' => NULL,
    'translation' => '既存の言語',
    'key' => '3c8721e3fccc6021cc91c095ea006a22',
  ),
  'd2b4f1f876230bc1ab451e774d32e0d6' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Use these radio buttons to select the desired main language.',
    'comment' => NULL,
    'translation' => '希望する主要言語を選んでラジオボタンにチェックを入れます。',
    'key' => 'd2b4f1f876230bc1ab451e774d32e0d6',
  ),
  'cb2de78d969584bcab4d3fadb2a69cbd' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Edit in <%language_name>.',
    'comment' => NULL,
    'translation' => '<%language_name>で編集',
    'key' => 'cb2de78d969584bcab4d3fadb2a69cbd',
  ),
  'a6d2c9b13302f9c23e0e0ac4396170c5' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Remove selected languages from the list above.',
    'comment' => NULL,
    'translation' => '上のリストから選択した言語を削除します。',
    'key' => 'a6d2c9b13302f9c23e0e0ac4396170c5',
  ),
  '95feeed8e1df8d9dd25137666236ffa7' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'There is no removable language.',
    'comment' => NULL,
    'translation' => '削除可能な言語はありません。',
    'key' => '95feeed8e1df8d9dd25137666236ffa7',
  ),
  '203aafcf3c28cedb21f35edfd9711273' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Use the main language if there is no prioritized translation.',
    'comment' => NULL,
    'translation' => '優先すべき翻訳がない場合、主要言語を使用します。',
    'key' => '203aafcf3c28cedb21f35edfd9711273',
  ),
  '2fc122c3e0490f5c2212b005938145c5' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Update',
    'comment' => NULL,
    'translation' => '更新',
    'key' => '2fc122c3e0490f5c2212b005938145c5',
  ),
  '0d52952bc20a5f6aa3114b212bfe51bf' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Use this button to store the value of the checkbox above.',
    'comment' => NULL,
    'translation' => 'このボタンでチェックボックスに設定された値を保存します。',
    'key' => '0d52952bc20a5f6aa3114b212bfe51bf',
  ),
  '32850bf355b3a754713aeb5bd4e0115e' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => '(locked)',
    'comment' => NULL,
    'translation' => '(ロック)',
    'key' => '32850bf355b3a754713aeb5bd4e0115e',
  ),
  '3f518a8edb84bcd5e8c6afcfe4a5862e' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Choose section',
    'comment' => NULL,
    'translation' => 'セクションを選択',
    'key' => '3f518a8edb84bcd5e8c6afcfe4a5862e',
  ),
  'fb3ae962c78e7af86f769fa0d8be398b' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Relation type',
    'comment' => NULL,
    'translation' => '関連タイプ',
    'key' => 'fb3ae962c78e7af86f769fa0d8be398b',
  ),
  '4442cec838c0dc129063085c1f443cc9' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You do not have permission to remove any of the items from the list above.',
    'comment' => NULL,
    'translation' => '上記のリストからオブジェクトを削除する権限を持っていません。',
    'key' => '4442cec838c0dc129063085c1f443cc9',
  ),
  '230e3361693494de15e864f773b95dff' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You cannot update the priorities because you do not have permission to edit the current item or because a non-priority sorting method is used.',
    'comment' => NULL,
    'translation' => 'アイテムを編集する権限を持っていないか、優先度のソートが選択されていないため、優先度を更新することはできません。',
    'key' => '230e3361693494de15e864f773b95dff',
  ),
  'ae7868dfc43c4641a15cce9b30cbfa92' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Use this menu to select the type of item you want to create then click the "Create here" button. The item will be created in the current location.',
    'comment' => NULL,
    'translation' => 'このメニューを使って、作成したいオブジェクトの種類を選択し、”ここに作成”ボタンをクリックしてください。現在の配置先に作成されます。',
    'key' => 'ae7868dfc43c4641a15cce9b30cbfa92',
  ),
  '4c71d6a1f72f3e1934c649561827a188' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Use this menu to select the language you want to use for the creation then click the "Create here" button. The item will be created in the current location.',
    'comment' => NULL,
    'translation' => 'このメニューを使って言語を選んでから、”ここに作成”ボタンをクリックしてください。現在の配置先にアイテムが作成されます。',
    'key' => '4c71d6a1f72f3e1934c649561827a188',
  ),
  'b3afbc4f01fd3fa250e7aa889c393ffc' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Create a new item in the current location. Use the menu on the left to select the type of  item.',
    'comment' => NULL,
    'translation' => '現在の配置先でオブジェクトを作成する。オブジェクトの種類を設定するには左にあるメニューを使ってください。',
    'key' => 'b3afbc4f01fd3fa250e7aa889c393ffc',
  ),
  'd92fb8abd70723095b7cf39090612c99' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You do not have permission to create new items in the current location.',
    'comment' => NULL,
    'translation' => '現在の配置先で新しいアイテムを作成する権限を持っていません。',
    'key' => 'd92fb8abd70723095b7cf39090612c99',
  ),
  '2e5cee9373863228ed9712b4819ef4ec' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You cannot set the sorting method for the current location because you do not have permission to edit the current item.',
    'comment' => NULL,
    'translation' => '現在のオブジェクトを編集する権限がないため、現在の配置先のソートの方法を設定することはできません。',
    'key' => '2e5cee9373863228ed9712b4819ef4ec',
  ),
  'd772a88dfb6a9a92d98e1443829e7104' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Use these checkboxes to select items for removal. Click the "Remove selected" button to remove the selected items.',
    'comment' => NULL,
    'translation' => 'このチェックボックスを使って、削除するアイテムを選択してください。選択したアイテムを削除するには”選択した項目の削除”ボタンをクリックしてください。',
    'key' => 'd772a88dfb6a9a92d98e1443829e7104',
  ),
  '0e10a0b891f44f4dc0210ddfafdf969c' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You do not have permission to remove this item.',
    'comment' => NULL,
    'translation' => 'このアイテムを削除する権限を持っていません。',
    'key' => '0e10a0b891f44f4dc0210ddfafdf969c',
  ),
  'ac1c65565955d2f1a4bc890607ab3461' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Use the priority fields to control the order in which the items appear. You can use both positive and negative integers. Click the "Update priorities" button to apply the changes.',
    'comment' => NULL,
    'translation' => 'アイテムが表示される順番を指定するには優先度のフィールドを使ってください。優先度の指定には整数を使用して下さい。変更を適用するには、”優先度の更新”をクリックしてください。',
    'key' => 'ac1c65565955d2f1a4bc890607ab3461',
  ),
  '7ea0e1912d2a4aa696482f805a516129' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You are not allowed to update the priorities because you do not have permission to edit <%node_name>.',
    'comment' => NULL,
    'translation' => '<%node_name>を編集する権限を持っていないため、優先度を更新することはできません。',
    'key' => '7ea0e1912d2a4aa696482f805a516129',
  ),
  '1e54e2f1a3b0701f20c4e0edfa8df4cc' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You cannot make a copy of <%child_name> because you do not have create permission for <%node_name>.',
    'comment' => NULL,
    'translation' => '<%node_name>に対する作成権限を持っていないため、<%child_name>を複製することはできません。',
    'key' => '1e54e2f1a3b0701f20c4e0edfa8df4cc',
  ),
  '428fa13ab0066409614a10bbe68812fb' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You do not have permission to edit <%child_name>.',
    'comment' => NULL,
    'translation' => '<%child_name>を編集する権限を持っていません。',
    'key' => '428fa13ab0066409614a10bbe68812fb',
  ),
  'a898ca42a0e393e2c5989cf587cd9230' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Use these checkboxes to select items for removal. Click the "Remove selected" button to  remove the selected items.',
    'comment' => NULL,
    'translation' => 'このチェックボックスを使って、削除するアイテムを選択してください。選択したアイテムを削除するには”選択した項目の削除”ボタンをクリックしてください。',
    'key' => 'a898ca42a0e393e2c5989cf587cd9230',
  ),
  'cd70d8a87e0ddac6cf9970748fe22c99' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You do not have permission to edit %child_name.',
    'comment' => NULL,
    'translation' => '%child_nameを編集する権限を持っていません。',
    'key' => 'cd70d8a87e0ddac6cf9970748fe22c99',
  ),
  '9089a07bb08e154456a4bf098cc119db' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'This location cannot be removed either because you do not have permission to remove it or because it is currently being displayed.',
    'comment' => NULL,
    'translation' => '削除する権限がないか、現在表示されているため、この配置先を削除することはできません。',
    'key' => '9089a07bb08e154456a4bf098cc119db',
  ),
  '3e45f68cacebd0d9f2c9f3abe8d81320' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'The item being displayed has only one location, which is by default the main location.',
    'comment' => NULL,
    'translation' => 'このアイテムは配置先を一つしか持っていないため、その配置先がメインの配置先になります。',
    'key' => '3e45f68cacebd0d9f2c9f3abe8d81320',
  ),
  'b30d0a529b76f4a0f74da587a7485b20' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You cannot set the main location because you do not have permission to edit the item being displayed.',
    'comment' => NULL,
    'translation' => '表示されているオブジェクトを編集する権限を持っていないため、メインの配置先を設定することはできません。',
    'key' => 'b30d0a529b76f4a0f74da587a7485b20',
  ),
  'fcc165dc40b6666de8ae1007c4b9529d' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You cannot remove any locations because you do not have permission to edit the current item.',
    'comment' => NULL,
    'translation' => '現在のオブジェクトを編集する権限がないため、配置先を削除することはできません。',
    'key' => 'fcc165dc40b6666de8ae1007c4b9529d',
  ),
  'caff1563061abfd509a5d5c148de3248' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You cannot add new locations because you do not have permission to edit the current item.',
    'comment' => NULL,
    'translation' => '現在のオブジェクトを編集する権限がないため、新しい配置先を追加することはできません。',
    'key' => 'caff1563061abfd509a5d5c148de3248',
  ),
  '72f288b56628a79e104deb5b3c101c61' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Select the desired main location using the radio buttons above then click this button to store the setting.',
    'comment' => NULL,
    'translation' => 'ラジオボタンを使って、メインの配置先を選んでからこのボタンをクリックして設定を保存してください。',
    'key' => '72f288b56628a79e104deb5b3c101c61',
  ),
  '2ae8bd90b564de01ba7446d965f962e2' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You cannot set the main location because there is only one location present.',
    'comment' => NULL,
    'translation' => '配置先が一つしかないため、メインの配置先を設定することはできません。',
    'key' => '2ae8bd90b564de01ba7446d965f962e2',
  ),
  '55e80828268bb56a565468e24e9a58ab' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You cannot set the main location because you do not have permission to edit the current item.',
    'comment' => NULL,
    'translation' => '現在のオブジェクトの編集権限がないため、メインの配置先を設定することはできません。',
    'key' => '55e80828268bb56a565468e24e9a58ab',
  ),
  '2b1b7cc022f579d40109b59e29ef3878' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You do not have permission to edit this item.',
    'comment' => NULL,
    'translation' => 'このアイテムを編集する権限を持っていません。',
    'key' => '2b1b7cc022f579d40109b59e29ef3878',
  ),
  '0e5e0ce076a82a6b4a3354fca2efe319' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You do not have permission to move this item to another location.',
    'comment' => NULL,
    'translation' => 'このアイテムを違う配置先に移動する権限を持っていません。',
    'key' => '0e5e0ce076a82a6b4a3354fca2efe319',
  ),
  'ed2318d64613a999a17a0b82c33c610a' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'The item being viewed is not used by any other objects.',
    'comment' => NULL,
    'translation' => 'このアイテムは他のオブジェクトに使用されていません。',
    'key' => 'ed2318d64613a999a17a0b82c33c610a',
  ),
  '6f1be2ed3dd2292a86c2898435e8faf5' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You cannot remove any language because you do not have permission to edit the current item.',
    'comment' => NULL,
    'translation' => '現在のオブジェクトを編集する権限がないため、言語を削除することはできません。',
    'key' => '6f1be2ed3dd2292a86c2898435e8faf5',
  ),
  'd285dbd83fc75141aee6389f7ae3ad72' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Select the desired main language using the radio buttons above then click this button to store the setting.',
    'comment' => NULL,
    'translation' => 'ラジオボタンを使って、主言語を選んでからこのボタンをクリックして設定を保存してください。',
    'key' => 'd285dbd83fc75141aee6389f7ae3ad72',
  ),
  '68df8a5902a2fea6d9b39e16c41488c6' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You cannot change the main language because the object is not translated to any other languages.',
    'comment' => NULL,
    'translation' => 'このオブジェクトは他の言語に翻訳されていないため、メインの言語を変更することはできません。',
    'key' => '68df8a5902a2fea6d9b39e16c41488c6',
  ),
  '7008d1481cc8a8519a542ca246249322' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You cannot change the main language because you do not have permission to edit the current item.',
    'comment' => NULL,
    'translation' => 'アイテムを編集する権限がを持っていないため、メインの言語を変更することはできません。',
    'key' => '7008d1481cc8a8519a542ca246249322',
  ),
  '80fad8c6510fd43cb1279c2d6b9214d1' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You do not have permission to change this setting.',
    'comment' => NULL,
    'translation' => 'この設定を変更する権限を持っていません。',
    'key' => '80fad8c6510fd43cb1279c2d6b9214d1',
  ),
  'c61a3872000eaa2e29734001c773e6d4' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Move selected',
    'comment' => NULL,
    'translation' => '選択した項目の移動',
    'key' => 'c61a3872000eaa2e29734001c773e6d4',
  ),
  '1a877316c7d1de5c1f99720878c54ec1' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Move the selected items from the list above.',
    'comment' => NULL,
    'translation' => '選択したアイテムを上記のリストから移動します。',
    'key' => '1a877316c7d1de5c1f99720878c54ec1',
  ),
  'f031f98a352e2f9d072bd0c22f458979' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'You do not have permission to move any of the items from the list above.',
    'comment' => NULL,
    'translation' => '上記のリストからオブジェクトを移動する権限を持っていません。',
    'key' => 'f031f98a352e2f9d072bd0c22f458979',
  ),
  'da5cd6f4e613b2508bc8ac51cf0f91b7' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Object states for object',
    'comment' => NULL,
    'translation' => 'オブジェクトのオブジェクトステート',
    'key' => 'da5cd6f4e613b2508bc8ac51cf0f91b7',
  ),
  '7d1b209917296a5d8a953ce704184824' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Content object state group',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクトステートグループ',
    'key' => '7d1b209917296a5d8a953ce704184824',
  ),
  '7cc099c98c1ff99062e6a31bce4b8b10' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Available states',
    'comment' => NULL,
    'translation' => '利用可能なステート',
    'key' => '7cc099c98c1ff99062e6a31bce4b8b10',
  ),
  '8d723accaf8a388395a0f719e2bdb0ae' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'No content object state is configured. This can be done %urlstart here %urlend.',
    'comment' => NULL,
    'translation' => '設定されているコンテンツオブジェクトステートはありません。%urlstartこちら%urlendから設定できます。',
    'key' => '8d723accaf8a388395a0f719e2bdb0ae',
  ),
  '52bf7967872025f24d62619b53dd10f2' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Set states',
    'comment' => NULL,
    'translation' => 'ステートを設定',
    'key' => '52bf7967872025f24d62619b53dd10f2',
  ),
  'cca23cdd70cd003d04d8d4cbc58b7530' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Apply states from the list above.',
    'comment' => NULL,
    'translation' => '上記のリストからステートを適用。',
    'key' => 'cca23cdd70cd003d04d8d4cbc58b7530',
  ),
  '678f2f6b01cb1f0bc617c56720a6c09a' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'No state to be applied to this content object. You might need to be assigned a more permissive access policy.',
    'comment' => NULL,
    'translation' => 'このコンテンツオブジェクトに適用出来るステートはありません。より高いアクセス権限が必要かもしれません。',
    'key' => '678f2f6b01cb1f0bc617c56720a6c09a',
  ),
  'd78a12f7b81902844323705a25880870' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Object states',
    'comment' => NULL,
    'translation' => 'オブジェクトステート',
    'key' => 'd78a12f7b81902844323705a25880870',
  ),
  'fd118bb67b503b759828fdbf9dcef356' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Hide state assignment widget.',
    'comment' => NULL,
    'translation' => 'ステート割当ウィジェットを隠す。',
    'key' => 'fd118bb67b503b759828fdbf9dcef356',
  ),
  'c57023b8b02cb7fe75d648b4a17a0d80' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show state assignment widget.',
    'comment' => NULL,
    'translation' => 'ステート割当ウィジェットを表示する。',
    'key' => 'c57023b8b02cb7fe75d648b4a17a0d80',
  ),
  '4c2bb86c2ebd8f98f8363100818103c4' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Tab is disabled, enable on dashboard.',
    'comment' => NULL,
    'translation' => 'タブは無効となっています、ダッシュボードで有効にしてください。',
    'key' => '4c2bb86c2ebd8f98f8363100818103c4',
  ),
  'a7bc27229a3f342f2e4e9f15f3052cde' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Translations (%count)',
    'comment' => NULL,
    'translation' => '翻訳(%count)',
    'key' => 'a7bc27229a3f342f2e4e9f15f3052cde',
  ),
  '08c3014cfb809227d4b43ad7e1bb7fcc' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Locations (%count)',
    'comment' => NULL,
    'translation' => '配置先(%count)',
    'key' => '08c3014cfb809227d4b43ad7e1bb7fcc',
  ),
  '33589979885527768082316362ad5116' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Relations (%count)',
    'comment' => NULL,
    'translation' => '関連(%count)',
    'key' => '33589979885527768082316362ad5116',
  ),
  '8450552be03491b52569d5172d658a5d' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Roles (%count)',
    'comment' => NULL,
    'translation' => 'ロール(%count)',
    'key' => '8450552be03491b52569d5172d658a5d',
  ),
  '26af233523c4beee2b6c62bac408947d' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Policies (%count)',
    'comment' => NULL,
    'translation' => 'ポリシー(%count)',
    'key' => '26af233523c4beee2b6c62bac408947d',
  ),
  '41a5152f1d865fb488ba203a85d01acb' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Sub items (%children_count)',
    'comment' => NULL,
    'translation' => '子アイテム (%children_count)',
    'key' => '41a5152f1d865fb488ba203a85d01acb',
  ),
  'c73302a3ff78749f9d9346f755be709c' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Create',
    'comment' => NULL,
    'translation' => '作成',
    'key' => 'c73302a3ff78749f9d9346f755be709c',
  ),
  'b0dbb5c0dc5e27988e0b0ce305392cb1' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'in',
    'comment' => NULL,
    'translation' => '言語',
    'key' => 'b0dbb5c0dc5e27988e0b0ce305392cb1',
  ),
  '126694d7746fb082d242f531fa8f07b7' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Here',
    'comment' => NULL,
    'translation' => 'ここ',
    'key' => '126694d7746fb082d242f531fa8f07b7',
  ),
  'ac4c78e672433787ba27b011679e6f9e' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Published order',
    'comment' => NULL,
    'translation' => '公開順番',
    'key' => 'ac4c78e672433787ba27b011679e6f9e',
  ),
  'def39a51741637e945acf40a9131cd39' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'List of sub items of current node, with controlls to edit, remove and move them directly.',
    'comment' => NULL,
    'translation' => '現在のノードのサブアイテムのリストを表示します。直接編集、削除と移動をすることができます。',
    'key' => 'def39a51741637e945acf40a9131cd39',
  ),
  '71447d00c9fe16648c4a1ddcc4c8fea5' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Node and object details like creator, when it was created, section it belongs to, number of versions and translations, Node ID and Object ID.',
    'comment' => NULL,
    'translation' => '作成者、セクション、バージョン、翻訳、ノードID、オブジェクトIDなどのノードとオブジェクトの詳細情報。',
    'key' => '71447d00c9fe16648c4a1ddcc4c8fea5',
  ),
  '9cf4301efd5b4177bce03756dbce789a' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Content state',
    'comment' => NULL,
    'translation' => 'コンテントステート',
    'key' => '9cf4301efd5b4177bce03756dbce789a',
  ),
  'bcfe7344008b653f180bdcd974529a3a' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'States and their states groups for current object.',
    'comment' => NULL,
    'translation' => '現在のオブジェクトのステートとステートグループ。',
    'key' => 'bcfe7344008b653f180bdcd974529a3a',
  ),
  '4644692f85dd306670558c81685dfee8' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'State group',
    'comment' => NULL,
    'translation' => 'ステートグループ',
    'key' => '4644692f85dd306670558c81685dfee8',
  ),
  'fd2be42a1328047965e61c6d74876e6e' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'No content object state is configured. This can be done %urlstart here%urlend.',
    'comment' => NULL,
    'translation' => 'コンテントオブジェクトステートは設定されています。%urlstartここ%urlendで設定できます。',
    'key' => 'fd2be42a1328047965e61c6d74876e6e',
  ),
  '1368dc095e109fb09646e7ddd8d94103' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Locations (aka Nodes) for current object.',
    'comment' => NULL,
    'translation' => '現在のオブジェクトの配置先（ノード）。',
    'key' => '1368dc095e109fb09646e7ddd8d94103',
  ),
  '73cb3d2e159cdfafae9ce697fffe6e86' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'New translation',
    'comment' => NULL,
    'translation' => '新しい翻訳',
    'key' => '73cb3d2e159cdfafae9ce697fffe6e86',
  ),
  '822ba28abe475c0d52ecbe92388dc847' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Policy list and the Role that are assignet to current node.',
    'comment' => NULL,
    'translation' => '現在のノードに割り当てられたポリシーとロール。',
    'key' => '822ba28abe475c0d52ecbe92388dc847',
  ),
  '055d62a085187d5e89cc209f76c2fb70' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Limited to',
    'comment' => NULL,
    'translation' => 'に制限された',
    'key' => '055d62a085187d5e89cc209f76c2fb70',
  ),
  'dfcc98f33572a46096456fe2609a371b' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => '%limitation_identifier %limitation_value',
    'comment' => NULL,
    'translation' => '%limitation_identifier %limitation_value',
    'key' => 'dfcc98f33572a46096456fe2609a371b',
  ),
  '4d7221352146ba1c837a89be6e5cf261' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Object relation list from current object.',
    'comment' => NULL,
    'translation' => '現在のオブジェクトの関連オブジェクトリスト。',
    'key' => '4d7221352146ba1c837a89be6e5cf261',
  ),
  '285011594a10d4531d8cf995a0c71982' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Related objects (%related_objects_count)',
    'comment' => NULL,
    'translation' => '関連付けしたオブジェクト: (%related_objects_count)',
    'key' => '285011594a10d4531d8cf995a0c71982',
  ),
  'c3f620b8c6b634ef4a4313fe46aedb89' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Reverse object relation list to current object.',
    'comment' => NULL,
    'translation' => '現在のオブジェクトの逆関連オブジェクトリスト。',
    'key' => 'c3f620b8c6b634ef4a4313fe46aedb89',
  ),
  '8fde2f97d714925624b4ccc7cefd1736' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Reverse related objects (%related_objects_count)',
    'comment' => NULL,
    'translation' => '関連付けしたオブジェクト (%related_objects_count) を反転',
    'key' => '8fde2f97d714925624b4ccc7cefd1736',
  ),
  'df6d9b175adb47df6dad344c9e9cce98' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'List of roles assigned with and without limitations for current node.',
    'comment' => NULL,
    'translation' => '現在のノードに割り当てられたロールのリスト。',
    'key' => 'df6d9b175adb47df6dad344c9e9cce98',
  ),
  'f2746ef0b365f48d97507bd6e6fc4045' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Translations (%translations)',
    'comment' => NULL,
    'translation' => '翻訳 (%translations)',
    'key' => 'f2746ef0b365f48d97507bd6e6fc4045',
  ),
  '587680775d82875374dc5fdb6a403b9c' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Existing translations',
    'comment' => NULL,
    'translation' => '存在する翻訳',
    'key' => '587680775d82875374dc5fdb6a403b9c',
  ),
  'b202a1b274894b17539690c54453ef8f' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Language list of translations for current object.',
    'comment' => NULL,
    'translation' => '現在のオブジェクト翻訳の言語リスト。',
    'key' => 'b202a1b274894b17539690c54453ef8f',
  ),
  '9bf96a0edfe37ded96a5e3e4eaedbf56' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Tab is disabled, enable with toggler to the left of these tabs.',
    'comment' => NULL,
    'translation' => 'タブは無効になっています、タブの左側にある切り替えボタンで有効にできます。',
    'key' => '9bf96a0edfe37ded96a5e3e4eaedbf56',
  ),
  'aed6b6c4019436e25224afbdcaae6def' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'View',
    'comment' => NULL,
    'translation' => '表示',
    'key' => 'aed6b6c4019436e25224afbdcaae6def',
  ),
  '8cb440cdb610036e759f6cab3d550622' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show simplified view of content.',
    'comment' => NULL,
    'translation' => 'コンテントの略式を表示。',
    'key' => '8cb440cdb610036e759f6cab3d550622',
  ),
  'f9502b026f78245215d8c74e79c7edc1' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Ordering',
    'comment' => NULL,
    'translation' => 'ソート順',
    'key' => 'f9502b026f78245215d8c74e79c7edc1',
  ),
  '0f232ed30bb05a051b9e97bbaf757bd5' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Show published ordering overview.',
    'comment' => NULL,
    'translation' => '公開順番を表示。',
    'key' => '0f232ed30bb05a051b9e97bbaf757bd5',
  ),
  '474d153b92a5b55e71512f4a2a72b1ed' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Loading ...',
    'comment' => NULL,
    'translation' => 'ローディング...',
    'key' => '474d153b92a5b55e71512f4a2a72b1ed',
  ),
  '24eed93b001fd0668a646b3debf3be26' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Node remote ID',
    'comment' => NULL,
    'translation' => 'ノードリモートID',
    'key' => '24eed93b001fd0668a646b3debf3be26',
  ),
  'e9f67d10713fb27a9e1d2c982ce3a669' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Object remote ID',
    'comment' => NULL,
    'translation' => 'オブジェクトリモートID',
    'key' => 'e9f67d10713fb27a9e1d2c982ce3a669',
  ),
  '6b1fed895e03142001ff24295f9c9cd4' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Table options',
    'comment' => NULL,
    'translation' => 'テーブルオプション',
    'key' => '6b1fed895e03142001ff24295f9c9cd4',
  ),
  'b8c4fac7a6c0661e3c68ed503b8501df' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Number of items per page:',
    'comment' => NULL,
    'translation' => 'ページで表示するオブジェクト数:',
    'key' => 'b8c4fac7a6c0661e3c68ed503b8501df',
  ),
  '72933678b55ee664e0cbe923c95962d9' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Visible table columns:',
    'comment' => NULL,
    'translation' => '表示されているテーブルのコラム:',
    'key' => '72933678b55ee664e0cbe923c95962d9',
  ),
  '038939524aa060dc720398302917fb2c' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Close',
    'comment' => NULL,
    'translation' => 'Close',
    'key' => '038939524aa060dc720398302917fb2c',
  ),
  '4c13858ed7bac56af2df8625573f483e' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Select',
    'comment' => NULL,
    'translation' => '選択',
    'key' => '4c13858ed7bac56af2df8625573f483e',
  ),
  '674e32f152c33b35ce0838f873bac586' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Select all visible',
    'comment' => NULL,
    'translation' => '表示アイテムを選択',
    'key' => '674e32f152c33b35ce0838f873bac586',
  ),
  'ada7514fbb9f4165e90615232fa30268' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Select none',
    'comment' => NULL,
    'translation' => '選択解除',
    'key' => 'ada7514fbb9f4165e90615232fa30268',
  ),
  'f6e834b7383c8901caffad059262465b' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Create new',
    'comment' => NULL,
    'translation' => '新規作成',
    'key' => 'f6e834b7383c8901caffad059262465b',
  ),
  '6707e879fe9bb7239c3871a13c33f0bb' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'More actions',
    'comment' => NULL,
    'translation' => 'その他の操作',
    'key' => '6707e879fe9bb7239c3871a13c33f0bb',
  ),
  'c785354c6bc5259bda5b28ee4a0266d9' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Use the checkboxes to select one or more items.',
    'comment' => NULL,
    'translation' => 'アイテムを選択するためにチェックボックスを使ってください。',
    'key' => 'c785354c6bc5259bda5b28ee4a0266d9',
  ),
  'f07034bc7b35e32c2264cc1225a669e7' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'first',
    'comment' => NULL,
    'translation' => '最初',
    'key' => 'f07034bc7b35e32c2264cc1225a669e7',
  ),
  '8125c6d586b6154e6cbec3f8cccfa1cb' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'prev',
    'comment' => NULL,
    'translation' => '前',
    'key' => '8125c6d586b6154e6cbec3f8cccfa1cb',
  ),
  '32fec5c5fa6df2256992749b8a1124dd' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'next',
    'comment' => NULL,
    'translation' => '次',
    'key' => '32fec5c5fa6df2256992749b8a1124dd',
  ),
  '097fb2938e26bd5db5d486c5f38e4b40' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'last',
    'comment' => NULL,
    'translation' => '最後',
    'key' => '097fb2938e26bd5db5d486c5f38e4b40',
  ),
  '98d5d03a73da3b565c5ea6e4998b45e5' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Invert selection',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => '98d5d03a73da3b565c5ea6e4998b45e5',
  ),
  'b0d0f3bcb24eba6a04468f169793bb04' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Path String',
    'comment' => NULL,
    'translation' => 'パスストリング',
    'key' => 'b0d0f3bcb24eba6a04468f169793bb04',
  ),
  '333861f4a56a62c49ca52b98a2e784e3' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Object state',
    'comment' => NULL,
    'translation' => 'Object state',
    'key' => '333861f4a56a62c49ca52b98a2e784e3',
  ),
  'bb8f7012ea0e9b13a965f63577ec5043' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Node Remote ID and Object Remote ID',
    'comment' => NULL,
    'translation' => 'Node Remote ID and Object Remote ID',
    'key' => 'bb8f7012ea0e9b13a965f63577ec5043',
  ),
  '624edb897ee3448601c084e234d79acd' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Node Remote ID',
    'comment' => NULL,
    'translation' => 'Node Remote ID',
    'key' => '624edb897ee3448601c084e234d79acd',
  ),
  '3fbea7527a488127cf9cc095ef37105a' => 
  array (
    'context' => 'design/admin/node/view/full',
    'source' => 'Object Remote ID',
    'comment' => NULL,
    'translation' => 'Object Remote ID',
    'key' => '3fbea7527a488127cf9cc095ef37105a',
  ),
);
?>
