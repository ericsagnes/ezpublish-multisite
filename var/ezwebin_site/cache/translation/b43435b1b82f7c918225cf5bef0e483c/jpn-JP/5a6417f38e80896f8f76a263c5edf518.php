<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/shop/productsoverview',
);

$TranslationRoot = array (
  '332d3f94f4c295ad31c48aa151baea1a' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Products overview',
    'comment' => NULL,
    'translation' => '商品一覧',
    'key' => '332d3f94f4c295ad31c48aa151baea1a',
  ),
  'd46079ad16d83a32e9b3e352a19d91a3' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'None',
    'comment' => NULL,
    'translation' => 'なし',
    'key' => 'd46079ad16d83a32e9b3e352a19d91a3',
  ),
  'beee584b4f4b86750a7e60c3af380b84' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => 'プロダクト名',
    'key' => 'beee584b4f4b86750a7e60c3af380b84',
  ),
  '78fcb6784f9e5fa4769a7f59ca0181bc' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Price',
    'comment' => NULL,
    'translation' => '価格',
    'key' => '78fcb6784f9e5fa4769a7f59ca0181bc',
  ),
  '7085f33cd3079af67caf4b1864af1314' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Show 10 items per page.',
    'comment' => NULL,
    'translation' => '10アイテム/ページ',
    'key' => '7085f33cd3079af67caf4b1864af1314',
  ),
  '210ab7c9eddb98005e8f1749cac75f40' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Show 50 items per page.',
    'comment' => NULL,
    'translation' => '50アイテム/ページ',
    'key' => '210ab7c9eddb98005e8f1749cac75f40',
  ),
  '898c90c1a196e7feab37c98a3dee77ed' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Show 25 items per page.',
    'comment' => NULL,
    'translation' => '25アイテム/ページ',
    'key' => '898c90c1a196e7feab37c98a3dee77ed',
  ),
  '8d1aeb5d2551c02093427a143404875a' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'The product list is empty.',
    'comment' => NULL,
    'translation' => '商品リストは空です。',
    'key' => '8d1aeb5d2551c02093427a143404875a',
  ),
  '8281b50ca2420bd8976d2543aab2e687' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Select product class.',
    'comment' => NULL,
    'translation' => '商品クラスの選択',
    'key' => '8281b50ca2420bd8976d2543aab2e687',
  ),
  'f77b475eca9ad35bf4cf605d34f150d9' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Show products',
    'comment' => NULL,
    'translation' => '商品の表示',
    'key' => 'f77b475eca9ad35bf4cf605d34f150d9',
  ),
  '7f63657917cff62c8495b03b04bbd829' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Show products of selected class.',
    'comment' => NULL,
    'translation' => '選択したクラスの商品の表示',
    'key' => '7f63657917cff62c8495b03b04bbd829',
  ),
  '4562baed3ac7d1b27bc6ec60b01f521d' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Select sorting field.',
    'comment' => NULL,
    'translation' => 'ソート・フィールドの選択',
    'key' => '4562baed3ac7d1b27bc6ec60b01f521d',
  ),
  'cc06d8da043b556c14d24e59c5c75521' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Select sorting order.',
    'comment' => NULL,
    'translation' => 'ソート順の選択',
    'key' => 'cc06d8da043b556c14d24e59c5c75521',
  ),
  '82930fbb3714949688c330612bf95a26' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Descending',
    'comment' => NULL,
    'translation' => '降順',
    'key' => '82930fbb3714949688c330612bf95a26',
  ),
  '5bbb5db2a639f425744a8bb566e7108e' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Ascending',
    'comment' => NULL,
    'translation' => '昇順',
    'key' => '5bbb5db2a639f425744a8bb566e7108e',
  ),
  '5921038148bd068d3f97a7bd459d2620' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Sort products',
    'comment' => NULL,
    'translation' => '商品のソート',
    'key' => '5921038148bd068d3f97a7bd459d2620',
  ),
  '1498744243a1f7d1dc60a0f8922a2bfa' => 
  array (
    'context' => 'design/standard/shop/productsoverview',
    'source' => 'Sort products.',
    'comment' => NULL,
    'translation' => '商品のソート',
    'key' => '1498744243a1f7d1dc60a0f8922a2bfa',
  ),
);
?>
