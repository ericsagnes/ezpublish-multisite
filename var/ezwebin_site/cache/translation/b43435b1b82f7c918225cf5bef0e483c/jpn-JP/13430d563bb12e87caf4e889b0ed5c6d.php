<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/shop/browse_discountcustomer',
);

$TranslationRoot = array (
  'eb565278ff68901b12d7f073ad198796' => 
  array (
    'context' => 'design/admin/shop/browse_discountcustomer',
    'source' => 'Choose customers for discount group',
    'comment' => NULL,
    'translation' => 'ディスカウント対象顧客の選択',
    'key' => 'eb565278ff68901b12d7f073ad198796',
  ),
  'cabbfd7300d55a91cf1a74c77a453b33' => 
  array (
    'context' => 'design/admin/shop/browse_discountcustomer',
    'source' => 'Use the checkboxes to select users and user groups (customers) that you wish to add to the discount group.',
    'comment' => NULL,
    'translation' => 'チェックボックスを使用してディスカウント対象に追加するユーザまたはユーザグループ（顧客）を選択して下さい。',
    'key' => 'cabbfd7300d55a91cf1a74c77a453b33',
  ),
  '2e2244d65df45b27fae4c866c84461fe' => 
  array (
    'context' => 'design/admin/shop/browse_discountcustomer',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => '2e2244d65df45b27fae4c866c84461fe',
  ),
);
?>
