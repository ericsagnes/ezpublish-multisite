<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/content/history',
);

$TranslationRoot = array (
  '996503184f09bce0d786576966bcf9ec' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Unable to create new version',
    'comment' => NULL,
    'translation' => '新しいバージョンを作成できません',
    'key' => '996503184f09bce0d786576966bcf9ec',
  ),
  'e8fa9a3fcb3e1c39c24d17eacb1e574e' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Version history limit has been exceeded and no archived version can be removed by the system.',
    'comment' => NULL,
    'translation' => 'バージョンの履歴制限を超えました。未保管のバージョンはシステムにより削除されます。',
    'key' => 'e8fa9a3fcb3e1c39c24d17eacb1e574e',
  ),
  'cd10102afc8f116e6d27a1fba4ffc4c2' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'You can change your version history settings in content.ini, remove draft versions or edit existing drafts.',
    'comment' => NULL,
    'translation' => 'バージョン履歴設定はcontent.ini設定により変更可能です。下書きバージョンを削除するか編集してください。',
    'key' => 'cd10102afc8f116e6d27a1fba4ffc4c2',
  ),
  '9c50a030a2a8c24215a0fcb205c1a1ee' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Versions for <%object_name> [%version_count]',
    'comment' => NULL,
    'translation' => '<%object_name>のバージョン数: [%version_count]',
    'key' => '9c50a030a2a8c24215a0fcb205c1a1ee',
  ),
  'bed65c40cb0bdb484c913e6a2ef17159' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => 'bed65c40cb0bdb484c913e6a2ef17159',
  ),
  '63746e14586c0e827f13d6742daf4392' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Status',
    'comment' => NULL,
    'translation' => 'ステータス',
    'key' => '63746e14586c0e827f13d6742daf4392',
  ),
  '5fff32d1a85bdf293e4833f6a762a951' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Creator',
    'comment' => NULL,
    'translation' => '作成者',
    'key' => '5fff32d1a85bdf293e4833f6a762a951',
  ),
  '70c545569656a5ff6a2a3fe284ca84d0' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Created',
    'comment' => NULL,
    'translation' => '作成日時',
    'key' => '70c545569656a5ff6a2a3fe284ca84d0',
  ),
  '2a72dc73c3435587e634d118b68e5410' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '2a72dc73c3435587e634d118b68e5410',
  ),
  '6333d355a02a2f42e136fdf1d3a31172' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Select version #%version_number for removal.',
    'comment' => NULL,
    'translation' => '削除するバージョン #%version_number の選択',
    'key' => '6333d355a02a2f42e136fdf1d3a31172',
  ),
  '7e6c1e6bc744e61ee66d9373fe3d55cf' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'View the contents of version #%version_number. Translation: %translation.',
    'comment' => NULL,
    'translation' => 'バージョン #%version_number を表示。 翻訳: %translation',
    'key' => '7e6c1e6bc744e61ee66d9373fe3d55cf',
  ),
  'bec8b198f17cdcf6f782d35e72bf96c4' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Draft',
    'comment' => NULL,
    'translation' => '下書き',
    'key' => 'bec8b198f17cdcf6f782d35e72bf96c4',
  ),
  'd95b5bbed3dd078fc893eb48914a0564' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Published',
    'comment' => NULL,
    'translation' => '公開中',
    'key' => 'd95b5bbed3dd078fc893eb48914a0564',
  ),
  'b3aee0dff868048a0d7be1624901b94e' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Pending',
    'comment' => NULL,
    'translation' => '保留中',
    'key' => 'b3aee0dff868048a0d7be1624901b94e',
  ),
  '450d668afec4bb0a09f21de731f2a594' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Archived',
    'comment' => NULL,
    'translation' => '保管',
    'key' => '450d668afec4bb0a09f21de731f2a594',
  ),
  '7616717ed625f2d69c9d7ed2f45e61f8' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Rejected',
    'comment' => NULL,
    'translation' => '非承認',
    'key' => '7616717ed625f2d69c9d7ed2f45e61f8',
  ),
  '3e35fcc1ab6ceb61d271146c9b2a6497' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Untouched draft',
    'comment' => NULL,
    'translation' => '未変更の下書き',
    'key' => '3e35fcc1ab6ceb61d271146c9b2a6497',
  ),
  '1d7383d3edce16cc2e13c13e00a9fd0e' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Create a copy of version #%version_number.',
    'comment' => NULL,
    'translation' => 'バージョン #%version_number の複製を作成。',
    'key' => '1d7383d3edce16cc2e13c13e00a9fd0e',
  ),
  '337b5e09425500e8eb760026fb75555a' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Edit the contents of version #%version_number.',
    'comment' => NULL,
    'translation' => 'コンテンツバージョン #%version_number を編集。',
    'key' => '337b5e09425500e8eb760026fb75555a',
  ),
  '307573704beeb292e3a04f21491eb386' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'This object does not have any versions.',
    'comment' => NULL,
    'translation' => 'このオブジェクトにバージョンはありません。',
    'key' => '307573704beeb292e3a04f21491eb386',
  ),
  'ffdfd359e4d167f1d88e6a970ebcc57d' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => 'ffdfd359e4d167f1d88e6a970ebcc57d',
  ),
  '0d2ded3948b43cc74dfb90237d77c2c8' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Remove the selected versions from the object.',
    'comment' => NULL,
    'translation' => 'オブジェクトから選択バージョンを削除',
    'key' => '0d2ded3948b43cc74dfb90237d77c2c8',
  ),
  'eb0b10e58b52c7c92ebecc1a479a93aa' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Show differences',
    'comment' => NULL,
    'translation' => '違いの表示',
    'key' => 'eb0b10e58b52c7c92ebecc1a479a93aa',
  ),
  '489cad6ffe59cc56e1c51d53bd42ad92' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Back',
    'comment' => NULL,
    'translation' => '戻る',
    'key' => '489cad6ffe59cc56e1c51d53bd42ad92',
  ),
  '8111e84df42dfe770340cc77094938df' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Published version',
    'comment' => NULL,
    'translation' => '公開バージョン',
    'key' => '8111e84df42dfe770340cc77094938df',
  ),
  '116d2526d5768e814a5443d0d1faa681' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Translations',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => '116d2526d5768e814a5443d0d1faa681',
  ),
  '225a32b428e327a4f07d74851eacc381' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'New drafts [%newerDraftCount]',
    'comment' => NULL,
    'translation' => '新規下書き [%newerDraftCount] ',
    'key' => '225a32b428e327a4f07d74851eacc381',
  ),
  '70e1ab97a87fd2cadd4df36701139c8d' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'This object does not have any drafts.',
    'comment' => NULL,
    'translation' => 'このオブジェクトには、下書きはありません。',
    'key' => '70e1ab97a87fd2cadd4df36701139c8d',
  ),
  '268e0041332a9bdfd7ba8ddb76a8a7d0' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Differences between versions %oldVersion and %newVersion',
    'comment' => NULL,
    'translation' => 'バージョン  %oldVersion と %newVersion の違い',
    'key' => '268e0041332a9bdfd7ba8ddb76a8a7d0',
  ),
  '8b5fc35f7f771d0966a0601ef08a8b04' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Old version',
    'comment' => NULL,
    'translation' => '旧バージョン',
    'key' => '8b5fc35f7f771d0966a0601ef08a8b04',
  ),
  'e249e1a4185d738ad13b4daf2e2b1408' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Inline changes',
    'comment' => NULL,
    'translation' => 'インライン変更',
    'key' => 'e249e1a4185d738ad13b4daf2e2b1408',
  ),
  '65599404036a38f09340667e988903fb' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Block changes',
    'comment' => NULL,
    'translation' => 'ブロック変更',
    'key' => '65599404036a38f09340667e988903fb',
  ),
  '9cb6f5ca49df29ed3bf3ba67656ab729' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'New version',
    'comment' => NULL,
    'translation' => '新規バージョン',
    'key' => '9cb6f5ca49df29ed3bf3ba67656ab729',
  ),
  '9c041d49b87888267261dcb50dd14b2b' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Back to history',
    'comment' => NULL,
    'translation' => '履歴に戻る',
    'key' => '9c041d49b87888267261dcb50dd14b2b',
  ),
  '87bc9ce0a1e24d564b051586320ae117' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Copy translation',
    'comment' => NULL,
    'translation' => '翻訳を複製する',
    'key' => '87bc9ce0a1e24d564b051586320ae117',
  ),
  'd1fb4d936fb52973042b2cd66b186e58' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Version is not a draft',
    'comment' => NULL,
    'translation' => 'バージョンは下書きではありません',
    'key' => 'd1fb4d936fb52973042b2cd66b186e58',
  ),
  '9a21ac51dbce500e8907db5321c67372' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Version %1 is not available for editing anymore. Only drafts can be edited.',
    'comment' => NULL,
    'translation' => 'バージョン%1は編集することができません。下書きのみ編集できます。',
    'key' => '9a21ac51dbce500e8907db5321c67372',
  ),
  'b8c0be8a15cbc7aa68f131361642f35b' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'To edit this version, first create a copy of it.',
    'comment' => NULL,
    'translation' => 'このバージョンを編集するには、先に複製を作成して下さい。',
    'key' => 'b8c0be8a15cbc7aa68f131361642f35b',
  ),
  'fe0cc19cc4de16f06f60f9318ab7df10' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Version is not yours',
    'comment' => NULL,
    'translation' => 'バージョンの所有者ではありません',
    'key' => 'fe0cc19cc4de16f06f60f9318ab7df10',
  ),
  '79a54691da04f1544eca556b0c695493' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Version %1 was not created by you. You can only edit your own drafts.',
    'comment' => NULL,
    'translation' => 'バージョン%1の作成者ではありません。自分が作成した下書きしか編集できません。',
    'key' => '79a54691da04f1544eca556b0c695493',
  ),
  'b10bce23ddc4bfd3bc25ef282b721e5c' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Modified translation',
    'comment' => NULL,
    'translation' => '変更された翻訳',
    'key' => 'b10bce23ddc4bfd3bc25ef282b721e5c',
  ),
  '12d26aaf4e785dc4ee598c6b8b90de21' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'Version #%version_number cannot be removed because it is either the published version of the object or because you do not have permission to remove it.',
    'comment' => NULL,
    'translation' => '公開されているバージョンか、削除する権限を持っていないため、バージョン#%version_numberを削除することはできません。',
    'key' => '12d26aaf4e785dc4ee598c6b8b90de21',
  ),
  '9f08d9a1ac83ab14fb29cb66cd680688' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'You cannot make copies of versions because you do not have permission to edit the object.',
    'comment' => NULL,
    'translation' => 'オブジェクトを編集する権限を持っていないため、バージョンを複製することはできません。',
    'key' => '9f08d9a1ac83ab14fb29cb66cd680688',
  ),
  '7d71d4fd10f62f47d7d04e306641170b' => 
  array (
    'context' => 'design/standard/content/history',
    'source' => 'You cannot edit the contents of version #%version_number either because it is not a draft or because you do not have permission to edit the object.',
    'comment' => NULL,
    'translation' => '下書きではないか、オブジェクトを編集する権限を持っていないため、#%version_numberのコンテンツを編集することはできません。',
    'key' => '7d71d4fd10f62f47d7d04e306641170b',
  ),
);
?>
