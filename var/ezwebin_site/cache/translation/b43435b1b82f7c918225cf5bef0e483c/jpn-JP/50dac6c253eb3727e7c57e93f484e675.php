<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/ezoe/searchreplace',
);

$TranslationRoot = array (
  'e73373a70d962fc8151fdd77f2669524' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'Find again',
    'comment' => NULL,
    'translation' => '再度検索',
    'key' => 'e73373a70d962fc8151fdd77f2669524',
  ),
  '76c270967058b75d6a2ee37c7b0100d8' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'The search has been completed. The search string could not be found.',
    'comment' => NULL,
    'translation' => '検索が完了しました。該当する結果は見つかりませんでした。',
    'key' => '76c270967058b75d6a2ee37c7b0100d8',
  ),
  'db9d1b98446f77744d61c6aad8b7e358' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'Find',
    'comment' => NULL,
    'translation' => '検索',
    'key' => 'db9d1b98446f77744d61c6aad8b7e358',
  ),
  '29c32be60afe2f802c4325c64b20192a' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'Find/Replace',
    'comment' => NULL,
    'translation' => '検索/置換',
    'key' => '29c32be60afe2f802c4325c64b20192a',
  ),
  'b64eb1cc938329ed052c4e532ddb795a' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'All occurrences of the search string were replaced.',
    'comment' => NULL,
    'translation' => '全て置き換えました。',
    'key' => 'b64eb1cc938329ed052c4e532ddb795a',
  ),
  '5822234afbcef58429c92d84e19d2b5e' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'Find what',
    'comment' => NULL,
    'translation' => '検索テキスト',
    'key' => '5822234afbcef58429c92d84e19d2b5e',
  ),
  '95dc99ccad20252b1be2035eaf68c57f' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'Replace with',
    'comment' => NULL,
    'translation' => '置き換えテキスト',
    'key' => '95dc99ccad20252b1be2035eaf68c57f',
  ),
  '38cdf7f903c40683059fe8f84424f791' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'Direction',
    'comment' => NULL,
    'translation' => '方向',
    'key' => '38cdf7f903c40683059fe8f84424f791',
  ),
  '43ca57be34af994812d367ae1ce6422e' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'Up',
    'comment' => NULL,
    'translation' => '上',
    'key' => '43ca57be34af994812d367ae1ce6422e',
  ),
  'e3a29b3dd52704681eef8266ff857737' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'Down',
    'comment' => NULL,
    'translation' => '下',
    'key' => 'e3a29b3dd52704681eef8266ff857737',
  ),
  '03f4d6cb22f446225d4d488962c0a303' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'Match case',
    'comment' => NULL,
    'translation' => '大文字と小文字を区別する',
    'key' => '03f4d6cb22f446225d4d488962c0a303',
  ),
  'ed2e02a333b81424499ebcb011846ece' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'Find next',
    'comment' => NULL,
    'translation' => '次を検索',
    'key' => 'ed2e02a333b81424499ebcb011846ece',
  ),
  '07a6099b77242145f7350fa310f3819e' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'Replace',
    'comment' => NULL,
    'translation' => '置換',
    'key' => '07a6099b77242145f7350fa310f3819e',
  ),
  'ae0bc717a322370f16394b99a5c0c61f' => 
  array (
    'context' => 'design/standard/ezoe/searchreplace',
    'source' => 'Replace all',
    'comment' => NULL,
    'translation' => '全て置換',
    'key' => 'ae0bc717a322370f16394b99a5c0c61f',
  ),
);
?>
