<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/shop/orderlist',
);

$TranslationRoot = array (
  'afca52595f528ba2c0fdb5b15fee06f4' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Orders [%count]',
    'comment' => NULL,
    'translation' => '注文数 [%count]',
    'key' => 'afca52595f528ba2c0fdb5b15fee06f4',
  ),
  '233423861b69db3588a65d2ec021c6c3' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'ID',
    'comment' => NULL,
    'translation' => 'ID',
    'key' => '233423861b69db3588a65d2ec021c6c3',
  ),
  '174a843e6248d733a447bc9957a5560e' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Customer',
    'comment' => NULL,
    'translation' => 'お客様',
    'key' => '174a843e6248d733a447bc9957a5560e',
  ),
  '567be54f9109f866887d28e75f750ac7' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Total (ex. VAT)',
    'comment' => NULL,
    'translation' => '合計（税抜）',
    'key' => '567be54f9109f866887d28e75f750ac7',
  ),
  '10ab767f0c176bc2ab0e96b03e1cd5c0' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Total (inc. VAT)',
    'comment' => NULL,
    'translation' => '合計（税込）',
    'key' => '10ab767f0c176bc2ab0e96b03e1cd5c0',
  ),
  '00a223ffce81e8873ad13809fd7a6b87' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Time',
    'comment' => NULL,
    'translation' => '注文日時',
    'key' => '00a223ffce81e8873ad13809fd7a6b87',
  ),
  'f3fb8f4a02c173d5216840cf5a61d46c' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Ascending',
    'comment' => NULL,
    'translation' => '昇順',
    'key' => 'f3fb8f4a02c173d5216840cf5a61d46c',
  ),
  '826d62a4564b50229671c4458a9030eb' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Descending',
    'comment' => NULL,
    'translation' => '降順',
    'key' => '826d62a4564b50229671c4458a9030eb',
  ),
  'ca99bf2476171f84c9c86e98ea17e730' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Invert selection.',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => 'ca99bf2476171f84c9c86e98ea17e730',
  ),
  '8dfd45d53ee9afaa30ecd96cae0a5a04' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Select order for removal.',
    'comment' => NULL,
    'translation' => '削除する注文の選択',
    'key' => '8dfd45d53ee9afaa30ecd96cae0a5a04',
  ),
  '13b11dda8d6d81c66220b9d8a729db7b' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'The order list is empty.',
    'comment' => NULL,
    'translation' => '注文はありません',
    'key' => '13b11dda8d6d81c66220b9d8a729db7b',
  ),
  '9ce56688b0159640aa11bf143f8ee385' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Status',
    'comment' => NULL,
    'translation' => 'ステータス',
    'key' => '9ce56688b0159640aa11bf143f8ee385',
  ),
  '4cda1c8bce97d2ba3cfe91d167e3b54e' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Apply changes',
    'comment' => NULL,
    'translation' => '変更を適用',
    'key' => '4cda1c8bce97d2ba3cfe91d167e3b54e',
  ),
  'b66ea61a777ebdbe41cec473f166bcd3' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Click this button to store changes if you have modified any of the fields above.',
    'comment' => NULL,
    'translation' => '修正した項目があれば、このボタンをクリックすると変更が保存されます。',
    'key' => 'b66ea61a777ebdbe41cec473f166bcd3',
  ),
  '27034ea97ea92cb7529f46b6532f2d23' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Archive selected',
    'comment' => NULL,
    'translation' => '選択した項目をアーカイブ',
    'key' => '27034ea97ea92cb7529f46b6532f2d23',
  ),
  'adf5bda411541e8148c0564bf0c080df' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Archive selected orders.',
    'comment' => NULL,
    'translation' => '選択した注文のアーカイブ。',
    'key' => 'adf5bda411541e8148c0564bf0c080df',
  ),
  '35d37b6fa79a2e8970f5bc3c2644db07' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => '( removed )',
    'comment' => NULL,
    'translation' => '( 削除済み )',
    'key' => '35d37b6fa79a2e8970f5bc3c2644db07',
  ),
  '84ec2375c7f4966307b632895f1e11ed' => 
  array (
    'context' => 'design/admin/shop/orderlist',
    'source' => 'Orders (%count)',
    'comment' => NULL,
    'translation' => '注文数 (%count)',
    'key' => '84ec2375c7f4966307b632895f1e11ed',
  ),
);
?>
