<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/dashboard/drafts',
);

$TranslationRoot = array (
  '813746a048d5cb6c34e8b2b4bc99fcc9' => 
  array (
    'context' => 'design/admin/dashboard/drafts',
    'source' => 'My drafts',
    'comment' => NULL,
    'translation' => '下書き',
    'key' => '813746a048d5cb6c34e8b2b4bc99fcc9',
  ),
  'b67a9a4a13ef9de703e2ef55d48a8c44' => 
  array (
    'context' => 'design/admin/dashboard/drafts',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => 'b67a9a4a13ef9de703e2ef55d48a8c44',
  ),
  'a6d799376a527e39a14e7f304c71af0c' => 
  array (
    'context' => 'design/admin/dashboard/drafts',
    'source' => 'Type',
    'comment' => NULL,
    'translation' => '種類',
    'key' => 'a6d799376a527e39a14e7f304c71af0c',
  ),
  '8e3fca08338da70b0f05e54d76d048bd' => 
  array (
    'context' => 'design/admin/dashboard/drafts',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => '8e3fca08338da70b0f05e54d76d048bd',
  ),
  'b61853b1859716982989162ddac6de32' => 
  array (
    'context' => 'design/admin/dashboard/drafts',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => 'b61853b1859716982989162ddac6de32',
  ),
  '630c7de5b11d5b8795ee8f3dd65318b7' => 
  array (
    'context' => 'design/admin/dashboard/drafts',
    'source' => 'Edit <%draft_name>.',
    'comment' => NULL,
    'translation' => '<%draft_name>を編集。',
    'key' => '630c7de5b11d5b8795ee8f3dd65318b7',
  ),
  '405090d1814cbb326a293010405988b3' => 
  array (
    'context' => 'design/admin/dashboard/drafts',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => '405090d1814cbb326a293010405988b3',
  ),
  '364759e9386f253c261bdd7c38ee3d61' => 
  array (
    'context' => 'design/admin/dashboard/drafts',
    'source' => 'Currently you do not have any drafts available.',
    'comment' => NULL,
    'translation' => '下書きがありません。',
    'key' => '364759e9386f253c261bdd7c38ee3d61',
  ),
);
?>
