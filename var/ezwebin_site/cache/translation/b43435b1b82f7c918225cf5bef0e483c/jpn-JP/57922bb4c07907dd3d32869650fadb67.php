<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/shop/editvatrule',
);

$TranslationRoot = array (
  '783a360f1d07ebbf40f31b23ef083d83' => 
  array (
    'context' => 'kernel/shop/editvatrule',
    'source' => 'Invalid data entered',
    'comment' => NULL,
    'translation' => '無効なデータが入力されました',
    'key' => '783a360f1d07ebbf40f31b23ef083d83',
  ),
  '0a64e1ee7f970def3621849b22bd5afe' => 
  array (
    'context' => 'kernel/shop/editvatrule',
    'source' => 'Choose a country.',
    'comment' => NULL,
    'translation' => '国を選択して下さい。',
    'key' => '0a64e1ee7f970def3621849b22bd5afe',
  ),
  'e8f3c3949d1907bc898ceca67949dcd0' => 
  array (
    'context' => 'kernel/shop/editvatrule',
    'source' => 'Choose a VAT type.',
    'comment' => NULL,
    'translation' => '課税方式を選択して下さい。',
    'key' => 'e8f3c3949d1907bc898ceca67949dcd0',
  ),
  '1b76a6ae739907bc57f8c838e59cd3b8' => 
  array (
    'context' => 'kernel/shop/editvatrule',
    'source' => 'Conflicting rule',
    'comment' => NULL,
    'translation' => '矛盾したルール',
    'key' => '1b76a6ae739907bc57f8c838e59cd3b8',
  ),
  'b57bc4dadf1d56d05fe9242e145105f7' => 
  array (
    'context' => 'kernel/shop/editvatrule',
    'source' => 'Default rule for any country already exists.',
    'comment' => NULL,
    'translation' => 'すでに登録済みの国に対するデフォルト・ルール。',
    'key' => 'b57bc4dadf1d56d05fe9242e145105f7',
  ),
  'd90f35e98fa0ee39d601324f12b3d7d6' => 
  array (
    'context' => 'kernel/shop/editvatrule',
    'source' => 'Rule not found',
    'comment' => NULL,
    'translation' => 'ルールが見つかりません',
    'key' => 'd90f35e98fa0ee39d601324f12b3d7d6',
  ),
  '97a4e16337fa578eb25a025dd5f1525f' => 
  array (
    'context' => 'kernel/shop/editvatrule',
    'source' => 'Edit VAT charging rule',
    'comment' => NULL,
    'translation' => '課税ルールの編集',
    'key' => '97a4e16337fa578eb25a025dd5f1525f',
  ),
  'b93250a99743efa145338be1d56013c5' => 
  array (
    'context' => 'kernel/shop/editvatrule',
    'source' => 'Create new VAT charging rule',
    'comment' => NULL,
    'translation' => '新規の課税ルール作成',
    'key' => 'b93250a99743efa145338be1d56013c5',
  ),
);
?>
