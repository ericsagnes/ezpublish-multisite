<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/ezoe/help',
);

$TranslationRoot = array (
  'ce2f4b16ac9a14dda50143c9b687fae1' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Using the toolbar',
    'comment' => NULL,
    'translation' => 'ツールバーの使用',
    'key' => 'ce2f4b16ac9a14dda50143c9b687fae1',
  ),
  '7317d89f5dde22a00b3dcbffcc628ff2' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Make the selected text <b>bold</b>. If the selected text is <b>bold</b> already, this button will remove the formating.',
    'comment' => NULL,
    'translation' => '選択したテキストを<b>太字</b>にする。選択されたテキストが既に<b>太字</b>の場合、太字を取り消します。',
    'key' => '7317d89f5dde22a00b3dcbffcc628ff2',
  ),
  'd215d083240f2d8b8d4ba6edb8a5c44f' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Make the selected text <i>italic</i>. If the selected text is <i>italic</i> already, this button will remove the formating.',
    'comment' => NULL,
    'translation' => '選択したテキストを<i>斜体</i>にする。選択されたテキストが既に<i>斜体</i>の場合、斜体を取り消します。',
    'key' => 'd215d083240f2d8b8d4ba6edb8a5c44f',
  ),
  'f63f5f31d2f58e7f66a2d5468f05e01a' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Create a bullet list. To create a new list item, press "Enter". To end a list, press "Enter" key on an empty list item. If you click this button when the cursor is on a list item, the formatting will be removed.',
    'comment' => NULL,
    'translation' => '箇条書きを作成。リストの新しい段落を作成するには“Enter”キーを押します。リストを終了するには、空のリスト行で“バックスペース”キーを押します。カーソルがリスト内にある時にこのボタンをクリックすると適用が解除されます。',
    'key' => 'f63f5f31d2f58e7f66a2d5468f05e01a',
  ),
  '3d363cce2b9893cd26623008990069a9' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Create a numbered list. To create a new list item, press "Enter". To end a list, press "Enter" key on an empty list item. If you click this button when the cursor is on a list item, the formatting will be removed.',
    'comment' => NULL,
    'translation' => '番号付きリストを作成。 新しい行を作るには"Enter"キーで改行します。リストを終了するには行が空の状態で"Enter"キーを押します。カーソルが既存のリスト上にある時にこのボタンをクリックするとリストが解除されます。',
    'key' => '3d363cce2b9893cd26623008990069a9',
  ),
  '045099382aee8ee522b2e8146b6d867e' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Increase list indent. Use this button to change the level of a list item in a nested list.',
    'comment' => NULL,
    'translation' => 'リストのインデントを増やす。リストの入れ子にあるリスト項目のレベルを変更するにはこのボタンを使います。',
    'key' => '045099382aee8ee522b2e8146b6d867e',
  ),
  '7dfbe63f628800bd928e11e67877e7b2' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Decrease list indent. Use this button to change the level of a list item in a nested list.',
    'comment' => NULL,
    'translation' => 'リストのインデントを減らす。リストの入れ子にあるリスト項目のレベルを変更するにはこのボタンを使います。',
    'key' => '7dfbe63f628800bd928e11e67877e7b2',
  ),
  '446f20c43943cdd7e59e2eb6ba34af2d' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Undo the last operation in the editor. To undo more than one operation, keep clicking the button.',
    'comment' => NULL,
    'translation' => 'エディタで行った最新の作業を元に戻す。一つ以上の作業を元に戻すには、このボタンを繰り返しクリックして下さい。',
    'key' => '446f20c43943cdd7e59e2eb6ba34af2d',
  ),
  '27de597586c733722e483d224e4d4092' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Reverse the "Undo" command.',
    'comment' => NULL,
    'translation' => '「元に戻す」を取り消す。',
    'key' => '27de597586c733722e483d224e4d4092',
  ),
  '23fe4e0f19a04a9c63c0265a34b69510' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Create a hyperlink. You can select text first and then click this button to make the text a link. If the checkbox "Open in new window" is checked, the link will be displayed in a new browser window.',
    'comment' => NULL,
    'translation' => 'リンクを作成。テキストをリンクにするには、テキストを選択し、このボタンをクリックしてください。”新規ウインドウで開く”チェックボックスをチェックすると、リンクは新しいブラウザウインドウに開かれます。',
    'key' => '23fe4e0f19a04a9c63c0265a34b69510',
  ),
  '33b3d7ea6b7810348a506141f290216d' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Removes a hyperlink. Select a link first and then click this button to remove the link (but not the content of the link).',
    'comment' => NULL,
    'translation' => 'ハイパーリンクを取り除く。まずリンクを選択してから、このボタンをクリックしてリンクを取り除いて下さい。(リンクされていたコンテンツは残ります。)',
    'key' => '33b3d7ea6b7810348a506141f290216d',
  ),
  'acb7010864cbe875137442492b1e5631' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Create a named anchor. An anchor-like icon will appear in the editor.',
    'comment' => NULL,
    'translation' => 'アンカーを作成。錨のアイコンが表示されます。',
    'key' => 'acb7010864cbe875137442492b1e5631',
  ),
  'df36c540459968c5dfdd5bf0b72faa1c' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Insert an image from the related images list, upload a new image, search for an existing images or browse for it. To upload a local image choose the local file, specify the name of the new object, choose placement from list, optionally write some caption text (You can use simple html formating here) and then click "Upload" button.',
    'comment' => NULL,
    'translation' => '関連画像リストから画像を挿入、新規画像をアップロード、既存の画像を検索、画像をブラウズ。ローカルファイルから画像をアップロードするには、ローカルファイルを選択し、新規オブジェクト名を指定、配置先を選択、オプショナルでキャプションテキストを指定し、"アップロード"ボタンを押して下さい。',
    'key' => 'df36c540459968c5dfdd5bf0b72faa1c',
  ),
  'aa4576ed3de7ff0bb1f7e1a0fec6fc01' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Insert an object from the related objects list, upload a new object, search for an existing object or browse for it. To upload a local file, click "Upload new" button choose the local file, specify the name of the new object, choose placement from list and then click "Upload" button. Note that embedded object will begin on a new line when displayed in the resulting XHTML.',
    'comment' => NULL,
    'translation' => '関連オブジェクトリストからファイルを挿入、新規オブジェクトをアップロード、既存のオブジェクトを検索、オブジェクトをブラウズ。ローカルファイルをアップロードするには、"新規アップロード"ボタンをクリックし、ローカルファイルを選択、新規オブジェクト名を指定、配置先を選択し、"アップロード"ボタンを押して下さい。XHTMLでは、埋め込みオブジェクトは改行後に表示されます。',
    'key' => 'aa4576ed3de7ff0bb1f7e1a0fec6fc01',
  ),
  '051b69afa0510ed1e1f0b3eb846f7ca4' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Create a custom tag. Optionally select the text you want to transform to a custom tag and click the button to open the insert custom tag window. Select the name of the custom tag you want to insert from the list, edit the attributes and click OK to insert it.',
    'comment' => NULL,
    'translation' => 'カスタムタグ挿入。カスタムタグの対象となるテキストを選択し、ボタンをクリックしてカスタムタグ挿入ウィンドウを開きます。リストから挿入するカスタムタグを選択し、属性を指定後、OKをクリックして挿入します。',
    'key' => '051b69afa0510ed1e1f0b3eb846f7ca4',
  ),
  'ce5031e9f3569ef342f1847ed29609df' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Insert literal text. Text written in this field will be rendered literally in the final output.',
    'comment' => NULL,
    'translation' => 'リテラルテキストを挿入。このボックスの中に書かれる内容はリテラルテキストとして出力されます。',
    'key' => 'ce5031e9f3569ef342f1847ed29609df',
  ),
  '4191fce2e28a272b3528bbe5a7d11d42' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Insert a special character. Click the button to open the special character window. Click on a character to insert it.',
    'comment' => NULL,
    'translation' => '特殊文字を挿入。特殊文字ウィンドウを開くにはこのボタンをクリックしてください。文字をクリックすると挿入されます。',
    'key' => '4191fce2e28a272b3528bbe5a7d11d42',
  ),
  'bf31d637d059434ddb76c26fdd9c05ad' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Insert a pagebreak. This button is only enabled if you have a custom tag named pagebreak, template code to handle pagebreaks is not included in Online Editor.',
    'comment' => NULL,
    'translation' => 'ページの区切りを挿入。このボタンはページ区切り用のカスタムタグ、カスタムタグを処理するテンプレートが設定されている場合のみ有効です。標準ではテンプレートは含まれていません。',
    'key' => 'bf31d637d059434ddb76c26fdd9c05ad',
  ),
  '1242e97678199fe3954cc10d8b7ae466' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Dialog to paste text from word, the dialog will handle cleaning the content from word.',
    'comment' => NULL,
    'translation' => 'Wordからテキストを貼付け。このダイアローグがWordのコンテントを整えます。',
    'key' => '1242e97678199fe3954cc10d8b7ae466',
  ),
  '1644f8dd9badc302e695e02f1e123c0c' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Insert a table at the selected position. Tables with their border set to 0 are displayed with a grey border color in the editor.',
    'comment' => NULL,
    'translation' => '選択した位置に表を挿入。境界線が0に設定されている表は、エディタ上では灰色の境界線で表示されます。',
    'key' => '1644f8dd9badc302e695e02f1e123c0c',
  ),
  '672ed690bee54684e04c5115c4e568f2' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Delete the currently selected table.',
    'comment' => NULL,
    'translation' => '現在選択している表を削除。',
    'key' => '672ed690bee54684e04c5115c4e568f2',
  ),
  '9f5b0de509c42afd8092c2e1cf6fb029' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Delete the current column.',
    'comment' => NULL,
    'translation' => '現在の列を削除。',
    'key' => '9f5b0de509c42afd8092c2e1cf6fb029',
  ),
  '56117682a6d6bf64e3d2d06567251fba' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Insert a column to the left of the current cell.',
    'comment' => NULL,
    'translation' => '列を現在の列の左に挿入。',
    'key' => '56117682a6d6bf64e3d2d06567251fba',
  ),
  '2dbbb721de2f35c7a2938b5acd9f681d' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Delete the current row.',
    'comment' => NULL,
    'translation' => '現在の行を削除。',
    'key' => '2dbbb721de2f35c7a2938b5acd9f681d',
  ),
  'b65625762c8e9ac4603f32aafe77fe1c' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Insert a row bellow the current row.',
    'comment' => NULL,
    'translation' => '行を現在の行の下に挿入。',
    'key' => 'b65625762c8e9ac4603f32aafe77fe1c',
  ),
  '5e033ce284c1af136391acbe7b5b68a0' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Split the current table cell into two cells.',
    'comment' => NULL,
    'translation' => '現在のセルを二つのセルに分割。',
    'key' => '5e033ce284c1af136391acbe7b5b68a0',
  ),
  '342038f20f2a762a48723d226671e26f' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Merge the selected table cells into one cell. (Select several cells with shift+click or Ctrl+click)',
    'comment' => NULL,
    'translation' => '選択されているセルを一つのセルに統合。(Shift+clickもしくはCtrl+clickで複数のセルを選択します)',
    'key' => '342038f20f2a762a48723d226671e26f',
  ),
  'fc5837931167e6a36747f64af1999696' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Open Online Editor xhtml source code editor. This button is not enabled by default, and is only intended for experienced users.',
    'comment' => NULL,
    'translation' => 'Online EditorのXHTMLソースコードエディタを開く。このボタンは標準では有効になっていません。経験豊富なユーザ向けです。',
    'key' => 'fc5837931167e6a36747f64af1999696',
  ),
  'ed0fa3148e8d009430512b5d8e0f9423' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Edit the current content attribute in the whole browser window("Fullscreen"). Click second time to go back to normal editing.',
    'comment' => NULL,
    'translation' => '現在のコンテント属性をフルスクリーンで編集する。フルスクリーンを中止するには再度このボタンをクリックして下さい。',
    'key' => 'ed0fa3148e8d009430512b5d8e0f9423',
  ),
  '5e87f3e34023b9466d78baee3419382d' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Open this help window.',
    'comment' => NULL,
    'translation' => 'ヘルプウィンドウを開く。',
    'key' => '5e87f3e34023b9466d78baee3419382d',
  ),
  '8401d1fa08d1743a627a70b3690aa6b7' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Icons in dialog windows',
    'comment' => NULL,
    'translation' => 'ダイアローグウィンドウのアイコン',
    'key' => '8401d1fa08d1743a627a70b3690aa6b7',
  ),
  'c00f5a56d2e05ff1b65c985093f8f446' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Browse for a node / object.',
    'comment' => NULL,
    'translation' => 'ノード/オブジェクトをブラウズする。',
    'key' => 'c00f5a56d2e05ff1b65c985093f8f446',
  ),
  '74c71b979855e0690446c5db99192366' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Search for a node / object.',
    'comment' => NULL,
    'translation' => 'ノード/オブジェクトを検索する。',
    'key' => '74c71b979855e0690446c5db99192366',
  ),
  'd5a5333b7b80ac694092b2c0e28e1268' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Browse for a node / object in your bookmarks.',
    'comment' => NULL,
    'translation' => 'ブックマークからノード/オブジェクトをブラウズする。',
    'key' => 'd5a5333b7b80ac694092b2c0e28e1268',
  ),
  'dc588a27db813857e7062bb76f0a8159' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Tips &amp; Tricks',
    'comment' => NULL,
    'translation' => 'こつ',
    'key' => 'dc588a27db813857e7062bb76f0a8159',
  ),
  '40ba383909cb836e438d56f00d1784a7' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'You can adjust the height of the editor by draging the bottom right corner of the editor.',
    'comment' => NULL,
    'translation' => 'エディタの右下をドラッグすると、エディタの高さを調整出来ます。',
    'key' => '40ba383909cb836e438d56f00d1784a7',
  ),
  '3ccc1113dbd1aeccac9ffa808e4ad864' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'You can create a new line by holding the Shift key down and pressing Enter key.',
    'comment' => NULL,
    'translation' => 'Shiftを押している間にEnterを入力すると、改行出来ます。',
    'key' => '3ccc1113dbd1aeccac9ffa808e4ad864',
  ),
  '7c3dd3be354b7fbd295b7e5f9c958f6e' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'You can create a new paragraph by pressing the Enter key.',
    'comment' => NULL,
    'translation' => 'Enterを押すと新しい段落が作成されます。',
    'key' => '7c3dd3be354b7fbd295b7e5f9c958f6e',
  ),
  '315fb42d831e83347af5d2700ee6a107' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'The status bar will show the current tag name and all its parent tags. You can view more information about the tags by hovering over them.',
    'comment' => NULL,
    'translation' => 'ステータスバーは、現在のタグ名と親タグを表示します。タグの上にカーソルを持っていくと、より詳しい情報を見ることができます。',
    'key' => '315fb42d831e83347af5d2700ee6a107',
  ),
  'bf3e1f95852aac10333f0f454dffe6a3' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'You can make an image-link by selecting the image first and clicking the link button in the toolbar.',
    'comment' => NULL,
    'translation' => '画像を選択した状態でリンクボタンをクリックすると、画像にリンクをつけることが出来ます。',
    'key' => 'bf3e1f95852aac10333f0f454dffe6a3',
  ),
  '7188baae5a2a2acab7232c1c830d762e' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'You can edit wordmatch.ini to make text copied from MS Word directly assigned to a desired class.',
    'comment' => NULL,
    'translation' => 'MS Wordからコピーされたテキストを直接希望のクラスに割り当てるには、wordmatch.iniを編集して下さい。',
    'key' => '7188baae5a2a2acab7232c1c830d762e',
  ),
  '68ebdcdf541aee9b20e8b99081e19f0d' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'You can find more documentation in the doc folder of this extension and online on %link.',
    'comment' => NULL,
    'translation' => 'このエクステンションのdocフォルダー、または次のリンクにドキュメンテーションが保存されてあります: %link',
    'key' => '68ebdcdf541aee9b20e8b99081e19f0d',
  ),
  'e1ca8b2b2c4e081ab85111f724bc9549' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Toggle <u>underline</u> style on the selected text. This button is only enabled if you have a custom tag named underline.',
    'comment' => NULL,
    'translation' => '選択したテキストに<u>下線付き文字</u>スタイルを当てる。「underline」カスタムタグが存在する場合にこのボタンは有効となります。オンラインエディターに下線付き文字を扱うテンプレートコードはついていません。',
    'key' => 'e1ca8b2b2c4e081ab85111f724bc9549',
  ),
  'b213bf9e06ca5b1bf960516190810112' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Toggle <sub>subscript</sub> style on the selected text. This button is only enabled if you have a custom tag named subscript, template code to handle subscript custom tags is not included in Online Editor.',
    'comment' => NULL,
    'translation' => '選択したテキストに<sub>下付き文字</sub>スタイルを当てる。「subscript」カスタムタグが存在する場合にこのボタンは有効となります。オンラインエディターに下付き文字を扱うテンプレートコードはついていません。',
    'key' => 'b213bf9e06ca5b1bf960516190810112',
  ),
  'beafe3e2e8d638ce7dfda4639b677671' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Toggle <sup>superscript</sup> style on the selected text. This button is only enabled if you have a custom tag named superscript, template code to handle superscript custom tags is not included in Online Editor.',
    'comment' => NULL,
    'translation' => '選択したテキストに<sup>上付き文字</sup>スタイルを当てる。「superscript」カスタムタグが存在する場合にこのボタンは有効となります。オンラインエディターに上付き文字を扱うテンプレートコードはついていません。',
    'key' => 'beafe3e2e8d638ce7dfda4639b677671',
  ),
  'ffa792467ffb27b12d0eaac7abfef5f0' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Toggle left align text, or float block content to the left.',
    'comment' => NULL,
    'translation' => 'テキストを左添えかコンテンツブロックを左にフロートする。',
    'key' => 'ffa792467ffb27b12d0eaac7abfef5f0',
  ),
  'ea23ef9fa9012a6144e872881e84ae26' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Toggle center align text, or float block content to the center (Same as not aligned by default).',
    'comment' => NULL,
    'translation' => 'テキストを中央添えかコンテンツブロックを中央にフロートする（デフォルトと同様）。',
    'key' => 'ea23ef9fa9012a6144e872881e84ae26',
  ),
  'e8700b3fb8ca5c7004d022a9bbca4cd8' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Toggle right align text, or float block content to the right.',
    'comment' => NULL,
    'translation' => 'テキストを左添えかコンテンツブロックを左にフロートする。',
    'key' => 'e8700b3fb8ca5c7004d022a9bbca4cd8',
  ),
  'd4dd3f8d6d2adb3527ce1cb40986f5eb' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Toggle justify text, stretches the lines so that each line has equal width.',
    'comment' => NULL,
    'translation' => 'テキスト正当化を有効に、全行は同じ幅になる様に伸ばす。',
    'key' => 'd4dd3f8d6d2adb3527ce1cb40986f5eb',
  ),
  'fbaccbbcbc8a48e0875b53e404008c7a' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Insert an file from the related file list, upload a new file, search for an existing file or browse for it. To upload a local file, click "Upload new" button choose the local file, specify the name of the new file, choose placement from list and then click "Upload" button. This button is not enabled by default.',
    'comment' => NULL,
    'translation' => '関連ファイルリストからファイルを挿入、新ファイルをアップロード、存在するファイルを検索やブラウズする。ローカルファイルをアップロードする様に「新規アップロード」ボタンをクリックして、ローカルファイルを選択し、ファイルの新しい名前を設定、リストから配置先を選んで、「アップロード」ボタンをクリックしてください。このボタンはデフォルトで無効となります。',
    'key' => 'fbaccbbcbc8a48e0875b53e404008c7a',
  ),
  '5d5934f0679c5bc36e0ee2a9cac4de4e' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Allows you to spellcheck your text using google api or other depending on ezoe.ini settings. This button is not enabled by default.',
    'comment' => NULL,
    'translation' => 'ezoeで設定したGoogle APIや他のAPIでスペルチェックを有効にします。このボタンはデフォルトで無効です。',
    'key' => '5d5934f0679c5bc36e0ee2a9cac4de4e',
  ),
  '774cbb5208a5edacf7328959841da295' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Find a string or a word in your text. This button is not enabled by default.',
    'comment' => NULL,
    'translation' => 'テキストで文字列や言葉の検索。このボタンはデフォルトで無効です。',
    'key' => '774cbb5208a5edacf7328959841da295',
  ),
  '08e9d0ba6cb882c244a03dbbf859b085' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'Replace a string or a word in your text. This button is not enabled by default.',
    'comment' => NULL,
    'translation' => 'テキストで文字列や言葉の置換。このボタンはデフォルトで無効です。',
    'key' => '08e9d0ba6cb882c244a03dbbf859b085',
  ),
  '9e8a7a0656e20112d9e15bd78aac2890' => 
  array (
    'context' => 'design/standard/ezoe/help',
    'source' => 'You can switch text style from paragraph to header using keybord shortcut CTRL+1 to 6, and back to paragraph using CTRL+7',
    'comment' => NULL,
    'translation' => 'CTRL+1～6キーボードショートカットで段落から見出しのスタイル変更できます、CTRL+7で段落に設定できます',
    'key' => '9e8a7a0656e20112d9e15bd78aac2890',
  ),
);
?>
