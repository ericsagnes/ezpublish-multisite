<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/section',
);

$TranslationRoot = array (
  '374474c44ff54d768650b34b9b88065c' => 
  array (
    'context' => 'kernel/section',
    'source' => 'Sections',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => '374474c44ff54d768650b34b9b88065c',
  ),
  '4d1f2283180d41914cec796a3b5d5a12' => 
  array (
    'context' => 'kernel/section',
    'source' => 'Assign section',
    'comment' => NULL,
    'translation' => 'セクションの割り当て',
    'key' => '4d1f2283180d41914cec796a3b5d5a12',
  ),
  '0aeff7c36cf56d48c3266516b6c6e941' => 
  array (
    'context' => 'kernel/section',
    'source' => 'New section',
    'comment' => NULL,
    'translation' => '新規セクション',
    'key' => '0aeff7c36cf56d48c3266516b6c6e941',
  ),
  'bc6c39e83441551a4a2b601a22982357' => 
  array (
    'context' => 'kernel/section',
    'source' => 'Edit Section',
    'comment' => NULL,
    'translation' => 'セクションの編集',
    'key' => 'bc6c39e83441551a4a2b601a22982357',
  ),
  '9257a9801ddb854a8beb6c34f4f6c15f' => 
  array (
    'context' => 'kernel/section',
    'source' => 'View section',
    'comment' => NULL,
    'translation' => 'セクションの表示',
    'key' => '9257a9801ddb854a8beb6c34f4f6c15f',
  ),
);
?>
