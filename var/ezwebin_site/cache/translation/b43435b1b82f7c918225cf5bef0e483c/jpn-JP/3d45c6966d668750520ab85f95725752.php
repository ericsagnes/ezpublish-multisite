<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/package',
);

$TranslationRoot = array (
  'dc48b47e7348e718c2e0c7d3a42d84b6' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Lead',
    'comment' => NULL,
    'translation' => 'リード',
    'key' => 'dc48b47e7348e718c2e0c7d3a42d84b6',
  ),
  '8283243e7af61a2b648b446209fc6350' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Developer',
    'comment' => NULL,
    'translation' => 'デベロッパー',
    'key' => '8283243e7af61a2b648b446209fc6350',
  ),
  '07f59dca207a038841066657441022a8' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Designer',
    'comment' => NULL,
    'translation' => 'デザイナー',
    'key' => '07f59dca207a038841066657441022a8',
  ),
  'd3ad67e6f9d786a4b9d98482c2d9f4f7' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Contributor',
    'comment' => NULL,
    'translation' => 'コントリビュータ',
    'key' => 'd3ad67e6f9d786a4b9d98482c2d9f4f7',
  ),
  '794fcc4955cee84963ab8457f4f296f0' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Tester',
    'comment' => NULL,
    'translation' => 'テスター',
    'key' => '794fcc4955cee84963ab8457f4f296f0',
  ),
  'b3d8789f7ce2b9a5e781c24de2fc990d' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Local',
    'comment' => NULL,
    'translation' => 'ローカル',
    'key' => 'b3d8789f7ce2b9a5e781c24de2fc990d',
  ),
  '97fcb97f5c56ab71b7f777f0410fc7b6' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Package information',
    'comment' => NULL,
    'translation' => 'パッケージプロパティ',
    'key' => '97fcb97f5c56ab71b7f777f0410fc7b6',
  ),
  'fe47a125f2186aeca2190c7c8f72a275' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Package maintainer',
    'comment' => NULL,
    'translation' => 'パッケージ保守管理者',
    'key' => 'fe47a125f2186aeca2190c7c8f72a275',
  ),
  'd9e772c5d36d3ba95df2fb4c20e9e1bc' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Package changelog',
    'comment' => NULL,
    'translation' => 'パッケージ変更履歴',
    'key' => 'd9e772c5d36d3ba95df2fb4c20e9e1bc',
  ),
  '7c92d88bae1c96f1d89f88e951540ddf' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Package thumbnail',
    'comment' => NULL,
    'translation' => 'パッケージのサムネール画像',
    'key' => '7c92d88bae1c96f1d89f88e951540ddf',
  ),
  '7b3f1ee830651ca3c7f63917ec02bfc1' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Package name',
    'comment' => NULL,
    'translation' => 'パッケージ名',
    'key' => '7b3f1ee830651ca3c7f63917ec02bfc1',
  ),
  '1e512835e448d903eff5051eb4af7021' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Package name is missing',
    'comment' => NULL,
    'translation' => 'パッケージ名が未入力です',
    'key' => '1e512835e448d903eff5051eb4af7021',
  ),
  '909cfc3731e4dfcac5a254100228d328' => 
  array (
    'context' => 'kernel/package',
    'source' => 'A package named %packagename already exists, please give another name',
    'comment' => NULL,
    'translation' => '%packagenameというパッケージ名は既に利用されています。他の名前を選択して下さい',
    'key' => '909cfc3731e4dfcac5a254100228d328',
  ),
  'fcbc217783fe34c4f769b406249441eb' => 
  array (
    'context' => 'kernel/package',
    'source' => 'The package name %packagename is not valid, it can only contain characters in the range a-z, 0-9 and underscore.',
    'comment' => NULL,
    'translation' => 'パッケージ名、%packagenameは不正です。パッケージ名にはアルファベット、数字、アンダースコアのみ利用出来ます。',
    'key' => 'fcbc217783fe34c4f769b406249441eb',
  ),
  '21c0658500b62d328cf2ee10f40767b1' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Summary',
    'comment' => NULL,
    'translation' => 'サマリ',
    'key' => '21c0658500b62d328cf2ee10f40767b1',
  ),
  'bc92570fdef93145747422c7be291679' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Summary is missing',
    'comment' => NULL,
    'translation' => 'サマリが未入力です',
    'key' => 'bc92570fdef93145747422c7be291679',
  ),
  '9eaf6c3a567777ce5884265c4a830971' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => '9eaf6c3a567777ce5884265c4a830971',
  ),
  '12f0292af3ec9b9baee69c653e00cd28' => 
  array (
    'context' => 'kernel/package',
    'source' => 'The version must only contain numbers (optionally followed by text) and must be delimited by dots (.), e.g. 1.0, 3.4.0beta1',
    'comment' => NULL,
    'translation' => 'バージョンは数字（末尾のテキストは許される）とドットのみで構成しなければなりません。例えば: 1.0や3.4.0beta1',
    'key' => '12f0292af3ec9b9baee69c653e00cd28',
  ),
  'b560d13dda4b110b81df7ee4a130275b' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => 'b560d13dda4b110b81df7ee4a130275b',
  ),
  'd00bdba8dcf29de198c41e81aeafaa54' => 
  array (
    'context' => 'kernel/package',
    'source' => 'You must enter a name for the changelog',
    'comment' => NULL,
    'translation' => '変更履歴の名前（作成者）は必須入力です',
    'key' => 'd00bdba8dcf29de198c41e81aeafaa54',
  ),
  '77e225188095a6f95a4694e6f492df1a' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Email',
    'comment' => NULL,
    'translation' => 'メールアドレス',
    'key' => '77e225188095a6f95a4694e6f492df1a',
  ),
  '0285f0f363522b3e678b061e07161ff2' => 
  array (
    'context' => 'kernel/package',
    'source' => 'You must enter an email for the changelog',
    'comment' => NULL,
    'translation' => '変更履歴のメールアドレス（作成者）は必須入力です',
    'key' => '0285f0f363522b3e678b061e07161ff2',
  ),
  'b06ba50f529732fbee2d928798fe116f' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Changelog',
    'comment' => NULL,
    'translation' => '変更履歴',
    'key' => 'b06ba50f529732fbee2d928798fe116f',
  ),
  '7e8ca93ebb3fe9c10018059eb36362f4' => 
  array (
    'context' => 'kernel/package',
    'source' => 'You must supply some text for the changelog entry',
    'comment' => NULL,
    'translation' => '変更点は必須入力です',
    'key' => '7e8ca93ebb3fe9c10018059eb36362f4',
  ),
  '6e5ddd1838f68411b88c8dfb5d681e0d' => 
  array (
    'context' => 'kernel/package',
    'source' => 'You must enter a name of the maintainer',
    'comment' => NULL,
    'translation' => '保守管理者の名前は必須入力です',
    'key' => '6e5ddd1838f68411b88c8dfb5d681e0d',
  ),
  'e87ef34c49c9ef02fa961af01b7e9f81' => 
  array (
    'context' => 'kernel/package',
    'source' => 'You must enter an email address of the maintainer',
    'comment' => NULL,
    'translation' => '保守管理者のeメールアドレスは必須入力です',
    'key' => 'e87ef34c49c9ef02fa961af01b7e9f81',
  ),
  'b7acc05255e56fcd74f93da3e00594cb' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Content classes to include',
    'comment' => NULL,
    'translation' => '対象クラス',
    'key' => 'b7acc05255e56fcd74f93da3e00594cb',
  ),
  '300ecf391a1955a5fa82520abd3ac4a0' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Content class export',
    'comment' => NULL,
    'translation' => 'コンテンツクラスエクスポート',
    'key' => '300ecf391a1955a5fa82520abd3ac4a0',
  ),
  '742f9e8b80c9a8aadbfba334f6185d67' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Class list',
    'comment' => NULL,
    'translation' => 'クラス一覧',
    'key' => '742f9e8b80c9a8aadbfba334f6185d67',
  ),
  '45f0d6f3b4202165ed3cd02708835a94' => 
  array (
    'context' => 'kernel/package',
    'source' => 'You must select at least one class for inclusion',
    'comment' => NULL,
    'translation' => 'エクスポートするクラスを一つ以上選択して下さい',
    'key' => '45f0d6f3b4202165ed3cd02708835a94',
  ),
  '4d2d6cf107ca0c1683668be038ef0981' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Content objects to include',
    'comment' => NULL,
    'translation' => '対象コンテンツオブジェクト',
    'key' => '4d2d6cf107ca0c1683668be038ef0981',
  ),
  'c8d8a8f34ad6566d5a1ca0f417b877cd' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Content object limits',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクトの制限',
    'key' => 'c8d8a8f34ad6566d5a1ca0f417b877cd',
  ),
  '240127488a130379c074c27ed47a96f6' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Content object export',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクトエクスポート',
    'key' => '240127488a130379c074c27ed47a96f6',
  ),
  '8b831aa98f8b51734c3c530eb1213355' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Selected nodes',
    'comment' => NULL,
    'translation' => '選択済みノード',
    'key' => '8b831aa98f8b51734c3c530eb1213355',
  ),
  'a9e05a58b178ec1d4c9ffab3e1352736' => 
  array (
    'context' => 'kernel/package',
    'source' => 'You must select one or more node(s)/subtree(s) for export.',
    'comment' => NULL,
    'translation' => 'エクスポートするノード/サブツリーを一つ以上選択してください。',
    'key' => 'a9e05a58b178ec1d4c9ffab3e1352736',
  ),
  '0481542e0162337ba7cc0b233b0362b0' => 
  array (
    'context' => 'kernel/package',
    'source' => 'You must choose one or more languages.',
    'comment' => NULL,
    'translation' => '一つ以上の言語を選択してください。',
    'key' => '0481542e0162337ba7cc0b233b0362b0',
  ),
  'dfaf858b97e93d9533a678bc3077535d' => 
  array (
    'context' => 'kernel/package',
    'source' => 'You must choose one or more site access.',
    'comment' => NULL,
    'translation' => '一つ以上のサイトアクセスを選択して下さい。',
    'key' => 'dfaf858b97e93d9533a678bc3077535d',
  ),
  '3c93f1a00a0e63c4c9298660e2388233' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Extensions to include',
    'comment' => NULL,
    'translation' => '対象エクステンション',
    'key' => '3c93f1a00a0e63c4c9298660e2388233',
  ),
  'f08b3a524c0099a0330b8980da2a3394' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Extension export',
    'comment' => NULL,
    'translation' => 'エクステンションエクスポート',
    'key' => 'f08b3a524c0099a0330b8980da2a3394',
  ),
  '937a42514a2fff9aeaea66252421d194' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Extension list',
    'comment' => NULL,
    'translation' => 'エクステンションリスト',
    'key' => '937a42514a2fff9aeaea66252421d194',
  ),
  '8485294adf7b158140292957acdb3532' => 
  array (
    'context' => 'kernel/package',
    'source' => 'You must select at least one extension',
    'comment' => NULL,
    'translation' => '一つ以上のエクステンションを選択して下さい',
    'key' => '8485294adf7b158140292957acdb3532',
  ),
  '9d93901e4ae7c7b4e7491b62ebc82ed9' => 
  array (
    'context' => 'kernel/package',
    'source' => 'CSS files',
    'comment' => NULL,
    'translation' => 'CSSファイル',
    'key' => '9d93901e4ae7c7b4e7491b62ebc82ed9',
  ),
  '07c18989259b38029b7e75937deba2e5' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Image files',
    'comment' => NULL,
    'translation' => '画像ファイル',
    'key' => '07c18989259b38029b7e75937deba2e5',
  ),
  'cf10ef9f783c7382f9bd8f20feb7bd83' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Site style',
    'comment' => NULL,
    'translation' => 'サイトスタイル',
    'key' => 'cf10ef9f783c7382f9bd8f20feb7bd83',
  ),
  '16f1354ce11d0186912a924120312689' => 
  array (
    'context' => 'kernel/package',
    'source' => 'CSS file',
    'comment' => NULL,
    'translation' => 'CSSファイル',
    'key' => '16f1354ce11d0186912a924120312689',
  ),
  'faab508c015c18a84c7b12cc4a9ae043' => 
  array (
    'context' => 'kernel/package',
    'source' => 'You must upload both CSS files',
    'comment' => NULL,
    'translation' => '両種のCSSファイルのアップロードが必要です',
    'key' => 'faab508c015c18a84c7b12cc4a9ae043',
  ),
  '41939f7b1a88afdf2051cdfc31bf372d' => 
  array (
    'context' => 'kernel/package',
    'source' => 'File did not have a .css suffix, this is most likely not a CSS file',
    'comment' => NULL,
    'translation' => 'ファイルの拡張子が .css とは異なります、CSSファイルではないようです',
    'key' => '41939f7b1a88afdf2051cdfc31bf372d',
  ),
  '4050b655b20302ff9bc488078655cfa1' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Content class \'%classname\' (%classidentifier)',
    'comment' => NULL,
    'translation' => 'コンテンツクラス\'%classname\' (%classidentifier)',
    'key' => '4050b655b20302ff9bc488078655cfa1',
  ),
  '1aef13edd16a1f081aaa7b697af04ce9' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Removing class \'%classname\' will result in the removal of %objectscount object(s) of this class and all their sub-items. Are you sure you want to uninstall it?',
    'comment' => NULL,
    'translation' => ' クラス\'%classname\' を削除すると、このクラスに属する%objectscount個のオブジェクトがそのサブアイテムと共に削除されます。削除しますか？',
    'key' => '1aef13edd16a1f081aaa7b697af04ce9',
  ),
  '868fb3ad7a4a92ea217604367778a0a1' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Class \'%classname\' already exists.',
    'comment' => NULL,
    'translation' => 'クラス\'%classname\'は既に存在します。',
    'key' => '868fb3ad7a4a92ea217604367778a0a1',
  ),
  '697ca956d1a8564afb239d41e35eadc5' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Replace existing class',
    'comment' => NULL,
    'translation' => '既存のクラスに上書きする',
    'key' => '697ca956d1a8564afb239d41e35eadc5',
  ),
  'ea3bd7ae8f493d454842bcb195f8473b' => 
  array (
    'context' => 'kernel/package',
    'source' => '(Warning! $objectsCount content object(s) and their sub-items will be removed)',
    'comment' => NULL,
    'translation' => '（警告！$objectsCount個のコンテンツオブジェクトとそのサブアイテムは削除されます）',
    'key' => 'ea3bd7ae8f493d454842bcb195f8473b',
  ),
  '27686c6b9959a77b1ab489b0c914335c' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Skip installing this class',
    'comment' => NULL,
    'translation' => 'このクラスのインストールをスキップ',
    'key' => '27686c6b9959a77b1ab489b0c914335c',
  ),
  '1368227e06ca0b4bb1e0e051bc9c4830' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Keep existing and create a new one',
    'comment' => NULL,
    'translation' => '既存のものを残し、新たに作成する',
    'key' => '1368227e06ca0b4bb1e0e051bc9c4830',
  ),
  '7ad7ca96fcc36c84d19e36248059986e' => 
  array (
    'context' => 'kernel/package',
    'source' => '%number content objects',
    'comment' => NULL,
    'translation' => '%number 個のコンテンツ・オブジェクト',
    'key' => '7ad7ca96fcc36c84d19e36248059986e',
  ),
  '4ab2b9f6e1b2a999edd5f69fc7e2c9a8' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Content object %objectname',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクト %objectname',
    'key' => '4ab2b9f6e1b2a999edd5f69fc7e2c9a8',
  ),
  '33b718cbc51cecc3864d19bec04079d7' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Object \'%objectname\' has been modified since installation. Are you sure you want to remove it?',
    'comment' => NULL,
    'translation' => 'オブジェクト  \'%objectname\' はインストール以来修正されています。削除してもよろしいですか?',
    'key' => '33b718cbc51cecc3864d19bec04079d7',
  ),
  '8f38edd935376ec6ef9abd833090a166' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => '8f38edd935376ec6ef9abd833090a166',
  ),
  'b5607819df01a2301e8c01e2f3299d52' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Keep object',
    'comment' => NULL,
    'translation' => 'オブジェクトを保持する',
    'key' => 'b5607819df01a2301e8c01e2f3299d52',
  ),
  '84d992006275dc446439e77f1552f5ed' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Object \'%objectname\' has %childrencount sub-item(s) that will be removed.',
    'comment' => NULL,
    'translation' => 'オブジェクト\'%objectname\' には 削除される %childrencount個のサブアイテムがあります。',
    'key' => '84d992006275dc446439e77f1552f5ed',
  ),
  '95c5c4f5080cccf1b8eabe6c907cda30' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Remove object and its sub-item(s)',
    'comment' => NULL,
    'translation' => 'オブジェクトとそのサブアイテムを削除する',
    'key' => '95c5c4f5080cccf1b8eabe6c907cda30',
  ),
  'ed61d4cd397c4f52e28321cbee3d5342' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Extension \'%extensionname\'',
    'comment' => NULL,
    'translation' => 'エクステンション \'%extensionname\'',
    'key' => 'ed61d4cd397c4f52e28321cbee3d5342',
  ),
  'eeec70a7314188257875adcddcb89e58' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Package contains an invalid extension name: %extensionname',
    'comment' => NULL,
    'translation' => 'パッケージに不正なエクステンション名が含まれています:  %extensionname',
    'key' => 'eeec70a7314188257875adcddcb89e58',
  ),
  'b51d6b54c83e70956a7092f0c8296d79' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Extension \'%extensionname\' already exists.',
    'comment' => NULL,
    'translation' => 'エクステンション名 \'%extensionname\'は既に存在します。',
    'key' => 'b51d6b54c83e70956a7092f0c8296d79',
  ),
  '39d64541f3d1d08c9e233705e4d8fdfe' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Replace extension',
    'comment' => NULL,
    'translation' => '既存のエクステンションに上書きする',
    'key' => '39d64541f3d1d08c9e233705e4d8fdfe',
  ),
  'd824b2cec614e4f45a2dfed11d98aea4' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Skip',
    'comment' => NULL,
    'translation' => 'スキップする',
    'key' => 'd824b2cec614e4f45a2dfed11d98aea4',
  ),
  '54d0d3ae336f0436a0b0d05bd1100612' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Install script: %description',
    'comment' => NULL,
    'translation' => 'インストールスクリプト: %description',
    'key' => '54d0d3ae336f0436a0b0d05bd1100612',
  ),
  'f35d34b133ba0040f44f00a112c20f41' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Site access mapping',
    'comment' => NULL,
    'translation' => 'サイトアクセスの割り当て',
    'key' => 'f35d34b133ba0040f44f00a112c20f41',
  ),
  '0ac1622a64e026580b611bc0867516cb' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Top node placements',
    'comment' => NULL,
    'translation' => 'トップノードの配置',
    'key' => '0ac1622a64e026580b611bc0867516cb',
  ),
  '953176901424776aaae1d628019711e5' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Advanced options',
    'comment' => NULL,
    'translation' => 'その他のオプション',
    'key' => '953176901424776aaae1d628019711e5',
  ),
  '32f0a21cbb51c9d100a14b4cb70d8f5e' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Content object import',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクトインポート',
    'key' => '32f0a21cbb51c9d100a14b4cb70d8f5e',
  ),
  '7cf35414612d30964d4b2eedb84c43fc' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Select parent nodes',
    'comment' => NULL,
    'translation' => '親ノードの選択',
    'key' => '7cf35414612d30964d4b2eedb84c43fc',
  ),
  '502b3388fcefb0106f2f0aee21558c09' => 
  array (
    'context' => 'kernel/package',
    'source' => 'You must assign all nodes to new parent nodes.',
    'comment' => NULL,
    'translation' => 'すべてのノードを新しい親ノードに割り当てる必要があります。',
    'key' => '502b3388fcefb0106f2f0aee21558c09',
  ),
  '595991fcfcfee5e2d4a40d4e11ec8d54' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Create package',
    'comment' => NULL,
    'translation' => 'パッケージの作成',
    'key' => '595991fcfcfee5e2d4a40d4e11ec8d54',
  ),
  '1dc000daf492b31ff7f04e28c6b3ce5c' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Packages',
    'comment' => NULL,
    'translation' => 'パッケージ',
    'key' => '1dc000daf492b31ff7f04e28c6b3ce5c',
  ),
  'edd358b1f2c36f9490daefd269a3f890' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Install',
    'comment' => NULL,
    'translation' => 'インストール',
    'key' => 'edd358b1f2c36f9490daefd269a3f890',
  ),
  'fa2536cd75fabf6b46a4306df962981b' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Uninstall',
    'comment' => NULL,
    'translation' => 'アンインストール',
    'key' => 'fa2536cd75fabf6b46a4306df962981b',
  ),
  '9cc5b61c67eb8547f2302b0544443168' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Package %packagename already exists, cannot import the package',
    'comment' => NULL,
    'translation' => 'パッケージ %packagename はすでにインストール済みのためインポートできません',
    'key' => '9cc5b61c67eb8547f2302b0544443168',
  ),
  '77e24be73cf1fa1bbcf7bf9374744fcc' => 
  array (
    'context' => 'kernel/package',
    'source' => 'The package name %packagename is invalid, cannot import the package',
    'comment' => NULL,
    'translation' => 'パッケージ名 %packagenameは不正であるためパッケージをインポートすることは出来ません',
    'key' => '77e24be73cf1fa1bbcf7bf9374744fcc',
  ),
  'ec08dc21571c438956ecaa30eca94489' => 
  array (
    'context' => 'kernel/package',
    'source' => 'Upload',
    'comment' => NULL,
    'translation' => 'アップロード',
    'key' => 'ec08dc21571c438956ecaa30eca94489',
  ),
);
?>
