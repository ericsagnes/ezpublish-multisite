<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/content/search',
);

$TranslationRoot = array (
  'd6417c96759cbd4cc28dd3d9b51c2792' => 
  array (
    'context' => 'design/ezwebin/content/search',
    'source' => 'Search',
    'comment' => NULL,
    'translation' => '検索',
    'key' => 'd6417c96759cbd4cc28dd3d9b51c2792',
  ),
  'bf5badddc1bc08f1818a0aee4e75c7e2' => 
  array (
    'context' => 'design/ezwebin/content/search',
    'source' => 'For more options try the %1Advanced search%2',
    'comment' => 'The parameters are link start and end tags.',
    'translation' => '検索オプションはこちら: %1Advanced search%2',
    'key' => 'bf5badddc1bc08f1818a0aee4e75c7e2',
  ),
  '28e0b463a2a3c207d0aa36a3c5e9be8a' => 
  array (
    'context' => 'design/ezwebin/content/search',
    'source' => 'Search tips',
    'comment' => NULL,
    'translation' => '検索ヒント',
    'key' => '28e0b463a2a3c207d0aa36a3c5e9be8a',
  ),
  '4b7fcb7051bbe9269f9cc32d6db50483' => 
  array (
    'context' => 'design/ezwebin/content/search',
    'source' => 'Check spelling of keywords.',
    'comment' => NULL,
    'translation' => ' 入力したキーワードに誤りがないかを確認して下さい。',
    'key' => '4b7fcb7051bbe9269f9cc32d6db50483',
  ),
  'ffd90c2d0ee22d1165dcdaf42a5bb507' => 
  array (
    'context' => 'design/ezwebin/content/search',
    'source' => 'Search for "%1" returned %2 matches',
    'comment' => NULL,
    'translation' => '"%1" の検索結果は %2 件です。',
    'key' => 'ffd90c2d0ee22d1165dcdaf42a5bb507',
  ),
  '7cbe8a192a23efd5c698c52a4aacb64a' => 
  array (
    'context' => 'design/ezwebin/content/search',
    'source' => 'The following words were excluded from the search:',
    'comment' => NULL,
    'translation' => '次のキーワードは検索から外されました:',
    'key' => '7cbe8a192a23efd5c698c52a4aacb64a',
  ),
  'c2d184a0059ed29cac1c7d876d6fb307' => 
  array (
    'context' => 'design/ezwebin/content/search',
    'source' => 'No results were found when searching for "%1".',
    'comment' => NULL,
    'translation' => '"%1"に対する検索結果は得られませんでした。',
    'key' => 'c2d184a0059ed29cac1c7d876d6fb307',
  ),
  '6616ea8e45ff319a2a4e2d6b25df977c' => 
  array (
    'context' => 'design/ezwebin/content/search',
    'source' => 'Try changing some keywords (eg, "car" instead of "cars").',
    'comment' => NULL,
    'translation' => 'キーワードを変更してみてください。',
    'key' => '6616ea8e45ff319a2a4e2d6b25df977c',
  ),
  'ee41bbc61a1cfd2016cc02751f771689' => 
  array (
    'context' => 'design/ezwebin/content/search',
    'source' => 'Try searching with less specific keywords.',
    'comment' => NULL,
    'translation' => '一般的なキーワードで検索してみて下さい。',
    'key' => 'ee41bbc61a1cfd2016cc02751f771689',
  ),
  '3f9387d3c1316a496ab9d74bc6b92430' => 
  array (
    'context' => 'design/ezwebin/content/search',
    'source' => 'Reduce number of keywords to get more results.',
    'comment' => NULL,
    'translation' => '検索結果を多くするには、キーワードの数を減らしてください。',
    'key' => '3f9387d3c1316a496ab9d74bc6b92430',
  ),
);
?>
