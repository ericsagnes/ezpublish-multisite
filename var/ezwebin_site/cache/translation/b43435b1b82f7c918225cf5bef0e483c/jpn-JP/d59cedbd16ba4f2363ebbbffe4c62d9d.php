<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/package',
);

$TranslationRoot = array (
  '17795e73d0e4b7bb3fa585ba43aaba00' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Packages',
    'comment' => NULL,
    'translation' => 'パッケージ',
    'key' => '17795e73d0e4b7bb3fa585ba43aaba00',
  ),
  '6faecec27a41c164e39132630fef8b91' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'The following packages are available on this system',
    'comment' => NULL,
    'translation' => '以下のパッケージがこのシステムで利用可能です',
    'key' => '6faecec27a41c164e39132630fef8b91',
  ),
  '67507d4ad30f4d6e0942cfd6088ec77b' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '67507d4ad30f4d6e0942cfd6088ec77b',
  ),
  '4c3a30a30ee514cb3d17bcbba62d1f0b' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => '4c3a30a30ee514cb3d17bcbba62d1f0b',
  ),
  '9a0708e88eb6f8ddeb4f95d1c65f23c1' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Summary',
    'comment' => NULL,
    'translation' => 'サマリ',
    'key' => '9a0708e88eb6f8ddeb4f95d1c65f23c1',
  ),
  '604a6a073ff292e3fabd7ef5bace755e' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Status',
    'comment' => NULL,
    'translation' => 'ステータス',
    'key' => '604a6a073ff292e3fabd7ef5bace755e',
  ),
  'a3e96e2dd064895c85a7855864b26cd9' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Upload package',
    'comment' => NULL,
    'translation' => 'パッケージのアップロード',
    'key' => 'a3e96e2dd064895c85a7855864b26cd9',
  ),
  'ad36d437dfd29afc2399289bdbfe621f' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Install package',
    'comment' => NULL,
    'translation' => 'パッケージのインストール',
    'key' => 'ad36d437dfd29afc2399289bdbfe621f',
  ),
  'b5a5eacad701e90b843a8f8b46e7e306' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Please provide information on the changes.',
    'comment' => NULL,
    'translation' => '変更情報を入力してください。',
    'key' => 'b5a5eacad701e90b843a8f8b46e7e306',
  ),
  '333a3985187ed2c39ab1e052acfe7f1f' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Changes',
    'comment' => NULL,
    'translation' => '変更点',
    'key' => '333a3985187ed2c39ab1e052acfe7f1f',
  ),
  '89da05097cad444e44e0b0e47eaeaedd' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Package name',
    'comment' => NULL,
    'translation' => 'パッケージ名',
    'key' => '89da05097cad444e44e0b0e47eaeaedd',
  ),
  '151ec1751e745a66c3b7e7466d330199' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Description',
    'comment' => NULL,
    'translation' => '説明',
    'key' => '151ec1751e745a66c3b7e7466d330199',
  ),
  '05c7d4bf13f09e36c511495dac122760' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Package host',
    'comment' => NULL,
    'translation' => 'パッケージ作成サーバ',
    'key' => '05c7d4bf13f09e36c511495dac122760',
  ),
  'e3ace79c6cde768a440d5d436e31032f' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Packager',
    'comment' => NULL,
    'translation' => 'パッケージ作成者',
    'key' => 'e3ace79c6cde768a440d5d436e31032f',
  ),
  '5031e45a5139c538dbdeb220ae106f55' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Name',
    'comment' => 'Maintainer name',
    'translation' => '名前',
    'key' => '5031e45a5139c538dbdeb220ae106f55',
  ),
  '45d58006ad23debec9775f784f429c6a' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Role',
    'comment' => 'Maintainer role',
    'translation' => 'ロール',
    'key' => '45d58006ad23debec9775f784f429c6a',
  ),
  '94ec1965498f7d7d993f07e519214c6f' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Create package',
    'comment' => NULL,
    'translation' => 'パッケージの作成',
    'key' => '94ec1965498f7d7d993f07e519214c6f',
  ),
  '683e1085b1a60dacf1f568d2c99607a3' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Available wizards',
    'comment' => NULL,
    'translation' => '利用可能なウイザード',
    'key' => '683e1085b1a60dacf1f568d2c99607a3',
  ),
  '78fe3011ae5d6190a6c49df93cebc533' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Choose one of the following wizards for creating a package',
    'comment' => NULL,
    'translation' => 'パッケージを作成するには以下のウイザードの一つを選んでください',
    'key' => '78fe3011ae5d6190a6c49df93cebc533',
  ),
  '404bbeda9ca9bca33842f17a14d752a4' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Class list',
    'comment' => NULL,
    'translation' => 'クラス一覧',
    'key' => '404bbeda9ca9bca33842f17a14d752a4',
  ),
  'b4cd0db4150c8cf196295195070612f1' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Currently added image files',
    'comment' => NULL,
    'translation' => '現在追加された画像ファイル',
    'key' => 'b4cd0db4150c8cf196295195070612f1',
  ),
  '5788210012579961293308d30489845f' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Package wizard: %wizardname',
    'comment' => NULL,
    'translation' => 'パッケージウイザード: %wizardname',
    'key' => '5788210012579961293308d30489845f',
  ),
  '29f88a2e0bd1d53025de817801d599cb' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Install items',
    'comment' => NULL,
    'translation' => 'インストールされるアイテム',
    'key' => '29f88a2e0bd1d53025de817801d599cb',
  ),
  '49eca641263d68fa0cc268ae17592931' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Skip installation',
    'comment' => NULL,
    'translation' => 'インストールのスキップ',
    'key' => '49eca641263d68fa0cc268ae17592931',
  ),
  '561145fe004309e4a1edab72fcfdffc9' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Removal of packages',
    'comment' => NULL,
    'translation' => 'パッケージの削除',
    'key' => '561145fe004309e4a1edab72fcfdffc9',
  ),
  '8dade023abef868adcaeade906931d46' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Confirm removal',
    'comment' => NULL,
    'translation' => '削除の確認',
    'key' => '8dade023abef868adcaeade906931d46',
  ),
  '45cf7b94c1a6e75228f0458de1f93e94' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Keep packages',
    'comment' => NULL,
    'translation' => 'パッケージを保持',
    'key' => '45cf7b94c1a6e75228f0458de1f93e94',
  ),
  '3c31ac907031da314b1df6a76f877013' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Selection',
    'comment' => NULL,
    'translation' => '選択リスト',
    'key' => '3c31ac907031da314b1df6a76f877013',
  ),
  '36b0aa97614be29f3d0e2174d2f9a7ae' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Installed',
    'comment' => NULL,
    'translation' => 'インストール済',
    'key' => '36b0aa97614be29f3d0e2174d2f9a7ae',
  ),
  'bfb32e7c3221a251b0cfe016f26044a3' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Not installed',
    'comment' => NULL,
    'translation' => '未インストール',
    'key' => 'bfb32e7c3221a251b0cfe016f26044a3',
  ),
  'bf77726b82335a0f1ba4b4c68b807fc8' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Imported',
    'comment' => NULL,
    'translation' => 'インポート済',
    'key' => 'bf77726b82335a0f1ba4b4c68b807fc8',
  ),
  '69112d26b9d4119a49195a918a39a37b' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Remove package',
    'comment' => NULL,
    'translation' => 'パッケージの削除',
    'key' => '69112d26b9d4119a49195a918a39a37b',
  ),
  '2edf411f9560ededf74e30c50e852c47' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Import package',
    'comment' => NULL,
    'translation' => 'パッケージのインポート',
    'key' => '2edf411f9560ededf74e30c50e852c47',
  ),
  '0b85e644df948de4eedca1566ea4d772' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Next %arrowright',
    'comment' => NULL,
    'translation' => '次へ %arrowright',
    'key' => '0b85e644df948de4eedca1566ea4d772',
  ),
  '5c94558591b87721f3bd683b3f2abd4d' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Finish',
    'comment' => NULL,
    'translation' => '完了',
    'key' => '5c94558591b87721f3bd683b3f2abd4d',
  ),
  'f6cb31c19cd8c9d5b3bb1678b04a5d60' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Uninstall package',
    'comment' => NULL,
    'translation' => 'パッケージのアンインストール',
    'key' => 'f6cb31c19cd8c9d5b3bb1678b04a5d60',
  ),
  'c0f7aa6163b2562dbce832dbfe9d9687' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Uninstall items',
    'comment' => NULL,
    'translation' => 'アンインストールされるアイテム',
    'key' => 'c0f7aa6163b2562dbce832dbfe9d9687',
  ),
  '4be463e1e1b6c7e774c6e5e88a84321c' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Skip uninstallation',
    'comment' => NULL,
    'translation' => 'アンインストールのスキップ',
    'key' => '4be463e1e1b6c7e774c6e5e88a84321c',
  ),
  '50fac8ae823abeebfb50d1d60f3d0ed7' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Files [%collectionname]',
    'comment' => NULL,
    'translation' => 'ファイル一覧 [%collectionname]',
    'key' => '50fac8ae823abeebfb50d1d60f3d0ed7',
  ),
  '22f770e3412c3848456533e7938b665d' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Details',
    'comment' => NULL,
    'translation' => 'パッケージ詳細',
    'key' => '22f770e3412c3848456533e7938b665d',
  ),
  '16191d32c3eadb369009e3c9faeadcf9' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Uninstall',
    'comment' => NULL,
    'translation' => 'アンインストール',
    'key' => '16191d32c3eadb369009e3c9faeadcf9',
  ),
  '00c04d758cc1803210a0d6b936deac16' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Install',
    'comment' => NULL,
    'translation' => 'インストール',
    'key' => '00c04d758cc1803210a0d6b936deac16',
  ),
  'f85bd34b5de5cad58cd2e3f34b63ad17' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Export to file',
    'comment' => NULL,
    'translation' => 'ファイルにエクスポート',
    'key' => 'f85bd34b5de5cad58cd2e3f34b63ad17',
  ),
  '416d1254b2d802bbadd779ae3fa7d242' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'State',
    'comment' => NULL,
    'translation' => 'ステータス',
    'key' => '416d1254b2d802bbadd779ae3fa7d242',
  ),
  '96f214a5b7691d043882bd677879a331' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Maintainers',
    'comment' => NULL,
    'translation' => '保守管理者',
    'key' => '96f214a5b7691d043882bd677879a331',
  ),
  '99804e14fe1dd1280c364e0caa068941' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Documents',
    'comment' => NULL,
    'translation' => 'ドキュメント',
    'key' => '99804e14fe1dd1280c364e0caa068941',
  ),
  '4f13415bc8d685ad537258ddee7e2c0d' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Changelog',
    'comment' => NULL,
    'translation' => '変更履歴',
    'key' => '4f13415bc8d685ad537258ddee7e2c0d',
  ),
  '158b2753d8c2470f67e58b5ad56b2170' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'File list',
    'comment' => NULL,
    'translation' => 'ファイル一覧',
    'key' => '158b2753d8c2470f67e58b5ad56b2170',
  ),
  '41636344d41a0ea9216a06258a8ee8c1' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Selected nodes',
    'comment' => NULL,
    'translation' => '選択済みノード',
    'key' => '41636344d41a0ea9216a06258a8ee8c1',
  ),
  'dad23478a051bc14cc2f96cd15b84548' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Please select the site CSS file to be included in the package.',
    'comment' => NULL,
    'translation' => 'パッケージに含めるサイトCSSファイルを選択してください。',
    'key' => 'dad23478a051bc14cc2f96cd15b84548',
  ),
  '39867e1d924326ce0d918839afe23547' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Please select the classes CSS file to be included in the package.',
    'comment' => NULL,
    'translation' => 'パッケージに含める各クラスのCSSファイルを選択してください。',
    'key' => '39867e1d924326ce0d918839afe23547',
  ),
  '80126555132d8664ccbdbc311e780111' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Package install wizard: %wizardname',
    'comment' => NULL,
    'translation' => 'パッケージインストールウイザード: %wizardname',
    'key' => '80126555132d8664ccbdbc311e780111',
  ),
  '4c69c20ce9c3275f229af3c7f0c30238' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'You must now choose which siteaccess the package contents should be installed to.
The chosen siteaccess determines where design files and settings are written to.
If unsure choose the siteaccess which reflects the user part of your site, i.e. not admin.',
    'comment' => NULL,
    'translation' => 'パッケージコンテンツのインストール先サイトアクセスを選択してください。設定したサイトアクセスによりデザイン関連ファイルと設定の書き込み先が決定します。
確実ではない場合、ユーザサイト（admin（管理者）サイトではない）を選んで下さい。',
    'key' => '4c69c20ce9c3275f229af3c7f0c30238',
  ),
  '9ec129b0be5bcb1cbf044eeb23f7b709' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Select siteaccess',
    'comment' => NULL,
    'translation' => 'サイトアクセスの選択',
    'key' => '9ec129b0be5bcb1cbf044eeb23f7b709',
  ),
  '07c75f93125fa92e05253ebd106fcc8f' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Please select where you want to place the imported items.',
    'comment' => NULL,
    'translation' => 'インポートするアイテムの配置先を選択してください',
    'key' => '07c75f93125fa92e05253ebd106fcc8f',
  ),
  '52de3a6e5979439a02b4f48499989796' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Place %object_name in node %node_placement',
    'comment' => NULL,
    'translation' => '%object_name はノード %node_placement に配置',
    'key' => '52de3a6e5979439a02b4f48499989796',
  ),
  '01abd6bd054029051cf2a039142d5608' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Choose placement for %object_name',
    'comment' => NULL,
    'translation' => '%object_nameの配置先を選択',
    'key' => '01abd6bd054029051cf2a039142d5608',
  ),
  'e19e8092e3ed903434bf97cbbde0eb43' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'All',
    'comment' => NULL,
    'translation' => 'すべて',
    'key' => 'e19e8092e3ed903434bf97cbbde0eb43',
  ),
  '8132d144003ab9f24298cd530aed877d' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Change repository',
    'comment' => NULL,
    'translation' => 'レポジトリの変更',
    'key' => '8132d144003ab9f24298cd530aed877d',
  ),
  '93b4a4ea8610edfaf963fc62100e573d' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Node',
    'comment' => NULL,
    'translation' => 'ノード',
    'key' => '93b4a4ea8610edfaf963fc62100e573d',
  ),
  '00406a39d21d9791a63c878483cc05d9' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Export type',
    'comment' => NULL,
    'translation' => 'エクスポート範囲',
    'key' => '00406a39d21d9791a63c878483cc05d9',
  ),
  'b474a645f49d905751c6f5f1156d910d' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => 'b474a645f49d905751c6f5f1156d910d',
  ),
  'b693e38714217a00ac63b4248ece341f' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Add subtree',
    'comment' => NULL,
    'translation' => 'サブツリーの追加',
    'key' => 'b693e38714217a00ac63b4248ece341f',
  ),
  '3e6ed83b1b08bb3493d0794e19219f88' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Add node',
    'comment' => NULL,
    'translation' => 'ノードの追加',
    'key' => '3e6ed83b1b08bb3493d0794e19219f88',
  ),
  'e383d5502d895f5e9c55ab8c3decaabf' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => 'e383d5502d895f5e9c55ab8c3decaabf',
  ),
  'd628082ae9103cd0798ce27a45b2109f' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Map %siteaccess_name to',
    'comment' => NULL,
    'translation' => '%siteaccess_name の割り当て先',
    'key' => 'd628082ae9103cd0798ce27a45b2109f',
  ),
  '05345d1eea154db8e2b5fd3d12c36167' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Browse',
    'comment' => NULL,
    'translation' => 'ブラウズ',
    'key' => '05345d1eea154db8e2b5fd3d12c36167',
  ),
  '5f06cbd8af20310772e2da43bf7affe1' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Repositories',
    'comment' => NULL,
    'translation' => 'レポジトリ',
    'key' => '5f06cbd8af20310772e2da43bf7affe1',
  ),
  '088a35096d8490c1dc7a1cc19d4c84a6' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '088a35096d8490c1dc7a1cc19d4c84a6',
  ),
  '96262d84a95f4065b99a33c9a223b1bb' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Please select an extension to be exported.',
    'comment' => NULL,
    'translation' => 'エクスポートするエクステンションを選択。',
    'key' => '96262d84a95f4065b99a33c9a223b1bb',
  ),
  '8f962aee55606f595b07a7c4c49f317e' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Installing package',
    'comment' => NULL,
    'translation' => 'パッケージのインストール中',
    'key' => '8f962aee55606f595b07a7c4c49f317e',
  ),
  'c13d90f597878c7ca420fcdaeda8b5b6' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Please choose action:',
    'comment' => NULL,
    'translation' => 'アクションの選択: ',
    'key' => 'c13d90f597878c7ca420fcdaeda8b5b6',
  ),
  '5996f467c0107d6710e76abfce415c53' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Continue',
    'comment' => NULL,
    'translation' => '次へ',
    'key' => '5996f467c0107d6710e76abfce415c53',
  ),
  '51ff81428a8935d8555f5c2e9fcfd545' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Cancel installation',
    'comment' => NULL,
    'translation' => 'インストールのキャンセル',
    'key' => '51ff81428a8935d8555f5c2e9fcfd545',
  ),
  '6f8fb9573cc42f07aa8d28ba9cf85984' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Uninstalling package',
    'comment' => NULL,
    'translation' => 'パッケージのアンインストール中',
    'key' => '6f8fb9573cc42f07aa8d28ba9cf85984',
  ),
  'ea8e0f1eb30ff5901a2908b3a7632d75' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Cancel uninstallation',
    'comment' => NULL,
    'translation' => 'アンインストールのキャンセル',
    'key' => 'ea8e0f1eb30ff5901a2908b3a7632d75',
  ),
  '0d10d21b1b62ccf5f01611b3e21687f9' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Use this choice for all items',
    'comment' => NULL,
    'translation' => '全てのアイテムに適用',
    'key' => '0d10d21b1b62ccf5f01611b3e21687f9',
  ),
  '6f827793a85f88b7dbd3e6c973ec37d6' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Use this choice for all the items',
    'comment' => NULL,
    'translation' => '全てのアイテムに適用',
    'key' => '6f827793a85f88b7dbd3e6c973ec37d6',
  ),
  '0f199ad582df2133efee2919a58d8ad2' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Email',
    'comment' => NULL,
    'translation' => 'e-mail',
    'key' => '0f199ad582df2133efee2919a58d8ad2',
  ),
  'c830703e34c21f32ea8bee08a7c519ce' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Send email to the maintainer',
    'comment' => NULL,
    'translation' => 'パッケージ保守管理者にメールを送信',
    'key' => 'c830703e34c21f32ea8bee08a7c519ce',
  ),
  '62967339dc9e9aceb27b5d3ca59315eb' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Please select a thumbnail file to be included in the package,
if you do not want to have a thumbnail simply click Next.',
    'comment' => NULL,
    'translation' => 'パッケージに含まれるサムネール画像を選択し、次をクリックしてください。￼￼サムネール画像を含めない場合は、選択せずに"次"をクリックして下さい。',
    'key' => '62967339dc9e9aceb27b5d3ca59315eb',
  ),
  'ad8c0d82cd1f5790fbba7e5541fc539a' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Unhandled installation error has occurred.',
    'comment' => NULL,
    'translation' => '修理できないインストレーションエラーが発生しました。',
    'key' => 'ad8c0d82cd1f5790fbba7e5541fc539a',
  ),
  '77bde7ba45781a60a2669ffe0ce2548f' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Unhandled uninstallation error has occurred.',
    'comment' => NULL,
    'translation' => '修理できないアンインストレーションエラーが発生しました。',
    'key' => '77bde7ba45781a60a2669ffe0ce2548f',
  ),
  '35d83bc7e9ef17521c2dcce2c5858a01' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Start an entry with a marker ( %emstart-%emend (dash) or %emstart*%emend (asterisk) ) at the beginning of the line.
The change will continue to the next change marker.',
    'comment' => NULL,
    'translation' => 'エントリは行頭にマーカー記号（%emstart-%emend （半角ハイフン）または %emstart*%emend（アスタリスク））を置いてください。一つの変更点は次のマーカー記号までが有効です。',
    'key' => '35d83bc7e9ef17521c2dcce2c5858a01',
  ),
  'da3c12ce18572e204d2e93eb72958caf' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Provide some basic information for your package.',
    'comment' => NULL,
    'translation' => 'パッケージの基本情報を入力してください。',
    'key' => 'da3c12ce18572e204d2e93eb72958caf',
  ),
  'dec75b6d6463e097836b0c39aa0f351c' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'License',
    'comment' => NULL,
    'translation' => 'ライセンス',
    'key' => 'dec75b6d6463e097836b0c39aa0f351c',
  ),
  'e149882715e6ca694cbc504d14086cb6' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Provide information about the maintainer of the package.',
    'comment' => NULL,
    'translation' => 'パッケージ管理者の情報を入力してください。',
    'key' => 'e149882715e6ca694cbc504d14086cb6',
  ),
  '628cb77156418698e6fad847e33af873' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Please choose the content classes you want to be included in the package.',
    'comment' => NULL,
    'translation' => 'パッケージに含めるコンテンツクラスを選択してください。',
    'key' => '628cb77156418698e6fad847e33af873',
  ),
  'a3f61c93cd178daa9d545f203e9e860f' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Choose the objects to include in the package.',
    'comment' => NULL,
    'translation' => 'パッケージに含めるオブジェクトを選択してください。',
    'key' => 'a3f61c93cd178daa9d545f203e9e860f',
  ),
  '323cdcbc10d8628c3d15e817111985b4' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Select an image file to be included in the package then click Next.
When you are done with adding images click Next without choosing an image.',
    'comment' => NULL,
    'translation' => 'パッケージに含める画像を選択し、次をクリックしてください。
￼次のステップに行くには、画像を選択せずに次をクリックして下さい。',
    'key' => '323cdcbc10d8628c3d15e817111985b4',
  ),
  '4e2477d4acf30ec6f9c98cbfe398bfad' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'The package can be installed on your system. Installing the package will copy files, create content classes etc., depending on the package.
If you do not want to install the package at this time, you can do so later on the view page for the package.',
    'comment' => NULL,
    'translation' => 'システムにパッケージをインストールすることができます。パッケージをインストールすると、パッケージによって、ファイルの複製が作成されたり、クラスが作成されたりします。今、インストールしたくない場合はパッケージのビューページでもインストールできます。',
    'key' => '4e2477d4acf30ec6f9c98cbfe398bfad',
  ),
  'b066c4e779ddd82b362f83ff41f01252' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'If you want to change the placement click the browse button.',
    'comment' => NULL,
    'translation' => '配置先を変更する場合はブラウズボタンをクリックしてください。',
    'key' => 'b066c4e779ddd82b362f83ff41f01252',
  ),
  '6526c363e0dc0a6457c1e96a092031b3' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Are you sure you want to remove the following packages?
The packages will be lost forever.
Note: The packages will not be uninstalled.',
    'comment' => NULL,
    'translation' => '次のパッケージを削除してもよろしいですか？パッケージは完全に削除されます。注意: パッケージはアンインストールされません。',
    'key' => '6526c363e0dc0a6457c1e96a092031b3',
  ),
  '6cb025a2e81726b36d42a78987a5a304' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Package removal was canceled.',
    'comment' => NULL,
    'translation' => 'パッケージの削除はキャンセルされました。',
    'key' => '6cb025a2e81726b36d42a78987a5a304',
  ),
  'c2a15756f2c71dd9d27225a1dfbb6fa7' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'The package can be uninstalled from your system. Uninstalling the package will remove any installed files, content classes etc., depending on the package.
If you do not want to uninstall the package at this time, you can do so later on the view page for the package.
You can also remove the package without uninstalling it from the package list.',
    'comment' => NULL,
    'translation' => 'システムからパッケージをアンインストールすることができます。パッケージによって、インストールされたファイルやクラスなどが削除されます。
今、パッケージのアンインストールをしたくない場合は、パッケージのビューページからアンインストールすることも出来ます。',
    'key' => 'c2a15756f2c71dd9d27225a1dfbb6fa7',
  ),
  '4f87ce2240e7ebce51348880900a8708' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Select the file containing the package then click the upload button',
    'comment' => NULL,
    'translation' => 'パッケージが入っているファイルを選択し、アップロードボタンをクリックしてください。',
    'key' => '4f87ce2240e7ebce51348880900a8708',
  ),
  '6c5d0644a528fb4cfd8a776e83b7fa25' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Regarding eZ Publish package \'%packagename\'',
    'comment' => NULL,
    'translation' => 'eZ Publishパッケージ \'%packagename\'に関して',
    'key' => '6c5d0644a528fb4cfd8a776e83b7fa25',
  ),
  '77350518e1b235426f84f432cb58a391' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Please select the extensions to be exported.',
    'comment' => NULL,
    'translation' => 'エクスポートするエクステンションを選択。',
    'key' => '77350518e1b235426f84f432cb58a391',
  ),
  '1908d86bb6bfd95c2d78037dd9e527b8' => 
  array (
    'context' => 'design/standard/package',
    'source' => 'Use content object modification and publication dates from the package.',
    'comment' => NULL,
    'translation' => 'パッケージのコンテンツオブジェクト編集日と公開日を利用する。',
    'key' => '1908d86bb6bfd95c2d78037dd9e527b8',
  ),
);
?>
