<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/visual/menuconfig',
);

$TranslationRoot = array (
  '9396fe253efed957239aca924415a5df' => 
  array (
    'context' => 'design/standard/visual/menuconfig',
    'source' => 'Menu management',
    'comment' => NULL,
    'translation' => 'メニュー管理',
    'key' => '9396fe253efed957239aca924415a5df',
  ),
  'a6c6396cbb96ba3a71968d37cb3634c6' => 
  array (
    'context' => 'design/standard/visual/menuconfig',
    'source' => 'Set',
    'comment' => NULL,
    'translation' => '設定',
    'key' => 'a6c6396cbb96ba3a71968d37cb3634c6',
  ),
  '17665213d5281ac74508e51273b91a78' => 
  array (
    'context' => 'design/standard/visual/menuconfig',
    'source' => 'Menu positioning',
    'comment' => NULL,
    'translation' => 'メニュー位置',
    'key' => '17665213d5281ac74508e51273b91a78',
  ),
  'be1b67b72e814f8b1315e0cb09dff49a' => 
  array (
    'context' => 'design/standard/visual/menuconfig',
    'source' => 'Apply changes',
    'comment' => NULL,
    'translation' => '変更の適用',
    'key' => 'be1b67b72e814f8b1315e0cb09dff49a',
  ),
  '022729de0bf01bdf119f34f9069c0396' => 
  array (
    'context' => 'design/standard/visual/menuconfig',
    'source' => 'Click here to store the changes if you have modified the menu settings above.',
    'comment' => NULL,
    'translation' => 'メニュー設定を修正した場合、このボタンをクリックして変更を保存します。',
    'key' => '022729de0bf01bdf119f34f9069c0396',
  ),
  '9b8b7d6e4a1c262a3d0583f10d57adb2' => 
  array (
    'context' => 'design/standard/visual/menuconfig',
    'source' => 'Siteaccess',
    'comment' => NULL,
    'translation' => 'サイトアクセス',
    'key' => '9b8b7d6e4a1c262a3d0583f10d57adb2',
  ),
);
?>
