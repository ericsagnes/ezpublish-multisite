<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/shop/vattype',
);

$TranslationRoot = array (
  '5d50a8a205ba2275db15609e4b3651b4' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'VAT types [%vat_types]',
    'comment' => NULL,
    'translation' => '課税方式 [%vat_types]',
    'key' => '5d50a8a205ba2275db15609e4b3651b4',
  ),
  '8764e8a93327089418fc20de704b360c' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '8764e8a93327089418fc20de704b360c',
  ),
  'c069ecbe9f32eeb41bea168aa6aa1004' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'Percentage',
    'comment' => NULL,
    'translation' => '税率',
    'key' => 'c069ecbe9f32eeb41bea168aa6aa1004',
  ),
  '8d86737ccab2a2197a80749877a0d781' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'Invert selection.',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => '8d86737ccab2a2197a80749877a0d781',
  ),
  'ab46108ea3064977d8dc969ab5e5ef17' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'Select VAT type for removal.',
    'comment' => NULL,
    'translation' => '削除する課税方式を選択',
    'key' => 'ab46108ea3064977d8dc969ab5e5ef17',
  ),
  'b32f07d2317864670089fa16f6173411' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'There are no VAT types.',
    'comment' => NULL,
    'translation' => '課税方式がありません',
    'key' => 'b32f07d2317864670089fa16f6173411',
  ),
  '086d262b2420f291effda798a4865ba9' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => '086d262b2420f291effda798a4865ba9',
  ),
  '7c10c5eda26699cffe84759fdc5b7ba1' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'Remove selected VAT types.',
    'comment' => NULL,
    'translation' => '選択した課税方式を削除',
    'key' => '7c10c5eda26699cffe84759fdc5b7ba1',
  ),
  'e9180bdaefc8e3e5f077aaf0acd224ab' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'New VAT type',
    'comment' => NULL,
    'translation' => '新規の課税方式',
    'key' => 'e9180bdaefc8e3e5f077aaf0acd224ab',
  ),
  'e18a8fde357954e91929192fbc64dff4' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'Create a new VAT type.',
    'comment' => NULL,
    'translation' => '新規の課税方式作成',
    'key' => 'e18a8fde357954e91929192fbc64dff4',
  ),
  '3ec83c6a3ec036440f0b909178e141b5' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'Apply changes',
    'comment' => NULL,
    'translation' => '変更を適用',
    'key' => '3ec83c6a3ec036440f0b909178e141b5',
  ),
  '338f0b9345597dbeca77db248d4ab90f' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'Click this button to store changes if you have modified any of the fields above.',
    'comment' => NULL,
    'translation' => '修正した項目があれば、このボタンをクリックすると変更が保存されます。',
    'key' => '338f0b9345597dbeca77db248d4ab90f',
  ),
  'ee22fc8b6c34784e2b09b41b2fea3bd3' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'Input did not validate',
    'comment' => NULL,
    'translation' => '入力が正しくありません',
    'key' => 'ee22fc8b6c34784e2b09b41b2fea3bd3',
  ),
  'f3614ae828e1ff97213e0244c3153c54' => 
  array (
    'context' => 'design/admin/shop/vattype',
    'source' => 'VAT types (%vat_types)',
    'comment' => NULL,
    'translation' => '課税方式 (%vat_types)',
    'key' => 'f3614ae828e1ff97213e0244c3153c54',
  ),
);
?>
