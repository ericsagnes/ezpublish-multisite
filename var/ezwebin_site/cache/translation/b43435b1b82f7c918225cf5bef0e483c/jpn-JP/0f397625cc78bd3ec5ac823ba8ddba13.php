<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
);

$TranslationRoot = array (
  '97a8b2623cb7f23a66984fef523eae5e' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '97a8b2623cb7f23a66984fef523eae5e',
  ),
  '38b2c726c72651faf87b34d550438e18' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Type',
    'comment' => NULL,
    'translation' => 'タイプ',
    'key' => '38b2c726c72651faf87b34d550438e18',
  ),
  'ddb56c0bdec34b87afd0933547b459a5' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Section',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => 'ddb56c0bdec34b87afd0933547b459a5',
  ),
  '7305a9be2d7647e44aa4cc8bd761e393' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => '7305a9be2d7647e44aa4cc8bd761e393',
  ),
  '02e5024782edfc6700e27e06baae3d1b' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'My item notifications [%notification_count]',
    'comment' => NULL,
    'translation' => '通知アイテム [%notification_count]',
    'key' => '02e5024782edfc6700e27e06baae3d1b',
  ),
  '6a16dcff06913d7ba0a7dbd9a278a15c' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Invert selection.',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => '6a16dcff06913d7ba0a7dbd9a278a15c',
  ),
  'c8ec58ad20fd2220277d41f536b80e41' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Select item for removal.',
    'comment' => NULL,
    'translation' => '削除する通知アイテムを選択',
    'key' => 'c8ec58ad20fd2220277d41f536b80e41',
  ),
  '3f01831215a2014ed372b85c836c896b' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'You have not subscribed to receive notifications about any items.',
    'comment' => NULL,
    'translation' => '登録した通知アイテムはありません',
    'key' => '3f01831215a2014ed372b85c836c896b',
  ),
  '2ffecbce9c2428fde0edeaa4791ddeb7' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Remove selected items.',
    'comment' => NULL,
    'translation' => '選択した通知アイテムを削除',
    'key' => '2ffecbce9c2428fde0edeaa4791ddeb7',
  ),
  '61c78758e46402dfd8e045a6ce19f10a' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Add items',
    'comment' => NULL,
    'translation' => '通知アイテムの登録',
    'key' => '61c78758e46402dfd8e045a6ce19f10a',
  ),
  'ace58f18504ad013de9e1030f4c1e475' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Add items to your personal notification list.',
    'comment' => NULL,
    'translation' => '通知リストにアイテムを追加',
    'key' => 'ace58f18504ad013de9e1030f4c1e475',
  ),
  'ddbab39fe1ce7cef6bd7f221402411ac' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'Unknown',
    'comment' => NULL,
    'translation' => '不明',
    'key' => 'ddbab39fe1ce7cef6bd7f221402411ac',
  ),
  'b54e86035357daf385458684371c46a2' => 
  array (
    'context' => 'design/admin/notification/handler/ezsubtree/settings/edit',
    'source' => 'My item notifications (%notification_count)',
    'comment' => NULL,
    'translation' => '通知アイテム (%notification_count)',
    'key' => 'b54e86035357daf385458684371c46a2',
  ),
);
?>
