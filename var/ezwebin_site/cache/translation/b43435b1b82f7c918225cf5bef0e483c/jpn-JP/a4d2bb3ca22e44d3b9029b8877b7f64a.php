<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/content/draft',
);

$TranslationRoot = array (
  'df2ec94e3bea861af45b2d22ca352dc1' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'Select all',
    'comment' => NULL,
    'translation' => '全て選択',
    'key' => 'df2ec94e3bea861af45b2d22ca352dc1',
  ),
  '643e937dc175f153e9f65daa73d9e08c' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'Deselect all',
    'comment' => NULL,
    'translation' => '全ての選択を解除',
    'key' => '643e937dc175f153e9f65daa73d9e08c',
  ),
  '5e9d304d371b07eea5b9523ab78459d7' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'My drafts',
    'comment' => NULL,
    'translation' => '編集中ドラフト',
    'key' => '5e9d304d371b07eea5b9523ab78459d7',
  ),
  '469d836d08db93bd8cfb815bf546b4a6' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '469d836d08db93bd8cfb815bf546b4a6',
  ),
  '75f9ced21b2730a983fde06842254283' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => '75f9ced21b2730a983fde06842254283',
  ),
  '05af1491afa7771bde228d00a81de050' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'Section',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => '05af1491afa7771bde228d00a81de050',
  ),
  'fd4314411e7b3b054047c7d42fffcb19' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => 'fd4314411e7b3b054047c7d42fffcb19',
  ),
  'fdaf5554cb85dd844e70cd5167723676' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'Language',
    'comment' => NULL,
    'translation' => '言語',
    'key' => 'fdaf5554cb85dd844e70cd5167723676',
  ),
  '973a26d54f2b3c5e9976dccfabeb80b4' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'Last modified',
    'comment' => NULL,
    'translation' => '最新の修正',
    'key' => '973a26d54f2b3c5e9976dccfabeb80b4',
  ),
  '964d06d52a88a6713986a61991f6d9f2' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => '964d06d52a88a6713986a61991f6d9f2',
  ),
  '623d3b9f07f527257a2c62d503469c42' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => '623d3b9f07f527257a2c62d503469c42',
  ),
  'eb6ba05a9d2656ac5a78f217ffddb954' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'You have no drafts',
    'comment' => NULL,
    'translation' => 'ドラフトがありません',
    'key' => 'eb6ba05a9d2656ac5a78f217ffddb954',
  ),
  '0d54f1d326eabba9bf1bf9891372387c' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'Empty draft',
    'comment' => NULL,
    'translation' => 'ドラフトを空にする',
    'key' => '0d54f1d326eabba9bf1bf9891372387c',
  ),
  '24b73e2824a6dfdbbc222d4dfc5e2273' => 
  array (
    'context' => 'design/ezwebin/content/draft',
    'source' => 'These are the current objects you are working on. The drafts are owned by you and can only be seen by you.
      You can either edit the drafts or remove them if you don\'t need them any more.',
    'comment' => NULL,
    'translation' => '現在作業中のオブジェクトです。ドラフトの所有者はあなたです、そのためあなたしか見えないです。
ドラフトを編集するか、必要ない場合に削除することができます。',
    'key' => '24b73e2824a6dfdbbc222d4dfc5e2273',
  ),
);
?>
