<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/ezodf/import',
);

$TranslationRoot = array (
  '9fe7bdc8b32d4b9ecf90dff297c19d6e' => 
  array (
    'context' => 'design/ezwebin/ezodf/import',
    'source' => 'Document is now imported',
    'comment' => NULL,
    'translation' => 'ドキュメントがインポートされました',
    'key' => '9fe7bdc8b32d4b9ecf90dff297c19d6e',
  ),
  '8191a9f052eec4821e8f1a2a0b84017f' => 
  array (
    'context' => 'design/ezwebin/ezodf/import',
    'source' => 'OpenOffice.org import',
    'comment' => NULL,
    'translation' => 'OpenOffice.orgファイルのインポート',
    'key' => '8191a9f052eec4821e8f1a2a0b84017f',
  ),
  '0e75ab77b0e4caff3d20b871d3eb3c15' => 
  array (
    'context' => 'design/ezwebin/ezodf/import',
    'source' => 'The object was imported as: %class_name',
    'comment' => NULL,
    'translation' => '%class_nameとして、オブジェクトがインポートされました',
    'key' => '0e75ab77b0e4caff3d20b871d3eb3c15',
  ),
  'c093fc60f13747417f75fb9f049d02ce' => 
  array (
    'context' => 'design/ezwebin/ezodf/import',
    'source' => 'Document imported as',
    'comment' => NULL,
    'translation' => 'ドキュメントは次の名前でインポートされました: ',
    'key' => 'c093fc60f13747417f75fb9f049d02ce',
  ),
  'fe9b90f90174d5f72d44bb5ca8ada3ae' => 
  array (
    'context' => 'design/ezwebin/ezodf/import',
    'source' => 'The images are placed in the media and can be re-used.',
    'comment' => NULL,
    'translation' => '画像はメディアリソースに配置されますので、後に再利用することができます。',
    'key' => 'fe9b90f90174d5f72d44bb5ca8ada3ae',
  ),
  '496f6715afc12da3cb81d79222682c40' => 
  array (
    'context' => 'design/ezwebin/ezodf/import',
    'source' => 'Import another document',
    'comment' => NULL,
    'translation' => 'もう一つのドキュメントをアップロードする',
    'key' => '496f6715afc12da3cb81d79222682c40',
  ),
  'c7914b5967c73f5f5c429d3cbdbcf079' => 
  array (
    'context' => 'design/ezwebin/ezodf/import',
    'source' => 'Upload file',
    'comment' => NULL,
    'translation' => 'ファイルのアップロード',
    'key' => 'c7914b5967c73f5f5c429d3cbdbcf079',
  ),
  '77abc63a5b25fc9648dcff8c8934b5dd' => 
  array (
    'context' => 'design/ezwebin/ezodf/import',
    'source' => 'Import OpenOffice.org document',
    'comment' => NULL,
    'translation' => 'OpenOffice.orgドキュメントをインポートする',
    'key' => '77abc63a5b25fc9648dcff8c8934b5dd',
  ),
  '8e8a71283769f3eb78d787694aa7a094' => 
  array (
    'context' => 'design/ezwebin/ezodf/import',
    'source' => 'Replace document',
    'comment' => NULL,
    'translation' => 'ドキュメントを入れ替える',
    'key' => '8e8a71283769f3eb78d787694aa7a094',
  ),
  'a6f5e2e535e22b47bf4dca3fdbcd5554' => 
  array (
    'context' => 'design/ezwebin/ezodf/import',
    'source' => 'Import to',
    'comment' => NULL,
    'translation' => 'インポート先',
    'key' => 'a6f5e2e535e22b47bf4dca3fdbcd5554',
  ),
  '59665af24f906fe6eaf342da13922a84' => 
  array (
    'context' => 'design/ezwebin/ezodf/import',
    'source' => 'You can import OpenOffice.org Writer documents directly into eZ publish from this page. You are
asked where to place the document and eZ publish does the rest. The document is converted into
the appropriate class during the import, you get a notice about this after the import is done.
Images are placed in the media library so you can re-use them in other articles.',
    'comment' => NULL,
    'translation' => 'OpenOffice.org Writerドキュメントを直接eZ Publishにインポートします。ドキュメントの配置先を選択する必要があります。
ドキュメントは自動的に適切なクラスに変換されます。インポートが終わりましたら、通知が表示されます。
画像はメディアリソースに登録されるため、他のオブジェクトと共有できます。',
    'key' => '59665af24f906fe6eaf342da13922a84',
  ),
);
?>
