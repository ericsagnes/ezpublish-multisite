<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/class/edit_denied',
);

$TranslationRoot = array (
  '5da6de83e7347ccf24b1992537593427' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'Class edit conflict',
    'comment' => NULL,
    'translation' => 'クラス定義の同時編集エラー',
    'key' => '5da6de83e7347ccf24b1992537593427',
  ),
  'a31d9d39b1cb50efa51cd4e690260590' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'This class is already being edited by someone else.',
    'comment' => NULL,
    'translation' => 'このクラスは別のユーザにより編集状態になっています。',
    'key' => 'a31d9d39b1cb50efa51cd4e690260590',
  ),
  '9645c28fd35d490e9182f06305bf2973' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'Possible actions',
    'comment' => NULL,
    'translation' => '選択可能な動作',
    'key' => '9645c28fd35d490e9182f06305bf2973',
  ),
  '8a7a1b946811e0697f0b9c8ec6abfc2b' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'Contact the person who is editing the class.',
    'comment' => NULL,
    'translation' => 'クラスを編集しているユーザに連絡する。',
    'key' => '8a7a1b946811e0697f0b9c8ec6abfc2b',
  ),
  '2551e0656cfcf0b3e7ebb5ad6f0f2aef' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'Wait until the lock expires and try again.',
    'comment' => NULL,
    'translation' => 'ロック解除を待って再試行する。',
    'key' => '2551e0656cfcf0b3e7ebb5ad6f0f2aef',
  ),
  'acb621f64f482d5f3151a32b8f3ca9cb' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'Edit <%class_name> [Class]',
    'comment' => NULL,
    'translation' => '<%class_name> [クラス] の編集',
    'key' => 'acb621f64f482d5f3151a32b8f3ca9cb',
  ),
  'bc5c08b4196ddc0ec16f358d1722a13d' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => 'bc5c08b4196ddc0ec16f358d1722a13d',
  ),
  '0a03ba6f7ff653af307d91f3235edd94' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'Current modifier',
    'comment' => NULL,
    'translation' => '現在の修正者',
    'key' => '0a03ba6f7ff653af307d91f3235edd94',
  ),
  '66ae97b2b958c38a4f9174e8ee94f8af' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'Unlock time',
    'comment' => NULL,
    'translation' => 'ロック解除日時',
    'key' => '66ae97b2b958c38a4f9174e8ee94f8af',
  ),
  '05a3a713c71b569064cd7fbca624ce89' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'Retry',
    'comment' => NULL,
    'translation' => '再試行',
    'key' => '05a3a713c71b569064cd7fbca624ce89',
  ),
  '92c37252ff85b19ab6f117febc29f9d8' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '92c37252ff85b19ab6f117febc29f9d8',
  ),
  '8bf233101efbefaee5026e0fa84172b8' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'The class is temporarily locked and thus it cannot be edited by you.',
    'comment' => NULL,
    'translation' => 'このクラスは一時的にロックされていて、現在編集できません。',
    'key' => '8bf233101efbefaee5026e0fa84172b8',
  ),
  '4a36b898f97961f55bb9ca29966f5a2d' => 
  array (
    'context' => 'design/admin/class/edit_denied',
    'source' => 'The class will be available for editing after it has been stored by the current modifier or when it is unlocked by the system.',
    'comment' => NULL,
    'translation' => '現在編集をしているユーザが編集を保存した後、もしくはシステムがこのクラスのロックを解除した後に編集が可能になります。',
    'key' => '4a36b898f97961f55bb9ca29966f5a2d',
  ),
);
?>
