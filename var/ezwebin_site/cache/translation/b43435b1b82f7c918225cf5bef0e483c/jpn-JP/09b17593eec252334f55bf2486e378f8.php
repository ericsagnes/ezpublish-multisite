<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/restore',
);

$TranslationRoot = array (
  '1562e1c1a171e2d831081f8ecdef0161' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'Object retrieval',
    'comment' => NULL,
    'translation' => 'オブジェクトの回復',
    'key' => '1562e1c1a171e2d831081f8ecdef0161',
  ),
  'da429d90384038d3d472fdf925651d71' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'The object will be restored at its original location.',
    'comment' => NULL,
    'translation' => 'オブジェクトは元の配置先に保存されます。',
    'key' => 'da429d90384038d3d472fdf925651d71',
  ),
  '5675cfe382552a09a81ade215be83573' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'Restore at original location (below <%nodeName>).',
    'comment' => NULL,
    'translation' => '元の配置先に保存（下記 <%nodeName>）。',
    'key' => '5675cfe382552a09a81ade215be83573',
  ),
  '0ba8da164f16e4c59be69e7d2de6d3ea' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'Select a location.',
    'comment' => NULL,
    'translation' => '配置先の選択。',
    'key' => '0ba8da164f16e4c59be69e7d2de6d3ea',
  ),
  'e045e69329e3899f0596c778dc24ebd5' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'Restore at original location (unavailable).',
    'comment' => NULL,
    'translation' => '元の配置先に復旧（利用不可）。',
    'key' => 'e045e69329e3899f0596c778dc24ebd5',
  ),
  '41b51e8cddaa4022c7b05455c71e896c' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => '41b51e8cddaa4022c7b05455c71e896c',
  ),
  '644299c6c183fe0e80109b1c37d7ba3c' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'Continue restoring <%name>.',
    'comment' => NULL,
    'translation' => '<%name>の復旧を継続。',
    'key' => '644299c6c183fe0e80109b1c37d7ba3c',
  ),
  '4db835972ee3fe2909170b6fe3bc73af' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '4db835972ee3fe2909170b6fe3bc73af',
  ),
  'de26a7ddfa3f477384f8f382e3f9d632' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'Do not restore <%name> and return to trash.',
    'comment' => NULL,
    'translation' => '<%name> を保存せずにごみ箱に戻す。',
    'key' => 'de26a7ddfa3f477384f8f382e3f9d632',
  ),
  'ae577fca4956dcce5f0fed910a9deb59' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'Restoring object <%name> [%className]',
    'comment' => NULL,
    'translation' => 'オブジェクト <%name> [%className]の保存',
    'key' => 'ae577fca4956dcce5f0fed910a9deb59',
  ),
  '721121ea061f69e00a5a199cc6602e96' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'The system will restore the original location of the object.',
    'comment' => NULL,
    'translation' => 'オブジェクトの元の配置先に保存します。',
    'key' => '721121ea061f69e00a5a199cc6602e96',
  ),
  '751ffbaeebeedd84b0682145ed763725' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'Restore original location <%nodeName>',
    'comment' => NULL,
    'translation' => '元の配置先 <%nodeName> に保存',
    'key' => '751ffbaeebeedd84b0682145ed763725',
  ),
  'af8e633799e3d09db1c5cde2209260be' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'Browse for location',
    'comment' => NULL,
    'translation' => '配置先のブラウズ',
    'key' => 'af8e633799e3d09db1c5cde2209260be',
  ),
  '6d3a8333073eb67fc402d5408a38a6bb' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'Restore original locations',
    'comment' => NULL,
    'translation' => '元の配置先に保存',
    'key' => '6d3a8333073eb67fc402d5408a38a6bb',
  ),
  '98ada9a86171a4679c11976e4cddbbcd' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'Restore <%name> to the specified location.',
    'comment' => NULL,
    'translation' => '<%name> を指定の配置先に保存。',
    'key' => '98ada9a86171a4679c11976e4cddbbcd',
  ),
  'c97732afed6740635dc957f0bb28430e' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'The system will prompt you to specify a location by browsing the tree.',
    'comment' => NULL,
    'translation' => '新しい配置先をツリーをブラウズして指定するように指示が出されます。',
    'key' => 'c97732afed6740635dc957f0bb28430e',
  ),
  'a4668327f7c6195d878a3f153bdacf6b' => 
  array (
    'context' => 'design/admin/content/restore',
    'source' => 'The system will prompt you to browse for a location for the object.',
    'comment' => NULL,
    'translation' => '新しい配置先をツリーをブラウズして指定するように指示が出されます。',
    'key' => 'a4668327f7c6195d878a3f153bdacf6b',
  ),
);
?>
