<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/rss',
);

$TranslationRoot = array (
  '6a202d0ec8afb2445744a0cfeb787a8a' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'Choose export node',
    'comment' => NULL,
    'translation' => 'RSSエクスポートするノードの選択',
    'key' => '6a202d0ec8afb2445744a0cfeb787a8a',
  ),
  '29a0ca70cdab6575f54587f3145d45b7' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'Select',
    'comment' => NULL,
    'translation' => '選択',
    'key' => '29a0ca70cdab6575f54587f3145d45b7',
  ),
  'ea0fa3b17839bf2c823bc64f85d65f7f' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'Choose import destination',
    'comment' => NULL,
    'translation' => 'RSSインポートの配置先の選択',
    'key' => 'ea0fa3b17839bf2c823bc64f85d65f7f',
  ),
  'e210ebae862f664334551064df829dcc' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'Choose RSS image',
    'comment' => NULL,
    'translation' => 'RSS画像の選択',
    'key' => 'e210ebae862f664334551064df829dcc',
  ),
  '6cb908502bfd571d98200349c187e39c' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'Choose export source',
    'comment' => NULL,
    'translation' => 'RSSエクスポートするソースの選択',
    'key' => '6cb908502bfd571d98200349c187e39c',
  ),
  '2634df37d7c85da889c0b8e7ef0e7a31' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'Choose owner of imported objects',
    'comment' => NULL,
    'translation' => 'RSSインポートオブジェクトの所有者の選択',
    'key' => '2634df37d7c85da889c0b8e7ef0e7a31',
  ),
  '6aa29b07f1d767e61f3e9200e4127dbe' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'RSS export is locked',
    'comment' => NULL,
    'translation' => 'RSSエクスポートはロックされています',
    'key' => '6aa29b07f1d767e61f3e9200e4127dbe',
  ),
  '6e1c957118e0bc0f85e2f1b699a56dae' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'The RSS export %name is currently locked by %user and was last modified on %datetime.',
    'comment' => NULL,
    'translation' => 'このRSSエクスポート %name は %user により現在ロックされています。最新の修正日時は %datetimeです。',
    'key' => '6e1c957118e0bc0f85e2f1b699a56dae',
  ),
  '792e1c0b06f2355f3c1cccea4d47ad6d' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'The RSS export will be available for editing once it is stored by the modifier or when it is automatically unlocked on %datetime.',
    'comment' => NULL,
    'translation' => 'このRSSエクスポートは修正中の編集が保存されるか %datetime に自動的にロック解除されることで編集可能になります。',
    'key' => '792e1c0b06f2355f3c1cccea4d47ad6d',
  ),
  '6e1e1457b0a4728020da7f04dc8b6f62' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'Retry',
    'comment' => NULL,
    'translation' => '再試行',
    'key' => '6e1e1457b0a4728020da7f04dc8b6f62',
  ),
  '09cdb4a4938c00ec922af3da389d3144' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'RSS import is locked',
    'comment' => NULL,
    'translation' => 'RSSインポートはロックされています',
    'key' => '09cdb4a4938c00ec922af3da389d3144',
  ),
  'b1b40b7bf92e8b6133d3059b4e9e5527' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'The RSS import %name is currently locked by %user and was last modified on %datetime.',
    'comment' => NULL,
    'translation' => 'このRSSエクスポート %name は %user により現在ロックされています。最新の修正日時は %datetimeです。',
    'key' => 'b1b40b7bf92e8b6133d3059b4e9e5527',
  ),
  'e8a98be133fbe688d012bd2a2043e648' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'The RSS import will be available for editing once it is stored by the modifier or when it is automatically unlocked on %datetime.',
    'comment' => NULL,
    'translation' => 'このRSSエクスポートは修正中の編集が保存されるか %datetime に自動的にロック解除されることで編集可能になります。',
    'key' => 'e8a98be133fbe688d012bd2a2043e648',
  ),
  '7039e8e0da5be87f4ccb0480d5383f85' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'Please choose where to export from.

    Select your placements then click the %buttonname button.
    Using the recent and bookmark items for quick placement is also possible.
    Click on placement names to change the browse listing.',
    'comment' => NULL,
    'translation' => 'どこからエクスポートするかを選択してください。￼￼￼

￼￼%buttonnameボタンをクリックして、配置先を選択してください。￼￼￼最近使われたオブジェクトとブックマークアイテムを使うこともできます。￼￼￼リストを変更するには、オブジェクト名をクリックしてください。',
    'key' => '7039e8e0da5be87f4ccb0480d5383f85',
  ),
  '3f517c019d5072962981077ead630dd2' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'Please choose where to store imported items.

    Select your placements then click the %buttonname button.
    Using the recent and bookmark items for quick placement is also possible.
    Click on placement names to change the browse listing.',
    'comment' => NULL,
    'translation' => 'インポートされたアイテムの保存先を選択してください。￼￼￼￼

￼￼￼￼￼%buttonnameボタンをクリックして、配置先を選択してください。￼￼￼最近使われたオブジェクトとブックマークアイテムを使うこともできます。￼￼￼リストを変更するには、オブジェクト名をクリックしてください。',
    'key' => '3f517c019d5072962981077ead630dd2',
  ),
  'ad29eb70969da9d664c3f2a6b9bb834f' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'Please choose image to use in RSS export.

    Select your placements then click the %buttonname button.
    Using the recent and bookmark items for quick placement is also possible.
    Click on placement names to change the browse listing.',
    'comment' => NULL,
    'translation' => 'RSSエクスポートに使用する画像を選択してください。￼￼￼￼￼

￼￼%buttonnameボタンをクリックして、オブジェクトを選択してください。￼￼￼最近使われたオブジェクトとブックマークアイテムを使うこともできます。￼￼￼リストを変更するには、オブジェクト名をクリックしてください。',
    'key' => 'ad29eb70969da9d664c3f2a6b9bb834f',
  ),
  '9e45452c9bab555003b075831bbc0cff' => 
  array (
    'context' => 'design/standard/rss',
    'source' => 'Please select the owner of the objects to import

    Select the user then click the %buttonname button.
    Using the recent and bookmark items for quick selection is also possible.
    Click on placement names to change the browse listing.',
    'comment' => NULL,
    'translation' => 'エクスポートするオブジェクトの所有者を選択してください。￼￼￼￼￼￼

￼￼%buttonnameボタンをクリックして、オブジェクトを選択してください。￼￼￼最近使われたオブジェクトとブックマークアイテムを使うこともできます。￼￼￼リストを変更するには、オブジェクト名をクリックしてください。',
    'key' => '9e45452c9bab555003b075831bbc0cff',
  ),
);
?>
