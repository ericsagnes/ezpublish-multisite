<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/user/',
);

$TranslationRoot = array (
  'aea0bf0c959b37c194d946b1c4ac321b' => 
  array (
    'context' => 'design/standard/user/',
    'source' => 'The new password must be at least %1 characters long. Please retype your new password.',
    'comment' => NULL,
    'translation' => '新しいパスワードは%1文字以上にしてください。パスワードを再度入力してください。',
    'key' => 'aea0bf0c959b37c194d946b1c4ac321b',
  ),
);
?>
