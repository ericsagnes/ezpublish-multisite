<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/user/password',
);

$TranslationRoot = array (
  'fca9e5e06f553600eb5ac681d57b1464' => 
  array (
    'context' => 'design/ezwebin/user/password',
    'source' => 'Change password for user',
    'comment' => NULL,
    'translation' => 'ユーザパスワードの変更',
    'key' => 'fca9e5e06f553600eb5ac681d57b1464',
  ),
  '27c0da95dcf0f8a645618035ccdcd07a' => 
  array (
    'context' => 'design/ezwebin/user/password',
    'source' => 'Please retype your old password.',
    'comment' => NULL,
    'translation' => '古いパスワードを再入力してください。',
    'key' => '27c0da95dcf0f8a645618035ccdcd07a',
  ),
  '4a88475a8757c7bf38c881bcf4b14aad' => 
  array (
    'context' => 'design/ezwebin/user/password',
    'source' => 'Password didn\'t match, please retype your new password.',
    'comment' => NULL,
    'translation' => 'パスワードがマッチしませんでした。新しいパスワードを再入力してください。',
    'key' => '4a88475a8757c7bf38c881bcf4b14aad',
  ),
  'a266811330eec9a698f01979148b6820' => 
  array (
    'context' => 'design/ezwebin/user/password',
    'source' => 'Password successfully updated.',
    'comment' => NULL,
    'translation' => 'パスワードは更新されました。',
    'key' => 'a266811330eec9a698f01979148b6820',
  ),
  '571a9bb5ab1c7a5002d56556a6331d29' => 
  array (
    'context' => 'design/ezwebin/user/password',
    'source' => 'Old password',
    'comment' => NULL,
    'translation' => '古いパスワード',
    'key' => '571a9bb5ab1c7a5002d56556a6331d29',
  ),
  '9c1df944d445857df4f3471c3dc1d68c' => 
  array (
    'context' => 'design/ezwebin/user/password',
    'source' => 'New password',
    'comment' => NULL,
    'translation' => '新しいパスワード',
    'key' => '9c1df944d445857df4f3471c3dc1d68c',
  ),
  '148d33f27e1b28e6ec8d4e96ae2bd8db' => 
  array (
    'context' => 'design/ezwebin/user/password',
    'source' => 'Retype password',
    'comment' => NULL,
    'translation' => 'パスワードを再入力してください',
    'key' => '148d33f27e1b28e6ec8d4e96ae2bd8db',
  ),
  '2856daddd3ee9e46288baa4df7e5a999' => 
  array (
    'context' => 'design/ezwebin/user/password',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => '2856daddd3ee9e46288baa4df7e5a999',
  ),
  'c8b97aa06db8672b4b668b0638215802' => 
  array (
    'context' => 'design/ezwebin/user/password',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => 'c8b97aa06db8672b4b668b0638215802',
  ),
  '570e453fb72ee4403cfbc9fcbab23e48' => 
  array (
    'context' => 'design/ezwebin/user/password',
    'source' => 'The new password must be at least %1 characters long, please retype your new password.',
    'comment' => NULL,
    'translation' => '新しいパスワードは%1以上である必要があります。再度新しいパスワードを入力して下さい。',
    'key' => '570e453fb72ee4403cfbc9fcbab23e48',
  ),
);
?>
