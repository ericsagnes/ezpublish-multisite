<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/visual/menuconfig',
);

$TranslationRoot = array (
  '712b60af9492c9d6994d5f7675f1466f' => 
  array (
    'context' => 'design/admin/visual/menuconfig',
    'source' => 'Menu management',
    'comment' => NULL,
    'translation' => 'メニュー管理',
    'key' => '712b60af9492c9d6994d5f7675f1466f',
  ),
  '0f14e6256cabd4ce267d187d1fe823fd' => 
  array (
    'context' => 'design/admin/visual/menuconfig',
    'source' => 'Set',
    'comment' => NULL,
    'translation' => '設定',
    'key' => '0f14e6256cabd4ce267d187d1fe823fd',
  ),
  'dee4092356c7ae1910b9a75539965af9' => 
  array (
    'context' => 'design/admin/visual/menuconfig',
    'source' => 'Menu positioning',
    'comment' => NULL,
    'translation' => 'メニュー位置',
    'key' => 'dee4092356c7ae1910b9a75539965af9',
  ),
  '7699eaf5981bc0090aa0802a94cd628e' => 
  array (
    'context' => 'design/admin/visual/menuconfig',
    'source' => 'Apply changes',
    'comment' => NULL,
    'translation' => '変更の適用',
    'key' => '7699eaf5981bc0090aa0802a94cd628e',
  ),
  '28a021f36f973d243eba8ec3e304a6fc' => 
  array (
    'context' => 'design/admin/visual/menuconfig',
    'source' => 'Click here to store the changes if you have modified the menu settings above.',
    'comment' => NULL,
    'translation' => 'メニュー設定を変更した場合、このボタンをクリックして修正を保存して下さい。',
    'key' => '28a021f36f973d243eba8ec3e304a6fc',
  ),
  '6c6ff07614266c3dfbd886bd3f9b89ec' => 
  array (
    'context' => 'design/admin/visual/menuconfig',
    'source' => 'Siteaccess',
    'comment' => NULL,
    'translation' => 'サイトアクセス',
    'key' => '6c6ff07614266c3dfbd886bd3f9b89ec',
  ),
);
?>
