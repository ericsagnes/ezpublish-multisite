<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/collectedinfo/poll',
);

$TranslationRoot = array (
  '5b12904eec15171b220cdef69b61b6cb' => 
  array (
    'context' => 'design/admin/content/collectedinfo/poll',
    'source' => 'Poll %pollname',
    'comment' => NULL,
    'translation' => '%pollname の投票',
    'key' => '5b12904eec15171b220cdef69b61b6cb',
  ),
  '91268d98477d7e38b652ae432fa6227a' => 
  array (
    'context' => 'design/admin/content/collectedinfo/poll',
    'source' => 'Poll results',
    'comment' => NULL,
    'translation' => '投票の結果',
    'key' => '91268d98477d7e38b652ae432fa6227a',
  ),
  'e9e2a8b85841a7edb14f5fd123ff7246' => 
  array (
    'context' => 'design/admin/content/collectedinfo/poll',
    'source' => '%count total votes',
    'comment' => NULL,
    'translation' => '投票総数 %count',
    'key' => 'e9e2a8b85841a7edb14f5fd123ff7246',
  ),
  '03fdbda5e2011b5268092babf73f7ab6' => 
  array (
    'context' => 'design/admin/content/collectedinfo/poll',
    'source' => 'Anonymous users are not allowed to vote in this poll. Please log in.',
    'comment' => NULL,
    'translation' => '匿名ユーザはこの投票に投票することができません。ログインしてください。',
    'key' => '03fdbda5e2011b5268092babf73f7ab6',
  ),
  'b288542248c3f87769a064c5bbfedb26' => 
  array (
    'context' => 'design/admin/content/collectedinfo/poll',
    'source' => 'You have already voted in this poll.',
    'comment' => NULL,
    'translation' => 'この投票には既に参加しています。',
    'key' => 'b288542248c3f87769a064c5bbfedb26',
  ),
);
?>
