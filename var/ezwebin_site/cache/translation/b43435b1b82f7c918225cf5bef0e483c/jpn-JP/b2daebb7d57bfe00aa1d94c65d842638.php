<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/notification/handler/ezgeneraldigest/settings/edit',
);

$TranslationRoot = array (
  '918bac630a4a34a55b6a2a0128d89624' => 
  array (
    'context' => 'design/admin/notification/handler/ezgeneraldigest/settings/edit',
    'source' => 'Receive all messages combined in one digest',
    'comment' => NULL,
    'translation' => 'すべての通知をダイジェスト形式で受け取る',
    'key' => '918bac630a4a34a55b6a2a0128d89624',
  ),
  'a110d7f5c54d3a7212857dab0de7147f' => 
  array (
    'context' => 'design/admin/notification/handler/ezgeneraldigest/settings/edit',
    'source' => 'Daily, at',
    'comment' => NULL,
    'translation' => '毎日（指定時刻）',
    'key' => 'a110d7f5c54d3a7212857dab0de7147f',
  ),
  '8a6ef81418fa3e81b937257ebcced2ed' => 
  array (
    'context' => 'design/admin/notification/handler/ezgeneraldigest/settings/edit',
    'source' => 'Once per week, on ',
    'comment' => NULL,
    'translation' => '週に１回（曜日指定）',
    'key' => '8a6ef81418fa3e81b937257ebcced2ed',
  ),
  'ffd38ef9657cdb049c02adc22e2a46a3' => 
  array (
    'context' => 'design/admin/notification/handler/ezgeneraldigest/settings/edit',
    'source' => 'If day number is larger than the number of days within the current month, the last day of the current month will be used.',
    'comment' => NULL,
    'translation' => '指定日が該当月に存在しない場合、月の最終日がそれに代わります。',
    'key' => 'ffd38ef9657cdb049c02adc22e2a46a3',
  ),
  '84b89f60e9129d1589e123dffa2eca6f' => 
  array (
    'context' => 'design/admin/notification/handler/ezgeneraldigest/settings/edit',
    'source' => 'Receive digests',
    'comment' => NULL,
    'translation' => 'ダイジェストの受信',
    'key' => '84b89f60e9129d1589e123dffa2eca6f',
  ),
  '94323e58c2b66e3ec104b5efe81e5fda' => 
  array (
    'context' => 'design/admin/notification/handler/ezgeneraldigest/settings/edit',
    'source' => 'Once per month, on day number',
    'comment' => NULL,
    'translation' => '月に１回（指定日）',
    'key' => '94323e58c2b66e3ec104b5efe81e5fda',
  ),
);
?>
