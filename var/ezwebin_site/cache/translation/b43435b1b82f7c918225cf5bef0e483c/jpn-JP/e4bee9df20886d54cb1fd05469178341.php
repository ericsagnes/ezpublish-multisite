<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/shop/orderview',
);

$TranslationRoot = array (
  '58fd36304b1e831e578e9f28ed49cadc' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Order %order_id [%order_status]',
    'comment' => NULL,
    'translation' => '注文ID %order_id [%order_status]',
    'key' => '58fd36304b1e831e578e9f28ed49cadc',
  ),
  '9478a283dcf2df9f913f9be51417760b' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Product items',
    'comment' => NULL,
    'translation' => '商品',
    'key' => '9478a283dcf2df9f913f9be51417760b',
  ),
  '14e3da8b03195fcfef4521d2ce3dd4d0' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Product',
    'comment' => NULL,
    'translation' => '商品',
    'key' => '14e3da8b03195fcfef4521d2ce3dd4d0',
  ),
  'b901005673577e8c768943ac4110aa79' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Count',
    'comment' => NULL,
    'translation' => '数量',
    'key' => 'b901005673577e8c768943ac4110aa79',
  ),
  '7bc852c9dfa37d3b8b5498fa71397f1d' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'VAT',
    'comment' => NULL,
    'translation' => '消費税',
    'key' => '7bc852c9dfa37d3b8b5498fa71397f1d',
  ),
  '32821bba62ccb74d609baf8d1e42bb6d' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Price inc. VAT',
    'comment' => NULL,
    'translation' => '価格（税込）',
    'key' => '32821bba62ccb74d609baf8d1e42bb6d',
  ),
  '3bf838a4414db0cbfbe55e28ea3c5c8b' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Discount',
    'comment' => NULL,
    'translation' => '割引',
    'key' => '3bf838a4414db0cbfbe55e28ea3c5c8b',
  ),
  'aa9484acf9191630f323ff8b59128020' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Order summary',
    'comment' => NULL,
    'translation' => 'ご注文のサマリ',
    'key' => 'aa9484acf9191630f323ff8b59128020',
  ),
  'd1a9bf05043ef6644861f69915f93d73' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Summary',
    'comment' => NULL,
    'translation' => 'サマリ',
    'key' => 'd1a9bf05043ef6644861f69915f93d73',
  ),
  'a2a32fe4d45fd22ee727fb635796bf9e' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Subtotal of items',
    'comment' => NULL,
    'translation' => '商品小計',
    'key' => 'a2a32fe4d45fd22ee727fb635796bf9e',
  ),
  '0423d9cd4ee08c622ecd4246553a7380' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Order total',
    'comment' => NULL,
    'translation' => 'お買い上げ合計',
    'key' => '0423d9cd4ee08c622ecd4246553a7380',
  ),
  'c0afb41d08bb796e639ae5846b145aa5' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Order history',
    'comment' => NULL,
    'translation' => 'お買い上げ履歴',
    'key' => 'c0afb41d08bb796e639ae5846b145aa5',
  ),
  '6013e8a2959e5dc8cabd7776fb5d3203' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Total price ex. VAT',
    'comment' => NULL,
    'translation' => '合計（税抜）',
    'key' => '6013e8a2959e5dc8cabd7776fb5d3203',
  ),
  '19e7a2c5c47589d23da477b99dc0880f' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Total price inc. VAT',
    'comment' => NULL,
    'translation' => '合計（税込）',
    'key' => '19e7a2c5c47589d23da477b99dc0880f',
  ),
  '3082452719ac17aa96e8acafdf141a47' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Date',
    'comment' => NULL,
    'translation' => '日付',
    'key' => '3082452719ac17aa96e8acafdf141a47',
  ),
  '62651ae6e18ae4feb01e6c5b2edc6fad' => 
  array (
    'context' => 'design/ezwebin/shop/orderview',
    'source' => 'Order status',
    'comment' => NULL,
    'translation' => '注文ステータス',
    'key' => '62651ae6e18ae4feb01e6c5b2edc6fad',
  ),
);
?>
