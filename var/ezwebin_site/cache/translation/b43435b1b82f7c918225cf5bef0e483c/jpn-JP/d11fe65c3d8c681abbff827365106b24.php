<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/browse_swap_node',
);

$TranslationRoot = array (
  '150bc4dc81ae6a9d9a06fd9f8cd8fd42' => 
  array (
    'context' => 'design/admin/content/browse_swap_node',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => '150bc4dc81ae6a9d9a06fd9f8cd8fd42',
  ),
  'ded60510cd07e478caa519b98faefb84' => 
  array (
    'context' => 'design/admin/content/browse_swap_node',
    'source' => 'Choose the node to exchange for <%object_name>',
    'comment' => NULL,
    'translation' => '<%object_name>と入れ替えるノードを選択して下さい',
    'key' => 'ded60510cd07e478caa519b98faefb84',
  ),
  '07538fbbad2a8aae947f1dc50a5c61d0' => 
  array (
    'context' => 'design/admin/content/browse_swap_node',
    'source' => 'Use the radio buttons to choose the node that you want to swap with <%object_name>.',
    'comment' => NULL,
    'translation' => '<%object_name>と入れ替えるノードをラジオボタンで選択してください。',
    'key' => '07538fbbad2a8aae947f1dc50a5c61d0',
  ),
);
?>
