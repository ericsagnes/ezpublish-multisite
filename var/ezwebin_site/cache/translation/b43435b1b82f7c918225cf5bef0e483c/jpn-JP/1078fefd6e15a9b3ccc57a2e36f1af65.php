<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/user',
);

$TranslationRoot = array (
  'aee5b88a4cc88fa42b30a9b4e294bb64' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Login',
    'comment' => NULL,
    'translation' => 'ログイン',
    'key' => 'aee5b88a4cc88fa42b30a9b4e294bb64',
  ),
  'c869d6a35fc99ab8937ec775d1d86dee' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Activate account',
    'comment' => NULL,
    'translation' => 'ユーザ登録のアクティベーション',
    'key' => 'c869d6a35fc99ab8937ec775d1d86dee',
  ),
  'a286af6895cff78049f63a265b64f3a8' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => 'a286af6895cff78049f63a265b64f3a8',
  ),
  'dc72a04c991542b1d30c583c24ba8363' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Could not login',
    'comment' => NULL,
    'translation' => 'ログインできません',
    'key' => 'dc72a04c991542b1d30c583c24ba8363',
  ),
  'ffad21a7218d4a0b7c92dba540dc7aa5' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'A valid username and password is required to login.',
    'comment' => NULL,
    'translation' => '正しいユーザ名とパスワードでログインしてください。',
    'key' => 'ffad21a7218d4a0b7c92dba540dc7aa5',
  ),
  'c8e705fa32ba1ea233045dc78d4ce73d' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Access not allowed',
    'comment' => NULL,
    'translation' => 'アクセスは許可されていません。',
    'key' => 'c8e705fa32ba1ea233045dc78d4ce73d',
  ),
  '6254d2a729c77d6dc1e2245349d03395' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'You are not allowed to access %1.',
    'comment' => NULL,
    'translation' => '%1 へのアクセスはできません。',
    'key' => '6254d2a729c77d6dc1e2245349d03395',
  ),
  'e7c09574acb1d78151b1b57f28d2677a' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Change password for user',
    'comment' => NULL,
    'translation' => 'パスワード変更',
    'key' => 'e7c09574acb1d78151b1b57f28d2677a',
  ),
  '634bfcaecb0ebe079ba1f7167fd73aa6' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => '634bfcaecb0ebe079ba1f7167fd73aa6',
  ),
  'e1638aa66929cfc1f25d5ea77165fa77' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Register user',
    'comment' => NULL,
    'translation' => 'ユーザ情報の登録',
    'key' => 'e1638aa66929cfc1f25d5ea77165fa77',
  ),
  'ca802bdc5dd894ae98a6ef85ce042e32' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Input did not validate',
    'comment' => NULL,
    'translation' => '無効な入力です',
    'key' => 'ca802bdc5dd894ae98a6ef85ce042e32',
  ),
  '9f198ff52227ff44de473194c7536107' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Input was stored successfully',
    'comment' => NULL,
    'translation' => '入力の保存に成功しました',
    'key' => '9f198ff52227ff44de473194c7536107',
  ),
  '0604c438eceaf05d4e3815aa4f8fc333' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Register',
    'comment' => NULL,
    'translation' => '新規登録',
    'key' => '0604c438eceaf05d4e3815aa4f8fc333',
  ),
  'becae797905029e365e0b1537ae48d31' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Discard',
    'comment' => NULL,
    'translation' => '破棄',
    'key' => 'becae797905029e365e0b1537ae48d31',
  ),
  '7cd55373466fd13f52237c52f825833c' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'User setting',
    'comment' => NULL,
    'translation' => 'ユーザ設定',
    'key' => '7cd55373466fd13f52237c52f825833c',
  ),
  '06f61b98896f580196bebb49c3910e68' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Maximum login',
    'comment' => NULL,
    'translation' => '同時ログイン許可数',
    'key' => '06f61b98896f580196bebb49c3910e68',
  ),
  'd0ae94b5f8dd9aba4dba51cd0565ab8f' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Is enabled',
    'comment' => NULL,
    'translation' => 'アクティブユーザ',
    'key' => 'd0ae94b5f8dd9aba4dba51cd0565ab8f',
  ),
  'a35c8d2e73916bd989cf490ccb7ad1b9' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Update',
    'comment' => NULL,
    'translation' => '更新',
    'key' => 'a35c8d2e73916bd989cf490ccb7ad1b9',
  ),
  '6e252b0e5f7c523133b9ecb9e38a84d2' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'User registered',
    'comment' => NULL,
    'translation' => 'ユーザは登録されました。',
    'key' => '6e252b0e5f7c523133b9ecb9e38a84d2',
  ),
  '61e3915dd5d81a7f4ff28f1f7bd8b640' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Your account was successfully created.',
    'comment' => NULL,
    'translation' => 'ユーザ登録に成功しました。',
    'key' => '61e3915dd5d81a7f4ff28f1f7bd8b640',
  ),
  '1e51427894b6145005eeaa5da5f31efd' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Login',
    'comment' => 'Button',
    'translation' => 'ログイン',
    'key' => '1e51427894b6145005eeaa5da5f31efd',
  ),
  'dc8fea114e2d3556e2bea11d6b0efefd' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'User profile',
    'comment' => NULL,
    'translation' => 'ユーザ・プロフィール',
    'key' => 'dc8fea114e2d3556e2bea11d6b0efefd',
  ),
  '90483e1b56759668cacb3781f45acfbe' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '90483e1b56759668cacb3781f45acfbe',
  ),
  '55fb1f06d06ac78720aa378ea20b7862' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Password',
    'comment' => NULL,
    'translation' => 'パスワード',
    'key' => '55fb1f06d06ac78720aa378ea20b7862',
  ),
  'a90b3d281f5d41c66538cfbedc521ec4' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Sign Up',
    'comment' => 'Button',
    'translation' => 'サインアップ',
    'key' => 'a90b3d281f5d41c66538cfbedc521ec4',
  ),
  '1a3a385eaed3bbf7bd82e51b1653b84b' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Please retype your old password.',
    'comment' => NULL,
    'translation' => '旧パスワードをもう一度入力してください。',
    'key' => '1a3a385eaed3bbf7bd82e51b1653b84b',
  ),
  '3bb6b45fe888535c6b111224e7a8cd94' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Password successfully updated.',
    'comment' => NULL,
    'translation' => 'パスワードの更新に成功しました。',
    'key' => '3bb6b45fe888535c6b111224e7a8cd94',
  ),
  '89e3b0b5509fd66a03960e3b435935c3' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Edit profile',
    'comment' => NULL,
    'translation' => 'プロフィールの編集',
    'key' => '89e3b0b5509fd66a03960e3b435935c3',
  ),
  'fbfa051c6eb59c2db9fe3f22a1fcc137' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Change password',
    'comment' => NULL,
    'translation' => 'パスワード変更',
    'key' => 'fbfa051c6eb59c2db9fe3f22a1fcc137',
  ),
  '5691498e6824cbdb52eb2ebf5eb8fc88' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Change setting',
    'comment' => NULL,
    'translation' => '設定の変更',
    'key' => '5691498e6824cbdb52eb2ebf5eb8fc88',
  ),
  '17a22ca5abc9499936a25f2fd8d64485' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Old password',
    'comment' => NULL,
    'translation' => '旧パスワード',
    'key' => '17a22ca5abc9499936a25f2fd8d64485',
  ),
  '9d8a1a4e1795a21b87125093dee42b47' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'New password',
    'comment' => NULL,
    'translation' => '新パスワード',
    'key' => '9d8a1a4e1795a21b87125093dee42b47',
  ),
  'd0d0b93550dcd2e1316a252c41dadc44' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Retype password',
    'comment' => NULL,
    'translation' => '新パスワード（確認）',
    'key' => 'd0d0b93550dcd2e1316a252c41dadc44',
  ),
  '6e73bab2a2a0b6c5cff70165db826de8' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Forgot your password?',
    'comment' => NULL,
    'translation' => 'パスワードを忘れましたか?',
    'key' => '6e73bab2a2a0b6c5cff70165db826de8',
  ),
  '20af1aed987c942c53a60b4df5009259' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Your account is now activated.',
    'comment' => NULL,
    'translation' => 'アカウントは有効になりました。',
    'key' => '20af1aed987c942c53a60b4df5009259',
  ),
  '9150c0ed5cf8dce6162ac11654ef95b2' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Sorry, the key submitted was not a valid key. Account was not activated.',
    'comment' => NULL,
    'translation' => 'パスワードが無効なためアカウントを有効に出来ません。',
    'key' => '9150c0ed5cf8dce6162ac11654ef95b2',
  ),
  '4cc9ffe6ff2aefceb56d8c584bdd2b34' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Username',
    'comment' => NULL,
    'translation' => 'ユーザ名',
    'key' => '4cc9ffe6ff2aefceb56d8c584bdd2b34',
  ),
  '2b2394c1d82370a740dd83f98f7b7976' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Username',
    'comment' => 'User name',
    'translation' => 'ユーザ名',
    'key' => '2b2394c1d82370a740dd83f98f7b7976',
  ),
  '996695559d61f8b04ccd8c40bc5cacb3' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Unable to register new user',
    'comment' => NULL,
    'translation' => '新規のユーザ登録はできません',
    'key' => '996695559d61f8b04ccd8c40bc5cacb3',
  ),
  '06795120f93daa1e63851ef0089a7fdf' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Back',
    'comment' => NULL,
    'translation' => '戻る',
    'key' => '06795120f93daa1e63851ef0089a7fdf',
  ),
  '7c02e3de5148c7dcde0480abe9d19d6b' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'The node (%1) specified in [UserSettings].DefaultUserPlacement setting in site.ini does not exist!',
    'comment' => NULL,
    'translation' => 'ノード (%1) は[UserSettings] の中に示されています。 site.ini の中にデフォルトのユーザ設定は存在しません!',
    'key' => '7c02e3de5148c7dcde0480abe9d19d6b',
  ),
  'c7863ad4da918ee662d1fdb7fab50f8e' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Email',
    'comment' => NULL,
    'translation' => 'e-mail',
    'key' => 'c7863ad4da918ee662d1fdb7fab50f8e',
  ),
  '1e86de4de126f4835edffb4e6958ce2b' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Your account is already active.',
    'comment' => NULL,
    'translation' => 'あなたのアカウントは既に有効です。',
    'key' => '1e86de4de126f4835edffb4e6958ce2b',
  ),
  '6e928b6d12c29db4bf7aff4e9d743d55' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Password did not match. Please retype your new password.',
    'comment' => NULL,
    'translation' => 'パスワードが一致していません。パスワードを再入力してください。',
    'key' => '6e928b6d12c29db4bf7aff4e9d743d55',
  ),
  '61219145941ec2f6f286811202cf94dd' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Your account was successfully created. An email will be sent to the specified
email address. Follow the instructions in that mail to activate
your account.',
    'comment' => NULL,
    'translation' => 'ユーザアカウントが作成されました。登録されたメールアドレスにメールが送られます。
メールの指示にしたがい、アカウントを有効にしてください。',
    'key' => '61219145941ec2f6f286811202cf94dd',
  ),
  '644e9f3d014d2c6bcd8877832ac927fd' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Please note that your browser must use and support cookies to register a new user.',
    'comment' => NULL,
    'translation' => '新しいユーザを登録するには、クッキーをサポートしているブラウザを使用し、クッキーをオンにしておく必要があります。',
    'key' => '644e9f3d014d2c6bcd8877832ac927fd',
  ),
  '0e6a172c80d371b14b042c8e6e1acc9d' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Your browser does not seem to support cookies, to register a new user, cookies need to be supported and enabled!',
    'comment' => NULL,
    'translation' => 'ご使用のブラウザはクッキーをサポートしていないようです。新しいユーザを登録するには、クッキーが有効になっている必要があります。',
    'key' => '0e6a172c80d371b14b042c8e6e1acc9d',
  ),
  '8e9062331d5d3a8b5c3dedc495276399' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Try again',
    'comment' => NULL,
    'translation' => '再実行',
    'key' => '8e9062331d5d3a8b5c3dedc495276399',
  ),
  '94719b8ac28da147c89aa7d3eed30a2f' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'The new password must be at least %1 characters long. Please retype your new password.',
    'comment' => NULL,
    'translation' => '新しいパスワードは%1文字以上にしてください。パスワードを再度入力してください。',
    'key' => '94719b8ac28da147c89aa7d3eed30a2f',
  ),
  'a73f0634ef5abdbc2586bf48e5c668eb' => 
  array (
    'context' => 'design/standard/user',
    'source' => 'Your email address has been confirmed. An administrator needs to approve your sign up request, before your login becomes valid.',
    'comment' => NULL,
    'translation' => 'Your email address has been confirmed. An administrator needs to approve your sign up request, before your login becomes valid.',
    'key' => 'a73f0634ef5abdbc2586bf48e5c668eb',
  ),
);
?>
