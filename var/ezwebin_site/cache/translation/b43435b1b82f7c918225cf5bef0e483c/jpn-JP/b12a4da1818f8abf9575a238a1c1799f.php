<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/contentstructuremenu',
);

$TranslationRoot = array (
  'ba4338bbf32bb9b54323d58ce1287a87' => 
  array (
    'context' => 'design/standard/contentstructuremenu',
    'source' => 'Fold/Unfold',
    'comment' => NULL,
    'translation' => '開く/たたむ',
    'key' => 'ba4338bbf32bb9b54323d58ce1287a87',
  ),
  '7f9f1485c1109efaef4fa07cfd9d4657' => 
  array (
    'context' => 'design/standard/contentstructuremenu',
    'source' => '[%classname] Click on the icon to display a context-sensitive menu.',
    'comment' => NULL,
    'translation' => '[%classname] コンテキストメニューを表示するにはアイコンをクリックしてください。',
    'key' => '7f9f1485c1109efaef4fa07cfd9d4657',
  ),
);
?>
