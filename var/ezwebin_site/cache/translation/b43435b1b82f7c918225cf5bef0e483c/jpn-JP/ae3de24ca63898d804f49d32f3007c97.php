<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/browse_copy_node',
);

$TranslationRoot = array (
  '942f6bd51c0c66673f8af3ea00dfd9f9' => 
  array (
    'context' => 'design/admin/content/browse_copy_node',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => '942f6bd51c0c66673f8af3ea00dfd9f9',
  ),
  'dad92779bb479700a161eca5d8a8e651' => 
  array (
    'context' => 'design/admin/content/browse_copy_node',
    'source' => 'Choose location for the copy of <%object_name>',
    'comment' => NULL,
    'translation' => '<%object_name> を複製する配置先の選択',
    'key' => 'dad92779bb479700a161eca5d8a8e651',
  ),
  '6cc85f4d7efe3eca7c383907e2a144c6' => 
  array (
    'context' => 'design/admin/content/browse_copy_node',
    'source' => 'Choose location for the copy of subtree of node <%node_name>',
    'comment' => NULL,
    'translation' => '<%node_name> のサブツリーを複製する配置先を選択',
    'key' => '6cc85f4d7efe3eca7c383907e2a144c6',
  ),
  '6660596d2204386cbb9ce9d242865031' => 
  array (
    'context' => 'design/admin/content/browse_copy_node',
    'source' => 'Choose a new location for the copy of <%object_name> using the radio buttons then click "Select".',
    'comment' => NULL,
    'translation' => '<%objectname>の複製の配置先をラジオボタンで選択し、"選択"をクリックしてください。',
    'key' => '6660596d2204386cbb9ce9d242865031',
  ),
  '7ea69572b6f9a1eefb3de22514c7b9da' => 
  array (
    'context' => 'design/admin/content/browse_copy_node',
    'source' => 'Choose a new location for the copy of subtree of node <%node_name> using the radio buttons then click "Select".',
    'comment' => NULL,
    'translation' => '<%node_name>サブツリーの複製の配置先をラジオボタンで選択し、"選択"をクリックしてください。',
    'key' => '7ea69572b6f9a1eefb3de22514c7b9da',
  ),
);
?>
