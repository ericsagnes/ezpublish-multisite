<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/upload_related',
);

$TranslationRoot = array (
  'd3eb6e2322c041850752f216cb591273' => 
  array (
    'context' => 'design/admin/content/upload_related',
    'source' => 'Upload a file and relate it to <%version_name>',
    'comment' => NULL,
    'translation' => '<%version_name> に関連させるファイルのアップロード',
    'key' => 'd3eb6e2322c041850752f216cb591273',
  ),
  '8d5706dabc162fa11c0450ca10dfce07' => 
  array (
    'context' => 'design/admin/content/upload_related',
    'source' => 'This operation allows you to upload a file and add it as a related object.',
    'comment' => NULL,
    'translation' => 'この操作はファイルのアップロードを行い、同時に関連付けをします。',
    'key' => '8d5706dabc162fa11c0450ca10dfce07',
  ),
  '40267e237865e635605c95c504e5d7b1' => 
  array (
    'context' => 'design/admin/content/upload_related',
    'source' => 'When the file is uploaded, an object will be created according to the type of the file.',
    'comment' => NULL,
    'translation' => 'ファイルのアップロード時に、オブジェクトはファイルタイプに応じて生成されます。',
    'key' => '40267e237865e635605c95c504e5d7b1',
  ),
  'a24eb924ddb9a104cc661f3ccb528696' => 
  array (
    'context' => 'design/admin/content/upload_related',
    'source' => 'The newly created object will be automatically related to the draft being edited (<%version_name>).',
    'comment' => NULL,
    'translation' => '生成された新規オブジェクトは自動的に編集中の下書き(<%version_name>)に関連づけされます。',
    'key' => 'a24eb924ddb9a104cc661f3ccb528696',
  ),
  '90c9d1f086cf2c7e9a889c1c23aa2734' => 
  array (
    'context' => 'design/admin/content/upload_related',
    'source' => 'The newly created object will be placed within the specified location.',
    'comment' => NULL,
    'translation' => '新しく作成されたオブジェクトは指定した配置先に保存されます。',
    'key' => '90c9d1f086cf2c7e9a889c1c23aa2734',
  ),
  '2de8db4de3b85ae6d493cae625d4db97' => 
  array (
    'context' => 'design/admin/content/upload_related',
    'source' => 'Select the file you want to upload then click the "Upload" button.',
    'comment' => NULL,
    'translation' => 'アップロードするファイルを選択して、”アップロード”ボタンをクリックしてください。',
    'key' => '2de8db4de3b85ae6d493cae625d4db97',
  ),
);
?>
