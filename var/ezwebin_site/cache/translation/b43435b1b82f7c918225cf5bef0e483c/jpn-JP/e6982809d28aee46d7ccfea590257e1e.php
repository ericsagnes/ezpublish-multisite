<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/dashboard/latest_content',
);

$TranslationRoot = array (
  '395e0fd80a6a5ff679df4b589ede4b30' => 
  array (
    'context' => 'design/admin/dashboard/latest_content',
    'source' => 'My latest content',
    'comment' => NULL,
    'translation' => '最新の自作コンテント',
    'key' => '395e0fd80a6a5ff679df4b589ede4b30',
  ),
  '28619739e675288cc87f3b0fbe53cafc' => 
  array (
    'context' => 'design/admin/dashboard/latest_content',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '28619739e675288cc87f3b0fbe53cafc',
  ),
  '51c89888750183fd403c9a2b638b33a2' => 
  array (
    'context' => 'design/admin/dashboard/latest_content',
    'source' => 'Type',
    'comment' => NULL,
    'translation' => '種類',
    'key' => '51c89888750183fd403c9a2b638b33a2',
  ),
  '33d1f21cf97126c4821f97fefe85700e' => 
  array (
    'context' => 'design/admin/dashboard/latest_content',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '33d1f21cf97126c4821f97fefe85700e',
  ),
  'ca7973539bf7fc7cf96d41ba27d024cd' => 
  array (
    'context' => 'design/admin/dashboard/latest_content',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => 'ca7973539bf7fc7cf96d41ba27d024cd',
  ),
  '3ad3ff2a0ab92abf6658c3939385307d' => 
  array (
    'context' => 'design/admin/dashboard/latest_content',
    'source' => 'Edit <%child_name>.',
    'comment' => NULL,
    'translation' => '<%child_name>を編集。',
    'key' => '3ad3ff2a0ab92abf6658c3939385307d',
  ),
  '267300662d0a3a7a53daebb62ff1a33b' => 
  array (
    'context' => 'design/admin/dashboard/latest_content',
    'source' => 'You do not have permission to edit <%child_name>.',
    'comment' => NULL,
    'translation' => '<%child_name>を編集する権限がありません。',
    'key' => '267300662d0a3a7a53daebb62ff1a33b',
  ),
  '482bff2f7eacd76e8c535c3e8b79ba54' => 
  array (
    'context' => 'design/admin/dashboard/latest_content',
    'source' => 'Your latest content list is empty.',
    'comment' => NULL,
    'translation' => '最新の自作コンテントがありません。',
    'key' => '482bff2f7eacd76e8c535c3e8b79ba54',
  ),
);
?>
