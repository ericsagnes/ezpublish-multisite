<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/ezoe/handler',
);

$TranslationRoot = array (
  '654bd55041d79dda9acf28cce853c1dd' => 
  array (
    'context' => 'design/standard/ezoe/handler',
    'source' => 'Object %1 does not exist.',
    'comment' => NULL,
    'translation' => 'オブジェクト %1 は存在しません。',
    'key' => '654bd55041d79dda9acf28cce853c1dd',
  ),
  '1b1be9756f9afc3a978cd1806b86f702' => 
  array (
    'context' => 'design/standard/ezoe/handler',
    'source' => 'Node %1 does not exist.',
    'comment' => NULL,
    'translation' => 'ノード %1 は存在しません。',
    'key' => '1b1be9756f9afc3a978cd1806b86f702',
  ),
  '3b35f1d2f875aeacf489cd6a7731fb2d' => 
  array (
    'context' => 'design/standard/ezoe/handler',
    'source' => 'Node &apos;%1&apos; does not exist.',
    'comment' => NULL,
    'translation' => 'ノード&apos;%1&apos;は存在しません。',
    'key' => '3b35f1d2f875aeacf489cd6a7731fb2d',
  ),
  'baa855ee1fff1c6421c10fac059ca73c' => 
  array (
    'context' => 'design/standard/ezoe/handler',
    'source' => 'Some objects used in embed(-inline) tags have been deleted and are no longer available.',
    'comment' => NULL,
    'translation' => '埋め込み(インライン)タグに利用されているオブジェクトの中に、既に削除されているものがあります。',
    'key' => 'baa855ee1fff1c6421c10fac059ca73c',
  ),
);
?>
