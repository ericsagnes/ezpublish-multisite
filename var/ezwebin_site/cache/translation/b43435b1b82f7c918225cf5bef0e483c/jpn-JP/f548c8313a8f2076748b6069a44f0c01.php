<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/confirmtranslationremove',
);

$TranslationRoot = array (
  '5ec6b9ab069bcb4a4f2ba29d1a8ce76a' => 
  array (
    'context' => 'design/admin/content/confirmtranslationremove',
    'source' => 'Confirm language removal',
    'comment' => NULL,
    'translation' => '言語削除の確認',
    'key' => '5ec6b9ab069bcb4a4f2ba29d1a8ce76a',
  ),
  '77612c93658ed0ed4fb3ca226e2fdaae' => 
  array (
    'context' => 'design/admin/content/confirmtranslationremove',
    'source' => 'Are you sure you want to remove the language?',
    'comment' => NULL,
    'translation' => '言語を削除してもよろしいですか?',
    'key' => '77612c93658ed0ed4fb3ca226e2fdaae',
  ),
  '446ca7f5e4d86e426599bfc0953f20c2' => 
  array (
    'context' => 'design/admin/content/confirmtranslationremove',
    'source' => 'Are you sure you want to remove the languages?',
    'comment' => NULL,
    'translation' => '言語を削除してもよろしいですか?',
    'key' => '446ca7f5e4d86e426599bfc0953f20c2',
  ),
  '781310e2557507584a15956cd67aa83c' => 
  array (
    'context' => 'design/admin/content/confirmtranslationremove',
    'source' => 'Removing <%1> will also result in the removal of %2 translated versions.',
    'comment' => NULL,
    'translation' => '<%1> の削除により %2 件の翻訳も削除されます。',
    'key' => '781310e2557507584a15956cd67aa83c',
  ),
  'd3b162bddcd32d18d8da6f39d58ae3d1' => 
  array (
    'context' => 'design/admin/content/confirmtranslationremove',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => 'd3b162bddcd32d18d8da6f39d58ae3d1',
  ),
  'c75d41354c7fe3cdd883b9242fb56398' => 
  array (
    'context' => 'design/admin/content/confirmtranslationremove',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => 'c75d41354c7fe3cdd883b9242fb56398',
  ),
);
?>
