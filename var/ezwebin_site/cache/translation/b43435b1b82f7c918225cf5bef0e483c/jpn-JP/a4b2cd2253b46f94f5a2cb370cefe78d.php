<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/content/history',
);

$TranslationRoot = array (
  '00b955c0d783ef88561f5ed7a60bc1f5' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Version not a draft',
    'comment' => NULL,
    'translation' => 'このバージョンはドラフトではありません。',
    'key' => '00b955c0d783ef88561f5ed7a60bc1f5',
  ),
  '7168477b4363b8b1f0aeace21c18d3d3' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Version not yours',
    'comment' => NULL,
    'translation' => 'このバージョンはあなたの所有ではありません。',
    'key' => '7168477b4363b8b1f0aeace21c18d3d3',
  ),
  '8f05110976be3706ab8dd97f37db9774' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Unable to create new version',
    'comment' => NULL,
    'translation' => '新規バージョンの作成が出来ません。',
    'key' => '8f05110976be3706ab8dd97f37db9774',
  ),
  '55a98bf2e94d9c52b9bb2c0c5cc6cc83' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Version history limit has been exceeded and no archived version can be removed by the system.',
    'comment' => NULL,
    'translation' => 'バージョン履歴のリミットを超えています。システムは保管されているバージョンを削除することは出来ません。',
    'key' => '55a98bf2e94d9c52b9bb2c0c5cc6cc83',
  ),
  '6f019b642805bee42a48ec34e56aa11f' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Versions for <%object_name> [%version_count]',
    'comment' => NULL,
    'translation' => '<%object_name> [%version_count] 
のバージョン',
    'key' => '6f019b642805bee42a48ec34e56aa11f',
  ),
  '4f248e78337e9e044100cd9c79b9c65a' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => '4f248e78337e9e044100cd9c79b9c65a',
  ),
  'ca2b67f742c01c3f12ac3fdf47541b33' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Status',
    'comment' => NULL,
    'translation' => 'ステータス',
    'key' => 'ca2b67f742c01c3f12ac3fdf47541b33',
  ),
  '99d5777ba3ca3ee4795ad14893b41362' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Creator',
    'comment' => NULL,
    'translation' => '作成者',
    'key' => '99d5777ba3ca3ee4795ad14893b41362',
  ),
  '6aec8d117270882577b9a07b91508d2e' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Created',
    'comment' => NULL,
    'translation' => '作成日時',
    'key' => '6aec8d117270882577b9a07b91508d2e',
  ),
  '25569ffc5b189a9962806ed63afa4d06' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '25569ffc5b189a9962806ed63afa4d06',
  ),
  '765a7ec98dea7116628a944c374855e4' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Select version #%version_number for removal.',
    'comment' => NULL,
    'translation' => 'バージョン#%version_numberを選択し削除',
    'key' => '765a7ec98dea7116628a944c374855e4',
  ),
  'bed90dea6022886a81695e611f5607e6' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'View the contents of version #%version_number. Translation: %translation.',
    'comment' => NULL,
    'translation' => 'バージョン#%version_numberのコンテンツを表示。翻訳:%translation',
    'key' => 'bed90dea6022886a81695e611f5607e6',
  ),
  '1b6c1d1a6e0a56776c95946a1e6cff8e' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Draft',
    'comment' => NULL,
    'translation' => 'ドラフト',
    'key' => '1b6c1d1a6e0a56776c95946a1e6cff8e',
  ),
  '15c424a649d9216bd88a20dfa7af587c' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Published',
    'comment' => NULL,
    'translation' => '公開中',
    'key' => '15c424a649d9216bd88a20dfa7af587c',
  ),
  '91204019e89c82243b582ef56e0763b5' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Pending',
    'comment' => NULL,
    'translation' => '保留',
    'key' => '91204019e89c82243b582ef56e0763b5',
  ),
  '30a147470072b6bcd3f5a87de4bfa71f' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Archived',
    'comment' => NULL,
    'translation' => '保管',
    'key' => '30a147470072b6bcd3f5a87de4bfa71f',
  ),
  '83a888d077ca6dedd6f8e46d650219eb' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Rejected',
    'comment' => NULL,
    'translation' => '非承認',
    'key' => '83a888d077ca6dedd6f8e46d650219eb',
  ),
  'bda92fde0f9b6618fffea30c40ee7be6' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Untouched draft',
    'comment' => NULL,
    'translation' => '未変更のドラフト',
    'key' => 'bda92fde0f9b6618fffea30c40ee7be6',
  ),
  '37c75d0cef5578ce41b842da0c7ddb36' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Create a copy of version #%version_number.',
    'comment' => NULL,
    'translation' => 'バージョン#%version_numberの複製を作成',
    'key' => '37c75d0cef5578ce41b842da0c7ddb36',
  ),
  'e1b855a579f5f38f2a0d52ef863f7d09' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Edit the contents of version #%version_number.',
    'comment' => NULL,
    'translation' => 'バージョン#%version_numberのコンテンツを編集',
    'key' => 'e1b855a579f5f38f2a0d52ef863f7d09',
  ),
  '86a00d68d9a44fde6e1009eeaf4cdeab' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'This object does not have any versions.',
    'comment' => NULL,
    'translation' => 'このオブジェクトにはバージョンはありません。',
    'key' => '86a00d68d9a44fde6e1009eeaf4cdeab',
  ),
  '74501c706dfc3b1cdaaffaa58e3ced6d' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択項目の削除',
    'key' => '74501c706dfc3b1cdaaffaa58e3ced6d',
  ),
  'f8361eaa61c4a261d2e7d68e79edc7d5' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Remove the selected versions from the object.',
    'comment' => NULL,
    'translation' => 'オブジェクトから選択したバージョンを削除する。',
    'key' => 'f8361eaa61c4a261d2e7d68e79edc7d5',
  ),
  '611aa102703ec8ec410b3a76753f629b' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Show differences',
    'comment' => NULL,
    'translation' => '違いを表示',
    'key' => '611aa102703ec8ec410b3a76753f629b',
  ),
  'dbcf5e939f4164c81b2c7ed78e11f3d4' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Back',
    'comment' => NULL,
    'translation' => '戻る',
    'key' => 'dbcf5e939f4164c81b2c7ed78e11f3d4',
  ),
  '8978a529b9333f2b3764e02080716d7b' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Published version',
    'comment' => NULL,
    'translation' => '公開バージョン',
    'key' => '8978a529b9333f2b3764e02080716d7b',
  ),
  '536621a919808a8bb282029264b772b4' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Translations',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => '536621a919808a8bb282029264b772b4',
  ),
  'ad040b82d8de7f0c0e39b8541b7609f0' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'New drafts [%newerDraftCount]',
    'comment' => NULL,
    'translation' => '新規ドラフト',
    'key' => 'ad040b82d8de7f0c0e39b8541b7609f0',
  ),
  '9c1c075b75de04f28475d7f305c86226' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'This object does not have any drafts.',
    'comment' => NULL,
    'translation' => 'このオブジェクトにはドラフトはありません。',
    'key' => '9c1c075b75de04f28475d7f305c86226',
  ),
  '85b6ef9bd7966a5209752f39c447280b' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Differences between versions %oldVersion and %newVersion',
    'comment' => NULL,
    'translation' => '%oldVersion と %newVersionの違い',
    'key' => '85b6ef9bd7966a5209752f39c447280b',
  ),
  'd99dc8f79f14a44c1a03dba5fb6659d7' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Old version',
    'comment' => NULL,
    'translation' => '旧バージョン',
    'key' => 'd99dc8f79f14a44c1a03dba5fb6659d7',
  ),
  'e89348a994a975765d567c6ee1315f49' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Inline changes',
    'comment' => NULL,
    'translation' => 'インライン変更',
    'key' => 'e89348a994a975765d567c6ee1315f49',
  ),
  '0ab331a0df6f4e5c8a00f6e2bb12f7c3' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Block changes',
    'comment' => NULL,
    'translation' => 'ブロック変更',
    'key' => '0ab331a0df6f4e5c8a00f6e2bb12f7c3',
  ),
  '3e8c64cbc768864df804625a697b7308' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'New version',
    'comment' => NULL,
    'translation' => '新バージョン',
    'key' => '3e8c64cbc768864df804625a697b7308',
  ),
  '34682b6abdd32e7fae671787afd5b756' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Back to history',
    'comment' => NULL,
    'translation' => '履歴へ戻る',
    'key' => '34682b6abdd32e7fae671787afd5b756',
  ),
  '96041d5dba1ce05100c8f240c21cb907' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Version %1 is not available for editing anymore. Only drafts can be edited.',
    'comment' => NULL,
    'translation' => 'バージョン%1は編集することができません。ドラフトを編集して下さい。',
    'key' => '96041d5dba1ce05100c8f240c21cb907',
  ),
  'b148504ef7eaf364c08ad7f0d150bb9e' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'To edit this version, first create a copy of it.',
    'comment' => NULL,
    'translation' => 'このバージョンを編集するには、最初にバージョンを複製して下さい。',
    'key' => 'b148504ef7eaf364c08ad7f0d150bb9e',
  ),
  'f9db1fe924030844206a8f86d7abc8a2' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Version %1 was not created by you. Only your own drafts can be edited.',
    'comment' => NULL,
    'translation' => '%1バージョンの作成者ではありません。自分が作成したドラフトのみ編集出来ます。',
    'key' => 'f9db1fe924030844206a8f86d7abc8a2',
  ),
  'e7f3d4614c87d93e7dc2e593b2ac1697' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'You can either change your version history settings in content.ini, remove draft versions or edit existing drafts.',
    'comment' => NULL,
    'translation' => 'content.iniでバージョン履歴の設定を変えるか、ドラフトバージョンを削除するか、すでに存在するドラフトを編集することができます。',
    'key' => 'e7f3d4614c87d93e7dc2e593b2ac1697',
  ),
  '9fa2d61bc8f4caf5718db5dee4a16d38' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Toggle selection',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => '9fa2d61bc8f4caf5718db5dee4a16d38',
  ),
  '0c327d7924bc4d94b74acb3e7d4b5e73' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Version #%version_number cannot be removed because it is either the published version of the object or because you do not have permission to remove it.',
    'comment' => NULL,
    'translation' => '#%version_numberバージョンは、現在公開中のバージョンであるか、削除権限を持っていないため、削除することができません。',
    'key' => '0c327d7924bc4d94b74acb3e7d4b5e73',
  ),
  'dc252c25d8fc74914fdff73915573668' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'There is no need to do a copies of untouched drafts.',
    'comment' => NULL,
    'translation' => '編集されてないドラフトをコピーすることは出来ません。',
    'key' => 'dc252c25d8fc74914fdff73915573668',
  ),
  '04f68c3b8424f2a9ca0116cc1d3d644d' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'You cannot make copies of versions because you do not have permission to edit the object.',
    'comment' => NULL,
    'translation' => 'オブジェクトを編集する権限を持っていないため、バージョンの複製を作ることはできません。',
    'key' => '04f68c3b8424f2a9ca0116cc1d3d644d',
  ),
  '7be4521b83aa583ee7e36c22eb80d408' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'You cannot edit the contents of version #%version_number either because it is not a draft or because you do not have permission to edit the object.',
    'comment' => NULL,
    'translation' => '#%version_numberバージョンは、ドラフトではないか、編集権限を持っていないため、コンテンツを編集できません。',
    'key' => '7be4521b83aa583ee7e36c22eb80d408',
  ),
  '0a9d6a9b9333bee595f678e85de6876e' => 
  array (
    'context' => 'design/ezwebin/content/history',
    'source' => 'Modified translation',
    'comment' => NULL,
    'translation' => '変更された翻訳バージョン',
    'key' => '0a9d6a9b9333bee595f678e85de6876e',
  ),
);
?>
