<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/reference/ez',
);

$TranslationRoot = array (
  '3d2570ed282ee8eda9376d83aed88a86' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'No generated documentation found',
    'comment' => NULL,
    'translation' => '生成されたドキュメントはありません',
    'key' => '3d2570ed282ee8eda9376d83aed88a86',
  ),
  '0c7dfb9cc4efd4248af336858662ee81' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'To create the reference documentation you must do the following step',
    'comment' => NULL,
    'translation' => '参照ドキュメントを作成するには以下のステップが必要です',
    'key' => '0c7dfb9cc4efd4248af336858662ee81',
  ),
  'fce6cd4d4be681c8a8a1c2aab968de19' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'Download and install doxygen',
    'comment' => NULL,
    'translation' => 'doxygen のダウンロードとインストール',
    'key' => 'fce6cd4d4be681c8a8a1c2aab968de19',
  ),
  'd6a0dc64343db85d4ed343011f02321f' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'Generate the documentation by running the following command',
    'comment' => NULL,
    'translation' => '以下のコマンドを実行してドキュメントを生成します',
    'key' => 'd6a0dc64343db85d4ed343011f02321f',
  ),
  '58e1cd634b5785e4ab25f9263eb6dc61' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'Download doxygen from %doxygenurl.',
    'comment' => NULL,
    'translation' => '%doxygenurl からdoxygenをダウンロードします。',
    'key' => '58e1cd634b5785e4ab25f9263eb6dc61',
  ),
  'd99941dd260c7b3ece32fd6d84134bfc' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'Main',
    'comment' => NULL,
    'translation' => 'メイン',
    'key' => 'd99941dd260c7b3ece32fd6d84134bfc',
  ),
  'd006c9b07d572e3194d6305d89d2d8d5' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'Modules',
    'comment' => NULL,
    'translation' => 'モジュール',
    'key' => 'd006c9b07d572e3194d6305d89d2d8d5',
  ),
  '9e4b0cd8ce2b4b2aaea560de369ec80b' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'Class hierarchy',
    'comment' => NULL,
    'translation' => 'クラス階層',
    'key' => '9e4b0cd8ce2b4b2aaea560de369ec80b',
  ),
  '26d299aff96db978fcb7ab7c142c28d9' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'Compound list',
    'comment' => NULL,
    'translation' => '結合リスト',
    'key' => '26d299aff96db978fcb7ab7c142c28d9',
  ),
  'd52091b1326e15da0be6cc98b9281f76' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'File list',
    'comment' => NULL,
    'translation' => 'ファイル一覧',
    'key' => 'd52091b1326e15da0be6cc98b9281f76',
  ),
  '26105f2e7b29387078ba79e60d94f3eb' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'Compound members',
    'comment' => NULL,
    'translation' => '結合の構成',
    'key' => '26105f2e7b29387078ba79e60d94f3eb',
  ),
  '231733de38717c6f4edb6c5dcb674c27' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'File members',
    'comment' => NULL,
    'translation' => 'ファイルの構成',
    'key' => '231733de38717c6f4edb6c5dcb674c27',
  ),
  '0ee6a8afe3dc0cebefcdd839e4f37cdd' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'Related pages',
    'comment' => NULL,
    'translation' => '関連するページ',
    'key' => '0ee6a8afe3dc0cebefcdd839e4f37cdd',
  ),
  'db72852ac9d3c5bacdc4f0fdb24e1573' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'Introduction',
    'comment' => NULL,
    'translation' => '紹介',
    'key' => 'db72852ac9d3c5bacdc4f0fdb24e1573',
  ),
  '684d39bbd23adbd2729efe5754771c87' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'All reference documentation has been made with %doxygenurl',
    'comment' => NULL,
    'translation' => 'すべての参照ドキュメントは %doxygenurl によって作成されています',
    'key' => '684d39bbd23adbd2729efe5754771c87',
  ),
  '0511e7361222020f0554b1f05d5472ae' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'The Reference Documentation for eZ Publish consists of multiple sections which
each have a different view on the documentation. The sections can be accessed at
menu on the top.',
    'comment' => NULL,
    'translation' => 'eZ Publishの参照ドキュメンテーションは複数のセクションから成りたっており、各セクションはドキュメンテーションの異なる分野を担っています。
トップメニューを使ってセクションに直接アクセスできます。',
    'key' => '0511e7361222020f0554b1f05d5472ae',
  ),
  '69f136ed4eb30442a1eb1b1049955289' => 
  array (
    'context' => 'design/standard/reference/ez',
    'source' => 'The documentation will give an overview of the API of eZ Publish.',
    'comment' => NULL,
    'translation' => 'ドキュメンテーションはeZ PublishのAPIの概略となります。',
    'key' => '69f136ed4eb30442a1eb1b1049955289',
  ),
);
?>
