<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/content/copy_subtree',
);

$TranslationRoot = array (
  '7ea98c450c2707bf775eea0ba8733084' => 
  array (
    'context' => 'design/standard/content/copy_subtree',
    'source' => 'Copying subtree from node %1',
    'comment' => NULL,
    'translation' => 'ノード %1 からサブツリーを複製',
    'key' => '7ea98c450c2707bf775eea0ba8733084',
  ),
  '842442f286c02d79e694d2dbee65d073' => 
  array (
    'context' => 'design/standard/content/copy_subtree',
    'source' => 'Copy all versions.',
    'comment' => NULL,
    'translation' => 'すべてのバージョンを複製',
    'key' => '842442f286c02d79e694d2dbee65d073',
  ),
  '6f5bdea70d88056c43e9f171964bfa19' => 
  array (
    'context' => 'design/standard/content/copy_subtree',
    'source' => 'Copy current version.',
    'comment' => NULL,
    'translation' => '現在のバージョンを複製',
    'key' => '6f5bdea70d88056c43e9f171964bfa19',
  ),
  '6118dbd2293cb7829a30c941c898abad' => 
  array (
    'context' => 'design/standard/content/copy_subtree',
    'source' => 'Copy',
    'comment' => NULL,
    'translation' => '複製',
    'key' => '6118dbd2293cb7829a30c941c898abad',
  ),
  '779b8140e9050fa60ed645ee20735576' => 
  array (
    'context' => 'design/standard/content/copy_subtree',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '779b8140e9050fa60ed645ee20735576',
  ),
  '522914ebfb7544d3ab642fd41974b39f' => 
  array (
    'context' => 'design/standard/content/copy_subtree',
    'source' => 'Keep creators of content objects being copied unchanged.',
    'comment' => NULL,
    'translation' => '複製されるコンテンツオブジェクトの作成者は変更しません。',
    'key' => '522914ebfb7544d3ab642fd41974b39f',
  ),
  'ad9a83271d52e530107a7a6e4243f59a' => 
  array (
    'context' => 'design/standard/content/copy_subtree',
    'source' => 'Set new creator for content objects being copied.',
    'comment' => NULL,
    'translation' => '複製されるコンテンツオブジェクトに新しい作成者を設定します。',
    'key' => 'ad9a83271d52e530107a7a6e4243f59a',
  ),
  'fe6a50ebb856e18d4d4518b52431f842' => 
  array (
    'context' => 'design/standard/content/copy_subtree',
    'source' => 'Keep time of creation and modification of content objects being copied unchanged.',
    'comment' => NULL,
    'translation' => '複製されたコンテンツオブジェクトの作成日時と更新日時は変更しません。',
    'key' => 'fe6a50ebb856e18d4d4518b52431f842',
  ),
  '9ac4ddf9c90c85edf3c2551aad0f0e1d' => 
  array (
    'context' => 'design/standard/content/copy_subtree',
    'source' => 'Copy and publish content objects at current time.',
    'comment' => NULL,
    'translation' => 'コンテンツを現在の時刻で複製し公開します。',
    'key' => '9ac4ddf9c90c85edf3c2551aad0f0e1d',
  ),
);
?>
