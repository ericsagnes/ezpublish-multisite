<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/workflow/workflowlist',
);

$TranslationRoot = array (
  '0d1b4c3df557baf467bf4c7bdc45db9d' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => '%group_name [Workflow group]',
    'comment' => NULL,
    'translation' => '%group_name [ワークフローグループ]',
    'key' => '0d1b4c3df557baf467bf4c7bdc45db9d',
  ),
  '5c0e137741542826c1c909421d817202' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'ID',
    'comment' => NULL,
    'translation' => 'ID',
    'key' => '5c0e137741542826c1c909421d817202',
  ),
  'd52e228b30e3175618b78e585c9b931f' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => 'd52e228b30e3175618b78e585c9b931f',
  ),
  '51f85307fa14c6d3c3d64977e5ab2170' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => '51f85307fa14c6d3c3d64977e5ab2170',
  ),
  '4d74575147659c0cd8a55c0a42ba46df' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => '4d74575147659c0cd8a55c0a42ba46df',
  ),
  '861a1b6386717795661166855df31e2b' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Workflows [%workflow_count]',
    'comment' => NULL,
    'translation' => 'ワークフロー数  [%workflow_count]',
    'key' => '861a1b6386717795661166855df31e2b',
  ),
  'c4e3b518343dd1000cf419e026c12d0c' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Modifier',
    'comment' => NULL,
    'translation' => '修正者',
    'key' => 'c4e3b518343dd1000cf419e026c12d0c',
  ),
  '099cba901af2d7b27559dcaa48841f13' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '099cba901af2d7b27559dcaa48841f13',
  ),
  '91a3556c7424dd9313b2c70e26cbc5fb' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'There are no workflows in this group.',
    'comment' => NULL,
    'translation' => 'グループにワークフローはありません',
    'key' => '91a3556c7424dd9313b2c70e26cbc5fb',
  ),
  '9f5344f6d51fec17d0eb5ae2b6f5b69d' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => '9f5344f6d51fec17d0eb5ae2b6f5b69d',
  ),
  '705391a28a3f291953d5d5ea8500fa7d' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'New workflow',
    'comment' => NULL,
    'translation' => '新規ワークフロー',
    'key' => '705391a28a3f291953d5d5ea8500fa7d',
  ),
  '5b2a310d29dfbe7aef1d65d6648d3d66' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Edit this workflow group.',
    'comment' => NULL,
    'translation' => 'ワークフローグループの編集',
    'key' => '5b2a310d29dfbe7aef1d65d6648d3d66',
  ),
  'ddc6242962dba23c1dc08120cf1c6cb4' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Remove this workflow group.',
    'comment' => NULL,
    'translation' => 'ワークフローグループの削除',
    'key' => 'ddc6242962dba23c1dc08120cf1c6cb4',
  ),
  '14b0e2a761b9ef61803dbba69819e343' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Back to workflow groups.',
    'comment' => NULL,
    'translation' => 'ワークフローグループに戻る',
    'key' => '14b0e2a761b9ef61803dbba69819e343',
  ),
  '9b593cfecde2985c5a270b111b4d52b2' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Invert selection.',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => '9b593cfecde2985c5a270b111b4d52b2',
  ),
  '32b122c4460442ec7a598e44834d42f6' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Select workflow for removal.',
    'comment' => NULL,
    'translation' => '削除するワークフローの選択',
    'key' => '32b122c4460442ec7a598e44834d42f6',
  ),
  'e803896613985be8178d81c7947b9785' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Edit the <%workflow_name> workflow.',
    'comment' => NULL,
    'translation' => '<%workflow_name> ワークフローの編集',
    'key' => 'e803896613985be8178d81c7947b9785',
  ),
  '5a56afd3e1b728908d83979bdcc63935' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Remove selected workflows.',
    'comment' => NULL,
    'translation' => '選択したワークフローの削除',
    'key' => '5a56afd3e1b728908d83979bdcc63935',
  ),
  'af2bd31fd57be723df3101797b7ea1a6' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Create a new workflow.',
    'comment' => NULL,
    'translation' => '新規ワークフローの作成',
    'key' => 'af2bd31fd57be723df3101797b7ea1a6',
  ),
  'ee9662e6cda4901bbf6dc9fc61b58e4c' => 
  array (
    'context' => 'design/admin/workflow/workflowlist',
    'source' => 'Workflows (%workflow_count)',
    'comment' => NULL,
    'translation' => 'ワークフロー数  (%workflow_count)',
    'key' => 'ee9662e6cda4901bbf6dc9fc61b58e4c',
  ),
);
?>
