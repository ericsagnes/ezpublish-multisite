<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/content/diff',
);

$TranslationRoot = array (
  '2ffdd91fe99172d764374ac6508dd0c0' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Versions for <%object_name> [%version_count]',
    'comment' => NULL,
    'translation' => '<%object_name> [%version_count] 
のバージョン',
    'key' => '2ffdd91fe99172d764374ac6508dd0c0',
  ),
  'dda4f9806c6c35c76f7826fa4532ce30' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => 'dda4f9806c6c35c76f7826fa4532ce30',
  ),
  'ef27178a011358cf999cdfdd79dbd909' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Status',
    'comment' => NULL,
    'translation' => 'ステータス',
    'key' => 'ef27178a011358cf999cdfdd79dbd909',
  ),
  '840ec9e13c38355829b28f6bb7e0d016' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Translations',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => '840ec9e13c38355829b28f6bb7e0d016',
  ),
  '662a423c077168700973b69337ef46bd' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Creator',
    'comment' => NULL,
    'translation' => '作成者',
    'key' => '662a423c077168700973b69337ef46bd',
  ),
  '5ebfead06a5d6c6929bd486b79ab49a8' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '5ebfead06a5d6c6929bd486b79ab49a8',
  ),
  '51606f97b3cf94f416424dd584aa77bf' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Draft',
    'comment' => NULL,
    'translation' => 'ドラフト',
    'key' => '51606f97b3cf94f416424dd584aa77bf',
  ),
  'dfb104a825ba4a9ce77e09c4b8973f1d' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Published',
    'comment' => NULL,
    'translation' => '公開中',
    'key' => 'dfb104a825ba4a9ce77e09c4b8973f1d',
  ),
  '96a6957f302ae17aa6f034e13867be51' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Pending',
    'comment' => NULL,
    'translation' => '保留中',
    'key' => '96a6957f302ae17aa6f034e13867be51',
  ),
  '6b23907de77bb53ef3bb496a14d47a85' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Archived',
    'comment' => NULL,
    'translation' => '保管',
    'key' => '6b23907de77bb53ef3bb496a14d47a85',
  ),
  'a4305b90a704d1b2ca75a66c671b977a' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Rejected',
    'comment' => NULL,
    'translation' => '非承認',
    'key' => 'a4305b90a704d1b2ca75a66c671b977a',
  ),
  'f401ffae98e00384635ebfd6f3191d7f' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Untouched draft',
    'comment' => NULL,
    'translation' => '未変更ドラフト',
    'key' => 'f401ffae98e00384635ebfd6f3191d7f',
  ),
  'a2512b7a5385c6619f4f7509dcda90db' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'This object does not have any versions.',
    'comment' => NULL,
    'translation' => 'このオブジェクトにはバージョンが存在しません。',
    'key' => 'a2512b7a5385c6619f4f7509dcda90db',
  ),
  '5df1845a3771df6f71bdd5f658b3f15e' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Show differences',
    'comment' => NULL,
    'translation' => '違いを表示',
    'key' => '5df1845a3771df6f71bdd5f658b3f15e',
  ),
  '6d1971c02c978b0fb64b17a15ec12e42' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Differences between versions %oldVersion and %newVersion',
    'comment' => NULL,
    'translation' => '%oldVersion と %newVersionの違い',
    'key' => '6d1971c02c978b0fb64b17a15ec12e42',
  ),
  'ecc9b24721b69f1be36b9191947b26b7' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Old version',
    'comment' => NULL,
    'translation' => '旧バージョン',
    'key' => 'ecc9b24721b69f1be36b9191947b26b7',
  ),
  '1fb5e60c9d2b4adf33963f2bf363c9d8' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Inline changes',
    'comment' => NULL,
    'translation' => 'インライン変更',
    'key' => '1fb5e60c9d2b4adf33963f2bf363c9d8',
  ),
  '2a59bfa6c120efca2a667d4f9e43c284' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'Block changes',
    'comment' => NULL,
    'translation' => 'ブロック変更',
    'key' => '2a59bfa6c120efca2a667d4f9e43c284',
  ),
  '3e552dfd50c088748e397e8d28444a49' => 
  array (
    'context' => 'design/ezwebin/content/diff',
    'source' => 'New version',
    'comment' => NULL,
    'translation' => '新バージョン',
    'key' => '3e552dfd50c088748e397e8d28444a49',
  ),
);
?>
