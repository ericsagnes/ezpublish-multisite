<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/edit',
);

$TranslationRoot = array (
  'd5c6e07de8b9dccaac1d0452dee0b581' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Edit <%object_name> [%class_name]',
    'comment' => NULL,
    'translation' => '<%object_name> [%class_name] の編集',
    'key' => 'd5c6e07de8b9dccaac1d0452dee0b581',
  ),
  'e811c8a9685fbfaf39c964a635600fd5' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Translating content from %from_lang to %to_lang',
    'comment' => NULL,
    'translation' => '%from_lang から %to_lang へ翻訳',
    'key' => 'e811c8a9685fbfaf39c964a635600fd5',
  ),
  '11fb129ce59c549f147420ee3451ac24' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Send for publishing',
    'comment' => NULL,
    'translation' => '送信して公開',
    'key' => '11fb129ce59c549f147420ee3451ac24',
  ),
  '8c5749194e3baf11dddd5c34b1811596' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Store draft',
    'comment' => NULL,
    'translation' => '下書きを保存 ',
    'key' => '8c5749194e3baf11dddd5c34b1811596',
  ),
  'd506eed04b8a1daa90d9dbe145445605' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Store the contents of the draft that is being edited and continue editing. Use this button to periodically save your work while editing.',
    'comment' => NULL,
    'translation' => '編集を下書きとして保存（編集は継続）。 このボタンは、編集中に定期的に保存するのに使用します。',
    'key' => 'd506eed04b8a1daa90d9dbe145445605',
  ),
  'd7ebb04b02e47d0bd52637484b18f14e' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Discard draft',
    'comment' => NULL,
    'translation' => '編集の破棄',
    'key' => 'd7ebb04b02e47d0bd52637484b18f14e',
  ),
  '8ff4bdf4365cea79d60c64729aa5a3d7' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Published',
    'comment' => NULL,
    'translation' => '公開日時',
    'key' => '8ff4bdf4365cea79d60c64729aa5a3d7',
  ),
  'd57eeb192509b0a21c9fb559e0875ee4' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => 'd57eeb192509b0a21c9fb559e0875ee4',
  ),
  'b5f9dcbfc8129dbad5a5a46430976cab' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Section',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => 'b5f9dcbfc8129dbad5a5a46430976cab',
  ),
  'a8122643230a05433801fbfdc325b0ac' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Depth',
    'comment' => NULL,
    'translation' => '階層',
    'key' => 'a8122643230a05433801fbfdc325b0ac',
  ),
  'de3a2815973e43df00aee9ea89c2e9ee' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => 'de3a2815973e43df00aee9ea89c2e9ee',
  ),
  '0c449515a963c751510ea4a1c5ad5a16' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Priority',
    'comment' => NULL,
    'translation' => '優先度',
    'key' => '0c449515a963c751510ea4a1c5ad5a16',
  ),
  '67bc86a3e97a3a3d92ea3beb65dd6e87' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Locations [%locations]',
    'comment' => NULL,
    'translation' => '配置先 [%locations]',
    'key' => '67bc86a3e97a3a3d92ea3beb65dd6e87',
  ),
  'ab439be2c1960ef2864aba2706f3da54' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Invert selection.',
    'comment' => NULL,
    'translation' => '選択の反転',
    'key' => 'ab439be2c1960ef2864aba2706f3da54',
  ),
  '49e8f0ddafe626663288d81e2fb49ca0' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Location',
    'comment' => NULL,
    'translation' => '配置先',
    'key' => '49e8f0ddafe626663288d81e2fb49ca0',
  ),
  'eef9b86d0bc32e55d39032df03d414f7' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Sub items',
    'comment' => NULL,
    'translation' => '子アイテム数',
    'key' => 'eef9b86d0bc32e55d39032df03d414f7',
  ),
  'cc6dc33d8904d3c3b934998b16c5f64b' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Sorting of sub items',
    'comment' => NULL,
    'translation' => '子アイテムのソート指定',
    'key' => 'cc6dc33d8904d3c3b934998b16c5f64b',
  ),
  '2f0a11483a590a1f6268a02200198343' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Current visibility',
    'comment' => NULL,
    'translation' => '現在の表示設定',
    'key' => '2f0a11483a590a1f6268a02200198343',
  ),
  '14d015346e3dcd669f6b7289e556c379' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Visibility after publishing',
    'comment' => NULL,
    'translation' => '公開後の表示設定',
    'key' => '14d015346e3dcd669f6b7289e556c379',
  ),
  'f2d0aed249fd63a9ad7750f5e3b05826' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Main',
    'comment' => NULL,
    'translation' => '主要ノード',
    'key' => 'f2d0aed249fd63a9ad7750f5e3b05826',
  ),
  'f0f5443a120379e97eaf90b672707d60' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Select location for removal.',
    'comment' => NULL,
    'translation' => '削除する配置先を選択',
    'key' => 'f0f5443a120379e97eaf90b672707d60',
  ),
  '0de946921daa4f766d6cb8b899f50c1f' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Desc.',
    'comment' => NULL,
    'translation' => '降順',
    'key' => '0de946921daa4f766d6cb8b899f50c1f',
  ),
  '876544ffd52dfaa722d262e22b1ccb50' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Asc.',
    'comment' => NULL,
    'translation' => '昇順',
    'key' => '876544ffd52dfaa722d262e22b1ccb50',
  ),
  '783d1958da4861ba7dc9f1d396513661' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Hidden by parent',
    'comment' => NULL,
    'translation' => '親アイテムによる非表示',
    'key' => '783d1958da4861ba7dc9f1d396513661',
  ),
  'a7e239075d454945331613656140d9e4' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Visible',
    'comment' => NULL,
    'translation' => '表示',
    'key' => 'a7e239075d454945331613656140d9e4',
  ),
  '275526eb7c8f1a68977c1239e1905d67' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Unchanged',
    'comment' => NULL,
    'translation' => '変更なし',
    'key' => '275526eb7c8f1a68977c1239e1905d67',
  ),
  '856d33ca67120cf3e125b3e674792f2d' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Hidden',
    'comment' => NULL,
    'translation' => '非表示',
    'key' => '856d33ca67120cf3e125b3e674792f2d',
  ),
  '9b78cdb793ba06dd24c1cb659fd8f18e' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Use these radio buttons to specify the main location (main node) for the object being edited.',
    'comment' => NULL,
    'translation' => '編集中のオブジェクトの配置先（主要ノード）をラジオボタンで設定して下さい。',
    'key' => '9b78cdb793ba06dd24c1cb659fd8f18e',
  ),
  '026f2a715a276e9d8d737446b9da0018' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Move to another location.',
    'comment' => NULL,
    'translation' => '配置先を移動',
    'key' => '026f2a715a276e9d8d737446b9da0018',
  ),
  '30e0f7f35119baaa4d4454fbe2913660' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => '30e0f7f35119baaa4d4454fbe2913660',
  ),
  '3bd76c0ad50229499785fd214b38a423' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Remove selected locations.',
    'comment' => NULL,
    'translation' => '選択した配置先を削除',
    'key' => '3bd76c0ad50229499785fd214b38a423',
  ),
  '91f558d19141ff4355d842ee41371638' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Add locations',
    'comment' => NULL,
    'translation' => '配置先の追加',
    'key' => '91f558d19141ff4355d842ee41371638',
  ),
  'd1348b630823ba147fee6a3080b93e6a' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Add one or more locations for the object being edited.',
    'comment' => NULL,
    'translation' => '編集中のオブジェクトの配置先の追加',
    'key' => 'd1348b630823ba147fee6a3080b93e6a',
  ),
  '24684de48a0e5f5cd59453cfaecf3fe2' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Object information',
    'comment' => NULL,
    'translation' => 'オブジェクトプロパティ',
    'key' => '24684de48a0e5f5cd59453cfaecf3fe2',
  ),
  '7ef3f2da93bc270f92af697bf09d2478' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'ID',
    'comment' => NULL,
    'translation' => 'ID',
    'key' => '7ef3f2da93bc270f92af697bf09d2478',
  ),
  'db6996ee096133dd3bf27a66d6c6a245' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Created',
    'comment' => NULL,
    'translation' => '作成日時',
    'key' => 'db6996ee096133dd3bf27a66d6c6a245',
  ),
  '3504c716aee9e860aea283ab634315f5' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Not yet published',
    'comment' => NULL,
    'translation' => '未公開',
    'key' => '3504c716aee9e860aea283ab634315f5',
  ),
  '8a20002c7443ef8d46e8fd516830827a' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Published version',
    'comment' => NULL,
    'translation' => '公開バージョン',
    'key' => '8a20002c7443ef8d46e8fd516830827a',
  ),
  '3b445a4b6127869d991f6b9f21540def' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Manage versions',
    'comment' => NULL,
    'translation' => 'バージョン管理',
    'key' => '3b445a4b6127869d991f6b9f21540def',
  ),
  '459828e3bbf9aca1b85a34598762c68f' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'View and manage (copy, delete, etc.) the versions of this object.',
    'comment' => NULL,
    'translation' => 'このオブジェクトのバージョン管理（コピー, 削除, その他）と参照',
    'key' => '459828e3bbf9aca1b85a34598762c68f',
  ),
  '337ce19fed6e1043de7d521d5f20a58c' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Current draft',
    'comment' => NULL,
    'translation' => '現在の下書き',
    'key' => '337ce19fed6e1043de7d521d5f20a58c',
  ),
  '3bc586e5f657cf80e47166d0049f6c75' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => '3bc586e5f657cf80e47166d0049f6c75',
  ),
  '34ea08d6e0326ca7a804a034afd76eba' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'View',
    'comment' => NULL,
    'translation' => 'プレビュー',
    'key' => '34ea08d6e0326ca7a804a034afd76eba',
  ),
  'e1668f1e0ee902c1ec6ce6b87fca974e' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Preview the draft that is being edited.',
    'comment' => NULL,
    'translation' => '編集中の下書きのプレビューを見る',
    'key' => 'e1668f1e0ee902c1ec6ce6b87fca974e',
  ),
  '9cfee90b36ba6a94a051395e41071d64' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Store and exit',
    'comment' => NULL,
    'translation' => '保存して編集終了',
    'key' => '9cfee90b36ba6a94a051395e41071d64',
  ),
  '1b7dc1e4c2c6fb9afa6ba1d0d2cfe22e' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Store the draft that is being edited and exit from edit mode.',
    'comment' => NULL,
    'translation' => '編集中の下書きを保存し、編集を終了',
    'key' => '1b7dc1e4c2c6fb9afa6ba1d0d2cfe22e',
  ),
  'deff3631a2a6a7aefe62c90e6f531393' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Translate',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => 'deff3631a2a6a7aefe62c90e6f531393',
  ),
  '15261022404c70f9d80d19670046617c' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Related objects [%related_objects]',
    'comment' => NULL,
    'translation' => '関連付けしたオブジェクト [%related_objects]',
    'key' => '15261022404c70f9d80d19670046617c',
  ),
  'bdc9407d88adf5b51f71300747bc65b4' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Related images [%related_images]',
    'comment' => NULL,
    'translation' => '関連付けした画像 [%related_images]',
    'key' => 'bdc9407d88adf5b51f71300747bc65b4',
  ),
  '49e4893c354086bd227f26833da6fae3' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Related files [%related_files]',
    'comment' => NULL,
    'translation' => '関連付けしたファイル [%related_files]',
    'key' => '49e4893c354086bd227f26833da6fae3',
  ),
  'fb0e2e8db005e5abbfc19f74d4d2ff59' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'File type',
    'comment' => NULL,
    'translation' => 'ファイル形式',
    'key' => 'fb0e2e8db005e5abbfc19f74d4d2ff59',
  ),
  'cbe610719066288bb0f8f16e66eb60bf' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Size',
    'comment' => NULL,
    'translation' => 'サイズ',
    'key' => 'cbe610719066288bb0f8f16e66eb60bf',
  ),
  '7629938cdf31fb430956705c4f25915c' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'XML code',
    'comment' => NULL,
    'translation' => 'XMLコード',
    'key' => '7629938cdf31fb430956705c4f25915c',
  ),
  '2e759ee05aa1ad12add670edbc15d2c2' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Related content [%related_objects]',
    'comment' => NULL,
    'translation' => '関連付けしたコンテンツ [%related_objects]',
    'key' => '2e759ee05aa1ad12add670edbc15d2c2',
  ),
  '42720949af15fd05c21a1220ce7dcaed' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Type',
    'comment' => NULL,
    'translation' => '形式',
    'key' => '42720949af15fd05c21a1220ce7dcaed',
  ),
  'a71c6cd033f06f84a746efa15bec425f' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'There are no objects related to the one that is currently being edited.',
    'comment' => NULL,
    'translation' => '編集中のアイテムに関連付けされたオブジェクトはありません',
    'key' => 'a71c6cd033f06f84a746efa15bec425f',
  ),
  '7c9c383b8a6efc73a57865ea173956f4' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Remove the selected items from the list(s) above. It is only the relations that will be removed. The items will not be deleted.',
    'comment' => NULL,
    'translation' => '選択したアイテムをリストから削除します。 関連づけのみが削除され、アイテム自体は残ります。',
    'key' => '7c9c383b8a6efc73a57865ea173956f4',
  ),
  'e4bc8d93d24d7f58fcd273d10a85a787' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Add existing',
    'comment' => NULL,
    'translation' => '既存アイテムを追加',
    'key' => 'e4bc8d93d24d7f58fcd273d10a85a787',
  ),
  '42bfc9071828350c26ed3c83b752a191' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Add an existing item as a related object.',
    'comment' => NULL,
    'translation' => '既存アイテムの関連付けを行います。',
    'key' => '42bfc9071828350c26ed3c83b752a191',
  ),
  'ba94e70cabd1cb7edd98f029ed9e6219' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Upload new',
    'comment' => NULL,
    'translation' => '新規アップロード',
    'key' => 'ba94e70cabd1cb7edd98f029ed9e6219',
  ),
  '5c9aa913f95d407fbb752e4cb03c31d5' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Upload a file and add it as a related object.',
    'comment' => NULL,
    'translation' => 'ファイルをアップロードして関連付けを行います。',
    'key' => '5c9aa913f95d407fbb752e4cb03c31d5',
  ),
  'c32705fc7ed02108c0cc8637bfb014a3' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'The draft could not be stored.',
    'comment' => NULL,
    'translation' => '下書きを保存できません。',
    'key' => 'c32705fc7ed02108c0cc8637bfb014a3',
  ),
  '0107a013d1906cdccde3185dac816952' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Required data is either missing or is invalid',
    'comment' => NULL,
    'translation' => '入力必須項目が未入力または不正です',
    'key' => '0107a013d1906cdccde3185dac816952',
  ),
  '4636bf4dbd364e83747d95d2e5291f09' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'The following locations are invalid',
    'comment' => NULL,
    'translation' => '以下の配置は不正です',
    'key' => '4636bf4dbd364e83747d95d2e5291f09',
  ),
  'a4e029b88453feb1b89b03e5f94d7417' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'The draft was only partially stored.',
    'comment' => NULL,
    'translation' => '下書きは一部のみ保存されました',
    'key' => 'a4e029b88453feb1b89b03e5f94d7417',
  ),
  '5547c5c42894c60e4f4e423b62a29ea1' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'The draft was successfully stored.',
    'comment' => NULL,
    'translation' => '下書きの保存に成功しました',
    'key' => '5547c5c42894c60e4f4e423b62a29ea1',
  ),
  '978390e9e54157ef85305dbab13222b8' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Are you sure you want to discard the draft?',
    'comment' => NULL,
    'translation' => '下書きを破棄してもよいですか?',
    'key' => '978390e9e54157ef85305dbab13222b8',
  ),
  'b2151a3ff3097b7918ffb0bf77ddd3d9' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Discard the draft that is being edited. This will also remove the translations that belong to the draft (if any).',
    'comment' => NULL,
    'translation' => '編集中の下書きを破棄（付属する翻訳も削除します）します。',
    'key' => 'b2151a3ff3097b7918ffb0bf77ddd3d9',
  ),
  '64b2382b215c4fa9830f9a21fb2c4ca8' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Translate from',
    'comment' => NULL,
    'translation' => '以下からの翻訳 ',
    'key' => '64b2382b215c4fa9830f9a21fb2c4ca8',
  ),
  '7410909128e3c35ccf9d14cceb80820e' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'No translation',
    'comment' => NULL,
    'translation' => '翻訳なし',
    'key' => '7410909128e3c35ccf9d14cceb80820e',
  ),
  'a535d0e0c96877445a89aef4ee173dc4' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Edit the current object showing the selected language as a reference.',
    'comment' => NULL,
    'translation' => '選択した言語を参照にしながら、現在表示中のオブジェクトを編集。',
    'key' => 'a535d0e0c96877445a89aef4ee173dc4',
  ),
  'd692cfde1a62954707c8672718099413' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Publish data',
    'comment' => NULL,
    'translation' => 'データの公開',
    'key' => 'd692cfde1a62954707c8672718099413',
  ),
  'eb0ae2aa2dc3d1e21c38bb5c4216db5d' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Back to edit',
    'comment' => NULL,
    'translation' => '編集へ戻る',
    'key' => 'eb0ae2aa2dc3d1e21c38bb5c4216db5d',
  ),
  'ec3eb846d3b45c2b95ac208645003f88' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Relation type',
    'comment' => NULL,
    'translation' => '関連タイプ',
    'key' => 'ec3eb846d3b45c2b95ac208645003f88',
  ),
  '54a0d205353a46d3d3a0e72dcb9de665' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Common',
    'comment' => NULL,
    'translation' => 'Common',
    'key' => '54a0d205353a46d3d3a0e72dcb9de665',
  ),
  '58e308c58d795fe23ce95b39e31631b0' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Embedded',
    'comment' => NULL,
    'translation' => 'Embedded',
    'key' => '58e308c58d795fe23ce95b39e31631b0',
  ),
  '3e931f75750447a56ff75badd13e66d5' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Linked',
    'comment' => NULL,
    'translation' => 'Linked',
    'key' => '3e931f75750447a56ff75badd13e66d5',
  ),
  '6f1be5a3016b77c751f014cba5abf62b' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Attribute',
    'comment' => NULL,
    'translation' => '属性',
    'key' => '6f1be5a3016b77c751f014cba5abf62b',
  ),
  '630cf7951abe12d79a7d8e68e4e59b8c' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Copy this code and paste it into an XML field to embed the object.',
    'comment' => NULL,
    'translation' => 'コードをコピーし、XMLフィールドへペーストする。',
    'key' => '630cf7951abe12d79a7d8e68e4e59b8c',
  ),
  '88fc57c68a451c3ae68461cd1be0ac09' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Copy this code and paste it into an XML field to link the object.',
    'comment' => NULL,
    'translation' => 'コードをコピーし、XMLフィールドへペーストする。',
    'key' => '88fc57c68a451c3ae68461cd1be0ac09',
  ),
  '2e6bc1eade5bc87d907d552c1f2850c2' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Top node',
    'comment' => NULL,
    'translation' => 'トップノード',
    'key' => '2e6bc1eade5bc87d907d552c1f2850c2',
  ),
  'e21865e61399551e932cfa56e2f037c3' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Publish the contents of the draft that is being edited. The draft will become the published version of the object.',
    'comment' => NULL,
    'translation' => '編集中の下書きを公開する。この下書きは公開オブジェクトになります。',
    'key' => 'e21865e61399551e932cfa56e2f037c3',
  ),
  'a0ff5452f01b802f02e9589528383468' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Class identifier',
    'comment' => NULL,
    'translation' => 'クラス識別子',
    'key' => 'a0ff5452f01b802f02e9589528383468',
  ),
  'a03bc27f29960a05a2082afa54bd767f' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Class name',
    'comment' => NULL,
    'translation' => 'クラス名',
    'key' => 'a03bc27f29960a05a2082afa54bd767f',
  ),
  '548d9e60b02ca3fea8f9eca6e5ea270b' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'This location will remain unchanged when the object is published.',
    'comment' => NULL,
    'translation' => 'オブジェクトを公開してもこの配置先は変更されません。',
    'key' => '548d9e60b02ca3fea8f9eca6e5ea270b',
  ),
  'b84aa5ba3b4a29912b256550b85372d2' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'This location will be created when the object is published.',
    'comment' => NULL,
    'translation' => 'オブジェクトを公開するとこの配置先が作成されます。',
    'key' => 'b84aa5ba3b4a29912b256550b85372d2',
  ),
  '8dfc007c6f91ff521eb15a6ae1cfda44' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'This location will be moved when the object is published.',
    'comment' => NULL,
    'translation' => 'オブジェクトを公開するとこの配置先は移動されます。',
    'key' => '8dfc007c6f91ff521eb15a6ae1cfda44',
  ),
  '99deb2fe1a6237dcc341c6a0f3dc7980' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'This location will be removed when the object is published.',
    'comment' => NULL,
    'translation' => 'オブジェクトを公開するとこの配置先は削除されます。',
    'key' => '99deb2fe1a6237dcc341c6a0f3dc7980',
  ),
  '1559f1473d1db10faf6251edb2bda77e' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'You do not have permission to remove this location.',
    'comment' => NULL,
    'translation' => 'この配置先の削除権限を持っていません。',
    'key' => '1559f1473d1db10faf6251edb2bda77e',
  ),
  'fdd63332e502d8369d3cf5e937afc089' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Use this menu to set the sorting method for the sub items in this location.',
    'comment' => NULL,
    'translation' => 'このメニューを使って、現在の配置先の子アイテムのソート方法を設定して下さい。',
    'key' => 'fdd63332e502d8369d3cf5e937afc089',
  ),
  '6deedda369647ff46f481b752d545896' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Use this menu to set the sorting direction for the sub items in this location.',
    'comment' => NULL,
    'translation' => 'このメニューを使って、現在の配置先の子アイテムのソート方法を設定して下さい。',
    'key' => '6deedda369647ff46f481b752d545896',
  ),
  '657e97cded3ee381946c164372ca6986' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'You cannot add or remove locations because the object being edited belongs to a top node.',
    'comment' => NULL,
    'translation' => 'トップノードに属しているため、配置先の追加及び削除はできません。',
    'key' => '657e97cded3ee381946c164372ca6986',
  ),
  'e18abee452b4714bda51c8f4e41135ea' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'You cannot manage the versions of this object because there is only one version available (the one that is being edited).',
    'comment' => NULL,
    'translation' => 'バージョンが一つしかないため（現在編集中のバージョン）、このオブジェクトのバージョンを管理することはできません。',
    'key' => 'e18abee452b4714bda51c8f4e41135ea',
  ),
  'cc637428ff7420fa16299a1534569ba3' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'You do not have permission to view this object',
    'comment' => NULL,
    'translation' => 'このオブジェクトを閲覧する権限をを持っていません。',
    'key' => 'cc637428ff7420fa16299a1534569ba3',
  ),
  'eb8bc6ee1e297b27503af0207afddca1' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'States',
    'comment' => NULL,
    'translation' => 'ステート',
    'key' => 'eb8bc6ee1e297b27503af0207afddca1',
  ),
  '6cd07f3dd6f612f4a629660e3f17f225' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'The following data is invalid according to the custom validation rules',
    'comment' => NULL,
    'translation' => '以下のデータはカスタムルールに従っておらず、不正です。',
    'key' => '6cd07f3dd6f612f4a629660e3f17f225',
  ),
  'c134eae4296615056100774aba5c737c' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Toggle fullscreen editing!',
    'comment' => NULL,
    'translation' => 'フールスクリーン編集を切り替える!',
    'key' => 'c134eae4296615056100774aba5c737c',
  ),
  '2b21fdd22aac1dc9d52badd4daa84cc8' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Store draft and exit',
    'comment' => NULL,
    'translation' => 'ドラフトを保存して、編集モードを終了します',
    'key' => '2b21fdd22aac1dc9d52badd4daa84cc8',
  ),
  'c19fe6950ef73d4a5aac038c6c2f3e1b' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Store the draft that is being edited and exit from edit mode. Use when you need to exit your work and return later to continue.',
    'comment' => NULL,
    'translation' => '下書きを保存して、編集モードを終了します。作業を一旦終わらせて、後から続く場合に選択してください。',
    'key' => 'c19fe6950ef73d4a5aac038c6c2f3e1b',
  ),
  '820c3bacd630fd295899e660ba22918c' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Edit <%object_name> (%class_name)',
    'comment' => NULL,
    'translation' => '<%object_name> (%class_name) の編集',
    'key' => '820c3bacd630fd295899e660ba22918c',
  ),
  '8950a2381757b6dd0db5091483661bb0' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Locations (%locations)',
    'comment' => NULL,
    'translation' => '配置先 (%locations)',
    'key' => '8950a2381757b6dd0db5091483661bb0',
  ),
  '3965117f5e3158b59fe549e6cdba2c9a' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Preview',
    'comment' => NULL,
    'translation' => 'プレビュー',
    'key' => '3965117f5e3158b59fe549e6cdba2c9a',
  ),
  '6042d6c6f7e4f2e7e22e7c65162514ae' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Existing translations',
    'comment' => NULL,
    'translation' => '存在する翻訳',
    'key' => '6042d6c6f7e4f2e7e22e7c65162514ae',
  ),
  '6763edccdd45769450ffd1434299bf10' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Base translation on',
    'comment' => NULL,
    'translation' => '翻訳のベースになる言語',
    'key' => '6763edccdd45769450ffd1434299bf10',
  ),
  '21d0fdacb0a7736484e419ed81e22956' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'None',
    'comment' => NULL,
    'translation' => 'なし',
    'key' => '21d0fdacb0a7736484e419ed81e22956',
  ),
  '06304ec517378373e9de1c95fb429df1' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Related objects (%related_objects)',
    'comment' => NULL,
    'translation' => '関連付けしたオブジェクト (%related_objects)',
    'key' => '06304ec517378373e9de1c95fb429df1',
  ),
  'b104c545ae07332644ae2d20a9f1384b' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Related images (%related_images)',
    'comment' => NULL,
    'translation' => '関連付けした画像 (%related_images)',
    'key' => 'b104c545ae07332644ae2d20a9f1384b',
  ),
  '3d94b5daec0e506ade7e107b6b73d978' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Related files (%related_files)',
    'comment' => NULL,
    'translation' => '関連付けしたファイル (%related_files)',
    'key' => '3d94b5daec0e506ade7e107b6b73d978',
  ),
  '8adb2a84570bcb0cc99ec5ae9d73bf95' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Related content (%related_objects)',
    'comment' => NULL,
    'translation' => '関連付けしたコンテンツ (%related_objects)',
    'key' => '8adb2a84570bcb0cc99ec5ae9d73bf95',
  ),
  '58607b576d3d0e5e08333fccc24d840c' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'View the draft that is being edited.',
    'comment' => NULL,
    'translation' => '編集されているドラフトを確認する。',
    'key' => '58607b576d3d0e5e08333fccc24d840c',
  ),
  '21cf1f883c2e361d5810fa6bc0eac58e' => 
  array (
    'context' => 'design/admin/content/edit',
    'source' => 'Path String',
    'comment' => NULL,
    'translation' => 'パスストリング',
    'key' => '21cf1f883c2e361d5810fa6bc0eac58e',
  ),
);
?>
