<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/section/assign_notification',
);

$TranslationRoot = array (
  '2909d85a5498a6616685c96a52ee68a5' => 
  array (
    'context' => 'design/admin/section/assign_notification',
    'source' => 'Assign section notification',
    'comment' => NULL,
    'translation' => 'セクション通知を割り当て',
    'key' => '2909d85a5498a6616685c96a52ee68a5',
  ),
  '14b6422136b23e6ffd8f00031f1b8d4d' => 
  array (
    'context' => 'design/admin/section/assign_notification',
    'source' => 'The section < %1 > was not assigned to the nodes listed below because of insufficient permission:',
    'comment' => NULL,
    'translation' => '正しい権限を持っていないため、<%1>セクションは、下記のノードに割り当てることができませんでした:',
    'key' => '14b6422136b23e6ffd8f00031f1b8d4d',
  ),
  '070258d29e8d5a4972318817b3a15d27' => 
  array (
    'context' => 'design/admin/section/assign_notification',
    'source' => 'There are no objects in the system that you could assign the section < %1 > to.',
    'comment' => NULL,
    'translation' => 'セクション<%1>を割り当てることの出来るるオブジェクトはシステムに存在しません。',
    'key' => '070258d29e8d5a4972318817b3a15d27',
  ),
  '02f7c2d26bedc2c0c79d41625d5f0b70' => 
  array (
    'context' => 'design/admin/section/assign_notification',
    'source' => 'You do not have permission to assign the section < %1 > to any object.',
    'comment' => NULL,
    'translation' => 'セクション<%1>をオブジェクトにアサインする権限を持っていません。',
    'key' => '02f7c2d26bedc2c0c79d41625d5f0b70',
  ),
  '6048c2d4b96cba933b293e2f2c54b596' => 
  array (
    'context' => 'design/admin/section/assign_notification',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => '6048c2d4b96cba933b293e2f2c54b596',
  ),
);
?>
