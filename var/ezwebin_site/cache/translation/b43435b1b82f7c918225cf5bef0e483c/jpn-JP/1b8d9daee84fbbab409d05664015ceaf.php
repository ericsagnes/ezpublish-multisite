<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/workflow',
);

$TranslationRoot = array (
  'ef96d2f6046d9135eaf2102d55edcaed' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'You have to have at least one group that the workflow belongs to!',
    'comment' => NULL,
    'translation' => 'ワークフローは最低一つのワークグループに所属する必要があります!',
    'key' => 'ef96d2f6046d9135eaf2102d55edcaed',
  ),
  '3df0f7d60e68cb57894075bc937ef681' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'Edit workflow',
    'comment' => NULL,
    'translation' => 'ワークフローの編集',
    'key' => '3df0f7d60e68cb57894075bc937ef681',
  ),
  '27fe338984ba217e771d597b9b48d3da' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'Workflow',
    'comment' => NULL,
    'translation' => 'ワークフロー',
    'key' => '27fe338984ba217e771d597b9b48d3da',
  ),
  '260d83a1dbd814050b17aa74422e4809' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => '260d83a1dbd814050b17aa74422e4809',
  ),
  '2eaa965f8075e9acaf111809b4cbc6e5' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'Edit workflow group',
    'comment' => NULL,
    'translation' => 'ワークフローグループの編集',
    'key' => '2eaa965f8075e9acaf111809b4cbc6e5',
  ),
  '640a0d88fea7309f523181a76d85b9dd' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'Group edit',
    'comment' => NULL,
    'translation' => 'グループ編集',
    'key' => '640a0d88fea7309f523181a76d85b9dd',
  ),
  'e173bb09a0703c3627b2e0fe4d7d4c11' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'Workflow group list',
    'comment' => NULL,
    'translation' => 'ワークフローグループ一覧',
    'key' => 'e173bb09a0703c3627b2e0fe4d7d4c11',
  ),
  'c41b1a3a9d3a71595f64880223b0e2fd' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'Group list',
    'comment' => NULL,
    'translation' => 'グループ一覧',
    'key' => 'c41b1a3a9d3a71595f64880223b0e2fd',
  ),
  '7cc384270636e91566e62e6529665c65' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'Workflow list',
    'comment' => NULL,
    'translation' => 'ワークフロー一覧',
    'key' => '7cc384270636e91566e62e6529665c65',
  ),
  'e00f4172fb952026771fb1d836d02e27' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'Process list',
    'comment' => NULL,
    'translation' => 'プロセス一覧',
    'key' => 'e00f4172fb952026771fb1d836d02e27',
  ),
  'cf9bea0a94eb0f5d7a0e1f591f99d742' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'View',
    'comment' => NULL,
    'translation' => '表示',
    'key' => 'cf9bea0a94eb0f5d7a0e1f591f99d742',
  ),
  '52e2ef6d885062252187f0042b13b3c1' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'Workflow list of group',
    'comment' => NULL,
    'translation' => 'ワークグループのワークフロー一覧',
    'key' => '52e2ef6d885062252187f0042b13b3c1',
  ),
  '0d552e91bc66a8fb4882fd7f85201f41' => 
  array (
    'context' => 'kernel/workflow',
    'source' => 'List',
    'comment' => NULL,
    'translation' => '一覧',
    'key' => '0d552e91bc66a8fb4882fd7f85201f41',
  ),
);
?>
