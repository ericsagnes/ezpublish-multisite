<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/collaboration',
);

$TranslationRoot = array (
  '3d8105072b60e7e9bf84b8e8f16d312f' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => 'Group list for \'%1\'',
    'comment' => NULL,
    'translation' => '\'%1\' のグループ一覧',
    'key' => '3d8105072b60e7e9bf84b8e8f16d312f',
  ),
  'f261d14f12d909e6f4713a0984f8f36e' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => 'No items in group.',
    'comment' => NULL,
    'translation' => 'グループにアイテムはありません。',
    'key' => 'f261d14f12d909e6f4713a0984f8f36e',
  ),
  '8e7148a58315f7d587281fd96fe31533' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => 'Groups',
    'comment' => NULL,
    'translation' => 'グループ',
    'key' => '8e7148a58315f7d587281fd96fe31533',
  ),
  '5bd78507d87e2fb7d7d6c022a308f200' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => 'Approval',
    'comment' => NULL,
    'translation' => '承認',
    'key' => '5bd78507d87e2fb7d7d6c022a308f200',
  ),
  '2b93e12f610484603d21e31d2bbe6e8e' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => 'Subject',
    'comment' => NULL,
    'translation' => '件名',
    'key' => '2b93e12f610484603d21e31d2bbe6e8e',
  ),
  'a620dc73a6cf8d4ec582960cd321d967' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => 'Date',
    'comment' => NULL,
    'translation' => '日付',
    'key' => 'a620dc73a6cf8d4ec582960cd321d967',
  ),
  '19c46abd1b8529e894b7ae3c9804687b' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => 'Read',
    'comment' => NULL,
    'translation' => '既読',
    'key' => '19c46abd1b8529e894b7ae3c9804687b',
  ),
  '9178d5315cfe76381bb7472cb2a71ac0' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => 'Unread',
    'comment' => NULL,
    'translation' => '未読',
    'key' => '9178d5315cfe76381bb7472cb2a71ac0',
  ),
  '32b8b7086e0588635182511aca885046' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => 'Inactive',
    'comment' => NULL,
    'translation' => '無効',
    'key' => '32b8b7086e0588635182511aca885046',
  ),
  'd147af9f5f1e1df89675742732c86d23' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => 'Posted: %1',
    'comment' => NULL,
    'translation' => 'ポスト済み： %1',
    'key' => 'd147af9f5f1e1df89675742732c86d23',
  ),
  'd3227d844db340f47849d0e83ba9b1da' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => 'No new items to be handled.',
    'comment' => NULL,
    'translation' => '割り当てられたアイテムはありません。',
    'key' => 'd3227d844db340f47849d0e83ba9b1da',
  ),
  '638475f880a01d0faf3e77070ed80796' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => 'Summary',
    'comment' => NULL,
    'translation' => '一覧',
    'key' => '638475f880a01d0faf3e77070ed80796',
  ),
  '36918a39891dc66dc69d8f41df35d228' => 
  array (
    'context' => 'design/standard/collaboration',
    'source' => '[more]',
    'comment' => NULL,
    'translation' => '[詳細]',
    'key' => '36918a39891dc66dc69d8f41df35d228',
  ),
);
?>
