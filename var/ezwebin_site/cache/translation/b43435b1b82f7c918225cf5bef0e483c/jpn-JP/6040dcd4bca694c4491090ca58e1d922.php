<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/content/tipafriend',
);

$TranslationRoot = array (
  '939b7fe9882569b2f0597064984093df' => 
  array (
    'context' => 'design/ezwebin/content/tipafriend',
    'source' => 'Tip a friend',
    'comment' => NULL,
    'translation' => '友達に教える',
    'key' => '939b7fe9882569b2f0597064984093df',
  ),
  '106b315b2c7b4ef11a2aa3a63e390e4a' => 
  array (
    'context' => 'design/ezwebin/content/tipafriend',
    'source' => 'The message was sent.',
    'comment' => NULL,
    'translation' => 'メッセージは送信されました。',
    'key' => '106b315b2c7b4ef11a2aa3a63e390e4a',
  ),
  'fc8f7e668dca4d22a7b1eae59b38354d' => 
  array (
    'context' => 'design/ezwebin/content/tipafriend',
    'source' => 'Click here to return to the original page.',
    'comment' => NULL,
    'translation' => '元のページに戻るにはここをクリックしてください。',
    'key' => 'fc8f7e668dca4d22a7b1eae59b38354d',
  ),
  'feff365216e06744071899e0c39a81e1' => 
  array (
    'context' => 'design/ezwebin/content/tipafriend',
    'source' => 'The message was not sent.',
    'comment' => NULL,
    'translation' => 'メッセージを送信することはできませんでした。',
    'key' => 'feff365216e06744071899e0c39a81e1',
  ),
  '08b7b2a949a5bfed359f2b848b8c9e41' => 
  array (
    'context' => 'design/ezwebin/content/tipafriend',
    'source' => 'The message was not sent due to an unknown error. Please notify the site administrator about this error.',
    'comment' => NULL,
    'translation' => '不明なエラーでメッセージを送信することができませんでした。サイトの管理者に連絡してください。',
    'key' => '08b7b2a949a5bfed359f2b848b8c9e41',
  ),
  '76b16e478b61a9d726b5a23bd22e211d' => 
  array (
    'context' => 'design/ezwebin/content/tipafriend',
    'source' => 'Please correct the following errors:',
    'comment' => NULL,
    'translation' => '次のエラーを直してください:',
    'key' => '76b16e478b61a9d726b5a23bd22e211d',
  ),
  '85880f746d2e3e035654d365f26385c5' => 
  array (
    'context' => 'design/ezwebin/content/tipafriend',
    'source' => 'Your name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '85880f746d2e3e035654d365f26385c5',
  ),
  'e6c5be8328b677ab1083129e5fd5b95b' => 
  array (
    'context' => 'design/ezwebin/content/tipafriend',
    'source' => 'Your email address',
    'comment' => NULL,
    'translation' => 'e-mail',
    'key' => 'e6c5be8328b677ab1083129e5fd5b95b',
  ),
  '1084bf3ef1bfdafcaf95bd268da3077d' => 
  array (
    'context' => 'design/ezwebin/content/tipafriend',
    'source' => 'Recipient\'s email address',
    'comment' => NULL,
    'translation' => '受信者のe-mail',
    'key' => '1084bf3ef1bfdafcaf95bd268da3077d',
  ),
  '01da054557e21a64d40a03f5ee889c21' => 
  array (
    'context' => 'design/ezwebin/content/tipafriend',
    'source' => 'Comment',
    'comment' => NULL,
    'translation' => 'コメント',
    'key' => '01da054557e21a64d40a03f5ee889c21',
  ),
  'aedbd9bf8377153e3576853deb9ba431' => 
  array (
    'context' => 'design/ezwebin/content/tipafriend',
    'source' => 'Send',
    'comment' => NULL,
    'translation' => '送信',
    'key' => 'aedbd9bf8377153e3576853deb9ba431',
  ),
  'afbc267914b5d2fda1816c4ed87269be' => 
  array (
    'context' => 'design/ezwebin/content/tipafriend',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => 'afbc267914b5d2fda1816c4ed87269be',
  ),
);
?>
