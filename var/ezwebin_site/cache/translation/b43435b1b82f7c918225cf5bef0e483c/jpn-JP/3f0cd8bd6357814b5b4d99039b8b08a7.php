<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/user/forgotpassword',
);

$TranslationRoot = array (
  '3637851bd489cae00fa06418b8ec8562' => 
  array (
    'context' => 'design/standard/user/forgotpassword',
    'source' => 'Have you forgotten your password?',
    'comment' => NULL,
    'translation' => 'パスワードを忘れましたか?',
    'key' => '3637851bd489cae00fa06418b8ec8562',
  ),
  '83348a1a784a6ef48454f5636562c95e' => 
  array (
    'context' => 'design/standard/user/forgotpassword',
    'source' => 'Generate new password',
    'comment' => NULL,
    'translation' => '新しいパスワードの作成',
    'key' => '83348a1a784a6ef48454f5636562c95e',
  ),
  '3eb7ab3ed0327a9a8e7cec1fa7b1c72d' => 
  array (
    'context' => 'design/standard/user/forgotpassword',
    'source' => 'Your account information',
    'comment' => NULL,
    'translation' => 'あなたの登録情報',
    'key' => '3eb7ab3ed0327a9a8e7cec1fa7b1c72d',
  ),
  '814124ca5bc2744416959f98ea2b4b4b' => 
  array (
    'context' => 'design/standard/user/forgotpassword',
    'source' => 'Password was successfully generated and sent to: %1',
    'comment' => NULL,
    'translation' => '新しいパスワードは %1 に送信されました。',
    'key' => '814124ca5bc2744416959f98ea2b4b4b',
  ),
  '9f257067aabd84de6f0aa6fa69b07037' => 
  array (
    'context' => 'design/standard/user/forgotpassword',
    'source' => 'The key is invalid or has been used. ',
    'comment' => NULL,
    'translation' => 'キーが無効またはすでに使用済みです。',
    'key' => '9f257067aabd84de6f0aa6fa69b07037',
  ),
  'eadd406d6807298901897a607f71ef22' => 
  array (
    'context' => 'design/standard/user/forgotpassword',
    'source' => '%siteurl new password',
    'comment' => NULL,
    'translation' => '%siteurl の新しいパスワード',
    'key' => 'eadd406d6807298901897a607f71ef22',
  ),
  'baef837af7130136170edd8449fde6a6' => 
  array (
    'context' => 'design/standard/user/forgotpassword',
    'source' => 'Click here to get new password',
    'comment' => NULL,
    'translation' => '新しいパスワードを取得するにはここをクリックします',
    'key' => 'baef837af7130136170edd8449fde6a6',
  ),
  'd1134dcd487303308ad24ccbbb09708c' => 
  array (
    'context' => 'design/standard/user/forgotpassword',
    'source' => 'New password',
    'comment' => NULL,
    'translation' => '新パスワード',
    'key' => 'd1134dcd487303308ad24ccbbb09708c',
  ),
  'f909270a40859054cac9db80e3c8b9c2' => 
  array (
    'context' => 'design/standard/user/forgotpassword',
    'source' => 'A mail has been sent to the following email address: %1. This email contains a link you need to click so that we can confirm that the correct user is getting the new password.',
    'comment' => NULL,
    'translation' => '次のメールアドレスにメールを送信しました: %1 
正規ユーザを確認し、新規パスワードを取得するには、このメールに含まれるリンクをクリックしてください。',
    'key' => 'f909270a40859054cac9db80e3c8b9c2',
  ),
  '2395ad6e2ebadb5df6657a521bf4818f' => 
  array (
    'context' => 'design/standard/user/forgotpassword',
    'source' => 'There is no registered user with that email address.',
    'comment' => NULL,
    'translation' => 'メールアドレスに該当する登録ユーザは存在しません。',
    'key' => '2395ad6e2ebadb5df6657a521bf4818f',
  ),
  '36d641e8141d79cb9c66730dde0a205e' => 
  array (
    'context' => 'design/standard/user/forgotpassword',
    'source' => 'If you have forgotten your password we can generate a new one for you. All you need to do is to enter your email address and we will create a new password for you.',
    'comment' => NULL,
    'translation' => 'パスワードを忘れた場合、システムは新しいパスワードを生成します。あなたのメールアドレスを入力してください。',
    'key' => '36d641e8141d79cb9c66730dde0a205e',
  ),
  '9634429d1d1df4cdd9ed9decd6f35a83' => 
  array (
    'context' => 'design/standard/user/forgotpassword',
    'source' => 'Email',
    'comment' => NULL,
    'translation' => 'e-mail',
    'key' => '9634429d1d1df4cdd9ed9decd6f35a83',
  ),
);
?>
