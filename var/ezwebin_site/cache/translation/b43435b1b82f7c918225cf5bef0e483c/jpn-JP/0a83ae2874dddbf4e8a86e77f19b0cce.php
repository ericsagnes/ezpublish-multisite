<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/parts/media/menu',
);

$TranslationRoot = array (
  'b23df4d979b3201fdbf4cc05949ce0e9' => 
  array (
    'context' => 'design/admin/parts/media/menu',
    'source' => 'Media library',
    'comment' => NULL,
    'translation' => 'メディアリソース',
    'key' => 'b23df4d979b3201fdbf4cc05949ce0e9',
  ),
  '2b890ce152362c49e5b355999489b241' => 
  array (
    'context' => 'design/admin/parts/media/menu',
    'source' => 'Trash',
    'comment' => NULL,
    'translation' => 'ゴミ箱',
    'key' => '2b890ce152362c49e5b355999489b241',
  ),
  '52bb02e9bdd14690e957cbc74108427e' => 
  array (
    'context' => 'design/admin/parts/media/menu',
    'source' => 'Small',
    'comment' => NULL,
    'translation' => '狭く',
    'key' => '52bb02e9bdd14690e957cbc74108427e',
  ),
  '56aa2a98e39f47459162b4f5f6951625' => 
  array (
    'context' => 'design/admin/parts/media/menu',
    'source' => 'Medium',
    'comment' => NULL,
    'translation' => '中間',
    'key' => '56aa2a98e39f47459162b4f5f6951625',
  ),
  '05f90f67842629d4e4eba46ad7ed49cf' => 
  array (
    'context' => 'design/admin/parts/media/menu',
    'source' => 'Large',
    'comment' => NULL,
    'translation' => '広く',
    'key' => '05f90f67842629d4e4eba46ad7ed49cf',
  ),
  'd1b9fc2a731773b25019ef88f92ae419' => 
  array (
    'context' => 'design/admin/parts/media/menu',
    'source' => 'View and manage the contents of the trash bin.',
    'comment' => NULL,
    'translation' => 'ゴミ箱の中身の表示と管理',
    'key' => 'd1b9fc2a731773b25019ef88f92ae419',
  ),
  'd16fbf054115578447b6bf11e3d198aa' => 
  array (
    'context' => 'design/admin/parts/media/menu',
    'source' => 'Change the left menu width to small size.',
    'comment' => NULL,
    'translation' => '左サイドメニュー幅を狭く',
    'key' => 'd16fbf054115578447b6bf11e3d198aa',
  ),
  'e9bc4c28bebf96d8b67424213895220a' => 
  array (
    'context' => 'design/admin/parts/media/menu',
    'source' => 'Change the left menu width to medium size.',
    'comment' => NULL,
    'translation' => '左サイドメニュー幅を中間に',
    'key' => 'e9bc4c28bebf96d8b67424213895220a',
  ),
  '13433116bfe839571550260dabbf871d' => 
  array (
    'context' => 'design/admin/parts/media/menu',
    'source' => 'Change the left menu width to large size.',
    'comment' => NULL,
    'translation' => '左サイドメニュー幅を広く',
    'key' => '13433116bfe839571550260dabbf871d',
  ),
);
?>
