<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
);

$TranslationRoot = array (
  '3e2b6d05a0076f5c7e93cc6e17f9b76b' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'Approval',
    'comment' => NULL,
    'translation' => '承認状況',
    'key' => '3e2b6d05a0076f5c7e93cc6e17f9b76b',
  ),
  'ad971ae84d89e971f9452b7e07738c31' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'The content object %1 awaits approval before it can be published.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクト %1 は公開前に承認が必要です。',
    'key' => 'ad971ae84d89e971f9452b7e07738c31',
  ),
  'e4f24765ef8cb7364af35c5d4e16d318' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'The content object %1 needs your approval before it can be published.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクト %1 は公開前にあなたの承認が必要です。',
    'key' => 'e4f24765ef8cb7364af35c5d4e16d318',
  ),
  '17d14431fe36cd33ce4b0931778d59b3' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'Do you approve of the content object being published?',
    'comment' => NULL,
    'translation' => 'このコンテンツオブジェクトの公開を承認しますか?',
    'key' => '17d14431fe36cd33ce4b0931778d59b3',
  ),
  'ae6712ee8d8c7ad5eb44b30ba04375eb' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'Edit the object',
    'comment' => NULL,
    'translation' => 'オブジェクトを編集',
    'key' => 'ae6712ee8d8c7ad5eb44b30ba04375eb',
  ),
  'a6b2e8f00d7d66e5ed86194dba93c6e2' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'The content object %1 was not accepted but will be available as a draft for the author.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクト %1 の公開は承認されませんでしたが、下書きとして利用可能です。',
    'key' => 'a6b2e8f00d7d66e5ed86194dba93c6e2',
  ),
  '91f8fa3527fe234b6f96788ccf4470b8' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'Comment',
    'comment' => NULL,
    'translation' => 'コメント',
    'key' => '91f8fa3527fe234b6f96788ccf4470b8',
  ),
  '29d27a8d5a37694d89158eb52f6d32b1' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'Add Comment',
    'comment' => NULL,
    'translation' => 'コメントする',
    'key' => '29d27a8d5a37694d89158eb52f6d32b1',
  ),
  'eae6835cb352e9717fb4f04814294924' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'Approve',
    'comment' => NULL,
    'translation' => '承認する',
    'key' => 'eae6835cb352e9717fb4f04814294924',
  ),
  'ca132a83a70d43ce070bae37243413ff' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'Deny',
    'comment' => NULL,
    'translation' => '承認しない',
    'key' => 'ca132a83a70d43ce070bae37243413ff',
  ),
  '644b97ab28e9ca68b5a6679cbd66a27d' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'Preview',
    'comment' => NULL,
    'translation' => 'プレビュー',
    'key' => '644b97ab28e9ca68b5a6679cbd66a27d',
  ),
  '79b8d9538847443cc301448cc611fef7' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'Participants',
    'comment' => NULL,
    'translation' => '関係ユーザ',
    'key' => '79b8d9538847443cc301448cc611fef7',
  ),
  '4980bb31bea92953ddf0bd98837fc445' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'Messages',
    'comment' => NULL,
    'translation' => 'メッセージ',
    'key' => '4980bb31bea92953ddf0bd98837fc445',
  ),
  '55558ea2c896afbdcbf8177090100b60' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'The content object %1 [deleted] was approved and will be published once the publishing workflow continues.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクト %1 [の削除] は承認されました。ワークフローが完了すれば公開される予定です。',
    'key' => '55558ea2c896afbdcbf8177090100b60',
  ),
  '5d095aedce2fef682f4aa81bdbeb4261' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'The content object %1 [deleted] was not accepted but is available as a draft again.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクト %1 [の削除] は承認されませんでしたが下書きとして再度編集可能です。',
    'key' => '5d095aedce2fef682f4aa81bdbeb4261',
  ),
  '06868cbdb82491de4382e30a47bf3d55' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'The content object %1 [deleted] was not accepted but will be available as a draft for the author.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクト %1 [の削除] は承認されませんでしたが下書きとして編集可能です。',
    'key' => '06868cbdb82491de4382e30a47bf3d55',
  ),
  '3d769b2bee57c711e5bb0cd2eb9cc6d0' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'Do you want to send a message to the person approving it?',
    'comment' => NULL,
    'translation' => '承認者にメッセージを送信しますか?',
    'key' => '3d769b2bee57c711e5bb0cd2eb9cc6d0',
  ),
  '082c7763c4faf50faec670640c41e0bc' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'The content object %1 was approved and will be published when the publishing workflow continues.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクト %1は承認されました。ワークフローが完了すれば公開される予定です。',
    'key' => '082c7763c4faf50faec670640c41e0bc',
  ),
  '3ae71ca6c704442282cbb60e7b3f8c3f' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'The content object %1 was not accepted but is still available as a draft.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクト %1 の公開は承認されませんでしたが、下書きとして利用可能です。',
    'key' => '3ae71ca6c704442282cbb60e7b3f8c3f',
  ),
  '518ce20fe88ba64514f9023d6b0621fe' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'You may edit the draft and publish it, in which case an approval is required again.',
    'comment' => NULL,
    'translation' => '下書きを編集後、再び公開の承認を要求することが可能です。',
    'key' => '518ce20fe88ba64514f9023d6b0621fe',
  ),
  '3a06841d57dcf7f9473abd07c1538fb8' => 
  array (
    'context' => 'design/admin/collaboration/handler/view/full/ezapprove',
    'source' => 'The author can edit the draft and publish it again, in which case a new approval is required.',
    'comment' => NULL,
    'translation' => '作成者は下書きを編集後、再び公開の承認を要求することが可能です。',
    'key' => '3a06841d57dcf7f9473abd07c1538fb8',
  ),
);
?>
