<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/error/shop',
);

$TranslationRoot = array (
  '35c10e12e507dda0bba7c2a3d4a998b6' => 
  array (
    'context' => 'design/standard/error/shop',
    'source' => 'Not a product',
    'comment' => NULL,
    'translation' => '商品ではありません',
    'key' => '35c10e12e507dda0bba7c2a3d4a998b6',
  ),
  '3ed37cb2ae70ace730314574a475aba8' => 
  array (
    'context' => 'design/standard/error/shop',
    'source' => 'The requested object is not a product and cannot be used by the shop module.',
    'comment' => NULL,
    'translation' => '要求されたオブジェクトは商品ではないため、ショップモジュールで取り扱うことは出来ません。',
    'key' => '3ed37cb2ae70ace730314574a475aba8',
  ),
  'db27ee024969907717163f9b719b504d' => 
  array (
    'context' => 'design/standard/error/shop',
    'source' => 'Incompatible product type',
    'comment' => NULL,
    'translation' => '矛盾した商品タイプ',
    'key' => 'db27ee024969907717163f9b719b504d',
  ),
  '7f7ca4bd8dac9f89f58a520decd49797' => 
  array (
    'context' => 'design/standard/error/shop',
    'source' => 'The requested object and current basket have incompatible price datatypes.',
    'comment' => NULL,
    'translation' => '要求されたオブジェクトと現在の買い物カゴが矛盾したデータタイプを持っています。',
    'key' => '7f7ca4bd8dac9f89f58a520decd49797',
  ),
  '36796f493a5bb55035ff947068e11562' => 
  array (
    'context' => 'design/standard/error/shop',
    'source' => 'Invalid preferred currency',
    'comment' => NULL,
    'translation' => '不正なデフォルト通貨',
    'key' => '36796f493a5bb55035ff947068e11562',
  ),
  'a0161032f7a91c9acdd259ce3aa8b470' => 
  array (
    'context' => 'design/standard/error/shop',
    'source' => '\'%1\' currency does not exist.',
    'comment' => NULL,
    'translation' => '\'%1\' 通貨は存在しません。',
    'key' => 'a0161032f7a91c9acdd259ce3aa8b470',
  ),
  '168087588b4bdab0025fbf633b0944d7' => 
  array (
    'context' => 'design/standard/error/shop',
    'source' => '\'%1\' cannot be used because it is inactive.',
    'comment' => NULL,
    'translation' => '\'%1\' は無効であるため、使用することはできません。',
    'key' => '168087588b4bdab0025fbf633b0944d7',
  ),
);
?>
