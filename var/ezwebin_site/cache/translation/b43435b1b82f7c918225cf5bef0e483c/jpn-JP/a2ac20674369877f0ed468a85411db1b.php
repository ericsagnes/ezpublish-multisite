<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/content/browse',
);

$TranslationRoot = array (
  'dae1893751830c87749e9573a073c847' => 
  array (
    'context' => 'design/ezwebin/content/browse',
    'source' => 'Browse',
    'comment' => NULL,
    'translation' => 'ブラウズ',
    'key' => 'dae1893751830c87749e9573a073c847',
  ),
  'fc7363d7298602e6ed7f7173cb6c7ed0' => 
  array (
    'context' => 'design/ezwebin/content/browse',
    'source' => 'To select objects, choose the appropriate radiobutton or checkbox(es), and click the "Select" button.',
    'comment' => NULL,
    'translation' => 'ラジオボタンもしくはチェックボックスでオブジェクトを選択し、”選択”ボタンをクリックして下さい。',
    'key' => 'fc7363d7298602e6ed7f7173cb6c7ed0',
  ),
  '189f120cb82d74257dd398c8b9679fb5' => 
  array (
    'context' => 'design/ezwebin/content/browse',
    'source' => 'Back',
    'comment' => NULL,
    'translation' => '戻る',
    'key' => '189f120cb82d74257dd398c8b9679fb5',
  ),
  'e632085c2aa76bb2f6f89d97e1a189a0' => 
  array (
    'context' => 'design/ezwebin/content/browse',
    'source' => 'Top level',
    'comment' => NULL,
    'translation' => 'トップレベル',
    'key' => 'e632085c2aa76bb2f6f89d97e1a189a0',
  ),
  'dffe70810a8fb9e0578e593611cdb5bd' => 
  array (
    'context' => 'design/ezwebin/content/browse',
    'source' => 'Select',
    'comment' => NULL,
    'translation' => '選択',
    'key' => 'dffe70810a8fb9e0578e593611cdb5bd',
  ),
  '2b7fa6856a46db0766c097a4ac68574a' => 
  array (
    'context' => 'design/ezwebin/content/browse',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '2b7fa6856a46db0766c097a4ac68574a',
  ),
  '0266d38a006eb4e20671cd35d68cc903' => 
  array (
    'context' => 'design/ezwebin/content/browse',
    'source' => 'To select an object that is a child of one of the displayed objects, click the parent object name to display a list of its children.',
    'comment' => NULL,
    'translation' => '表示されてるオブジェクトの子オブジェクトを選択する場合は、親オブジェクトの名前をクリックし、子オブジェクトのリストを表示して下さい。',
    'key' => '0266d38a006eb4e20671cd35d68cc903',
  ),
);
?>
