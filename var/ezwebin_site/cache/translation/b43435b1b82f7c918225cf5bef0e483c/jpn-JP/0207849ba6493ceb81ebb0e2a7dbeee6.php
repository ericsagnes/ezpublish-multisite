<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/content/browse',
);

$TranslationRoot = array (
  '9f3d4449cd47b7919199f1e8bf4bc32a' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Create new',
    'comment' => NULL,
    'translation' => '新規作成',
    'key' => '9f3d4449cd47b7919199f1e8bf4bc32a',
  ),
  '758b5c14c0aa2e2cc9b3ac39e62d44c8' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Browse',
    'comment' => NULL,
    'translation' => 'ブラウズ',
    'key' => '758b5c14c0aa2e2cc9b3ac39e62d44c8',
  ),
  '740b801e2441bb703ad94aac2a6077a0' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '740b801e2441bb703ad94aac2a6077a0',
  ),
  'b6df95de29f5a4c406a2b1db8b999ec8' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => 'b6df95de29f5a4c406a2b1db8b999ec8',
  ),
  '926d8985af9971393dcca4b9644d7be4' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Section',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => '926d8985af9971393dcca4b9644d7be4',
  ),
  '34f37a3e2f076aae499147c533ff0169' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Up one level',
    'comment' => NULL,
    'translation' => '上の階層へ移動',
    'key' => '34f37a3e2f076aae499147c533ff0169',
  ),
  '0e5902812801c60c22818be59170aefd' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Top levels',
    'comment' => NULL,
    'translation' => 'トップレベル',
    'key' => '0e5902812801c60c22818be59170aefd',
  ),
  'bf9a3846f06740b3bd279982f2b1d7d9' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Switch top levels by clicking one of these items.',
    'comment' => NULL,
    'translation' => 'これらのアイテムの一つをクリックするとトップレベルが切り替わります。',
    'key' => 'bf9a3846f06740b3bd279982f2b1d7d9',
  ),
  '9dd8ce1201bd7d85f63fe392f05cdadf' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Bookmarks',
    'comment' => NULL,
    'translation' => 'ブックマーク',
    'key' => '9dd8ce1201bd7d85f63fe392f05cdadf',
  ),
  '6d06bc0475d1a53518bcdadac44e433b' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Bookmark items are managed using %bookmarkname in the %personalname part.',
    'comment' => NULL,
    'translation' => 'ブックマークは、%personalname 部分の %bookmarkname を使用して管理されています。',
    'key' => '6d06bc0475d1a53518bcdadac44e433b',
  ),
  'ebbf0f31d83022eee4990fd874380ac4' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'My bookmarks',
    'comment' => NULL,
    'translation' => 'マイブックマーク',
    'key' => 'ebbf0f31d83022eee4990fd874380ac4',
  ),
  'a46b1fb96e2f26c04a4206e7633f8da9' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Personal',
    'comment' => NULL,
    'translation' => 'パーソナル',
    'key' => 'a46b1fb96e2f26c04a4206e7633f8da9',
  ),
  '9f3f888e0a3012a58e316bc17817b6ac' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Recent items',
    'comment' => NULL,
    'translation' => '最新のアイテム',
    'key' => '9f3f888e0a3012a58e316bc17817b6ac',
  ),
  'c00b8e16cec237a2865762eddc87f1d0' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Recent items are added on publishing.',
    'comment' => NULL,
    'translation' => '最新アイテムは、公開サイトに追加されました。',
    'key' => 'c00b8e16cec237a2865762eddc87f1d0',
  ),
  '9d24278bbadb391f2c2e584668e4b8bd' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Select',
    'comment' => NULL,
    'translation' => '選択',
    'key' => '9d24278bbadb391f2c2e584668e4b8bd',
  ),
  '2a48c2ff732b7874fc8957aa7857e87c' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '2a48c2ff732b7874fc8957aa7857e87c',
  ),
  'a4ae9a54a9b3f64d4c1c3cc0ba983a89' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'New',
    'comment' => NULL,
    'translation' => '新規',
    'key' => 'a4ae9a54a9b3f64d4c1c3cc0ba983a89',
  ),
  'a530ec60e426bbf65ef0bad6ff7747ad' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'To select objects, choose the appropriate radio button or checkbox(es), then click the "Select" button.',
    'comment' => NULL,
    'translation' => '適切なラジオボタンかチェックボックスを使ってオブジェクトを選択した後、”選択”ボタンをクリックしてください。',
    'key' => 'a530ec60e426bbf65ef0bad6ff7747ad',
  ),
  'dc21803a01cf19b9306ff5871745ee7e' => 
  array (
    'context' => 'design/standard/content/browse',
    'source' => 'To select an object that is a child of one of the displayed objects, click the object name for a list of the children of the object.',
    'comment' => NULL,
    'translation' => '表示されているオブジェクトの子オブジェクトを選択するには、オブジェクト名をクリックし、子オブジェクトのリストから選択できます。',
    'key' => 'dc21803a01cf19b9306ff5871745ee7e',
  ),
);
?>
