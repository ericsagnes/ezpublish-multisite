<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/shop',
);

$TranslationRoot = array (
  '42b0bd20afaa9329ac3db5387dc2d20d' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Order status',
    'comment' => NULL,
    'translation' => '注文ステータス',
    'key' => '42b0bd20afaa9329ac3db5387dc2d20d',
  ),
  '719aa0f6337ff31461e9e75b1b9c2f6b' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Undefined',
    'comment' => NULL,
    'translation' => '未定義',
    'key' => '719aa0f6337ff31461e9e75b1b9c2f6b',
  ),
  '2f17bea18a5cd468fc2bd2c2566b53fe' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Any',
    'comment' => NULL,
    'translation' => '指定なし',
    'key' => '2f17bea18a5cd468fc2bd2c2566b53fe',
  ),
  'f2828925be2e19443f4047f354938904' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'VAT type',
    'comment' => NULL,
    'translation' => '課税方式',
    'key' => 'f2828925be2e19443f4047f354938904',
  ),
  '7996fdd7c0e534cf925d8625ab622e2c' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'None',
    'comment' => NULL,
    'translation' => 'なし',
    'key' => '7996fdd7c0e534cf925d8625ab622e2c',
  ),
  '0a9b7c3e0d1f49efb0756c5e89a127df' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Order list',
    'comment' => NULL,
    'translation' => 'ご注文一覧',
    'key' => '0a9b7c3e0d1f49efb0756c5e89a127df',
  ),
  'a38889b52bfe44b77feeac7c4849bcdf' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Basket',
    'comment' => NULL,
    'translation' => '買い物かご',
    'key' => 'a38889b52bfe44b77feeac7c4849bcdf',
  ),
  '7b4ec1cf2f483a2878318d77c493692b' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Checkout',
    'comment' => NULL,
    'translation' => 'レジに進む',
    'key' => '7b4ec1cf2f483a2878318d77c493692b',
  ),
  '5351780ddfb1e74d9eaf1e3c0c5aec84' => 
  array (
    'context' => 'kernel/shop',
    'source' => '\'Autorates\' were retrieved successfully',
    'comment' => NULL,
    'translation' => '"為替レート"の取得に成功しました',
    'key' => '5351780ddfb1e74d9eaf1e3c0c5aec84',
  ),
  '159b6d83f6e2ba47934177ceb6531f19' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Unknown body format in HTTP response. Expected \'text/xml\'',
    'comment' => NULL,
    'translation' => 'HTTPレスポンスのbody内容が不正、\'text/xml\'が指定されています',
    'key' => '159b6d83f6e2ba47934177ceb6531f19',
  ),
  '393390722b2f463356e38add7fbbae8a' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Invalid HTTP response',
    'comment' => NULL,
    'translation' => '無効なHTTPレスポンス',
    'key' => '393390722b2f463356e38add7fbbae8a',
  ),
  'd8c8da5ed6793763cc5a42007f56a073' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Unable to send http request: %1:%2/%3',
    'comment' => NULL,
    'translation' => 'httpリクエスト: %1:%2/%3 を送信することができません',
    'key' => 'd8c8da5ed6793763cc5a42007f56a073',
  ),
  '8b1863bd87d1f77013c1b8be3fbab0d3' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'eZExchangeRatesUpdateHandler: you should reimplement \'requestRates\' method',
    'comment' => NULL,
    'translation' => 'eZExchangeRatesUpdateHandler: "requestRates" 方法を再実行してください',
    'key' => '8b1863bd87d1f77013c1b8be3fbab0d3',
  ),
  '483d5bd2c4f787873d0336b8795c04e1' => 
  array (
    'context' => 'kernel/shop',
    'source' => '\'Auto\' prices were updated successfully.',
    'comment' => NULL,
    'translation' => '\'自動\'価格の更新に成功しました。',
    'key' => '483d5bd2c4f787873d0336b8795c04e1',
  ),
  'abf350c5353885d000e9cbed1d303b6f' => 
  array (
    'context' => 'kernel/shop',
    'source' => '\'Auto\' rates were updated successfully.',
    'comment' => NULL,
    'translation' => '\'為替レート\'の更新に成功しました。',
    'key' => 'abf350c5353885d000e9cbed1d303b6f',
  ),
  '96814ea5ba7bf295fc47971192d00af5' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Unable to calculate cross-rate for currency-pair \'%1\'/\'%2\'',
    'comment' => NULL,
    'translation' => '通貨 \'%1\'/\'%2\' のペアではクロス・レート計算はできません',
    'key' => '96814ea5ba7bf295fc47971192d00af5',
  ),
  '297595e2aba33ae0a1a2d9504cdce155' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Unable to determine currency for retrieved rates.',
    'comment' => NULL,
    'translation' => '為替レートの取得ができません。',
    'key' => '297595e2aba33ae0a1a2d9504cdce155',
  ),
  '6eadb336006e4cd10f6afc809e95d3b0' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Retrieved empty list of rates.',
    'comment' => NULL,
    'translation' => '替レートの空リストを取得しました。',
    'key' => '6eadb336006e4cd10f6afc809e95d3b0',
  ),
  '85de9ea5058226df1de8e09edb66bbe1' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Unable to create handler to update auto rates.',
    'comment' => NULL,
    'translation' => '為替レートを更新するハンドラを作成できません。',
    'key' => '85de9ea5058226df1de8e09edb66bbe1',
  ),
  'adf9929633a2541eaff190a31350f78f' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Confirm order',
    'comment' => NULL,
    'translation' => '注文内容の確認',
    'key' => 'adf9929633a2541eaff190a31350f78f',
  ),
  '5acfb8662fed2773acaf3a0539b2f8fa' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'The confirm order operation was canceled. Try to checkout again.',
    'comment' => NULL,
    'translation' => '注文の確認処理はキャンセルされました。もう一度実行してください。',
    'key' => '5acfb8662fed2773acaf3a0539b2f8fa',
  ),
  'a925092ff07c129804926e322fdf6454' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Changes were stored successfully.',
    'comment' => NULL,
    'translation' => '変更は保存されました。',
    'key' => 'a925092ff07c129804926e322fdf6454',
  ),
  '0e3e77832bc8ed3f9467cfeb95d2b61c' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Available currency list',
    'comment' => NULL,
    'translation' => '利用可能な通貨リスト',
    'key' => '0e3e77832bc8ed3f9467cfeb95d2b61c',
  ),
  'b8f7e6cf6acc64073ae00260ac159501' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Customer list',
    'comment' => NULL,
    'translation' => '顧客一覧',
    'key' => 'b8f7e6cf6acc64073ae00260ac159501',
  ),
  '36e4dc6279bece202ea6bbafc5a25770' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Customer order view',
    'comment' => NULL,
    'translation' => '注文リスト',
    'key' => '36e4dc6279bece202ea6bbafc5a25770',
  ),
  'c4df1e7d7a9b8c9968e9d865210aff56' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Discount group',
    'comment' => NULL,
    'translation' => 'ディスカウント・グループ',
    'key' => 'c4df1e7d7a9b8c9968e9d865210aff56',
  ),
  '1cabd84d785ac571e340e711cd3fa0b3' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Classes',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => '1cabd84d785ac571e340e711cd3fa0b3',
  ),
  'b0e8e04d5d4405d83dd7fb13851ab454' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Any class',
    'comment' => NULL,
    'translation' => '指定なし',
    'key' => 'b0e8e04d5d4405d83dd7fb13851ab454',
  ),
  '5923e073028f6f869f88d6edc5b6a060' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'in sections',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => '5923e073028f6f869f88d6edc5b6a060',
  ),
  'b64a3cafa444df4a58612a649c55ca0f' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'in any section',
    'comment' => NULL,
    'translation' => '指定なし',
    'key' => 'b64a3cafa444df4a58612a649c55ca0f',
  ),
  '2db98c7e57af118643400f1b51aa5d75' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Products',
    'comment' => NULL,
    'translation' => '商品',
    'key' => '2db98c7e57af118643400f1b51aa5d75',
  ),
  'fec1080851de10e6775fb19094a85634' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Any product',
    'comment' => NULL,
    'translation' => '指定なし',
    'key' => 'fec1080851de10e6775fb19094a85634',
  ),
  'e4b1466afc431f4070ec9153123c1c19' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Group view of discount rule',
    'comment' => NULL,
    'translation' => '割引設定のグループ表示',
    'key' => 'e4b1466afc431f4070ec9153123c1c19',
  ),
  '2cb24589609dcf10181ceb902da596ba' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Editing rule',
    'comment' => NULL,
    'translation' => '割引設定の編集',
    'key' => '2cb24589609dcf10181ceb902da596ba',
  ),
  '9549e8a0be73090181bee930453162f5' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Edit currency',
    'comment' => NULL,
    'translation' => '通貨の編集',
    'key' => '9549e8a0be73090181bee930453162f5',
  ),
  '74cf86293528bc66b6258b61c9aa0c86' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Create new currency',
    'comment' => NULL,
    'translation' => '新規通貨の作成',
    'key' => '74cf86293528bc66b6258b61c9aa0c86',
  ),
  'c979844dfc60a28aa5421585586ac44c' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Error checking out',
    'comment' => NULL,
    'translation' => 'チェックアウトエラー',
    'key' => 'c979844dfc60a28aa5421585586ac44c',
  ),
  '2919d17dfc5a1071bf6c4bdabf257963' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Unable to calculate VAT percentage because your country is unknown. You can either fill country manually in your account information (if you are a registered user) or contact site administrator.',
    'comment' => NULL,
    'translation' => '国名が不明のため税率の計算ができません。(登録済みのユーザである場合)ご自分のアカウント情報に国名を入力するか、サイト管理者に連絡してください。',
    'key' => '2919d17dfc5a1071bf6c4bdabf257963',
  ),
  '2f8e26a6e9b8f4faff1a0aaac6307ee2' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Statistics',
    'comment' => NULL,
    'translation' => '統計',
    'key' => '2f8e26a6e9b8f4faff1a0aaac6307ee2',
  ),
  'b483003257c8aeb7489ace426a2da20f' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Order #%order_id',
    'comment' => NULL,
    'translation' => '注文 #%order_id',
    'key' => 'b483003257c8aeb7489ace426a2da20f',
  ),
  '1ec6eccbe97fd0d229913132554ea1f7' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Preferred currency',
    'comment' => NULL,
    'translation' => 'デフォルト通貨',
    'key' => '1ec6eccbe97fd0d229913132554ea1f7',
  ),
  'e9f3db8c8837bffff34f7d155ba7229f' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Products overview',
    'comment' => NULL,
    'translation' => '商品一覧',
    'key' => 'e9f3db8c8837bffff34f7d155ba7229f',
  ),
  '02a9f8532bc1988699ed20517ca088e9' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Enter account information',
    'comment' => NULL,
    'translation' => 'お客様情報の入力',
    'key' => '02a9f8532bc1988699ed20517ca088e9',
  ),
  '4d1b8ca20d7b116b6d509e89efcff3ec' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Remove order',
    'comment' => NULL,
    'translation' => '注文の削除',
    'key' => '4d1b8ca20d7b116b6d509e89efcff3ec',
  ),
  '834326b2471c4d2c055d5b9286caffcb' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'New order status was successfully added.',
    'comment' => NULL,
    'translation' => '新しい注文ステータスの追加に成功しました。',
    'key' => '834326b2471c4d2c055d5b9286caffcb',
  ),
  'ecae4acbef706f1ffb52e2774586db2d' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Changes to order status were successfully stored.',
    'comment' => NULL,
    'translation' => '注文ステータスの変更を保存しました。',
    'key' => 'ecae4acbef706f1ffb52e2774586db2d',
  ),
  '9a3d38436087484e962d936b0a47084f' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Selected order statuses were successfully removed.',
    'comment' => NULL,
    'translation' => '選択した注文ステータスの削除に成功しました。',
    'key' => '9a3d38436087484e962d936b0a47084f',
  ),
  '724219bc7f39540655d4199aad37185b' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Internal orders cannot be removed.',
    'comment' => NULL,
    'translation' => '内部の注文は削除できません。',
    'key' => '724219bc7f39540655d4199aad37185b',
  ),
  '08ff0cae9af8276aabf423c7a7e6534d' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Status',
    'comment' => NULL,
    'translation' => 'ステータス',
    'key' => '08ff0cae9af8276aabf423c7a7e6534d',
  ),
  'dbb0dc6625e9f30ac404090a7d5c2be7' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'VAT types',
    'comment' => NULL,
    'translation' => '課税方法',
    'key' => 'dbb0dc6625e9f30ac404090a7d5c2be7',
  ),
  '634afd8c22efe7833172cea2e2281bf3' => 
  array (
    'context' => 'kernel/shop',
    'source' => 'Wishlist',
    'comment' => NULL,
    'translation' => 'ウイッシュリスト',
    'key' => '634afd8c22efe7833172cea2e2281bf3',
  ),
  'e235b09d9b2cd45ec71b6dae19477ef2' => 
  array (
    'context' => 'kernel/shop',
    'source' => '\'%value\' is not a valid custom rate value (positive number expected)',
    'comment' => NULL,
    'translation' => '\'%value\'は有効なカスタムレートバリューではありません（正数予想）',
    'key' => 'e235b09d9b2cd45ec71b6dae19477ef2',
  ),
  '0a000eaf928bb32176ec0e7ff79b5092' => 
  array (
    'context' => 'kernel/shop',
    'source' => '\'%value\' is not a valid rate_factor value (positive number expected)',
    'comment' => NULL,
    'translation' => '\'%value\'は有効はrate_factorバリュー（正数予想）',
    'key' => '0a000eaf928bb32176ec0e7ff79b5092',
  ),
);
?>
