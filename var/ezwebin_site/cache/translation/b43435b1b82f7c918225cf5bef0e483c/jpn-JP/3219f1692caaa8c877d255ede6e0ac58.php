<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/shop',
);

$TranslationRoot = array (
  'c40b1b89c2059c9198eb82ebe0a54e2e' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Basket',
    'comment' => NULL,
    'translation' => '買い物かご',
    'key' => 'c40b1b89c2059c9198eb82ebe0a54e2e',
  ),
  '34e7587025eb14cdbd0f5b2781b36ce1' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Store',
    'comment' => NULL,
    'translation' => '保存',
    'key' => '34e7587025eb14cdbd0f5b2781b36ce1',
  ),
  'e5e964be06ce746f0b36f32af09d7bf4' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => 'e5e964be06ce746f0b36f32af09d7bf4',
  ),
  '98007959a3863a4b4774f78ab021c4e1' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Continue shopping',
    'comment' => NULL,
    'translation' => '買い物を続ける',
    'key' => '98007959a3863a4b4774f78ab021c4e1',
  ),
  'ed36b153878fc983515d52b660b15a5c' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Checkout',
    'comment' => NULL,
    'translation' => 'レジに進む',
    'key' => 'ed36b153878fc983515d52b660b15a5c',
  ),
  '3fa6340897a44f9c315cb9f17ed9c2b2' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'You have no products in your basket',
    'comment' => NULL,
    'translation' => '買い物かごは空です',
    'key' => '3fa6340897a44f9c315cb9f17ed9c2b2',
  ),
  'a11f0c8151a34d6c0a570764475c143d' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Confirm order',
    'comment' => NULL,
    'translation' => '注文内容の確認',
    'key' => 'a11f0c8151a34d6c0a570764475c143d',
  ),
  '6376d1f85bcb9d2a22b9874c26c70746' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Product items',
    'comment' => NULL,
    'translation' => '商品明細',
    'key' => '6376d1f85bcb9d2a22b9874c26c70746',
  ),
  '91290b86451a25546c571711484259aa' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Confirm',
    'comment' => NULL,
    'translation' => '確認',
    'key' => '91290b86451a25546c571711484259aa',
  ),
  '788c208debfb55a7f38420311c4d3d9c' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '788c208debfb55a7f38420311c4d3d9c',
  ),
  '807da39820413934e134aeb7c2ba3700' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => '807da39820413934e134aeb7c2ba3700',
  ),
  '4dc4930d8fa612be908e0a8af03baf5b' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'New',
    'comment' => NULL,
    'translation' => '新規',
    'key' => '4dc4930d8fa612be908e0a8af03baf5b',
  ),
  '17aa3153173b50a63a57d9ee811dfdac' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Discard',
    'comment' => NULL,
    'translation' => '破棄',
    'key' => '17aa3153173b50a63a57d9ee811dfdac',
  ),
  'e6b97263db80b83dbb17188ee53c18c1' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Group view',
    'comment' => NULL,
    'translation' => 'グループ一覧',
    'key' => 'e6b97263db80b83dbb17188ee53c18c1',
  ),
  'c09214dc0c7b7fb7272e0206d6ff3e49' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Percent',
    'comment' => NULL,
    'translation' => '割引率',
    'key' => 'c09214dc0c7b7fb7272e0206d6ff3e49',
  ),
  'd45de439dec8dea3411d2dbf60579983' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Add Rule',
    'comment' => NULL,
    'translation' => '設定の追加',
    'key' => 'd45de439dec8dea3411d2dbf60579983',
  ),
  'e009b5d2b5e174a6db4b5c1cd44a48d3' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Remove Rule',
    'comment' => NULL,
    'translation' => '設定の削除',
    'key' => 'e009b5d2b5e174a6db4b5c1cd44a48d3',
  ),
  '475af77709875c332bc12b6136223b18' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Customers',
    'comment' => NULL,
    'translation' => '顧客',
    'key' => '475af77709875c332bc12b6136223b18',
  ),
  'f9f2b018db34d1f16fd4d7d656dae39b' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Add customer',
    'comment' => NULL,
    'translation' => '顧客の追加',
    'key' => 'f9f2b018db34d1f16fd4d7d656dae39b',
  ),
  '708da010c4c43581ee3813c77a6f02e5' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Remove customer',
    'comment' => NULL,
    'translation' => '顧客の削除',
    'key' => '708da010c4c43581ee3813c77a6f02e5',
  ),
  'da2db13f0756ae53a070febf16e3d684' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Editing rule',
    'comment' => NULL,
    'translation' => '割引設定の編集',
    'key' => 'da2db13f0756ae53a070febf16e3d684',
  ),
  '45e89900ec75b776a7c81c6f4e3a6b33' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Any',
    'comment' => NULL,
    'translation' => '指定なし',
    'key' => '45e89900ec75b776a7c81c6f4e3a6b33',
  ),
  '29e5ccc19426a6d23110122edb079f9d' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Order',
    'comment' => NULL,
    'translation' => '注文',
    'key' => '29e5ccc19426a6d23110122edb079f9d',
  ),
  'ece7c6b54932f003b9cd00f4a838b653' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Subtotal of items',
    'comment' => NULL,
    'translation' => '商品小計',
    'key' => 'ece7c6b54932f003b9cd00f4a838b653',
  ),
  'e7f14265c098a669649edec83962e5c9' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Order list',
    'comment' => NULL,
    'translation' => 'ご注文一覧',
    'key' => 'e7f14265c098a669649edec83962e5c9',
  ),
  'c919538299a7025061e854a8c92dc280' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'ID',
    'comment' => NULL,
    'translation' => 'ID',
    'key' => 'c919538299a7025061e854a8c92dc280',
  ),
  'cc3afc43964043d85c1aebcf6a68c761' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Date',
    'comment' => NULL,
    'translation' => '日時',
    'key' => 'cc3afc43964043d85c1aebcf6a68c761',
  ),
  'e332c0186cac4209b4813938e49d555d' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Customer',
    'comment' => NULL,
    'translation' => 'お客様',
    'key' => 'e332c0186cac4209b4813938e49d555d',
  ),
  '63a5983f359a7399b768a76ac5461af4' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Total ex. VAT',
    'comment' => NULL,
    'translation' => '税抜合計',
    'key' => '63a5983f359a7399b768a76ac5461af4',
  ),
  'bbe917107d4d2c515dbe3fde3e2a22e5' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Total inc. VAT',
    'comment' => NULL,
    'translation' => '税込合計',
    'key' => 'bbe917107d4d2c515dbe3fde3e2a22e5',
  ),
  '0605e334f505d39c8e444f4678491536' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'The order list is empty',
    'comment' => NULL,
    'translation' => '注文はありません',
    'key' => '0605e334f505d39c8e444f4678491536',
  ),
  '60d64575a1cc2c354af79a530cc9cea7' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Register account information',
    'comment' => NULL,
    'translation' => 'お客様情報の登録',
    'key' => '60d64575a1cc2c354af79a530cc9cea7',
  ),
  '8df5b2d9057c84d2a1814523ea516bf0' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Input did not validate, fill in all fields',
    'comment' => NULL,
    'translation' => '入力が無効です。すべての項目を入力してください',
    'key' => '8df5b2d9057c84d2a1814523ea516bf0',
  ),
  '255d34f2a41736e10e7a18b65f0fa076' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Wish list',
    'comment' => NULL,
    'translation' => 'ウイッシュリスト',
    'key' => '255d34f2a41736e10e7a18b65f0fa076',
  ),
  '338a658efa0b9ff817c065d5477df4bd' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Empty wish list',
    'comment' => NULL,
    'translation' => 'ウイッシュリストを空にする',
    'key' => '338a658efa0b9ff817c065d5477df4bd',
  ),
  '8d152eeeb17eef0924dea527c1df5d8c' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Find',
    'comment' => NULL,
    'translation' => '検索',
    'key' => '8d152eeeb17eef0924dea527c1df5d8c',
  ),
  '4a57ce408543cdeb6a807f4c0982e5f4' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Product',
    'comment' => NULL,
    'translation' => '商品',
    'key' => '4a57ce408543cdeb6a807f4c0982e5f4',
  ),
  '4c5cd2143ee58a18a37759c9c909bd35' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Count',
    'comment' => NULL,
    'translation' => '数量',
    'key' => '4c5cd2143ee58a18a37759c9c909bd35',
  ),
  'eb1715a433d64c8ccc63002b1f59b15a' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'VAT',
    'comment' => NULL,
    'translation' => '税率',
    'key' => 'eb1715a433d64c8ccc63002b1f59b15a',
  ),
  '2543ae8572a2378ff1fb2d0e19ce907a' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Price ex. VAT',
    'comment' => NULL,
    'translation' => '価格（税抜）',
    'key' => '2543ae8572a2378ff1fb2d0e19ce907a',
  ),
  '025d0a340c0c7cd7af21f0c98c4f201d' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Price inc. VAT',
    'comment' => NULL,
    'translation' => '価格（税込）',
    'key' => '025d0a340c0c7cd7af21f0c98c4f201d',
  ),
  '89e6f99f23884aa3a28018f998d12b9a' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Discount',
    'comment' => NULL,
    'translation' => '割引',
    'key' => '89e6f99f23884aa3a28018f998d12b9a',
  ),
  'd220cfec214c1a257cce529383c44e75' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Discount groups',
    'comment' => NULL,
    'translation' => 'ディスカウント・グループ',
    'key' => 'd220cfec214c1a257cce529383c44e75',
  ),
  '52a0b24354849b9be297fc3ef9b19be9' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => '52a0b24354849b9be297fc3ef9b19be9',
  ),
  'e2638b3fa65e04558811efb08d306e80' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Edit discount group - %1',
    'comment' => NULL,
    'translation' => 'ディスカウント - %1 を編集',
    'key' => 'e2638b3fa65e04558811efb08d306e80',
  ),
  '3130d2feb5ae27008d85adcecadeedc9' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Group Name',
    'comment' => NULL,
    'translation' => 'グループ名',
    'key' => '3130d2feb5ae27008d85adcecadeedc9',
  ),
  '5d797ef3e94911b79fad216e1df8b2ee' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Defined rules',
    'comment' => NULL,
    'translation' => '定義されたルール',
    'key' => '5d797ef3e94911b79fad216e1df8b2ee',
  ),
  '027d1a7a047433c0636ed0551ea76f92' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Apply to',
    'comment' => NULL,
    'translation' => '適用先',
    'key' => '027d1a7a047433c0636ed0551ea76f92',
  ),
  '36fcc96ef5dc5d895d99880404a32083' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Name',
    'comment' => 'Customer name',
    'translation' => '名前',
    'key' => '36fcc96ef5dc5d895d99880404a32083',
  ),
  'dbb96836e50af8f0891052178051cbdb' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Attributes',
    'comment' => NULL,
    'translation' => '属性',
    'key' => 'dbb96836e50af8f0891052178051cbdb',
  ),
  'a6a28dead05e8ead1fc1ddf0512f4258' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Discount percent',
    'comment' => NULL,
    'translation' => '割引率',
    'key' => 'a6a28dead05e8ead1fc1ddf0512f4258',
  ),
  '1613ad1e91910fb8dcae8f0fef09f96b' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Rule settings',
    'comment' => NULL,
    'translation' => 'ルール設定',
    'key' => '1613ad1e91910fb8dcae8f0fef09f96b',
  ),
  'e251be43748c07a1f8a151ae076ba7aa' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Choose which classes, sections or objects ( products ) applied to this sub rule, \'Any\' means the rule will applied to all.',
    'comment' => NULL,
    'translation' => 'このサブ・ルールを適用するクラス、セクション、あるいはオブジェクト（商品）を選択してください。\'指定なし\' を選択すると、全てに適用されます。',
    'key' => 'e251be43748c07a1f8a151ae076ba7aa',
  ),
  'ae0acdfaad74e96601e8a0e2d1f0cd9c' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => 'ae0acdfaad74e96601e8a0e2d1f0cd9c',
  ),
  '8457c49d7f1298b7af1261675354f92e' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Section',
    'comment' => NULL,
    'translation' => 'セクション',
    'key' => '8457c49d7f1298b7af1261675354f92e',
  ),
  '0ba715b38fd2d05934af2f9b49308735' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Object',
    'comment' => NULL,
    'translation' => 'オブジェクト',
    'key' => '0ba715b38fd2d05934af2f9b49308735',
  ),
  'b7fcbaf8500cc9cc57cd412347396b28' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Not specified.',
    'comment' => NULL,
    'translation' => '未定義',
    'key' => 'b7fcbaf8500cc9cc57cd412347396b28',
  ),
  'f7ee38fd49542fba455e916ea20ac236' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Sort',
    'comment' => NULL,
    'translation' => 'ソート',
    'key' => 'f7ee38fd49542fba455e916ea20ac236',
  ),
  'fda797750602a1efadfd5b421e545be8' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Order total',
    'comment' => NULL,
    'translation' => 'お買上げ合計',
    'key' => 'fda797750602a1efadfd5b421e545be8',
  ),
  'a8555d45e6d6dac46989ddffb721b833' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'First name',
    'comment' => NULL,
    'translation' => '名',
    'key' => 'a8555d45e6d6dac46989ddffb721b833',
  ),
  '7d00a348630327a9924847358a0ac208' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Last name',
    'comment' => NULL,
    'translation' => '姓',
    'key' => '7d00a348630327a9924847358a0ac208',
  ),
  '07283d155d07a54a818564beb4899dae' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Email',
    'comment' => NULL,
    'translation' => 'e-mail',
    'key' => '07283d155d07a54a818564beb4899dae',
  ),
  '0e47b66fb092e56a8d656ae1e6e5addd' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Address',
    'comment' => NULL,
    'translation' => 'ご住所',
    'key' => '0e47b66fb092e56a8d656ae1e6e5addd',
  ),
  'dbfaa910010befb69e72361adf67bfd8' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Order summary',
    'comment' => NULL,
    'translation' => 'ご注文の要約',
    'key' => 'dbfaa910010befb69e72361adf67bfd8',
  ),
  '1976a9d2f2f5153aa90e02eaa0146f39' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Sort Result by',
    'comment' => NULL,
    'translation' => 'ソート順',
    'key' => '1976a9d2f2f5153aa90e02eaa0146f39',
  ),
  'cdb66961f5128229e49bca030389bbed' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Order Time',
    'comment' => NULL,
    'translation' => 'ご注文日時',
    'key' => 'cdb66961f5128229e49bca030389bbed',
  ),
  '2c39425e3d7bd3bb9a50e9edd776b7fe' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'User Name',
    'comment' => NULL,
    'translation' => 'ユーザ名',
    'key' => '2c39425e3d7bd3bb9a50e9edd776b7fe',
  ),
  '8a58488642449f762131992a0382d910' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Order ID',
    'comment' => NULL,
    'translation' => '注文ID',
    'key' => '8a58488642449f762131992a0382d910',
  ),
  '7f3da8c76e37bc63b3fd5ed9c848749b' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Ascending',
    'comment' => NULL,
    'translation' => '昇順',
    'key' => '7f3da8c76e37bc63b3fd5ed9c848749b',
  ),
  '2a34184fc3ecf0c29bf7e381c3cad0e5' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Sort ascending',
    'comment' => NULL,
    'translation' => '昇順にソート',
    'key' => '2a34184fc3ecf0c29bf7e381c3cad0e5',
  ),
  '657edaf0bc3cb5ebd801d65f4522d68b' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Descending',
    'comment' => NULL,
    'translation' => '降順',
    'key' => '657edaf0bc3cb5ebd801d65f4522d68b',
  ),
  '422b5af9aa223196a9e36b00bb579475' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Sort descending',
    'comment' => NULL,
    'translation' => '降順にソート',
    'key' => '422b5af9aa223196a9e36b00bb579475',
  ),
  '19a4c4ea16e7644b6cad270225c73c39' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Remove items',
    'comment' => NULL,
    'translation' => 'アイテムの削除',
    'key' => '19a4c4ea16e7644b6cad270225c73c39',
  ),
  '3206c2cd102fdae830769cc5fb12cca0' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Customer list',
    'comment' => NULL,
    'translation' => '顧客一覧',
    'key' => '3206c2cd102fdae830769cc5fb12cca0',
  ),
  '2c5b7a851be51383ab971211e9599b3c' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Number of orders',
    'comment' => NULL,
    'translation' => '注文数',
    'key' => '2c5b7a851be51383ab971211e9599b3c',
  ),
  '6790556d12486353fb778ffffba289e8' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'The customer list is empty',
    'comment' => NULL,
    'translation' => '顧客はありません',
    'key' => '6790556d12486353fb778ffffba289e8',
  ),
  '270f00b3520ea529a378ce7423ca2bea' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Customer Information',
    'comment' => NULL,
    'translation' => 'お客様情報',
    'key' => '270f00b3520ea529a378ce7423ca2bea',
  ),
  '24081c93d6e411c7e1b9694c035e2ecb' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Purchase list',
    'comment' => NULL,
    'translation' => 'ご購入一覧',
    'key' => '24081c93d6e411c7e1b9694c035e2ecb',
  ),
  'dbadcaa399feeb529a46536ff33d8286' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Amount',
    'comment' => NULL,
    'translation' => '総数',
    'key' => 'dbadcaa399feeb529a46536ff33d8286',
  ),
  '26c5e911818279f99373cfb9bff2083f' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'View',
    'comment' => NULL,
    'translation' => '表示',
    'key' => '26c5e911818279f99373cfb9bff2083f',
  ),
  '9319c332b915f03fa41a5248ce65ede5' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'SUM:',
    'comment' => NULL,
    'translation' => '合計:',
    'key' => '9319c332b915f03fa41a5248ce65ede5',
  ),
  '64a35a131d18eb52c701884b52c40a34' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Are you sure you want to remove order: ',
    'comment' => NULL,
    'translation' => 'ご注文を削除しますか 注文ID:',
    'key' => '64a35a131d18eb52c701884b52c40a34',
  ),
  '0898077179ecc3cccd037f7ed654bf0f' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Selected options',
    'comment' => NULL,
    'translation' => '選択したオプション',
    'key' => '0898077179ecc3cccd037f7ed654bf0f',
  ),
  '617786fd4a3c447356ea0d8b05fa6bb0' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Statistics',
    'comment' => NULL,
    'translation' => '統計',
    'key' => '617786fd4a3c447356ea0d8b05fa6bb0',
  ),
  '5f774307d45acaa2f357d4098acf8a5f' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Your account information',
    'comment' => NULL,
    'translation' => 'お客様情報',
    'key' => '5f774307d45acaa2f357d4098acf8a5f',
  ),
  '7c6628ecbf42e25e63370a0c0c20ab20' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Input did not validate, all fields marked with * must be filled in',
    'comment' => NULL,
    'translation' => '入力が正しくありません。 * 印の付いた項目は入力必須です',
    'key' => '7c6628ecbf42e25e63370a0c0c20ab20',
  ),
  '088265cbaea8ada2ca34104f03f74265' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'All fields marked with * must be filled in.',
    'comment' => NULL,
    'translation' => '* 印の付いた項目は入力必須です。',
    'key' => '088265cbaea8ada2ca34104f03f74265',
  ),
  'eb0ab176c5ee807847f8c1dfa36cf4f2' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Customer information',
    'comment' => NULL,
    'translation' => '顧客情報',
    'key' => 'eb0ab176c5ee807847f8c1dfa36cf4f2',
  ),
  '743482fe54ab870f0ae59255d4928e7d' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Shipping address',
    'comment' => NULL,
    'translation' => '出荷先住所',
    'key' => '743482fe54ab870f0ae59255d4928e7d',
  ),
  '480c29b7af307d2b09bd433500a9686b' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Company',
    'comment' => NULL,
    'translation' => '会社・法人名',
    'key' => '480c29b7af307d2b09bd433500a9686b',
  ),
  '087fc9390a01b47be5418e06ea66b793' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Street',
    'comment' => NULL,
    'translation' => '住所',
    'key' => '087fc9390a01b47be5418e06ea66b793',
  ),
  'cee797a610290dd2009dcdbebdefe297' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Zip',
    'comment' => NULL,
    'translation' => '郵便番号',
    'key' => 'cee797a610290dd2009dcdbebdefe297',
  ),
  '4845556acaee3410d6718b1371335bf4' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Place',
    'comment' => NULL,
    'translation' => '市区町村',
    'key' => '4845556acaee3410d6718b1371335bf4',
  ),
  '5c429f518cde376383cce79b3731eee7' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'State',
    'comment' => NULL,
    'translation' => '都道府県',
    'key' => '5c429f518cde376383cce79b3731eee7',
  ),
  '52ff435b483b8fafc132d127e6649b5e' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Comment',
    'comment' => NULL,
    'translation' => 'コメント',
    'key' => '52ff435b483b8fafc132d127e6649b5e',
  ),
  'df28759e77fee7aeeeeceec309e5e4a9' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Continue',
    'comment' => NULL,
    'translation' => '次へ',
    'key' => 'df28759e77fee7aeeeeceec309e5e4a9',
  ),
  '57b8606e1ee592ea30eb254307ddb956' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Attempted to add object without price to basket.',
    'comment' => NULL,
    'translation' => '価格未設定の商品を買い物かごに追加しようとしました。',
    'key' => '57b8606e1ee592ea30eb254307ddb956',
  ),
  'ee9524070162b99a6b576cc1d033b2aa' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Your payment was aborted.',
    'comment' => NULL,
    'translation' => '決済は中止されました。',
    'key' => 'ee9524070162b99a6b576cc1d033b2aa',
  ),
  '9d882a5a704b9d6166cabf13fc6c2834' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Order %order_id [%order_status]',
    'comment' => NULL,
    'translation' => '注文ID %order_id [%order_status]',
    'key' => '9d882a5a704b9d6166cabf13fc6c2834',
  ),
  '25d5345ece74c87e44a63a1cfcb8253d' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Order history',
    'comment' => NULL,
    'translation' => 'ご注文ステータス（履歴）',
    'key' => '25d5345ece74c87e44a63a1cfcb8253d',
  ),
  'f5f81a152facae30c4003c9c2931fa9f' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'We did not get a confirmation from the payment server.',
    'comment' => NULL,
    'translation' => '決済サービスから確認が取得できませんでした。',
    'key' => 'f5f81a152facae30c4003c9c2931fa9f',
  ),
  '57777c8192e8ecbdb7cd8277baa28380' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Waiting for a response from the payment server. This can take some time.',
    'comment' => NULL,
    'translation' => '決済サービスからのレスポンスを待っています。多少時間がかかります。',
    'key' => '57777c8192e8ecbdb7cd8277baa28380',
  ),
  'b2bf4b578aeeee8ae2a405541471f4ec' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Retrying to get a valid response.',
    'comment' => NULL,
    'translation' => '有効なレスポンスを再取得中です。',
    'key' => 'b2bf4b578aeeee8ae2a405541471f4ec',
  ),
  '09a78d94fa8cc4f75b9bfa5d9c2fd481' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Retry ',
    'comment' => NULL,
    'translation' => '再試行',
    'key' => '09a78d94fa8cc4f75b9bfa5d9c2fd481',
  ),
  '688260858fcd2c9f5f1e0667e8157d72' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'out of ',
    'comment' => NULL,
    'translation' => '/',
    'key' => '688260858fcd2c9f5f1e0667e8157d72',
  ),
  '487d32f62cbb2e9f22e06a6e8a2de48b' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'If your page does not automatically refresh then press the refresh button manually.',
    'comment' => NULL,
    'translation' => 'ページが自動的に更新されない場合は、更新ボタンをクリックしてください。',
    'key' => '487d32f62cbb2e9f22e06a6e8a2de48b',
  ),
  '305bdd6b8bf9dcab5ff1494b1d70560b' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Order status',
    'comment' => NULL,
    'translation' => '注文ステータス',
    'key' => '305bdd6b8bf9dcab5ff1494b1d70560b',
  ),
  '775fbb20e291ed507e04444294973394' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Active',
    'comment' => NULL,
    'translation' => '有効',
    'key' => '775fbb20e291ed507e04444294973394',
  ),
  '344aabb332d8e3a46519010d100624f8' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Is active',
    'comment' => NULL,
    'translation' => '有効',
    'key' => '344aabb332d8e3a46519010d100624f8',
  ),
  '7c3f8019e14b56c94888958f57ab18ba' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Is inactive',
    'comment' => NULL,
    'translation' => '無効',
    'key' => '7c3f8019e14b56c94888958f57ab18ba',
  ),
  'aac4ab0315620b2fd79a01aef87fbd09' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Please contact the owner of the webshop and provide your order ID',
    'comment' => NULL,
    'translation' => 'ショップに連絡してください（必ず注文IDを添えてください）',
    'key' => 'aac4ab0315620b2fd79a01aef87fbd09',
  ),
  '1082e3129a7c21f6b7f4808d9993bdcb' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Archive list',
    'comment' => NULL,
    'translation' => '保管一覧',
    'key' => '1082e3129a7c21f6b7f4808d9993bdcb',
  ),
  '0456ccb979833e965e3d035bb82a4275' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Unarchive',
    'comment' => NULL,
    'translation' => 'アクティブにする',
    'key' => '0456ccb979833e965e3d035bb82a4275',
  ),
  '77aa45e76b6726ba09958fd44cc52f80' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Archive',
    'comment' => NULL,
    'translation' => 'アーカイブ ',
    'key' => '77aa45e76b6726ba09958fd44cc52f80',
  ),
  'c1572656e261f70245d9e382e64fabc7' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'All Years',
    'comment' => NULL,
    'translation' => '年の指定なし',
    'key' => 'c1572656e261f70245d9e382e64fabc7',
  ),
  '9dc085c048c586f6a37313d588516601' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'All Months',
    'comment' => NULL,
    'translation' => '月の指定なし',
    'key' => '9dc085c048c586f6a37313d588516601',
  ),
  'dbe386addba0548133554583520cf85a' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'VAT is unknown',
    'comment' => NULL,
    'translation' => '税が不明です',
    'key' => 'dbe386addba0548133554583520cf85a',
  ),
  'ac12a207d02055573142038b114de8fe' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'VAT percentage is not yet known for some of the items being purchased.',
    'comment' => NULL,
    'translation' => '購入するいくつかのアイテムの税率は不明です。',
    'key' => 'ac12a207d02055573142038b114de8fe',
  ),
  'ccb26e4dcf6430fa8f2cc9b5f0abf00b' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'This probably means that some information about you is not yet available and will be obtained during checkout.',
    'comment' => NULL,
    'translation' => '現段階ではあなたの情報の一部が不足していますが、決済までに全ての情報が補充されることを示しています。',
    'key' => 'ccb26e4dcf6430fa8f2cc9b5f0abf00b',
  ),
  '30e6f71db511b65791bde2a0eb7c7c9d' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'unknown',
    'comment' => NULL,
    'translation' => '不明',
    'key' => '30e6f71db511b65791bde2a0eb7c7c9d',
  ),
  '123bcfaa4a4d25e9c1f679c37d266102' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Back',
    'comment' => NULL,
    'translation' => '戻る',
    'key' => '123bcfaa4a4d25e9c1f679c37d266102',
  ),
  'c4e3a7ea2c8f179ea08253180c740596' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Incorrect quantity! The quantity of the product(s) must be numeric and not less than 1.',
    'comment' => NULL,
    'translation' => '数量が不正です。1以上の数字を入力してください。',
    'key' => 'c4e3a7ea2c8f179ea08253180c740596',
  ),
  'dee471f96294493c86079f421ec47ea5' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'You have chosen invalid combination of options',
    'comment' => NULL,
    'translation' => '選択無効な組み合わせを選択しました。',
    'key' => 'dee471f96294493c86079f421ec47ea5',
  ),
  '7d812c482dfb2a330ff58605e19ab5cd' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Country/region',
    'comment' => NULL,
    'translation' => '国/県',
    'key' => '7d812c482dfb2a330ff58605e19ab5cd',
  ),
  '1b0c449850e3d12ba5ad4d6a916ecabd' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'The following items were removed from your basket because the products were changed',
    'comment' => NULL,
    'translation' => 'プロダクトが変更されたため、次のアイテムは買い物かごから削除されました。',
    'key' => '1b0c449850e3d12ba5ad4d6a916ecabd',
  ),
  '010adb101e96f89abb3b8570e58447bd' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Total price ex. VAT',
    'comment' => NULL,
    'translation' => '合計（税抜）',
    'key' => '010adb101e96f89abb3b8570e58447bd',
  ),
  'ab697da971e8b88ad0e1e457bcafe410' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Total price inc. VAT',
    'comment' => NULL,
    'translation' => '合計（税込）',
    'key' => 'ab697da971e8b88ad0e1e457bcafe410',
  ),
  '993f49048b52074cc326219f8338ab6a' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Subtotal ex. VAT',
    'comment' => NULL,
    'translation' => '小計（税抜）',
    'key' => '993f49048b52074cc326219f8338ab6a',
  ),
  '68108ddb4abd4a105e36d9222db14ba1' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Subtotal inc. VAT',
    'comment' => NULL,
    'translation' => '小計（税込）',
    'key' => '68108ddb4abd4a105e36d9222db14ba1',
  ),
  'd7f584553a9eaeb8ba825a58b9abeaa8' => 
  array (
    'context' => 'design/standard/shop',
    'source' => 'Payment was canceled for an unknown reason. Please try to buy again.',
    'comment' => NULL,
    'translation' => '不明な理由で支払いプロセスは中止されました。もう一度試してください。',
    'key' => 'd7f584553a9eaeb8ba825a58b9abeaa8',
  ),
);
?>
