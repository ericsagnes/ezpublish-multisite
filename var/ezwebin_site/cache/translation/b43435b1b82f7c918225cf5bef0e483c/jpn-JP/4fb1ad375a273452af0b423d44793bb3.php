<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/search/stats',
);

$TranslationRoot = array (
  '44ce9165136a9c3b231fce1eb757a697' => 
  array (
    'context' => 'design/admin/search/stats',
    'source' => 'Search statistics',
    'comment' => NULL,
    'translation' => '検索統計',
    'key' => '44ce9165136a9c3b231fce1eb757a697',
  ),
  '00d9a701c611e0faaab39954e172429e' => 
  array (
    'context' => 'design/admin/search/stats',
    'source' => 'Phrase',
    'comment' => NULL,
    'translation' => 'フレーズ',
    'key' => '00d9a701c611e0faaab39954e172429e',
  ),
  'b09e7fb50f96072de1ef3be7cf3009a4' => 
  array (
    'context' => 'design/admin/search/stats',
    'source' => 'Number of phrases',
    'comment' => NULL,
    'translation' => 'フレーズ数',
    'key' => 'b09e7fb50f96072de1ef3be7cf3009a4',
  ),
  'd2e4c9faaaefbd9ab650b70417e83f9a' => 
  array (
    'context' => 'design/admin/search/stats',
    'source' => 'Average result returned',
    'comment' => NULL,
    'translation' => '平均結果件数',
    'key' => 'd2e4c9faaaefbd9ab650b70417e83f9a',
  ),
  '16a6ce1b5d4ae7c516264f0d865f22e8' => 
  array (
    'context' => 'design/admin/search/stats',
    'source' => 'Reset statistics',
    'comment' => NULL,
    'translation' => '統計のリセット',
    'key' => '16a6ce1b5d4ae7c516264f0d865f22e8',
  ),
  '0003219920d87747be3995f607ea4e65' => 
  array (
    'context' => 'design/admin/search/stats',
    'source' => 'The list is empty.',
    'comment' => NULL,
    'translation' => 'リストは空です',
    'key' => '0003219920d87747be3995f607ea4e65',
  ),
  'c2c470f219e0b55ba74df8056a5939d0' => 
  array (
    'context' => 'design/admin/search/stats',
    'source' => 'Clear the search log.',
    'comment' => NULL,
    'translation' => '検索ログをクリア',
    'key' => 'c2c470f219e0b55ba74df8056a5939d0',
  ),
);
?>
