<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/content/upload',
);

$TranslationRoot = array (
  'cb2a5ecfd97fd71df25e63c6c88e5e9b' => 
  array (
    'context' => 'design/standard/content/upload',
    'source' => 'Upload file',
    'comment' => NULL,
    'translation' => 'ファイルをアップロード',
    'key' => 'cb2a5ecfd97fd71df25e63c6c88e5e9b',
  ),
  '67b580eacee0a653430114b495325307' => 
  array (
    'context' => 'design/standard/content/upload',
    'source' => 'Some errors occurred',
    'comment' => NULL,
    'translation' => 'エラーが発生しました',
    'key' => '67b580eacee0a653430114b495325307',
  ),
  '25b1f83193b50c1d763158461cf66aff' => 
  array (
    'context' => 'design/standard/content/upload',
    'source' => 'Location',
    'comment' => NULL,
    'translation' => '配置先',
    'key' => '25b1f83193b50c1d763158461cf66aff',
  ),
  'ca033c710b44f4251673acda0dc0a497' => 
  array (
    'context' => 'design/standard/content/upload',
    'source' => 'Automatic',
    'comment' => NULL,
    'translation' => '自動',
    'key' => 'ca033c710b44f4251673acda0dc0a497',
  ),
  '99d7db5547e189e43f3bf186f57f4f77' => 
  array (
    'context' => 'design/standard/content/upload',
    'source' => 'Upload',
    'comment' => NULL,
    'translation' => 'アップロード',
    'key' => '99d7db5547e189e43f3bf186f57f4f77',
  ),
  'e252388d76b60d37a227fcc19ed84765' => 
  array (
    'context' => 'design/standard/content/upload',
    'source' => 'Click here to upload a file. The file will be placed within the location that is specified using the dropdown menu on the top.',
    'comment' => NULL,
    'translation' => 'ファイルをアップロードするにはここをクリックします。ファイルは上のメニューで指定した場所に配置されます。',
    'key' => 'e252388d76b60d37a227fcc19ed84765',
  ),
  'dae22352f0689cf8af36342cec15da7b' => 
  array (
    'context' => 'design/standard/content/upload',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => 'dae22352f0689cf8af36342cec15da7b',
  ),
  '9ca9bdb08f3c38c1da80a7a6e209e380' => 
  array (
    'context' => 'design/standard/content/upload',
    'source' => 'File',
    'comment' => NULL,
    'translation' => 'ファイル名',
    'key' => '9ca9bdb08f3c38c1da80a7a6e209e380',
  ),
  '58d5bd9b5d67c772419707479f5d15fb' => 
  array (
    'context' => 'design/standard/content/upload',
    'source' => 'Choose a file from your locale machine then click the "Upload" button. An object will be created according to file type and placed in your chosen location.',
    'comment' => NULL,
    'translation' => 'お使いのパソコンからファイルを選択した後、”アップロード”ボタンをクリックしてください。ファイルタイプに合わせたオブジェクトが選択された配置先に作成されます。',
    'key' => '58d5bd9b5d67c772419707479f5d15fb',
  ),
);
?>
