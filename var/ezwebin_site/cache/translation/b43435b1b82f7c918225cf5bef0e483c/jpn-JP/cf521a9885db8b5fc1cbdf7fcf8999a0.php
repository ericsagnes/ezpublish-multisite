<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/translationnew',
);

$TranslationRoot = array (
  '8502eab8f676998e5f1f131368c7b748' => 
  array (
    'context' => 'design/admin/content/translationnew',
    'source' => 'Translation',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => '8502eab8f676998e5f1f131368c7b748',
  ),
  '18a25c8a710eb884978066d749a3aaca' => 
  array (
    'context' => 'design/admin/content/translationnew',
    'source' => 'New translation for content',
    'comment' => NULL,
    'translation' => '新規コンテンツ翻訳',
    'key' => '18a25c8a710eb884978066d749a3aaca',
  ),
  'd8d1d171ff3d0a0374d75ee2d53b15df' => 
  array (
    'context' => 'design/admin/content/translationnew',
    'source' => 'Custom',
    'comment' => NULL,
    'translation' => 'カスタム',
    'key' => 'd8d1d171ff3d0a0374d75ee2d53b15df',
  ),
  '8149a468481f94f8e3b0abaeba9859d6' => 
  array (
    'context' => 'design/admin/content/translationnew',
    'source' => 'Name of custom translation',
    'comment' => NULL,
    'translation' => 'カスタム翻訳名',
    'key' => '8149a468481f94f8e3b0abaeba9859d6',
  ),
  'f275a5ffab8233a4b9e1f5d7c9d2f0cc' => 
  array (
    'context' => 'design/admin/content/translationnew',
    'source' => 'Locale for custom translation',
    'comment' => NULL,
    'translation' => 'カスタム翻訳ロケール',
    'key' => 'f275a5ffab8233a4b9e1f5d7c9d2f0cc',
  ),
  '4949ec3d5442aa8a747e6f61bb37af27' => 
  array (
    'context' => 'design/admin/content/translationnew',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => '4949ec3d5442aa8a747e6f61bb37af27',
  ),
  '18b54b34bcb366694e720f3c622de055' => 
  array (
    'context' => 'design/admin/content/translationnew',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '18b54b34bcb366694e720f3c622de055',
  ),
);
?>
