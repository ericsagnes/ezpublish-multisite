<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/content/edit_languages',
);

$TranslationRoot = array (
  'db11ade9f3c420059a14c3942a223158' => 
  array (
    'context' => 'design/ezwebin/content/edit_languages',
    'source' => 'Existing languages',
    'comment' => NULL,
    'translation' => '使用中の言語',
    'key' => 'db11ade9f3c420059a14c3942a223158',
  ),
  '4895bd73673099b9719163c7bf4d8ae6' => 
  array (
    'context' => 'design/ezwebin/content/edit_languages',
    'source' => 'New languages',
    'comment' => NULL,
    'translation' => '新しい言語',
    'key' => '4895bd73673099b9719163c7bf4d8ae6',
  ),
  'cbd54a376f592276317e0e05eb82389a' => 
  array (
    'context' => 'design/ezwebin/content/edit_languages',
    'source' => 'You do not have permission to edit the object in any available languages.',
    'comment' => NULL,
    'translation' => '言語に関わらず、編集の権限がありません.',
    'key' => 'cbd54a376f592276317e0e05eb82389a',
  ),
  '16c6aaa3f076df38e2393844dceb0f44' => 
  array (
    'context' => 'design/ezwebin/content/edit_languages',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => '16c6aaa3f076df38e2393844dceb0f44',
  ),
  '327a7fcaa7ebdac23c3486dd3fe8db3c' => 
  array (
    'context' => 'design/ezwebin/content/edit_languages',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '327a7fcaa7ebdac23c3486dd3fe8db3c',
  ),
  '276cc6327a9781aaa10cbf895835a2ab' => 
  array (
    'context' => 'design/ezwebin/content/edit_languages',
    'source' => 'Select the language you want to use when editing the object.',
    'comment' => NULL,
    'translation' => 'オブジェクトを編集する時に参考にしたい言語を選択してください。',
    'key' => '276cc6327a9781aaa10cbf895835a2ab',
  ),
  'e32759d0c57253e0967dd8c630831c83' => 
  array (
    'context' => 'design/ezwebin/content/edit_languages',
    'source' => 'Select the language you want to add to the object.',
    'comment' => NULL,
    'translation' => 'オブジェクトに追加する言語を選択してください',
    'key' => 'e32759d0c57253e0967dd8c630831c83',
  ),
  '4840393ebeefbc0acab9a642f229695e' => 
  array (
    'context' => 'design/ezwebin/content/edit_languages',
    'source' => 'Select the language the new translation will be based on.',
    'comment' => NULL,
    'translation' => '新しい翻訳をベースにする言語を選択してください',
    'key' => '4840393ebeefbc0acab9a642f229695e',
  ),
  'd96a8380dd45f1e79fdde7115c4cf1d5' => 
  array (
    'context' => 'design/ezwebin/content/edit_languages',
    'source' => 'Use an empty, untranslated draft',
    'comment' => NULL,
    'translation' => '翻訳されていない、空のドラフトを使う',
    'key' => 'd96a8380dd45f1e79fdde7115c4cf1d5',
  ),
  '7b905f2e5af6f33e892cf0a7d4799de8' => 
  array (
    'context' => 'design/ezwebin/content/edit_languages',
    'source' => 'You do not have permission to create a translation in another language.',
    'comment' => NULL,
    'translation' => '違う言語で翻訳を作る権限がありません。',
    'key' => '7b905f2e5af6f33e892cf0a7d4799de8',
  ),
  'cea588185d6975a996e615e90ca64a1e' => 
  array (
    'context' => 'design/ezwebin/content/edit_languages',
    'source' => 'However, you can select one of the following languages for editing.',
    'comment' => NULL,
    'translation' => 'ですが、次の言語で編集することができます。',
    'key' => 'cea588185d6975a996e615e90ca64a1e',
  ),
);
?>
