<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/full/product',
);

$TranslationRoot = array (
  '6f01022eeaaeef331d4db26e6b199956' => 
  array (
    'context' => 'design/ezwebin/full/product',
    'source' => 'Add to basket',
    'comment' => NULL,
    'translation' => '買い物カゴへ追加',
    'key' => '6f01022eeaaeef331d4db26e6b199956',
  ),
  'd991859e370257f7ae4f1ccb2a11b9ec' => 
  array (
    'context' => 'design/ezwebin/full/product',
    'source' => 'Add to wish list',
    'comment' => NULL,
    'translation' => 'ウイッシュリストへ追加',
    'key' => 'd991859e370257f7ae4f1ccb2a11b9ec',
  ),
  '1d5b95d1abf39fedf2da93875fce6a73' => 
  array (
    'context' => 'design/ezwebin/full/product',
    'source' => 'People who bought this also bought',
    'comment' => NULL,
    'translation' => 'これを購入した人はこんな商品を買っています',
    'key' => '1d5b95d1abf39fedf2da93875fce6a73',
  ),
);
?>
