<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/state/edit',
);

$TranslationRoot = array (
  '26121edd7b14839730f143df16246ef8' => 
  array (
    'context' => 'kernel/state/edit',
    'source' => 'Identifier: input required',
    'comment' => NULL,
    'translation' => '識別子: 入力が必要です',
    'key' => '26121edd7b14839730f143df16246ef8',
  ),
  '32a07e8cfe429099b7c00943a7cf7fde' => 
  array (
    'context' => 'kernel/state/edit',
    'source' => 'Identifier: invalid, it can only consist of characters in the range a-z, 0-9 and underscore.',
    'comment' => NULL,
    'translation' => '識別子: 不正。使用できる文字は英小文字と数字およびアンダースコアです。',
    'key' => '32a07e8cfe429099b7c00943a7cf7fde',
  ),
  '50c5ab449d9fbd367e6af00b93935801' => 
  array (
    'context' => 'kernel/state/edit',
    'source' => 'Identifier: invalid, maximum %max characters allowed.',
    'comment' => NULL,
    'translation' => '識別子: 不正です、最大で%max文字利用出来ます。',
    'key' => '50c5ab449d9fbd367e6af00b93935801',
  ),
  '0e3a2b0dd302b7beb73d854322fc7e4a' => 
  array (
    'context' => 'kernel/state/edit',
    'source' => 'Identifier: a content object state group with this identifier already exists, please give another identifier',
    'comment' => NULL,
    'translation' => '識別子: この識別子を利用するコンテンツオブジェクトステートグループは既に存在します。他の識別子を選択して下さい',
    'key' => '0e3a2b0dd302b7beb73d854322fc7e4a',
  ),
  'da18d1c68e057a8415c4874a15c6b3dd' => 
  array (
    'context' => 'kernel/state/edit',
    'source' => '%language_name: this language is the default but neither name or description were provided for this language',
    'comment' => NULL,
    'translation' => '%language_name: この言語がデフォルトに設定されていますが、名前も説明文も入力されていません',
    'key' => 'da18d1c68e057a8415c4874a15c6b3dd',
  ),
  'c91ff2281e48944ecd8a9ccdfadd9e5b' => 
  array (
    'context' => 'kernel/state/edit',
    'source' => 'Translations: you need to add at least one localization',
    'comment' => NULL,
    'translation' => '翻訳: 一つ以上の翻訳を追加する必要があります',
    'key' => 'c91ff2281e48944ecd8a9ccdfadd9e5b',
  ),
  '9bdebd9db20e33289222a88496571cf9' => 
  array (
    'context' => 'kernel/state/edit',
    'source' => 'Translations: there are multiple localizations but you did not specify which is the default one',
    'comment' => NULL,
    'translation' => '翻訳: 複数の翻訳が存在しますが、デフォルトが指定されていません',
    'key' => '9bdebd9db20e33289222a88496571cf9',
  ),
  '598a2f3cbd6f01a6ea22ae42b0cd968f' => 
  array (
    'context' => 'kernel/state/edit',
    'source' => 'Identifier: identifiers starting with "ez" are reserved.',
    'comment' => NULL,
    'translation' => '識別子: "ez"で始まる識別子は利用出来ません。',
    'key' => '598a2f3cbd6f01a6ea22ae42b0cd968f',
  ),
  '3b26600818960951220b51a5697ba9d0' => 
  array (
    'context' => 'kernel/state/edit',
    'source' => 'Name in %language_name is too long. Maximum 45 characters allowed.',
    'comment' => NULL,
    'translation' => '%language_nameでの名前が長すぎます。最大で45文字までです。',
    'key' => '3b26600818960951220b51a5697ba9d0',
  ),
  '6e90f5cef431396082c60d1684d6b388' => 
  array (
    'context' => 'kernel/state/edit',
    'source' => 'Name in %language_name: input required',
    'comment' => NULL,
    'translation' => '%language_nameでの名前: 入力が必要です',
    'key' => '6e90f5cef431396082c60d1684d6b388',
  ),
);
?>
