<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/location',
);

$TranslationRoot = array (
  'a3752fd658f54def2023726d3358277e' => 
  array (
    'context' => 'design/standard/location',
    'source' => 'Removal of locations',
    'comment' => NULL,
    'translation' => '配置先の削除',
    'key' => 'a3752fd658f54def2023726d3358277e',
  ),
  'bd250bf2bc22fb2e0a475a08d09debca' => 
  array (
    'context' => 'design/standard/location',
    'source' => 'Some of the locations you tried to remove has children, are you really sure you want to remove those locations?
If you do all the children will be removed as well.',
    'comment' => NULL,
    'translation' => '削除対象の配置先の一部は子アイテムを持っています。 
これらの配置先を削除してもよいですか？
子アイテムも同時に削除されます。',
    'key' => 'bd250bf2bc22fb2e0a475a08d09debca',
  ),
  '5aafcbfe5aa3ef0e01ab2141641ca669' => 
  array (
    'context' => 'design/standard/location',
    'source' => 'Path',
    'comment' => NULL,
    'translation' => 'パス',
    'key' => '5aafcbfe5aa3ef0e01ab2141641ca669',
  ),
  '20bac4be61718963a4d902e3c6aae18c' => 
  array (
    'context' => 'design/standard/location',
    'source' => 'Count',
    'comment' => NULL,
    'translation' => 'アイテム数',
    'key' => '20bac4be61718963a4d902e3c6aae18c',
  ),
  'b20c3c9000c58fbe87665100b961ca55' => 
  array (
    'context' => 'design/standard/location',
    'source' => 'Remove locations',
    'comment' => NULL,
    'translation' => '配置先の削除',
    'key' => 'b20c3c9000c58fbe87665100b961ca55',
  ),
  '349a8e898f6bb2c161d0bbff9d52255d' => 
  array (
    'context' => 'design/standard/location',
    'source' => 'Cancel removal',
    'comment' => NULL,
    'translation' => '削除のキャンセル',
    'key' => '349a8e898f6bb2c161d0bbff9d52255d',
  ),
  '7889e2927c949b98bd94939db251c8bb' => 
  array (
    'context' => 'design/standard/location',
    'source' => 'Please wait while your content is being published',
    'comment' => NULL,
    'translation' => 'コンテントが公開されるまでにお待ちください',
    'key' => '7889e2927c949b98bd94939db251c8bb',
  ),
  '29eabe1211009a022bd3af65d2c58f1e' => 
  array (
    'context' => 'design/standard/location',
    'source' => 'Your content has been published successfully',
    'comment' => NULL,
    'translation' => 'コンテントは公開されました',
    'key' => '29eabe1211009a022bd3af65d2c58f1e',
  ),
  '58a2b055b28403d71bd4b5a9cade3b6b' => 
  array (
    'context' => 'design/standard/location',
    'source' => 'View the published item',
    'comment' => NULL,
    'translation' => '公開されたアイテムをの確認',
    'key' => '58a2b055b28403d71bd4b5a9cade3b6b',
  ),
  '5fb0c41a84d3fee0d277a44f68095745' => 
  array (
    'context' => 'design/standard/location',
    'source' => 'Publishing has been deferred to crontab and will be published when the operation resumes. The object is also listed in your dashboard under pending items.',
    'comment' => NULL,
    'translation' => '公開はクロンタブに繰延され、操作が再開されましたら公開されます。ダッシュボードのペンデイングアイテムにオブジェクトがリストアップされています。',
    'key' => '5fb0c41a84d3fee0d277a44f68095745',
  ),
  'edde45273df3376de400c768cc966905' => 
  array (
    'context' => 'design/standard/location',
    'source' => 'View your pending content',
    'comment' => NULL,
    'translation' => 'ペンディングコンテンツの確認',
    'key' => 'edde45273df3376de400c768cc966905',
  ),
);
?>
