<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'extension/ezstarrating/datatype',
);

$TranslationRoot = array (
  '38d6eca5b88793c86f772601c6e06214' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => 'Star Rating',
    'comment' => 'Datatype name',
    'translation' => 'スターレーティング',
    'key' => '38d6eca5b88793c86f772601c6e06214',
  ),
  'fea0d418cd95058c9bcb55f88e96f6b0' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => 'Currently %current_rating out of 5 Stars.',
    'comment' => NULL,
    'translation' => '現在の評価は5内で%current_rating。',
    'key' => 'fea0d418cd95058c9bcb55f88e96f6b0',
  ),
  '216c26edacf64b238077a46e6a325e53' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => 'Rate %rating stars out of 5',
    'comment' => NULL,
    'translation' => '5内で評価を付ける',
    'key' => '216c26edacf64b238077a46e6a325e53',
  ),
  '73a1070843c30b32bc95adddf2a214b5' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => 'Rating: %current_rating/5',
    'comment' => NULL,
    'translation' => '評価:%current_rating/5',
    'key' => '73a1070843c30b32bc95adddf2a214b5',
  ),
  'd84cf1cb76fe4aeee908ddcfdb3b50e6' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => '%rating_count votes cast',
    'comment' => NULL,
    'translation' => '%rating_count投票',
    'key' => 'd84cf1cb76fe4aeee908ddcfdb3b50e6',
  ),
  '543f29ab21c8c2124f22e474e21ac792' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => 'Thank you for rating!',
    'comment' => 'When rating',
    'translation' => '評価ありがとうございます!',
    'key' => '543f29ab21c8c2124f22e474e21ac792',
  ),
  '55bc436206454cc8c7c09dc01120f883' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => 'You have already rated this page, you can only rate it once!',
    'comment' => 'When rating',
    'translation' => 'このページをすでに評価しました、評価は一回しかできないです!',
    'key' => '55bc436206454cc8c7c09dc01120f883',
  ),
  'a27bdac1b348f6933e9cb581046988dd' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => '%login_link_startLog in%login_link_end or %create_link_startcreate a user account%create_link_end to rate this page.',
    'comment' => NULL,
    'translation' => 'このページを評価するために、%login_link_startログイン%login_link_endか%create_link_startユーザ登録%create_link_endをしてください。',
    'key' => 'a27bdac1b348f6933e9cb581046988dd',
  ),
  '649a108e11e4ccc4342ee924f59856f1' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => '%login_link_startLog in%login_link_end to rate this page.',
    'comment' => NULL,
    'translation' => 'このページを評価するために、%login_link_startログイン%login_link_endしてください。',
    'key' => '649a108e11e4ccc4342ee924f59856f1',
  ),
  'c664df51b98cb0889884706cca59646b' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => 'Disabled',
    'comment' => NULL,
    'translation' => '無効',
    'key' => 'c664df51b98cb0889884706cca59646b',
  ),
  '52f99c2bbed72f91e7b70f826195e3e5' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => 'disabled',
    'comment' => NULL,
    'translation' => '無効',
    'key' => '52f99c2bbed72f91e7b70f826195e3e5',
  ),
  'da109c0bc2dbfa6e2d9e2147c365c561' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => 'Your rating has been changed, thanks for rating!',
    'comment' => 'When rating',
    'translation' => '評価を変更しました、評価ありがとうございます!',
    'key' => 'da109c0bc2dbfa6e2d9e2147c365c561',
  ),
  '00e363e7121f02220e05edb8b12373fd' => 
  array (
    'context' => 'extension/ezstarrating/datatype',
    'source' => 'You don\'t have access to rate this page.',
    'comment' => NULL,
    'translation' => 'このページを評価できる権限がありません。',
    'key' => '00e363e7121f02220e05edb8b12373fd',
  ),
);
?>
