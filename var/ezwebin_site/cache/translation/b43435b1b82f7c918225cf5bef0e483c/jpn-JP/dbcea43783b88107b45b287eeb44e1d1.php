<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/shop/basket',
);

$TranslationRoot = array (
  '703caa3cf4a7f642cca6e38dfe067419' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Shopping basket',
    'comment' => NULL,
    'translation' => '買い物カゴ',
    'key' => '703caa3cf4a7f642cca6e38dfe067419',
  ),
  '28c49e4afc9c450db81f2e34387d839b' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Account information',
    'comment' => NULL,
    'translation' => 'お客様情報',
    'key' => '28c49e4afc9c450db81f2e34387d839b',
  ),
  '635db62c950bfd3b6e5de038354e030f' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Confirm order',
    'comment' => NULL,
    'translation' => 'オーダーの確認',
    'key' => '635db62c950bfd3b6e5de038354e030f',
  ),
  'c4a78b9c0630e0ccfd57e5bc1ba4331c' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Basket',
    'comment' => NULL,
    'translation' => '買い物カゴ',
    'key' => 'c4a78b9c0630e0ccfd57e5bc1ba4331c',
  ),
  'de2195a399ce46a86943739869990e0e' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'VAT is unknown',
    'comment' => NULL,
    'translation' => '消費税が不明です',
    'key' => 'de2195a399ce46a86943739869990e0e',
  ),
  '599aafe156777cbc59397f3384a953ad' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'VAT percentage is not yet known for some of the items being purchased.',
    'comment' => NULL,
    'translation' => '購入されるアイテムには税率が不明なものがあます。',
    'key' => '599aafe156777cbc59397f3384a953ad',
  ),
  '84b7790210a0998d9a7fbed4dbdb83cf' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'This probably means that some information about you is not yet available and will be obtained during checkout.',
    'comment' => NULL,
    'translation' => '現段階では情報の一部が欠けていますが、会計までに全ての情報が補充されることを示しています。',
    'key' => '84b7790210a0998d9a7fbed4dbdb83cf',
  ),
  '6ff30eea84498e48ad9d4951d942477e' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Attempted to add object without price to basket.',
    'comment' => NULL,
    'translation' => '価格未設定の商品を買い物かごに追加しようとしました。',
    'key' => '6ff30eea84498e48ad9d4951d942477e',
  ),
  'e915e4187e3cf8b97408e0767781126a' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Your payment was aborted.',
    'comment' => NULL,
    'translation' => '決済は中止されました.',
    'key' => 'e915e4187e3cf8b97408e0767781126a',
  ),
  '4c8092feaf6512b80f9ae23075448e2a' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Count',
    'comment' => NULL,
    'translation' => '数量',
    'key' => '4c8092feaf6512b80f9ae23075448e2a',
  ),
  '61f55ef8832e27360d1578815ebc05c2' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'VAT',
    'comment' => NULL,
    'translation' => '消費税',
    'key' => '61f55ef8832e27360d1578815ebc05c2',
  ),
  '9900337badf1865c307d5a35e3a09752' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Price inc. VAT',
    'comment' => NULL,
    'translation' => '価格（税込）',
    'key' => '9900337badf1865c307d5a35e3a09752',
  ),
  'a77841d67548cdcf68f498752657c857' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Discount',
    'comment' => NULL,
    'translation' => '割引',
    'key' => 'a77841d67548cdcf68f498752657c857',
  ),
  'b60ba74fbcc55c086717778181293314' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Update',
    'comment' => NULL,
    'translation' => '更新',
    'key' => 'b60ba74fbcc55c086717778181293314',
  ),
  '900bfad575551d77e9e522e420f1f3a6' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Remove',
    'comment' => NULL,
    'translation' => '削除',
    'key' => '900bfad575551d77e9e522e420f1f3a6',
  ),
  'a1504493196d245276385482972f0e4e' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Selected options',
    'comment' => NULL,
    'translation' => '選択したオプション',
    'key' => 'a1504493196d245276385482972f0e4e',
  ),
  'e266cfb86ea56c4cf4368934f908b389' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Shipping',
    'comment' => NULL,
    'translation' => '配送',
    'key' => 'e266cfb86ea56c4cf4368934f908b389',
  ),
  'e0b99d1d43eb661949e2e662a4ec49fd' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Order total',
    'comment' => NULL,
    'translation' => 'お買い上げ合計',
    'key' => 'e0b99d1d43eb661949e2e662a4ec49fd',
  ),
  '48b98b7e30e255661cb38426f7f5c8cd' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Continue shopping',
    'comment' => NULL,
    'translation' => '買い物を続ける',
    'key' => '48b98b7e30e255661cb38426f7f5c8cd',
  ),
  'b3bac9d48aa00b4184ce7d5502b1d366' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Checkout',
    'comment' => NULL,
    'translation' => 'チェックアウト',
    'key' => 'b3bac9d48aa00b4184ce7d5502b1d366',
  ),
  '72e765351a352e5748be4e6e599c5ba3' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'The following items were removed from your basket because the products were changed.',
    'comment' => NULL,
    'translation' => '商品が更新されたため、次のアイテムはあなたの買い物かごから削除されました。',
    'key' => '72e765351a352e5748be4e6e599c5ba3',
  ),
  '20c5f22658afe059385da9887286100c' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Total price ex. VAT',
    'comment' => NULL,
    'translation' => '合計（税別）',
    'key' => '20c5f22658afe059385da9887286100c',
  ),
  'be31775a752fec77c0560d137b2cbdc5' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Total price inc. VAT',
    'comment' => NULL,
    'translation' => '合計（税込）',
    'key' => 'be31775a752fec77c0560d137b2cbdc5',
  ),
  'c70d09ffb13f3d084071a35d601ce115' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Unknown',
    'comment' => NULL,
    'translation' => '不明',
    'key' => 'c70d09ffb13f3d084071a35d601ce115',
  ),
  'fe205acb37b91ffa0b989d6ebe507647' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Subtotal ex. VAT',
    'comment' => NULL,
    'translation' => '小計（税別）',
    'key' => 'fe205acb37b91ffa0b989d6ebe507647',
  ),
  'df21d2b803562ae0e040b205b9b751cc' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'Subtotal inc. VAT',
    'comment' => NULL,
    'translation' => '小計（税込）',
    'key' => 'df21d2b803562ae0e040b205b9b751cc',
  ),
  '403e4d0967c77ae96f57137d978dbd79' => 
  array (
    'context' => 'design/ezwebin/shop/basket',
    'source' => 'You have no products in your basket.',
    'comment' => NULL,
    'translation' => '買い物かごには何も入っていません。',
    'key' => '403e4d0967c77ae96f57137d978dbd79',
  ),
);
?>
