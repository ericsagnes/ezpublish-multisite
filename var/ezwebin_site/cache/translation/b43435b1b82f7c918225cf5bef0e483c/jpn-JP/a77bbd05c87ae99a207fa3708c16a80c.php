<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/content/create_languages',
);

$TranslationRoot = array (
  '759b54687a94520e573391b08133c00a' => 
  array (
    'context' => 'design/standard/content/create_languages',
    'source' => 'Existing languages',
    'comment' => NULL,
    'translation' => '既存の言語',
    'key' => '759b54687a94520e573391b08133c00a',
  ),
  'e581a7ef9b2b69f387e67394e99d9336' => 
  array (
    'context' => 'design/standard/content/create_languages',
    'source' => 'Select the language in which you want to create an object',
    'comment' => NULL,
    'translation' => 'オブジェクトを作成する言語の選択',
    'key' => 'e581a7ef9b2b69f387e67394e99d9336',
  ),
  '99a4e87afcd4111752003ce1f8d00448' => 
  array (
    'context' => 'design/standard/content/create_languages',
    'source' => 'Create',
    'comment' => NULL,
    'translation' => '作成',
    'key' => '99a4e87afcd4111752003ce1f8d00448',
  ),
  '5fcd0c264f07e811788937ca3449e288' => 
  array (
    'context' => 'design/standard/content/create_languages',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '5fcd0c264f07e811788937ca3449e288',
  ),
);
?>
