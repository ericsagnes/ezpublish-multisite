<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/class/datatype/browse_objectrelationlist_placement',
);

$TranslationRoot = array (
  '8ed7920f91348de6942505b959510744' => 
  array (
    'context' => 'design/admin/class/datatype/browse_objectrelationlist_placement',
    'source' => 'Choose initial location',
    'comment' => NULL,
    'translation' => 'デフォルトでブラウズをスタートするノードの選択',
    'key' => '8ed7920f91348de6942505b959510744',
  ),
  '460c7395feb0a6ae59900b602ac931a4' => 
  array (
    'context' => 'design/admin/class/datatype/browse_objectrelationlist_placement',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => '460c7395feb0a6ae59900b602ac931a4',
  ),
  'bb5d3d401c9f1b7907966826ab01bbf7' => 
  array (
    'context' => 'design/admin/class/datatype/browse_objectrelationlist_placement',
    'source' => 'Select the location that should be the default location then click "OK".',
    'comment' => NULL,
    'translation' => 'デフォルトでブラウズをスタートするノードを選択し、OKをクリックして下さい。',
    'key' => 'bb5d3d401c9f1b7907966826ab01bbf7',
  ),
);
?>
