<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/content/version',
);

$TranslationRoot = array (
  '36eb8b49b699558d3f762c378e2942c2' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => '36eb8b49b699558d3f762c378e2942c2',
  ),
  '1a6e8183514c68f924fe486a247fc54f' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Versions for: %1',
    'comment' => NULL,
    'translation' => '%1 のバージョン',
    'key' => '1a6e8183514c68f924fe486a247fc54f',
  ),
  'd9867eeae497d0f9b9ae08535be00b53' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Unable to create new version',
    'comment' => NULL,
    'translation' => '新しいバージョンを作成できません',
    'key' => 'd9867eeae497d0f9b9ae08535be00b53',
  ),
  '354f9ddf439bb8a63785f3d016a10704' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => '354f9ddf439bb8a63785f3d016a10704',
  ),
  '974851ab9732fbee8f4092597e025ac5' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Copy and edit',
    'comment' => NULL,
    'translation' => '複製／編集',
    'key' => '974851ab9732fbee8f4092597e025ac5',
  ),
  '243379785f2e2a1e413951ed8cec1fca' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Version history limit has been exceeded and no archived version can be removed by the system.',
    'comment' => NULL,
    'translation' => 'バージョンの履歴制限を超えました。未保管のバージョンはシステムにより削除されます。',
    'key' => '243379785f2e2a1e413951ed8cec1fca',
  ),
  '3b8d2ce538924d59fb9784c40432af03' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'You can change your version history settings in content.ini, remove draft versions or edit existing drafts.',
    'comment' => NULL,
    'translation' => 'バージョン履歴設定はcontent.ini設定により変更可能です。下書きバージョンを削除するか編集してください。',
    'key' => '3b8d2ce538924d59fb9784c40432af03',
  ),
  'a7b99e47d34463ec210d073d4d6b542b' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Version %1 is not available for editing any more, only drafts can be edited.',
    'comment' => NULL,
    'translation' => 'バージョン%1は編集することができません。下書きのみ編集できます。',
    'key' => 'a7b99e47d34463ec210d073d4d6b542b',
  ),
  '2c589bfe529252a3c4f8db2fe1563388' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Status',
    'comment' => NULL,
    'translation' => 'ステータス',
    'key' => '2c589bfe529252a3c4f8db2fe1563388',
  ),
  'db79600360e4fcfe0c16c6a7ace880f9' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Translations',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => 'db79600360e4fcfe0c16c6a7ace880f9',
  ),
  '7f0a0676b1972e180ffe97fb8b61fa9f' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Creator',
    'comment' => NULL,
    'translation' => '作成者',
    'key' => '7f0a0676b1972e180ffe97fb8b61fa9f',
  ),
  '06f5e9eb2fb4e63c87742bf5c4b037b8' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '06f5e9eb2fb4e63c87742bf5c4b037b8',
  ),
  '50ef970ed63d2e6e22aad5716abefb9c' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Version is not a draft',
    'comment' => NULL,
    'translation' => 'バージョンは下書きではありません',
    'key' => '50ef970ed63d2e6e22aad5716abefb9c',
  ),
  'fdd4988597049a50e6cf76ec448c21bb' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'To edit this version, first create a copy of it.',
    'comment' => NULL,
    'translation' => 'このバージョンを編集するには、先に複製を作成して下さい。',
    'key' => 'fdd4988597049a50e6cf76ec448c21bb',
  ),
  '48b1e193374aff2a02d358968e5e3a3b' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Version is not yours',
    'comment' => NULL,
    'translation' => 'バージョンの所有者ではありません',
    'key' => '48b1e193374aff2a02d358968e5e3a3b',
  ),
  'aa6f7b83b4b45cb734a9970e4a8ea404' => 
  array (
    'context' => 'design/standard/content/version',
    'source' => 'Version %1 was not created by you. You can only edit your own drafts.',
    'comment' => NULL,
    'translation' => 'バージョン%1の作成者ではありません。自分が作成した下書き以外は編集できません。',
    'key' => 'aa6f7b83b4b45cb734a9970e4a8ea404',
  ),
);
?>
