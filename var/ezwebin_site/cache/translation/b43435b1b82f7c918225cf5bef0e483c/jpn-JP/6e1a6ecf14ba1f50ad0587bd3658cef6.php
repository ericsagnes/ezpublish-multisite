<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/rss/edit_import',
);

$TranslationRoot = array (
  '337b256583c635408d9d1017531b85c5' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Edit <%rss_import_name> [RSS Import]',
    'comment' => NULL,
    'translation' => '<%rss_import_name> [RSSインポート]の編集',
    'key' => '337b256583c635408d9d1017531b85c5',
  ),
  'a5d4f631f93b3a22503693bb9a4fe6d6' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => 'a5d4f631f93b3a22503693bb9a4fe6d6',
  ),
  '86e0a25c1ddae7b192b80b99afd163f9' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Name of the RSS import. This name is used in the Administration Interface only, to distinguish the different imports from each other.',
    'comment' => NULL,
    'translation' => 'RSSインポート名。この名前は、インポートを区別出きるように、管理画面で利用されています。',
    'key' => '86e0a25c1ddae7b192b80b99afd163f9',
  ),
  'ea0f8971018938bde3b64ba8a2c56f64' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Source URL',
    'comment' => NULL,
    'translation' => 'ソースURL',
    'key' => 'ea0f8971018938bde3b64ba8a2c56f64',
  ),
  'f01d9dbc20b6b8b58fb9733c597481fe' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Use this field to enter the source URL of the RSS feed to import.',
    'comment' => NULL,
    'translation' => 'このフィールドにインポートするRSSフィードのソースを入力してください。',
    'key' => 'f01d9dbc20b6b8b58fb9733c597481fe',
  ),
  '06d5834b3eeefd113e09fc0b57dc80aa' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Update',
    'comment' => NULL,
    'translation' => '更新',
    'key' => '06d5834b3eeefd113e09fc0b57dc80aa',
  ),
  '7d0baa0004a970c6b88ac7916d25cc9b' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Click this button to proceed and analyze the import feed.',
    'comment' => NULL,
    'translation' => 'このボータンをクリックして、インポートフィードの分析を実効できます。',
    'key' => '7d0baa0004a970c6b88ac7916d25cc9b',
  ),
  '68fe0bcaf7adddcbda8484f7a4af1a3b' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'RSS Version',
    'comment' => NULL,
    'translation' => 'RSSバージョン',
    'key' => '68fe0bcaf7adddcbda8484f7a4af1a3b',
  ),
  '06ac6cc65cc9e871ebbbea8311388e10' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Destination path',
    'comment' => NULL,
    'translation' => '宛先パス',
    'key' => '06ac6cc65cc9e871ebbbea8311388e10',
  ),
  'b9f3a1fbf265ce105f227f26201f0755' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Browse',
    'comment' => NULL,
    'translation' => 'ブラウズ',
    'key' => 'b9f3a1fbf265ce105f227f26201f0755',
  ),
  '29f091a9aef24ae8017bb26e071fb751' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Click this button to select the destination node where objects created by the import are located.',
    'comment' => NULL,
    'translation' => 'インポートされたオブジェクトの保存先を設定するにはこのボタンを使ってください。',
    'key' => '29f091a9aef24ae8017bb26e071fb751',
  ),
  'a588547214525b514b692072724686c5' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Imported objects will be owned by',
    'comment' => NULL,
    'translation' => 'インポートされたオブジェクトの所有者は',
    'key' => 'a588547214525b514b692072724686c5',
  ),
  'bac3012f6df15a1126de78d132cce83e' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Change user',
    'comment' => NULL,
    'translation' => 'ユーザ変更',
    'key' => 'bac3012f6df15a1126de78d132cce83e',
  ),
  '956831ea049ecded0b283e17f4b54e39' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Click this button to select the user who should own the objects created by the import.',
    'comment' => NULL,
    'translation' => 'インポートされたオブジェクトの所有者を設定するにはこのボタンを使ってください。',
    'key' => '956831ea049ecded0b283e17f4b54e39',
  ),
  '864968b6f397357f1f4aa59e95fabcb5' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Class',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => '864968b6f397357f1f4aa59e95fabcb5',
  ),
  'c85c8874787c4b599a7ad67e64f1b058' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Use this drop-down to select the type of object the import should create. Click the "Set" button to load the attribute types for the remaining fields.',
    'comment' => NULL,
    'translation' => 'インポートはどのオブジェクト種類を作るのを選ぶにはこのドロップダウンを使ってください。他のフィールドの属性タイプを読み込む様に、「設定」ボタンを使ってください。',
    'key' => 'c85c8874787c4b599a7ad67e64f1b058',
  ),
  'f3958ea6ac153dc8f9297d5f7a0e75d3' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Set',
    'comment' => NULL,
    'translation' => '設定',
    'key' => 'f3958ea6ac153dc8f9297d5f7a0e75d3',
  ),
  '4a7814d253b078230897a0f01cd920b2' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Click this button to load the correct values into the drop-down fields below. Use the drop-down menu on the left to select the class.',
    'comment' => NULL,
    'translation' => '下のドロップダウンフィールドに正確な値を読み込む様にこのボタンを使ってください。クラスを選択する様に左のドロップダウンメニューを使ってください。',
    'key' => '4a7814d253b078230897a0f01cd920b2',
  ),
  '2539518c4167e64b0a8a93d4a83ac9a1' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Class attributes',
    'comment' => NULL,
    'translation' => 'クラス属性',
    'key' => '2539518c4167e64b0a8a93d4a83ac9a1',
  ),
  '7b475e1d3d13c5d64794f6419304ff60' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Use this drop-down menu to select the attribute that should bet set as information from the RSS stream.',
    'comment' => NULL,
    'translation' => 'RSSストリームの情報になる属性をこのドロップダウンメニューで選んでください。',
    'key' => '7b475e1d3d13c5d64794f6419304ff60',
  ),
  '16b415340a09a3c0a393f33e6e7d1a54' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Ignore',
    'comment' => NULL,
    'translation' => '無視',
    'key' => '16b415340a09a3c0a393f33e6e7d1a54',
  ),
  '2df438ca6b5c82ad3604a6a7dbc03d82' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Object attributes',
    'comment' => NULL,
    'translation' => 'オブジェクト属性',
    'key' => '2df438ca6b5c82ad3604a6a7dbc03d82',
  ),
  '8b36992fa3d36a0a16b56e93cb91760d' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Active',
    'comment' => NULL,
    'translation' => '有効',
    'key' => '8b36992fa3d36a0a16b56e93cb91760d',
  ),
  '7cf213c12b362ad6084ea8e746e46206' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Use this checkbox to control if the RSS feed is active or not. An inactive feed will not be automatically updated.',
    'comment' => NULL,
    'translation' => 'このチェックボックスでRSSフィードはか無効に設定できます。無効なフィードは自動更新されません。',
    'key' => '7cf213c12b362ad6084ea8e746e46206',
  ),
  'a5dc545340b569526287c8c3f467f737' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => 'a5dc545340b569526287c8c3f467f737',
  ),
  '7edeeed6543f53c0de4b740686b828e7' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Apply the changes and return to the RSS overview.',
    'comment' => NULL,
    'translation' => '変更を保存し、RSS概要に戻る。',
    'key' => '7edeeed6543f53c0de4b740686b828e7',
  ),
  '52b1d516dc736019431b48be90c4551a' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '52b1d516dc736019431b48be90c4551a',
  ),
  'ac4d1ea5ce06c0aa50794e94ff6eb19e' => 
  array (
    'context' => 'design/ezwebin/rss/edit_import',
    'source' => 'Cancel the changes and return to the RSS overview.',
    'comment' => NULL,
    'translation' => '変更をキャンセルし、RSS概要に戻る。',
    'key' => 'ac4d1ea5ce06c0aa50794e94ff6eb19e',
  ),
);
?>
