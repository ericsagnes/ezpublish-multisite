<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/datatype',
);

$TranslationRoot = array (
  '607114cce6e46b048a89ec60a4611475' => 
  array (
    'context' => 'design/admin/content/datatype',
    'source' => 'No media file is available.',
    'comment' => NULL,
    'translation' => '利用可能なメディアファイルがありません。',
    'key' => '607114cce6e46b048a89ec60a4611475',
  ),
  '9adab5d0d709f93a6d1c6d5e2a0c8e25' => 
  array (
    'context' => 'design/admin/content/datatype',
    'source' => 'Year',
    'comment' => NULL,
    'translation' => '年',
    'key' => '9adab5d0d709f93a6d1c6d5e2a0c8e25',
  ),
  '2e02cb3992d56f1d23ba26c120cc3b61' => 
  array (
    'context' => 'design/admin/content/datatype',
    'source' => 'Month',
    'comment' => NULL,
    'translation' => '月',
    'key' => '2e02cb3992d56f1d23ba26c120cc3b61',
  ),
  '230ad1aa50f705a9fe9b4f954bbddccf' => 
  array (
    'context' => 'design/admin/content/datatype',
    'source' => 'Day',
    'comment' => NULL,
    'translation' => '日',
    'key' => '230ad1aa50f705a9fe9b4f954bbddccf',
  ),
  'a629a7de5d9225716bc015d784ba838b' => 
  array (
    'context' => 'design/admin/content/datatype',
    'source' => 'Hour',
    'comment' => NULL,
    'translation' => '時間',
    'key' => 'a629a7de5d9225716bc015d784ba838b',
  ),
  'ef87d7b9a344ed9816a24876d892fc12' => 
  array (
    'context' => 'design/admin/content/datatype',
    'source' => 'Minute',
    'comment' => NULL,
    'translation' => '分',
    'key' => 'ef87d7b9a344ed9816a24876d892fc12',
  ),
);
?>
