<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/pdf',
);

$TranslationRoot = array (
  'e222a91ee8970fb87d3c8cab9f7fc74c' => 
  array (
    'context' => 'kernel/pdf',
    'source' => 'An export with such filename already exists.',
    'comment' => NULL,
    'translation' => 'このファイル名のエクスポートは既に存在します。',
    'key' => 'e222a91ee8970fb87d3c8cab9f7fc74c',
  ),
  'c3b0bc95ed6abf40fd9715cd2cc56216' => 
  array (
    'context' => 'kernel/pdf',
    'source' => 'PDF Export',
    'comment' => NULL,
    'translation' => 'PDFエクスポート',
    'key' => 'c3b0bc95ed6abf40fd9715cd2cc56216',
  ),
);
?>
