<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/shop/view',
);

$TranslationRoot = array (
  'e483df7aca1da4bafdd3cdd53db399d2' => 
  array (
    'context' => 'design/standard/shop/view',
    'source' => 'Choose customers',
    'comment' => NULL,
    'translation' => '顧客の選択',
    'key' => 'e483df7aca1da4bafdd3cdd53db399d2',
  ),
  'b086776fc9d2c400108fff001cf32891' => 
  array (
    'context' => 'design/standard/shop/view',
    'source' => 'Choose product for discount',
    'comment' => NULL,
    'translation' => 'ディスカウント対象商品の選択',
    'key' => 'b086776fc9d2c400108fff001cf32891',
  ),
  '99e09a2364e7de13f7b091f7a5322b0a' => 
  array (
    'context' => 'design/standard/shop/view',
    'source' => 'Please choose the customers you want to add to discount group %groupname.

    Select your customers then click the %buttonname button.
    Using the recent and bookmark items for quick selection is also possible.
    Click on object names to change the browse listing.',
    'comment' => NULL,
    'translation' => '%groupnameディスカウントグループに追加するカスタマーを選択してください。￼カスタマーを選択した後に、%buttonnameボタンをクリックしてください。￼￼￼￼￼最近使われたオブジェクトとブックマークアイテムを使うこともできます。￼￼￼￼リストを変更するには、オブジェクト名をクリックしてください。',
    'key' => '99e09a2364e7de13f7b091f7a5322b0a',
  ),
  '1a20962c6bf09d80349973b4f47dd468' => 
  array (
    'context' => 'design/standard/shop/view',
    'source' => 'Please choose the products you want to add to discount rule %discountname in discount group %groupname.

    Select your products then click the %buttonname button.
    Using the recent and bookmark items for quick selection is also possible.
    Click on product names to change the browse listing.',
    'comment' => NULL,
    'translation' => '%groupnameディスカウントグループの%discountnameディスカウントルールに追加する商品を選択してください。
商品を選択後、%buttonnameボタンをクリックしてください。￼最近使われたオブジェクトとブックマークアイテムを使うこともできます。￼￼￼￼リストを変更するには、オブジェクト名をクリックしてください。',
    'key' => '1a20962c6bf09d80349973b4f47dd468',
  ),
);
?>
