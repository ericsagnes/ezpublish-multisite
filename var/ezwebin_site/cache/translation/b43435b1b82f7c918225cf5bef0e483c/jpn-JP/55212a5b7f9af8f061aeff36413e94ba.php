<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/parts/shop/menu',
);

$TranslationRoot = array (
  '75c35dc8e6b6e0a10660b3a8af4e1622' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'Customers',
    'comment' => NULL,
    'translation' => '顧客',
    'key' => '75c35dc8e6b6e0a10660b3a8af4e1622',
  ),
  'af85dab9f740db11502bb0300a2660da' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'Discounts',
    'comment' => NULL,
    'translation' => 'ディスカウント',
    'key' => 'af85dab9f740db11502bb0300a2660da',
  ),
  'a6b5462eba35f727f2e7c0b209e19dff' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'Orders',
    'comment' => NULL,
    'translation' => '注文',
    'key' => 'a6b5462eba35f727f2e7c0b209e19dff',
  ),
  '1c2f6d35943137fb863b1068016ea911' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'Product statistics',
    'comment' => NULL,
    'translation' => '商品の集計',
    'key' => '1c2f6d35943137fb863b1068016ea911',
  ),
  '05577727e0a9684feb7a795158229de0' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'VAT types',
    'comment' => NULL,
    'translation' => '課税方法',
    'key' => '05577727e0a9684feb7a795158229de0',
  ),
  '6fb357675a3bd61a6e8f7d3c79f60db0' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'Shop',
    'comment' => NULL,
    'translation' => 'ウエブショップ管理',
    'key' => '6fb357675a3bd61a6e8f7d3c79f60db0',
  ),
  'cb1ae634ce518b3f3aef29781ea66e84' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'Order status',
    'comment' => NULL,
    'translation' => '注文ステータス',
    'key' => 'cb1ae634ce518b3f3aef29781ea66e84',
  ),
  '96084a225b630b50d287e01f80bc5b55' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'Archive',
    'comment' => NULL,
    'translation' => 'アーカイブ ',
    'key' => '96084a225b630b50d287e01f80bc5b55',
  ),
  '9f67aaea8738974620046d0f49c4a3ab' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'VAT rules',
    'comment' => NULL,
    'translation' => '課税ルール',
    'key' => '9f67aaea8738974620046d0f49c4a3ab',
  ),
  '5de217261755b9826887a8f3af1d9e73' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'Product categories',
    'comment' => NULL,
    'translation' => '商品カテゴリ',
    'key' => '5de217261755b9826887a8f3af1d9e73',
  ),
  '74783ade7dc3cd73179f2295cc3f5f5a' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'Currencies',
    'comment' => NULL,
    'translation' => '通貨',
    'key' => '74783ade7dc3cd73179f2295cc3f5f5a',
  ),
  'ac703d5974bfd7afef72050df22b6c46' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'Preferred currency',
    'comment' => NULL,
    'translation' => 'デフォルト通貨',
    'key' => 'ac703d5974bfd7afef72050df22b6c46',
  ),
  'd0224f66f2069f8bffb0fc2d62487f95' => 
  array (
    'context' => 'design/admin/parts/shop/menu',
    'source' => 'Products overview',
    'comment' => NULL,
    'translation' => '商品一覧',
    'key' => 'd0224f66f2069f8bffb0fc2d62487f95',
  ),
);
?>
