<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/class/view',
);

$TranslationRoot = array (
  'b4df0f310d791c04f9af38c43e4c8946' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Name',
    'comment' => NULL,
    'translation' => '名前',
    'key' => 'b4df0f310d791c04f9af38c43e4c8946',
  ),
  'e0a05b650f2390e619bbaec726e1f7a8' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Identifier',
    'comment' => NULL,
    'translation' => '識別子',
    'key' => 'e0a05b650f2390e619bbaec726e1f7a8',
  ),
  'f13adead29079c0da4fec2ef364febec' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => 'f13adead29079c0da4fec2ef364febec',
  ),
  'be375af8c8a7ccd16b63222c02d78285' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Object count',
    'comment' => NULL,
    'translation' => 'オブジェクト数',
    'key' => 'be375af8c8a7ccd16b63222c02d78285',
  ),
  '219108e47679ae14a2fd92b19c97554d' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Class - %1',
    'comment' => NULL,
    'translation' => 'クラス - %1',
    'key' => '219108e47679ae14a2fd92b19c97554d',
  ),
  'cd312f733052bbe839a05fe34915bfbe' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Last modified by %username on %time',
    'comment' => NULL,
    'translation' => '最終修正者と日時：%username %time',
    'key' => 'cd312f733052bbe839a05fe34915bfbe',
  ),
  'e3205f544c9eafe8560021c908ead418' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Object name pattern',
    'comment' => NULL,
    'translation' => 'オブジェクトの名前生成パターン',
    'key' => 'e3205f544c9eafe8560021c908ead418',
  ),
  '587ad6ec1a7f58a23b88fb0d584e7a27' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Container',
    'comment' => NULL,
    'translation' => 'コンテナ',
    'key' => '587ad6ec1a7f58a23b88fb0d584e7a27',
  ),
  '4b1819e6d40b6183cffa113f47ef0fbb' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Yes',
    'comment' => NULL,
    'translation' => 'はい',
    'key' => '4b1819e6d40b6183cffa113f47ef0fbb',
  ),
  '1ac3c45411a410bfb1baa8d1901ac6f8' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'No',
    'comment' => NULL,
    'translation' => 'いいえ',
    'key' => '1ac3c45411a410bfb1baa8d1901ac6f8',
  ),
  '88adc37d9ce2e38cc969e3dec7c65704' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Member of groups',
    'comment' => NULL,
    'translation' => 'グループのメンバー',
    'key' => '88adc37d9ce2e38cc969e3dec7c65704',
  ),
  'f825a5a46d6d821560d1b1a0edff6dfb' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Attributes',
    'comment' => NULL,
    'translation' => '属性',
    'key' => 'f825a5a46d6d821560d1b1a0edff6dfb',
  ),
  '8398ef2c69679c5119c4a9635abdf6f1' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Is required',
    'comment' => NULL,
    'translation' => '入力必須',
    'key' => '8398ef2c69679c5119c4a9635abdf6f1',
  ),
  'd3bed148c33d14fbd67fd8059bdc9f28' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Is not required',
    'comment' => NULL,
    'translation' => '自由入力',
    'key' => 'd3bed148c33d14fbd67fd8059bdc9f28',
  ),
  '8a7f3097369007ba33ca74e51a975b7e' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Is searchable',
    'comment' => NULL,
    'translation' => '検索対象',
    'key' => '8a7f3097369007ba33ca74e51a975b7e',
  ),
  'ffd33e983e726ef37c0a8026b67f91a8' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Is not searchable',
    'comment' => NULL,
    'translation' => '非検索対象',
    'key' => 'ffd33e983e726ef37c0a8026b67f91a8',
  ),
  '9e4b9ca6b430c9830686df9318b10fd1' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Collects information',
    'comment' => NULL,
    'translation' => '情報収集に利用する',
    'key' => '9e4b9ca6b430c9830686df9318b10fd1',
  ),
  '4136aeb361c67021d81ff40ebc0722bc' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Does not collect information',
    'comment' => NULL,
    'translation' => '情報収集に利用しない',
    'key' => '4136aeb361c67021d81ff40ebc0722bc',
  ),
  'e9dd32dffdc2b8eec8df0ac745f873c4' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Translation is disabled',
    'comment' => NULL,
    'translation' => '翻訳不可',
    'key' => 'e9dd32dffdc2b8eec8df0ac745f873c4',
  ),
  'f7a7b10f17fd3212700ba487d335af0f' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Translation is enabled',
    'comment' => NULL,
    'translation' => '翻訳可',
    'key' => 'f7a7b10f17fd3212700ba487d335af0f',
  ),
  '9c0a6be49407ea2676bce53c6db29986' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Override templates',
    'comment' => NULL,
    'translation' => 'オーバーライド・テンプレート',
    'key' => '9c0a6be49407ea2676bce53c6db29986',
  ),
  'c656dfe20d2efc641d44893c01e7c769' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Override',
    'comment' => NULL,
    'translation' => 'オーバーライド',
    'key' => 'c656dfe20d2efc641d44893c01e7c769',
  ),
  'b5a1d69e47cac4d504c5d2ae464a989a' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Source template',
    'comment' => NULL,
    'translation' => 'ソース・テンプレート',
    'key' => 'b5a1d69e47cac4d504c5d2ae464a989a',
  ),
  '1059de15e18c24ba37c9b42fcb222199' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Override template',
    'comment' => NULL,
    'translation' => 'オーバーライド・テンプレート',
    'key' => '1059de15e18c24ba37c9b42fcb222199',
  ),
  'e32ff7d5d5cbfb9562342b4bf89e61b1' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Objects always available (default value)',
    'comment' => NULL,
    'translation' => '常に利用可能なオブジェクト（デフォルト値）',
    'key' => 'e32ff7d5d5cbfb9562342b4bf89e61b1',
  ),
  '86b7072f0338ea59d2c9c0198fc49d09' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Siteaccess',
    'comment' => NULL,
    'translation' => 'サイトアクセス',
    'key' => '86b7072f0338ea59d2c9c0198fc49d09',
  ),
  '52abeca9ea02385acc38909a430ef835' => 
  array (
    'context' => 'design/standard/class/view',
    'source' => 'Description',
    'comment' => NULL,
    'translation' => '概要',
    'key' => '52abeca9ea02385acc38909a430ef835',
  ),
);
?>
