<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/visual/toolbar',
);

$TranslationRoot = array (
  'c80e21d2d558a062117b6962e9c9d310' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Browse',
    'comment' => NULL,
    'translation' => 'ブラウズ',
    'key' => 'c80e21d2d558a062117b6962e9c9d310',
  ),
  '2be4cf3fb6972cc89bd62c55b58d4f51' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'True',
    'comment' => NULL,
    'translation' => '可',
    'key' => '2be4cf3fb6972cc89bd62c55b58d4f51',
  ),
  '3d644f86963bc55e8a9737ea8910798c' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'False',
    'comment' => NULL,
    'translation' => '不可',
    'key' => '3d644f86963bc55e8a9737ea8910798c',
  ),
  '66d00185787dc85a3d18f888b9ddda87' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Yes',
    'comment' => NULL,
    'translation' => 'はい',
    'key' => '66d00185787dc85a3d18f888b9ddda87',
  ),
  'c28087220fe30211a1e3e8bdc8e86196' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'No',
    'comment' => NULL,
    'translation' => 'いいえ',
    'key' => 'c28087220fe30211a1e3e8bdc8e86196',
  ),
  '0e71295050dda9707ee89eb860a104c0' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'There are currently no tools in this toolbar',
    'comment' => NULL,
    'translation' => 'このツールバーに設定されたツールはありません',
    'key' => '0e71295050dda9707ee89eb860a104c0',
  ),
  'c3c8e68e3c8afeef858485b97f86321c' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => 'c3c8e68e3c8afeef858485b97f86321c',
  ),
  '44e587cf78020d131033f2b3a27600f7' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Update priorities',
    'comment' => NULL,
    'translation' => '優先度の更新',
    'key' => '44e587cf78020d131033f2b3a27600f7',
  ),
  '1aa66ce697ef08bbd53be624aec7b5ca' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Add Tool',
    'comment' => NULL,
    'translation' => 'ツールの追加',
    'key' => '1aa66ce697ef08bbd53be624aec7b5ca',
  ),
  'a8c9203ab4f8b553ed21c1cc3ec42941' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Apply changes',
    'comment' => NULL,
    'translation' => '変更の適用',
    'key' => 'a8c9203ab4f8b553ed21c1cc3ec42941',
  ),
  '0f1d326668afc3f0c1bba7d9b80ae15b' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Click this button to store changes if you have modified the parameters above.',
    'comment' => NULL,
    'translation' => 'パラメータを修正した場合、このボタンをクリックして変更を保存します。',
    'key' => '0f1d326668afc3f0c1bba7d9b80ae15b',
  ),
  'b59280352480e07161b7182b22b3a33b' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Back to toolbars',
    'comment' => NULL,
    'translation' => 'ツールバー一覧に戻る',
    'key' => 'b59280352480e07161b7182b22b3a33b',
  ),
  'b75ce7f1be0138d6c5ae6fc1972b7554' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Go back to the toolbar list.',
    'comment' => NULL,
    'translation' => 'ツールバー一覧に戻ります。',
    'key' => 'b75ce7f1be0138d6c5ae6fc1972b7554',
  ),
  'eca5debe1354f43c96cbb2b5e84d7f83' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Toolbar management',
    'comment' => NULL,
    'translation' => 'ツールバー管理',
    'key' => 'eca5debe1354f43c96cbb2b5e84d7f83',
  ),
  '09273701e8fe1363270612edd24695bc' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Current siteaccess',
    'comment' => NULL,
    'translation' => '現在のサイトアクセス',
    'key' => '09273701e8fe1363270612edd24695bc',
  ),
  'de9a143fb5f3c5c4e686073e41a892fd' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Select siteaccess',
    'comment' => NULL,
    'translation' => 'サイトアクセスの選択',
    'key' => 'de9a143fb5f3c5c4e686073e41a892fd',
  ),
  '062f3debbcef0334f4109af3a9bf9706' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Set',
    'comment' => NULL,
    'translation' => '設定',
    'key' => '062f3debbcef0334f4109af3a9bf9706',
  ),
  'bfec5973f7a7ae46f811266051784e44' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Available toolbars for the <%siteaccess> siteaccess',
    'comment' => NULL,
    'translation' => '<%siteaccess> サイトアクセスで利用可能なツールバー',
    'key' => 'bfec5973f7a7ae46f811266051784e44',
  ),
  '0e0165f66df7f25432a7f2f760c426ce' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Tool list for <Toolbar_%toolbar_position>',
    'comment' => NULL,
    'translation' => '<Toolbar_%toolbar_position>のツール一覧',
    'key' => '0e0165f66df7f25432a7f2f760c426ce',
  ),
  'a530fdf3a2e17c46f26a3299ec902934' => 
  array (
    'context' => 'design/standard/visual/toolbar',
    'source' => 'Siteaccess',
    'comment' => NULL,
    'translation' => 'サイトアクセス',
    'key' => 'a530fdf3a2e17c46f26a3299ec902934',
  ),
);
?>
