<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/browse_first_placement',
);

$TranslationRoot = array (
  '508af541e0eb20c39729d810a22934a2' => 
  array (
    'context' => 'design/admin/content/browse_first_placement',
    'source' => 'Choose location for new <%classname>',
    'comment' => NULL,
    'translation' => '新規 <%classname> の配置先の選択',
    'key' => '508af541e0eb20c39729d810a22934a2',
  ),
  '5d9007ec033facbb897fd630b12ccb65' => 
  array (
    'context' => 'design/admin/content/browse_first_placement',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => '5d9007ec033facbb897fd630b12ccb65',
  ),
  '22b8c836f1a1138119f1bc55e53cd252' => 
  array (
    'context' => 'design/admin/content/browse_first_placement',
    'source' => 'Choose a location for the new <%classname> using the radio buttons then click "Select".',
    'comment' => NULL,
    'translation' => '新しい<%classname>の配置先をラジオボタンで選択し、"選択"をクリックしてください。',
    'key' => '22b8c836f1a1138119f1bc55e53cd252',
  ),
);
?>
