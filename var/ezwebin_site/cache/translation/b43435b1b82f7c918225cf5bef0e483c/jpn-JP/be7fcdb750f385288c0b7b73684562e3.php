<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/content/advancedsearch',
);

$TranslationRoot = array (
  '19258ec8b9d4e7a5cf4d64443e9bf870' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Advanced search',
    'comment' => NULL,
    'translation' => '検索オプション',
    'key' => '19258ec8b9d4e7a5cf4d64443e9bf870',
  ),
  '9a032225e720d9a4de6ff8b362c309ef' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Search all the words',
    'comment' => NULL,
    'translation' => '全ての文字を検索',
    'key' => '9a032225e720d9a4de6ff8b362c309ef',
  ),
  '09009ab4b90ced3c5eedcaa203210a5d' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Search the exact phrase',
    'comment' => NULL,
    'translation' => '一致する句を検索',
    'key' => '09009ab4b90ced3c5eedcaa203210a5d',
  ),
  '365e8fa3ca08ac5c7bdd12be41d15fa8' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Published',
    'comment' => NULL,
    'translation' => '公開中',
    'key' => '365e8fa3ca08ac5c7bdd12be41d15fa8',
  ),
  '31cbaee3535ad40b43382a7867828906' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Any time',
    'comment' => NULL,
    'translation' => '指定無し',
    'key' => '31cbaee3535ad40b43382a7867828906',
  ),
  '37b635964e955fe3310a1e351d721fb2' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Last day',
    'comment' => NULL,
    'translation' => '昨日',
    'key' => '37b635964e955fe3310a1e351d721fb2',
  ),
  'f05126292fa1f26281a03e2e553043d7' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Last week',
    'comment' => NULL,
    'translation' => '先週',
    'key' => 'f05126292fa1f26281a03e2e553043d7',
  ),
  '31b974ddc6b75626098907b4e1507c59' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Last three months',
    'comment' => NULL,
    'translation' => '三ヶ月前から',
    'key' => '31b974ddc6b75626098907b4e1507c59',
  ),
  'cffe3f8619d05cefa2a95bd09a039b28' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Last year',
    'comment' => NULL,
    'translation' => '昨年',
    'key' => 'cffe3f8619d05cefa2a95bd09a039b28',
  ),
  'fb0ff9511a34f0ce7330cda430d5fd2d' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Display per page',
    'comment' => NULL,
    'translation' => 'ページあたりの表示件数',
    'key' => 'fb0ff9511a34f0ce7330cda430d5fd2d',
  ),
  '082b4a893108139389d1d2784d3f8280' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => '5 items',
    'comment' => NULL,
    'translation' => '5アイテム',
    'key' => '082b4a893108139389d1d2784d3f8280',
  ),
  '8c8c9660f3533fd2f79e318a47639853' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => '10 items',
    'comment' => NULL,
    'translation' => '10アイテム',
    'key' => '8c8c9660f3533fd2f79e318a47639853',
  ),
  'e5b6f367095541255450bc7de0966cdb' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => '20 items',
    'comment' => NULL,
    'translation' => '20アイテム',
    'key' => 'e5b6f367095541255450bc7de0966cdb',
  ),
  '5ceda561792e8edd363fe0a414805011' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => '30 items',
    'comment' => NULL,
    'translation' => '30アイテム',
    'key' => '5ceda561792e8edd363fe0a414805011',
  ),
  '182cd6723c770d4f83f8b80980dc2e0a' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => '50 items',
    'comment' => NULL,
    'translation' => '50アイテム',
    'key' => '182cd6723c770d4f83f8b80980dc2e0a',
  ),
  'e3fd0cff6539c955997900310d34605a' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Search',
    'comment' => NULL,
    'translation' => '検索',
    'key' => 'e3fd0cff6539c955997900310d34605a',
  ),
  'caf92f8e900dc36d55f3c34052d1a84e' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'No results were found when searching for "%1"',
    'comment' => NULL,
    'translation' => '"%1"に対する検索結果は得られませんでした。',
    'key' => 'caf92f8e900dc36d55f3c34052d1a84e',
  ),
  'd4b7fa29348d29346411118849050075' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Search for "%1" returned %2 matches',
    'comment' => NULL,
    'translation' => '"%1" の検索結果は %2 件です。',
    'key' => 'd4b7fa29348d29346411118849050075',
  ),
  '7bf8e20afbe09c50df1d857488f186ab' => 
  array (
    'context' => 'design/ezwebin/content/advancedsearch',
    'source' => 'Last month',
    'comment' => NULL,
    'translation' => '先月',
    'key' => '7bf8e20afbe09c50df1d857488f186ab',
  ),
);
?>
