<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/content/edit_languages',
);

$TranslationRoot = array (
  'a8218c14f88a1ea57b4b29d03d637d13' => 
  array (
    'context' => 'design/standard/content/edit_languages',
    'source' => 'Existing languages',
    'comment' => NULL,
    'translation' => '既存の言語',
    'key' => 'a8218c14f88a1ea57b4b29d03d637d13',
  ),
  'c7774b0829915e910913bc86d81afcb2' => 
  array (
    'context' => 'design/standard/content/edit_languages',
    'source' => 'Select the language you want to use when editing the object',
    'comment' => NULL,
    'translation' => 'オブジェクト編集中に使用する言語の選択',
    'key' => 'c7774b0829915e910913bc86d81afcb2',
  ),
  'bac0a4ae828e3cb77051a1f5ce6f5caa' => 
  array (
    'context' => 'design/standard/content/edit_languages',
    'source' => 'New languages',
    'comment' => NULL,
    'translation' => '新規の言語',
    'key' => 'bac0a4ae828e3cb77051a1f5ce6f5caa',
  ),
  '7267f0a37d1b6c16e06ebcd2bd93b5b7' => 
  array (
    'context' => 'design/standard/content/edit_languages',
    'source' => 'Select the language you want to add to the object',
    'comment' => NULL,
    'translation' => 'オブジェクトに追加する言語の選択',
    'key' => '7267f0a37d1b6c16e06ebcd2bd93b5b7',
  ),
  '03e3568f40e29aa518c03d926c2e80db' => 
  array (
    'context' => 'design/standard/content/edit_languages',
    'source' => 'Select the language the added translation will be based on',
    'comment' => NULL,
    'translation' => '追加する翻訳のベースとなる言語の選択',
    'key' => '03e3568f40e29aa518c03d926c2e80db',
  ),
  '58583fbb1e2480e5e5bfb66f9b883045' => 
  array (
    'context' => 'design/standard/content/edit_languages',
    'source' => 'However you can select one of the following languages for editing',
    'comment' => NULL,
    'translation' => '但し、編集するのに以下の言語の中から一つ選択することができます',
    'key' => '58583fbb1e2480e5e5bfb66f9b883045',
  ),
  '48f892194388a637f26a2bc8ea59e08d' => 
  array (
    'context' => 'design/standard/content/edit_languages',
    'source' => 'You do not have permission to edit the object in any available languages.',
    'comment' => NULL,
    'translation' => '利用できるどの言語においてもオブジェクトを編集する権限を持っていません。',
    'key' => '48f892194388a637f26a2bc8ea59e08d',
  ),
  'a27deb420620526c9367b6f2751729d4' => 
  array (
    'context' => 'design/standard/content/edit_languages',
    'source' => 'Edit',
    'comment' => NULL,
    'translation' => '編集',
    'key' => 'a27deb420620526c9367b6f2751729d4',
  ),
  'cd02378b2489dcb6bec9e50ac000e482' => 
  array (
    'context' => 'design/standard/content/edit_languages',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => 'cd02378b2489dcb6bec9e50ac000e482',
  ),
  'c8f0a2b53868352f9593f3854c1ce507' => 
  array (
    'context' => 'design/standard/content/edit_languages',
    'source' => 'Use an empty, untranslated draft',
    'comment' => NULL,
    'translation' => '翻訳されていない、空な下書きを使ってください',
    'key' => 'c8f0a2b53868352f9593f3854c1ce507',
  ),
  '1d504572f486361f76fd1d941a9158aa' => 
  array (
    'context' => 'design/standard/content/edit_languages',
    'source' => 'You do not have permission to create a translation in another language.',
    'comment' => NULL,
    'translation' => '違う言語で翻訳を作る権限がありません。',
    'key' => '1d504572f486361f76fd1d941a9158aa',
  ),
);
?>
