<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/user/activate',
);

$TranslationRoot = array (
  'e954fdce838147c44f47a4abf0762dad' => 
  array (
    'context' => 'design/ezwebin/user/activate',
    'source' => 'Activate account',
    'comment' => NULL,
    'translation' => 'アカウントを有効にする',
    'key' => 'e954fdce838147c44f47a4abf0762dad',
  ),
  '35dc28d5778d7099eb12a17f1868e056' => 
  array (
    'context' => 'design/ezwebin/user/activate',
    'source' => 'Your account is now activated.',
    'comment' => NULL,
    'translation' => 'アカウントは有効になりました。',
    'key' => '35dc28d5778d7099eb12a17f1868e056',
  ),
  '71d451bd25cdcce4d7fab45232d092e3' => 
  array (
    'context' => 'design/ezwebin/user/activate',
    'source' => 'Sorry, the key submitted was not a valid key. Account was not activated.',
    'comment' => NULL,
    'translation' => 'パスワードが無効なためアカウントを有効に出来ません。',
    'key' => '71d451bd25cdcce4d7fab45232d092e3',
  ),
  '2dbda07560e1eca82f62bc73b443e9bc' => 
  array (
    'context' => 'design/ezwebin/user/activate',
    'source' => 'Your account is already active.',
    'comment' => NULL,
    'translation' => 'あなたのアカウントは既に有効です。',
    'key' => '2dbda07560e1eca82f62bc73b443e9bc',
  ),
  'c5581e858628f7170eda8437cfbe7ec2' => 
  array (
    'context' => 'design/ezwebin/user/activate',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => 'c5581e858628f7170eda8437cfbe7ec2',
  ),
);
?>
