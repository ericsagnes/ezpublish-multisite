<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/visual/toolbar',
);

$TranslationRoot = array (
  '99cc4678bb1b0cc37a5dfb5b65f3f485' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Browse',
    'comment' => NULL,
    'translation' => 'ブラウズ',
    'key' => '99cc4678bb1b0cc37a5dfb5b65f3f485',
  ),
  '91514033b5375ab1a711f9d93cec3a7f' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'True',
    'comment' => NULL,
    'translation' => '真',
    'key' => '91514033b5375ab1a711f9d93cec3a7f',
  ),
  '75d707e2eb0d0a6d722fcee998db6a03' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'False',
    'comment' => NULL,
    'translation' => '偽',
    'key' => '75d707e2eb0d0a6d722fcee998db6a03',
  ),
  'abe8cfc8b319d631c0dcb867ece90bb6' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Yes',
    'comment' => NULL,
    'translation' => 'はい',
    'key' => 'abe8cfc8b319d631c0dcb867ece90bb6',
  ),
  'fd7d4992ce3cf9e785a53cafa5ff5536' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'No',
    'comment' => NULL,
    'translation' => 'いいえ',
    'key' => 'fd7d4992ce3cf9e785a53cafa5ff5536',
  ),
  '24e20c36533a9084747fef1f6b3cf45d' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'There are currently no tools in this toolbar',
    'comment' => NULL,
    'translation' => 'ツールバーには、ツールが設定されていません',
    'key' => '24e20c36533a9084747fef1f6b3cf45d',
  ),
  'b93e663e6c2ac47cc3712a25205cedf6' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Remove selected',
    'comment' => NULL,
    'translation' => '選択した項目の削除',
    'key' => 'b93e663e6c2ac47cc3712a25205cedf6',
  ),
  '7446b75616c37ae6bcb500c0fd6a8a01' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Update priorities',
    'comment' => NULL,
    'translation' => '優先度の更新',
    'key' => '7446b75616c37ae6bcb500c0fd6a8a01',
  ),
  'bf88300647409f6b65771fd9f776bb2a' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Add Tool',
    'comment' => NULL,
    'translation' => 'ツールの追加',
    'key' => 'bf88300647409f6b65771fd9f776bb2a',
  ),
  'cb4431f8a01c8795f1ba26f0058cd7c6' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Apply changes',
    'comment' => NULL,
    'translation' => '変更の適用',
    'key' => 'cb4431f8a01c8795f1ba26f0058cd7c6',
  ),
  'ee0a77a8b85fff28a0e3c7f7cc7f3c66' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Click this button to store changes if you have modified the parameters above.',
    'comment' => NULL,
    'translation' => '上記のパラメータを修正した場合、このボタンをクリックして修正を保存して下さい。',
    'key' => 'ee0a77a8b85fff28a0e3c7f7cc7f3c66',
  ),
  'df52ed30ce660f025173fd481c26839d' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Back to toolbars',
    'comment' => NULL,
    'translation' => 'ツールバーに戻る',
    'key' => 'df52ed30ce660f025173fd481c26839d',
  ),
  '13ceef39baadf37859eac957d770fa48' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Go back to the toolbar list.',
    'comment' => NULL,
    'translation' => 'ツールバー一覧に戻る',
    'key' => '13ceef39baadf37859eac957d770fa48',
  ),
  '2026ef2177070654f3785f366a869c1b' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Toolbar management',
    'comment' => NULL,
    'translation' => 'ツールバー管理',
    'key' => '2026ef2177070654f3785f366a869c1b',
  ),
  '34b9e0deeb8eb6e44db4dfedabcef876' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Current siteaccess',
    'comment' => NULL,
    'translation' => '現在のサイトアクセス',
    'key' => '34b9e0deeb8eb6e44db4dfedabcef876',
  ),
  'ed7e78ff7909ccb14af38351519a9850' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Select siteaccess',
    'comment' => NULL,
    'translation' => 'サイトアクセスの選択',
    'key' => 'ed7e78ff7909ccb14af38351519a9850',
  ),
  'c38a316572e659022f247caa25c706c1' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Set',
    'comment' => NULL,
    'translation' => '設定',
    'key' => 'c38a316572e659022f247caa25c706c1',
  ),
  'ec32aa95fbb2e2e76bbd7aded5aa1f3e' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Available toolbars for the <%siteaccess> siteaccess',
    'comment' => NULL,
    'translation' => '<%siteaccess> サイトアクセスで利用可能なツールバー',
    'key' => 'ec32aa95fbb2e2e76bbd7aded5aa1f3e',
  ),
  'f659d76a9442eacc475ea89997392e18' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Tool list for <Toolbar_%toolbar_position>',
    'comment' => NULL,
    'translation' => '<Toolbar_%toolbar_position>のツールリスト',
    'key' => 'f659d76a9442eacc475ea89997392e18',
  ),
  'daca325ac0de52e65274f2edbdf08199' => 
  array (
    'context' => 'design/admin/visual/toolbar',
    'source' => 'Siteaccess',
    'comment' => NULL,
    'translation' => 'サイトアクセス',
    'key' => 'daca325ac0de52e65274f2edbdf08199',
  ),
);
?>
