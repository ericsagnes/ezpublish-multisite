<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/class',
);

$TranslationRoot = array (
  '8934225f0c98ff0da607a501c14b4a63' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Class list of group',
    'comment' => NULL,
    'translation' => 'クラスカテゴリのクラス一覧',
    'key' => '8934225f0c98ff0da607a501c14b4a63',
  ),
  '4ac5ff7c1b12e46de4bcea6eb2b19df9' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Class group list',
    'comment' => NULL,
    'translation' => 'クラスカテゴリ一覧',
    'key' => '4ac5ff7c1b12e46de4bcea6eb2b19df9',
  ),
  '3047f688f4f863f8ac85566367348682' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Remove class',
    'comment' => NULL,
    'translation' => 'クラスの削除',
    'key' => '3047f688f4f863f8ac85566367348682',
  ),
  '96b7494b68f25db972326c8a25bc560e' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Class edit',
    'comment' => NULL,
    'translation' => 'クラスの編集',
    'key' => '96b7494b68f25db972326c8a25bc560e',
  ),
  '47545c937d65ef9713192e1915528c43' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Classes',
    'comment' => NULL,
    'translation' => 'クラス',
    'key' => '47545c937d65ef9713192e1915528c43',
  ),
  '4f12af7ddf2dc90b869b39c8f0c77462' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Class list',
    'comment' => NULL,
    'translation' => 'クラス一覧',
    'key' => '4f12af7ddf2dc90b869b39c8f0c77462',
  ),
  'a99cc4ecca825c91eb72ad569108640b' => 
  array (
    'context' => 'kernel/class',
    'source' => '(no classes)',
    'comment' => NULL,
    'translation' => '（クラスなし）',
    'key' => 'a99cc4ecca825c91eb72ad569108640b',
  ),
  'f3b0b60b39feca001d5e14525860cd50' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Remove class groups',
    'comment' => NULL,
    'translation' => 'クラスカテゴリの削除',
    'key' => 'f3b0b60b39feca001d5e14525860cd50',
  ),
  'ee7626c7b1598a6be2dbee1c260e8d46' => 
  array (
    'context' => 'kernel/class',
    'source' => 'You have to have at least one group that the class belongs to!',
    'comment' => NULL,
    'translation' => 'クラスは最低一つのクラスカテゴリに所属する必要があります!',
    'key' => 'ee7626c7b1598a6be2dbee1c260e8d46',
  ),
  'cd8720a97dfc3616496eaf9d97653049' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Remove classes %class_id',
    'comment' => NULL,
    'translation' => 'クラス %class_id の削除',
    'key' => 'cd8720a97dfc3616496eaf9d97653049',
  ),
  '777cc3de6104f7a596d6999d7587f15f' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Copy of %class_name',
    'comment' => NULL,
    'translation' => '%class_name の複製',
    'key' => '777cc3de6104f7a596d6999d7587f15f',
  ),
  'c2acf926a055fe40bd8eaf4adc20ee12' => 
  array (
    'context' => 'kernel/class',
    'source' => 'The class should have nonempty \'Name\' attribute.',
    'comment' => NULL,
    'translation' => 'クラスには入力必須の \'Name\' 属性が必要です。',
    'key' => 'c2acf926a055fe40bd8eaf4adc20ee12',
  ),
  '89c7766befea10af075f946008a921a3' => 
  array (
    'context' => 'kernel/class',
    'source' => 'The class should have at least one attribute.',
    'comment' => NULL,
    'translation' => 'クラスには属性が一つ以上必要です。',
    'key' => '89c7766befea10af075f946008a921a3',
  ),
  '5b4f4d36fe521246464267d709e5d060' => 
  array (
    'context' => 'kernel/class',
    'source' => 'There is a class already having the same identifier.',
    'comment' => NULL,
    'translation' => '同じ識別子のクラスがすでに存在します。',
    'key' => '5b4f4d36fe521246464267d709e5d060',
  ),
  '406aa00f8d77f4dd761185eecfca5d8f' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Remove translation',
    'comment' => NULL,
    'translation' => '翻訳を削除',
    'key' => '406aa00f8d77f4dd761185eecfca5d8f',
  ),
  '1d796bdef2a779d2261e970e65e85901' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Could not load datatype: ',
    'comment' => NULL,
    'translation' => 'データタイプを読み込めません:',
    'key' => '1d796bdef2a779d2261e970e65e85901',
  ),
  'ded5cad415f080ec7f4b2eb932bfa23b' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Editing this content class may cause data corruption in your system.',
    'comment' => NULL,
    'translation' => 'このコンテンツクラスを編集すると、データ損失の恐れがあります。',
    'key' => 'ded5cad415f080ec7f4b2eb932bfa23b',
  ),
  '14cc9d6dbae76449fecb39aece3f5ed4' => 
  array (
    'context' => 'kernel/class',
    'source' => 'duplicate attribute placement',
    'comment' => NULL,
    'translation' => '属性の配置が重複しています',
    'key' => '14cc9d6dbae76449fecb39aece3f5ed4',
  ),
  '0c8abaf27fab58b1b93f0fcde7ee3bbb' => 
  array (
    'context' => 'kernel/class',
    'source' => 'duplicate attribute identifier',
    'comment' => NULL,
    'translation' => '属性の識別子が重複しています',
    'key' => '0c8abaf27fab58b1b93f0fcde7ee3bbb',
  ),
  'dbb3e43a92859f2e622049974281f094' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Press "Cancel" to safely exit this operation.',
    'comment' => NULL,
    'translation' => 'このオペレーションを安全に止めたい場合は、キャンセルをクリックして下さい。',
    'key' => 'dbb3e43a92859f2e622049974281f094',
  ),
  'f5c86a8823f517a69ddd26534cc23511' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Please contact your eZ Publish administrator to solve this problem.',
    'comment' => NULL,
    'translation' => 'この問題を解決するには、eZ Publish管理者へ連絡してください。',
    'key' => 'f5c86a8823f517a69ddd26534cc23511',
  ),
  '3eb55262e36abc2ee38ed17a0296ae3f' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Class groups',
    'comment' => NULL,
    'translation' => 'クラスカテゴリ',
    'key' => '3eb55262e36abc2ee38ed17a0296ae3f',
  ),
  '718f784e64e842b0b83dc1677b636b85' => 
  array (
    'context' => 'kernel/class',
    'source' => 'Remove classes',
    'comment' => NULL,
    'translation' => 'クラスの削除',
    'key' => '718f784e64e842b0b83dc1677b636b85',
  ),
);
?>
