<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/article/comments',
);

$TranslationRoot = array (
  '82aef47849ce43a3c2cb52cb2fe8fee7' => 
  array (
    'context' => 'design/ezwebin/article/comments',
    'source' => 'Comments',
    'comment' => NULL,
    'translation' => 'コメント',
    'key' => '82aef47849ce43a3c2cb52cb2fe8fee7',
  ),
  '65c606faee2137205b1c3618cb74c664' => 
  array (
    'context' => 'design/ezwebin/article/comments',
    'source' => 'New comment',
    'comment' => NULL,
    'translation' => '新しいコメント',
    'key' => '65c606faee2137205b1c3618cb74c664',
  ),
  '581abb54793eb25c979e25de12f6dca3' => 
  array (
    'context' => 'design/ezwebin/article/comments',
    'source' => '%login_link_startLog in%login_link_end or %create_link_startcreate a user account%create_link_end to comment.',
    'comment' => NULL,
    'translation' => 'コメントを投稿するには、%login_link_startログインする%login_link_end か %create_link_startユーザアカウントを作成してください%create_link_end',
    'key' => '581abb54793eb25c979e25de12f6dca3',
  ),
  '811a9133431a0a8ef225206bba03c3ef' => 
  array (
    'context' => 'design/ezwebin/article/comments',
    'source' => '%login_link_startLog in%login_link_end to comment.',
    'comment' => NULL,
    'translation' => 'コメントを投稿するには、%login_link_startログイン%login_link_endして下さい。',
    'key' => '811a9133431a0a8ef225206bba03c3ef',
  ),
);
?>
