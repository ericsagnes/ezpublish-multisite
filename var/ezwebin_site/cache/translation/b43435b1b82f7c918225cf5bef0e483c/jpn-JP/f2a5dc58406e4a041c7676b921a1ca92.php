<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/rss/browse_image',
);

$TranslationRoot = array (
  'dba7d8fd4dea584e4cd6e801c372b6bd' => 
  array (
    'context' => 'design/admin/rss/browse_image',
    'source' => 'Choose image for RSS export',
    'comment' => NULL,
    'translation' => 'RSSエクスポート画像の選択',
    'key' => 'dba7d8fd4dea584e4cd6e801c372b6bd',
  ),
  'c012884ce35e2b1f2f5a8db635a0d88f' => 
  array (
    'context' => 'design/admin/rss/browse_image',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => 'c012884ce35e2b1f2f5a8db635a0d88f',
  ),
  '4ad42a0e56bcf39851ef6a7e0c223e5e' => 
  array (
    'context' => 'design/admin/rss/browse_image',
    'source' => 'Use the radio buttons to choose an image to use in the RSS export then click "OK".',
    'comment' => NULL,
    'translation' => 'ラジオボタンを使って、RSSエクスポートで使用する画像を選択し、"OK"をクリックしてください。',
    'key' => '4ad42a0e56bcf39851ef6a7e0c223e5e',
  ),
);
?>
