<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/setup/operatorcode',
);

$TranslationRoot = array (
  '1e77b983a8df86ddb5a5138796c8bc41' => 
  array (
    'context' => 'design/standard/setup/operatorcode',
    'source' => 'Example',
    'comment' => NULL,
    'translation' => '例',
    'key' => '1e77b983a8df86ddb5a5138796c8bc41',
  ),
  'c9bd8e7e82684fad8d41567e990d7123' => 
  array (
    'context' => 'design/standard/setup/operatorcode',
    'source' => 'Constructor, does nothing by default.',
    'comment' => NULL,
    'translation' => 'コンストラクター。デフォルトでは何も行いません。',
    'key' => 'c9bd8e7e82684fad8d41567e990d7123',
  ),
  'c2c4a4d37519f2f8a8c07e2e377f764c' => 
  array (
    'context' => 'design/standard/setup/operatorcode',
    'source' => 'Executes the PHP function for the operator cleanup and modifies \\a $operatorValue.',
    'comment' => NULL,
    'translation' => 'オペレーターのクリーンアップPHPファンクションを実行し、\\a $operatorValueを編集して下さい。',
    'key' => 'c2c4a4d37519f2f8a8c07e2e377f764c',
  ),
  '88d23f8bf974f4eafc63c6494f713ffe' => 
  array (
    'context' => 'design/standard/setup/operatorcode',
    'source' => 'Example code. This code must be modified to do what the operator should do. Currently it only trims text.',
    'comment' => NULL,
    'translation' => 'コードの例。このコードはオペレータの目的に合わせて変更してください。現在は文字列の先頭および末尾にあるホワイトスペースを取り除くだけです。',
    'key' => '88d23f8bf974f4eafc63c6494f713ffe',
  ),
  '6a14cf17c0ab3a66632c78d1226b60d4' => 
  array (
    'context' => 'design/standard/setup/operatorcode',
    'source' => '\\\\return an array with the template operator name.',
    'comment' => NULL,
    'translation' => '\\\\テンプレートオペレーター名が入っている配列を戻します。',
    'key' => '6a14cf17c0ab3a66632c78d1226b60d4',
  ),
);
?>
