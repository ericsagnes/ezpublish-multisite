<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/notification/addingresult',
);

$TranslationRoot = array (
  'f77da0ebaa3f05e4bd6abb2552bdc4f8' => 
  array (
    'context' => 'design/ezwebin/notification/addingresult',
    'source' => 'Add to my notifications',
    'comment' => NULL,
    'translation' => '通知リストに追加',
    'key' => 'f77da0ebaa3f05e4bd6abb2552bdc4f8',
  ),
  '0d0957e56416810b7d13916fb1505ce9' => 
  array (
    'context' => 'design/ezwebin/notification/addingresult',
    'source' => 'Notification for node <%node_name> already exists.',
    'comment' => NULL,
    'translation' => 'ノード <%node_name>の通知は既に追加されています。',
    'key' => '0d0957e56416810b7d13916fb1505ce9',
  ),
  'a2ac2a7490120727ffead292b3097598' => 
  array (
    'context' => 'design/ezwebin/notification/addingresult',
    'source' => 'Notification for node <%node_name> was added successfully.',
    'comment' => NULL,
    'translation' => 'ノード<%node_name>の通知が加わりました。',
    'key' => 'a2ac2a7490120727ffead292b3097598',
  ),
  'b660fa796012d82150c84615d28840ab' => 
  array (
    'context' => 'design/ezwebin/notification/addingresult',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => 'b660fa796012d82150c84615d28840ab',
  ),
);
?>
