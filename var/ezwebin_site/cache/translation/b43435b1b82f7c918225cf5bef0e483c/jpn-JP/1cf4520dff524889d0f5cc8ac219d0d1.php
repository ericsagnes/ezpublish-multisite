<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/standard/content/diff',
);

$TranslationRoot = array (
  'a58acc19620418a8f2b4a014d53e59b4' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => 'a58acc19620418a8f2b4a014d53e59b4',
  ),
  'e59b87c8f7ab44e1694b3b087eb197e7' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Status',
    'comment' => NULL,
    'translation' => 'ステータス',
    'key' => 'e59b87c8f7ab44e1694b3b087eb197e7',
  ),
  '36a02b2234234fef6eaa65ee48291ecf' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Translations',
    'comment' => NULL,
    'translation' => '翻訳',
    'key' => '36a02b2234234fef6eaa65ee48291ecf',
  ),
  '2b8870843443b2e75a1e16bd4852b71c' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Creator',
    'comment' => NULL,
    'translation' => '作成者',
    'key' => '2b8870843443b2e75a1e16bd4852b71c',
  ),
  '70fb147d77893b61590281ceac5e3ac2' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '70fb147d77893b61590281ceac5e3ac2',
  ),
  'eb18a5ae976653576139a78d72ea2ed1' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Versions for <%object_name> [%version_count]',
    'comment' => NULL,
    'translation' => '<%object_name> [%version_count] のバージョン',
    'key' => 'eb18a5ae976653576139a78d72ea2ed1',
  ),
  'c4b285ab6427771afc809f4631c9dc3d' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Draft',
    'comment' => NULL,
    'translation' => '下書き',
    'key' => 'c4b285ab6427771afc809f4631c9dc3d',
  ),
  'b2f79d8d45f35f0c7642f6d84d0603f0' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Published',
    'comment' => NULL,
    'translation' => '公開済み',
    'key' => 'b2f79d8d45f35f0c7642f6d84d0603f0',
  ),
  'dd1405f25a7e24354819ff4463242b48' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Pending',
    'comment' => NULL,
    'translation' => '保留中',
    'key' => 'dd1405f25a7e24354819ff4463242b48',
  ),
  '7f178f2b45db7eaf2bed0ba9d569beda' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Archived',
    'comment' => NULL,
    'translation' => '保管',
    'key' => '7f178f2b45db7eaf2bed0ba9d569beda',
  ),
  '506631dea01a067d2bbc5ca245107c34' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Rejected',
    'comment' => NULL,
    'translation' => '非承認',
    'key' => '506631dea01a067d2bbc5ca245107c34',
  ),
  'e1c3c715f7f0885ad98f97a1a1535651' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Untouched draft',
    'comment' => NULL,
    'translation' => '未変更の下書き',
    'key' => 'e1c3c715f7f0885ad98f97a1a1535651',
  ),
  'ba67f9644bfde007295005ecb1661663' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Show differences',
    'comment' => NULL,
    'translation' => '違いの表示',
    'key' => 'ba67f9644bfde007295005ecb1661663',
  ),
  '9d59f7e0a70f3ea55912d602a45d0abc' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Differences between versions %oldVersion and %newVersion',
    'comment' => NULL,
    'translation' => 'バージョン %oldVersion と %newVersion の違い',
    'key' => '9d59f7e0a70f3ea55912d602a45d0abc',
  ),
  '8ac8a100702cfe41c37396695d54185c' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Inline changes',
    'comment' => NULL,
    'translation' => 'インライン変更',
    'key' => '8ac8a100702cfe41c37396695d54185c',
  ),
  '05ce1ec9ba32980fab3bc36d57cf3eb9' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Block changes',
    'comment' => NULL,
    'translation' => 'ブロックの変更',
    'key' => '05ce1ec9ba32980fab3bc36d57cf3eb9',
  ),
  '3f8cc685deb3ed997fc3adc53ee9ad09' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'Old version',
    'comment' => NULL,
    'translation' => '旧バージョン',
    'key' => '3f8cc685deb3ed997fc3adc53ee9ad09',
  ),
  '0a7125d7384912fab909d9374621da83' => 
  array (
    'context' => 'design/standard/content/diff',
    'source' => 'New version',
    'comment' => NULL,
    'translation' => '新バージョン',
    'key' => '0a7125d7384912fab909d9374621da83',
  ),
);
?>
