<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/notification/collaboration',
);

$TranslationRoot = array (
  'ec8049550ef5e7d6a357f233a337c6a0' => 
  array (
    'context' => 'design/admin/notification/collaboration',
    'source' => 'Collaboration notification',
    'comment' => NULL,
    'translation' => '協同作業の通知',
    'key' => 'ec8049550ef5e7d6a357f233a337c6a0',
  ),
  '234508895ca0783b511b6c069ec84261' => 
  array (
    'context' => 'design/admin/notification/collaboration',
    'source' => 'Choose which collaboration items you want to get notifications for.',
    'comment' => NULL,
    'translation' => '通知を受けたい協同作業アイテムを選択してください。',
    'key' => '234508895ca0783b511b6c069ec84261',
  ),
);
?>
