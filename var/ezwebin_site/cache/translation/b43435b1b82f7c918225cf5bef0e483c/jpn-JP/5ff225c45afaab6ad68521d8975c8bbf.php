<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/ezwebin/edit/forum_reply',
);

$TranslationRoot = array (
  'cb8f8f499e37289356f6b12f54de3a5c' => 
  array (
    'context' => 'design/ezwebin/edit/forum_reply',
    'source' => 'Edit %1 - %2',
    'comment' => NULL,
    'translation' => '%1 - %2の編集',
    'key' => 'cb8f8f499e37289356f6b12f54de3a5c',
  ),
  '77371d425404f2f97653e76dba4832f3' => 
  array (
    'context' => 'design/ezwebin/edit/forum_reply',
    'source' => 'Send for publishing',
    'comment' => NULL,
    'translation' => '送信して公開',
    'key' => '77371d425404f2f97653e76dba4832f3',
  ),
  '58d89507dced58ba89c5a01761d84b10' => 
  array (
    'context' => 'design/ezwebin/edit/forum_reply',
    'source' => 'Discard',
    'comment' => NULL,
    'translation' => '破棄',
    'key' => '58d89507dced58ba89c5a01761d84b10',
  ),
);
?>
