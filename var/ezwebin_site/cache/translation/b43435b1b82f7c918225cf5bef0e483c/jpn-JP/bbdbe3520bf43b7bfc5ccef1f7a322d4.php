<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'extension/ezodf/import/error',
);

$TranslationRoot = array (
  '256ba41430ad759705ce01aff8106d37' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'File extension or type is not allowed.',
    'comment' => NULL,
    'translation' => 'ファイルエクステンション又はタイプが無効です。',
    'key' => '256ba41430ad759705ce01aff8106d37',
  ),
  '7846d274b94c74d1713ce5e541bc6485' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Could not parse XML.',
    'comment' => NULL,
    'translation' => 'XMLを分析できませんでした',
    'key' => '7846d274b94c74d1713ce5e541bc6485',
  ),
  '6e7800c9dffff59fb6d4b15a66ea0520' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Can not open socket. Please check if extension/ezodf/daemon.php is running.',
    'comment' => NULL,
    'translation' => 'ソケットを開けません。extension/ezodf/daemon.phpが起動しているか確認して下さい。',
    'key' => '6e7800c9dffff59fb6d4b15a66ea0520',
  ),
  '6717742745cedb3ceb6c002399248b60' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Can not convert the given document.',
    'comment' => NULL,
    'translation' => 'このドキュメントを変換できません。',
    'key' => '6717742745cedb3ceb6c002399248b60',
  ),
  'f1c07342b9999e429a150fbe46098b91' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Unable to call daemon. Fork can not create child process.',
    'comment' => NULL,
    'translation' => 'デーモンへアクセスできません。フォークがチャイルドプロセスを作成できません。',
    'key' => 'f1c07342b9999e429a150fbe46098b91',
  ),
  '23b2f51844841665ab4a4201cd1cc222' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Daemon reported error.',
    'comment' => NULL,
    'translation' => 'デーモンがエラーを起こしています。',
    'key' => '23b2f51844841665ab4a4201cd1cc222',
  ),
  '9015d4ad82e83ee0a962403d1e5ac82e' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Unknown node.',
    'comment' => NULL,
    'translation' => '不明なノード',
    'key' => '9015d4ad82e83ee0a962403d1e5ac82e',
  ),
  '6edb51272258d3566f247b65a84f108f' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Access denied.',
    'comment' => NULL,
    'translation' => 'アクセスが拒否されました',
    'key' => '6edb51272258d3566f247b65a84f108f',
  ),
  '3c5192748a68cf847b659bcd5e147a45' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Error during import.',
    'comment' => NULL,
    'translation' => 'インポート中にエラーが起きました',
    'key' => '3c5192748a68cf847b659bcd5e147a45',
  ),
  'e0fcc75f3fc29bece774644005a7b70d' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Unknown content class specified in odf.ini:',
    'comment' => NULL,
    'translation' => 'odf.iniに不明なコンテンツクラスが見つかりました:',
    'key' => 'e0fcc75f3fc29bece774644005a7b70d',
  ),
  '00c29ce6fa8916fddf71ed51797b8f98' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Unknown error.',
    'comment' => NULL,
    'translation' => '不明なエラー',
    'key' => '00c29ce6fa8916fddf71ed51797b8f98',
  ),
  'bc6ade820c92c58ff873aa9c94e0389c' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Filetype: ',
    'comment' => NULL,
    'translation' => 'ファイルタイプ:',
    'key' => 'bc6ade820c92c58ff873aa9c94e0389c',
  ),
  '787dd514b93a12adbcdaf077f55597b8' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Folder for images could not be created, access denied.',
    'comment' => NULL,
    'translation' => 'アクセスが拒否されました。画像フォルダーが作成できませんでした。',
    'key' => '787dd514b93a12adbcdaf077f55597b8',
  ),
  '1e1927a2af17225dec8ab102c687c6b9' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Cannot import. File not found. Already imported?',
    'comment' => NULL,
    'translation' => 'インポートできません。ファイルが見つかりませんでした。既にインポートしていますか?',
    'key' => '1e1927a2af17225dec8ab102c687c6b9',
  ),
  '411ca5db0f257180d4153a5e856167ec' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Cannot import document, supplied placement nodeID is not valid.',
    'comment' => NULL,
    'translation' => 'ドキュメントをインポートできません。ノードIDが無効です。',
    'key' => '411ca5db0f257180d4153a5e856167ec',
  ),
  '4cf18c9b43e0f7a972870f0d6104dc6c' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Cannot store uploaded file, cannot import.',
    'comment' => NULL,
    'translation' => 'アップロードしたファイルを保存できません。',
    'key' => '4cf18c9b43e0f7a972870f0d6104dc6c',
  ),
  '2d0e2fb9b74caa41db03e7473aa84bc2' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Unable to fetch node with id ',
    'comment' => NULL,
    'translation' => 'ノードが見つかりません',
    'key' => '2d0e2fb9b74caa41db03e7473aa84bc2',
  ),
  'b58394b88c56a25eb871cf62ddc7f835' => 
  array (
    'context' => 'extension/ezodf/import/error',
    'source' => 'Document is not supported.',
    'comment' => NULL,
    'translation' => 'ドキュメントはサポートされていません。',
    'key' => 'b58394b88c56a25eb871cf62ddc7f835',
  ),
);
?>
