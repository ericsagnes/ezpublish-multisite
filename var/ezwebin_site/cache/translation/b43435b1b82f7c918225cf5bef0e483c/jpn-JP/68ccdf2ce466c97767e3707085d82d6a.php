<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'kernel/content/upload',
);

$TranslationRoot = array (
  'c86fcb0f4aa20d3bbabfa36394e3d483' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'The file %filename does not exist, cannot insert file.',
    'comment' => NULL,
    'translation' => 'ファイル %filenameは存在しません。ファイルを挿入出来ません。',
    'key' => 'c86fcb0f4aa20d3bbabfa36394e3d483',
  ),
  '5e2795767372b81bbb9a0e1540a8c062' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'There was an error trying to instantiate content upload handler.',
    'comment' => NULL,
    'translation' => 'コンテンツアップロードハンドラのインスタンス化時にエラーが発生しました。',
    'key' => '5e2795767372b81bbb9a0e1540a8c062',
  ),
  'ee4083961eca2b879668c8889bc3afea' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'No matching class identifier found.',
    'comment' => NULL,
    'translation' => '一致するクラス識別子は見つかりませんでした。',
    'key' => 'ee4083961eca2b879668c8889bc3afea',
  ),
  '193e9101f443027d56d33f67f4b42729' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'The class %class_identifier does not exist.',
    'comment' => NULL,
    'translation' => 'クラス%class_identifierは存在しません。',
    'key' => '193e9101f443027d56d33f67f4b42729',
  ),
  'ba2f5ff84d0ee9b64ff570c2ad9db2b3' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'Was not able to figure out placement of object.',
    'comment' => NULL,
    'translation' => 'オブジェクトの配置先を見つけることが出来ませんでした。',
    'key' => 'ba2f5ff84d0ee9b64ff570c2ad9db2b3',
  ),
  '895362820ebfc0afe259c11ec770c695' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'Permission denied',
    'comment' => NULL,
    'translation' => 'アクセス許可が拒否されました',
    'key' => '895362820ebfc0afe259c11ec770c695',
  ),
  '11d597cf5b6ec7d6241637f1ebcd7e16' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'No configuration group in upload.ini for class identifier %class_identifier.',
    'comment' => NULL,
    'translation' => 'upload.iniにクラス識別子%class_identifierの設定グループが見つかりません。',
    'key' => '11d597cf5b6ec7d6241637f1ebcd7e16',
  ),
  'aaf902b23128772b5ab9f307872fd258' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'No matching file attribute found, cannot create content object without this.',
    'comment' => NULL,
    'translation' => '一致するファイル属性が見つかりません。コンテンツオブジェクトを作成することは出来ません。',
    'key' => 'aaf902b23128772b5ab9f307872fd258',
  ),
  '44a198836cb0e8bf7b80bfdf5027990a' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'No matching name attribute found, cannot create content object without this.',
    'comment' => NULL,
    'translation' => '一致する名前属性が見つかりません。コンテンツオブジェクトを作成することは出来ません。',
    'key' => '44a198836cb0e8bf7b80bfdf5027990a',
  ),
  '9df5c2386d028da9a40e6024ffa4109b' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'The attribute %class_identifier does not support regular file storage.',
    'comment' => NULL,
    'translation' => '属性%class_identifierは通常のファイルストレージをサポートしません。',
    'key' => '9df5c2386d028da9a40e6024ffa4109b',
  ),
  '9d793c2471b012c9b3cef0f092d2997e' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'The attribute %class_identifier does not support simple string storage.',
    'comment' => NULL,
    'translation' => '属性%class_identifierは単純文字列ストレージをサポートしていません。',
    'key' => '9d793c2471b012c9b3cef0f092d2997e',
  ),
  'b427b8221eb6db47848ef0fba3443a8c' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'No HTTP file found, cannot fetch uploaded file.',
    'comment' => NULL,
    'translation' => 'HTTPファイルが見つかりません。アップロードされたファイルを取得することが出来ません。',
    'key' => 'b427b8221eb6db47848ef0fba3443a8c',
  ),
  '00676c5a5280a3aba58bbafbca5241d6' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'The size of the uploaded file exceeds the limit set for this site: %1 bytes.',
    'comment' => NULL,
    'translation' => 'アップロードしたファイルはこのサイトの制限である%1バイトを超えています。',
    'key' => '00676c5a5280a3aba58bbafbca5241d6',
  ),
  '6561eefb72d989e0f7ceeb5dbf0b9ab4' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'The attribute %class_identifier does not support HTTP file storage.',
    'comment' => NULL,
    'translation' => '属性%class_identifierはHTTPファイル保存をサポートしていません。',
    'key' => '6561eefb72d989e0f7ceeb5dbf0b9ab4',
  ),
  '37f11de51d14549b548437fb051c2a38' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'Publishing of content object was halted.',
    'comment' => NULL,
    'translation' => 'コンテンツオブジェクトの公開は停止されました。',
    'key' => '37f11de51d14549b548437fb051c2a38',
  ),
  '0ce6e07aa313aa5d3b782ccdec509b93' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'Publish process was cancelled.',
    'comment' => NULL,
    'translation' => '公開プロセスはキャンセルされました。',
    'key' => '0ce6e07aa313aa5d3b782ccdec509b93',
  ),
  '49776e71bfc223025646a1b5fc8b4fae' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'A file is required for upload, no file were found.',
    'comment' => NULL,
    'translation' => 'アップロードにはファイルが必要です。ファイルが見つかりません。',
    'key' => '49776e71bfc223025646a1b5fc8b4fae',
  ),
  '129d0938c9cbfe1252866ea4314735fc' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'Expected a eZHTTPFile object but got nothing.',
    'comment' => NULL,
    'translation' => 'eZHTTPFileオブジェクトを期待しましたが、何も見つかりませんでした。',
    'key' => '129d0938c9cbfe1252866ea4314735fc',
  ),
  '15c942eba06a57d0c72385b988e8fc89' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'Could not find content upload handler \'%handler_name\'',
    'comment' => NULL,
    'translation' => 'コンテンツアップロードハンドラ \'%handler_name\' が見つかりません',
    'key' => '15c942eba06a57d0c72385b988e8fc89',
  ),
  '7d4975a7d96f068dcaecaecef14f3979' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'The uploaded file size is above the maximum limit.',
    'comment' => NULL,
    'translation' => 'アップロードされたファイルのサイズは最大限を越えています。',
    'key' => '7d4975a7d96f068dcaecaecef14f3979',
  ),
  'bedef40b792ef4ef1e3f2ffb6e0d4fae' => 
  array (
    'context' => 'kernel/content/upload',
    'source' => 'A system error occured while writing the uploaded file.',
    'comment' => NULL,
    'translation' => 'アップロードファイル書き込みの際にシステムエラーが発生しました。',
    'key' => 'bedef40b792ef4ef1e3f2ffb6e0d4fae',
  ),
);
?>
