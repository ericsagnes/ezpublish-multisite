<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/section/browse_assign',
);

$TranslationRoot = array (
  '9134b1fd78352439f85f226ccf17c3d1' => 
  array (
    'context' => 'design/admin/section/browse_assign',
    'source' => 'Choose start location for the <%section_name> section',
    'comment' => NULL,
    'translation' => '<%section_name> セクションを開始する配置を選択',
    'key' => '9134b1fd78352439f85f226ccf17c3d1',
  ),
  'ee8bd703e15d61d39c3a76195136875b' => 
  array (
    'context' => 'design/admin/section/browse_assign',
    'source' => 'Use the radio buttons to select an item that should have the <%section_name> section assigned.',
    'comment' => NULL,
    'translation' => 'ラジオボタンを使用して <%section_name> セクションを割り当てるアイテムを選択してください。',
    'key' => 'ee8bd703e15d61d39c3a76195136875b',
  ),
  'cf4490ab70efee440b4df72ddec94666' => 
  array (
    'context' => 'design/admin/section/browse_assign',
    'source' => 'Navigate using the available tabs (above), the tree menu (left) and the content list (middle).',
    'comment' => NULL,
    'translation' => 'ナビゲーションにはタブ（上部)、コンテンツツリー（左)、コンテンツ一覧（真ん中）を使用して下さい。',
    'key' => 'cf4490ab70efee440b4df72ddec94666',
  ),
  '2ef0033f79f0f23777285418ad484bb5' => 
  array (
    'context' => 'design/admin/section/browse_assign',
    'source' => 'Note that the section assignment of the sub items will also be changed.',
    'comment' => NULL,
    'translation' => 'サブアイテムのセクションも一緒に変更されますので、ご注意下さい。',
    'key' => '2ef0033f79f0f23777285418ad484bb5',
  ),
);
?>
