<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'extension/ezmultiupload',
);

$TranslationRoot = array (
  'bd524c0fff836686a7c2bde5359e103f' => 
  array (
    'context' => 'extension/ezmultiupload',
    'source' => 'Allowed Files',
    'comment' => NULL,
    'translation' => '利用できるファイル',
    'key' => 'bd524c0fff836686a7c2bde5359e103f',
  ),
  '5fda3f2711eb67896846496ae6b2ab52' => 
  array (
    'context' => 'extension/ezmultiupload',
    'source' => 'All files received.',
    'comment' => NULL,
    'translation' => 'すべてのファイルを受信できました。',
    'key' => '5fda3f2711eb67896846496ae6b2ab52',
  ),
  'b397645928903acc2c0fb43b03988569' => 
  array (
    'context' => 'extension/ezmultiupload',
    'source' => 'Upload canceled.',
    'comment' => NULL,
    'translation' => 'アップロードは中止されました。',
    'key' => 'b397645928903acc2c0fb43b03988569',
  ),
  '5fb1d14b0e0b7c09fa496e82c95588c0' => 
  array (
    'context' => 'extension/ezmultiupload',
    'source' => 'Thumbnail created.',
    'comment' => NULL,
    'translation' => 'サムネイルを作成しました。',
    'key' => '5fb1d14b0e0b7c09fa496e82c95588c0',
  ),
  'fa0b1acc0a5b2989627e55fe7eeb113d' => 
  array (
    'context' => 'extension/ezmultiupload',
    'source' => 'Multiupload',
    'comment' => NULL,
    'translation' => 'マルチアップロード',
    'key' => 'fa0b1acc0a5b2989627e55fe7eeb113d',
  ),
  '4ded238e8da1c8c86e671b7d0ed391bf' => 
  array (
    'context' => 'extension/ezmultiupload',
    'source' => 'The files are uploaded to',
    'comment' => NULL,
    'translation' => 'ファイルのアップロード先',
    'key' => '4ded238e8da1c8c86e671b7d0ed391bf',
  ),
  '77dcad2030003df4094873f605fb8057' => 
  array (
    'context' => 'extension/ezmultiupload',
    'source' => 'Select files',
    'comment' => NULL,
    'translation' => 'ファイルを選択してください',
    'key' => '77dcad2030003df4094873f605fb8057',
  ),
  '8da388dcbe2bbf83ab2ab532976f0f5f' => 
  array (
    'context' => 'extension/ezmultiupload',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '8da388dcbe2bbf83ab2ab532976f0f5f',
  ),
  '14a54a7bee45be765f24925234ea3d25' => 
  array (
    'context' => 'extension/ezmultiupload',
    'source' => 'Error',
    'comment' => NULL,
    'translation' => 'エラー',
    'key' => '14a54a7bee45be765f24925234ea3d25',
  ),
  '48efbf36ba6820ec0d0e5785e2aff9e2' => 
  array (
    'context' => 'extension/ezmultiupload',
    'source' => 'Could not load flash(or not loaded yet), this is needed for multiupload!',
    'comment' => NULL,
    'translation' => 'フラッシュを読み込めませんでした、マルチアップロードに必要です!',
    'key' => '48efbf36ba6820ec0d0e5785e2aff9e2',
  ),
  '8836ddeca8ebe85cf856ac1680de7f7a' => 
  array (
    'context' => 'extension/ezmultiupload',
    'source' => 'Javascript has been disabled, this is needed for multiupload!',
    'comment' => NULL,
    'translation' => 'Javascriptが無効です、マルチアップロードに必要です!',
    'key' => '8836ddeca8ebe85cf856ac1680de7f7a',
  ),
);
?>
