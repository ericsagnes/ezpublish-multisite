<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/content/removeassignment',
);

$TranslationRoot = array (
  'b98ba059190a620eee6f9aef1766e303' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Object information',
    'comment' => NULL,
    'translation' => 'オブジェクトプロパティ',
    'key' => 'b98ba059190a620eee6f9aef1766e303',
  ),
  'f0b2cd8263596a74ecf05f311f9790d2' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'ID',
    'comment' => NULL,
    'translation' => 'ID',
    'key' => 'f0b2cd8263596a74ecf05f311f9790d2',
  ),
  '22790ae6067419c5e6d80f9d48d0be3c' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Created',
    'comment' => NULL,
    'translation' => '作成日時',
    'key' => '22790ae6067419c5e6d80f9d48d0be3c',
  ),
  '8988779507b93dc55bff093a085aa484' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Not yet published',
    'comment' => NULL,
    'translation' => '未公開',
    'key' => '8988779507b93dc55bff093a085aa484',
  ),
  '43b376162caca7ba4616848ccf6d6ebe' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Modified',
    'comment' => NULL,
    'translation' => '修正日時',
    'key' => '43b376162caca7ba4616848ccf6d6ebe',
  ),
  'a4c77b93b6cfd265e6e153a72c77e991' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Published version',
    'comment' => NULL,
    'translation' => '公開バージョン',
    'key' => 'a4c77b93b6cfd265e6e153a72c77e991',
  ),
  '498dba4345083d594fa3c5f8bd1d8591' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Manage versions',
    'comment' => NULL,
    'translation' => 'バージョン管理',
    'key' => '498dba4345083d594fa3c5f8bd1d8591',
  ),
  '1cf845625ebbad3d3e987e559216447f' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Current draft',
    'comment' => NULL,
    'translation' => '現在の下書き',
    'key' => '1cf845625ebbad3d3e987e559216447f',
  ),
  '73de4857105dd77b9a1c6fc6ecd07db3' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Version',
    'comment' => NULL,
    'translation' => 'バージョン',
    'key' => '73de4857105dd77b9a1c6fc6ecd07db3',
  ),
  '4c79a7f99022419448045a050b1728bd' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Confirm location removal',
    'comment' => NULL,
    'translation' => '削除の確認',
    'key' => '4c79a7f99022419448045a050b1728bd',
  ),
  '8baf44aa3b4b5e73e331468689660cfa' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Insufficient permissions',
    'comment' => NULL,
    'translation' => '権限がありません',
    'key' => '8baf44aa3b4b5e73e331468689660cfa',
  ),
  '10ddd3905a5c6f2b5db73ce27fa3f92f' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Some of the locations that are about to be removed contain sub items.',
    'comment' => NULL,
    'translation' => '削除対象には子アイテムが含まれます。',
    'key' => '10ddd3905a5c6f2b5db73ce27fa3f92f',
  ),
  'bf6f9cd4e8d3c08308dcb6151acea073' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Removing the locations will also result in the removal of the sub items.',
    'comment' => NULL,
    'translation' => '削除を実行すると子アイテムも同時に削除されます。',
    'key' => 'bf6f9cd4e8d3c08308dcb6151acea073',
  ),
  '9dc01b8a94aa6ae0b5ebd5ffe6b54cc7' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Are you sure you want to remove the locations along with their contents?',
    'comment' => NULL,
    'translation' => '子アイテムも含めて削除してもよろしいですか?',
    'key' => '9dc01b8a94aa6ae0b5ebd5ffe6b54cc7',
  ),
  '5602ea0da7dc7d2e8638474ab8b38ca4' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Click the "Cancel" button and try removing only the locations that you are allowed to remove.',
    'comment' => NULL,
    'translation' => '“キャンセル”ボタンをクリックしてから、削除可能な配置先のみを再度削除してください。',
    'key' => '5602ea0da7dc7d2e8638474ab8b38ca4',
  ),
  'd0ebb706442b96946b9a46f7d96fff2c' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Location',
    'comment' => NULL,
    'translation' => '配置先',
    'key' => 'd0ebb706442b96946b9a46f7d96fff2c',
  ),
  '20d44f88319ee25dcf0c77740d2fc367' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Sub items',
    'comment' => NULL,
    'translation' => '子アイテム数',
    'key' => '20d44f88319ee25dcf0c77740d2fc367',
  ),
  '6a9837c404263f4b76999a0c9a7317d9' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => '%child_count item',
    'comment' => NULL,
    'translation' => '%child_count アイテム',
    'key' => '6a9837c404263f4b76999a0c9a7317d9',
  ),
  '0adde160bf78f2c97c7b08255d8990d4' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => '%child_count items',
    'comment' => NULL,
    'translation' => '%child_count アイテム',
    'key' => '0adde160bf78f2c97c7b08255d8990d4',
  ),
  '1c84ddf81c92ac9013117630be8d1af6' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'OK',
    'comment' => NULL,
    'translation' => 'OK',
    'key' => '1c84ddf81c92ac9013117630be8d1af6',
  ),
  '87bd1710677899d8cd653c10b895afb9' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Remove the locations along with all the sub items.',
    'comment' => NULL,
    'translation' => 'すべての子アイテムを含めた配置先の削除',
    'key' => '87bd1710677899d8cd653c10b895afb9',
  ),
  '35a7604ed58e47bfb0941294ac3cef82' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Cancel',
    'comment' => NULL,
    'translation' => 'キャンセル',
    'key' => '35a7604ed58e47bfb0941294ac3cef82',
  ),
  '827e0b94858387974d73aa789372f134' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'Cancel the removal of locations.',
    'comment' => NULL,
    'translation' => '配置先削除のキャンセル',
    'key' => '827e0b94858387974d73aa789372f134',
  ),
  '406c10d99f737c017f2a3a6d3ac3db4c' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'The locations marked with red contain items that you do not have permission to remove.',
    'comment' => NULL,
    'translation' => '削除できない配置先は赤い文字で書かれています。',
    'key' => '406c10d99f737c017f2a3a6d3ac3db4c',
  ),
  '5bf60bf649f9e3bbbb77252b95ffedfb' => 
  array (
    'context' => 'design/admin/content/removeassignment',
    'source' => 'You cannot continue because you do not have permission to remove some of the selected locations.',
    'comment' => NULL,
    'translation' => '選択した配置先を削除する権限がないため、作業を続けることは出来ません。',
    'key' => '5bf60bf649f9e3bbbb77252b95ffedfb',
  ),
);
?>
