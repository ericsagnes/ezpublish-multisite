<?php
$eZTranslationCacheCodeDate = 1058863428;

$CacheInfo = array (
  'charset' => 'utf-8',
);
$TranslationInfo = array (
  'context' => 'design/admin/setup/rad',
);

$TranslationRoot = array (
  'cb71d571fd058e3b96b6a5b81f23d12f' => 
  array (
    'context' => 'design/admin/setup/rad',
    'source' => 'Rapid Application Development Tools',
    'comment' => NULL,
    'translation' => '高速アプリケーション開発ツール',
    'key' => 'cb71d571fd058e3b96b6a5b81f23d12f',
  ),
  '916500aa1eb67e7909d038d7a235590b' => 
  array (
    'context' => 'design/admin/setup/rad',
    'source' => 'Available RAD tools',
    'comment' => NULL,
    'translation' => '利用可能なアプリ開発ツール',
    'key' => '916500aa1eb67e7909d038d7a235590b',
  ),
  '5b509052568e9b56136bad72d967d472' => 
  array (
    'context' => 'design/admin/setup/rad',
    'source' => 'Template operator wizard',
    'comment' => NULL,
    'translation' => 'テンプレートオペレータウイザード',
    'key' => '5b509052568e9b56136bad72d967d472',
  ),
  '6326ee9ebf31712dc9fe2302d05d290e' => 
  array (
    'context' => 'design/admin/setup/rad',
    'source' => 'Datatype wizard',
    'comment' => NULL,
    'translation' => 'データタイプウイザード',
    'key' => '6326ee9ebf31712dc9fe2302d05d290e',
  ),
  '21c8a9061a282fe20e2506e22fa51498' => 
  array (
    'context' => 'design/admin/setup/rad',
    'source' => 'Template operator wizard (step 1 of 3)',
    'comment' => NULL,
    'translation' => 'テンプレートオペレーターウィザード（ステップ1/3）',
    'key' => '21c8a9061a282fe20e2506e22fa51498',
  ),
  '97ba80565163e2c11a458bc545a27f06' => 
  array (
    'context' => 'design/admin/setup/rad',
    'source' => 'Restart',
    'comment' => NULL,
    'translation' => '最初から',
    'key' => '97ba80565163e2c11a458bc545a27f06',
  ),
  '75736a3f4d7c6e40cfbda79da22c13e7' => 
  array (
    'context' => 'design/admin/setup/rad',
    'source' => 'Next',
    'comment' => NULL,
    'translation' => '次へ',
    'key' => '75736a3f4d7c6e40cfbda79da22c13e7',
  ),
  'e4381ffdbb17da7f386296cbc880be96' => 
  array (
    'context' => 'design/admin/setup/rad',
    'source' => 'The rapid application development (RAD) tools make the creation of new/extended functionality for eZ Publish easier. Currently there are two RAD tools available: the template operator wizard and the datatype wizard. The template operator wizard basically generates a valid framework (PHP code) for a new template operator. The datatype wizard generates a valid framework (PHP code) for a new datatype.',
    'comment' => NULL,
    'translation' => '高速アプリケーション開発（RAD）ツールはeZ Publish用の新しい機能の開発や機能の拡張をお手伝いします。現時点では二つのRADツールがあります: テンプレートオペレーターウィザードとデータタイプウィザードです。テンプレートオペレーターウィザードは、新しいテンプレートオペレーター用に有効なフレームワーク（PHPコード）を生成します。データタイプウィザードは新しいデータタイプ用に有効なフレームワーク（PHPコード）を生成します。',
    'key' => 'e4381ffdbb17da7f386296cbc880be96',
  ),
  'c67c0fc0b9eb842d2ec9d5000db0bcec' => 
  array (
    'context' => 'design/admin/setup/rad',
    'source' => 'Welcome to the template operator wizard. Template operators are usually used for manipulating template variables. However, they can also be used to generate or fetch data. This wizard will take you through a couple of steps with some basic choices. When finished, eZ Publish will generate a PHP framework for a new operator (which will be available for download).',
    'comment' => NULL,
    'translation' => 'テンプレートオペレーターウィザードへようこそ。テンプレートオペレーターはテンプレート変数を操作する時に使われます。データのフェッチか生成にも利用できます。このウィザードは数ステップで完了します。完了すると、eZ Publishは新しいオペレーター用にPHPフレームワークを生成します（その後ダウンロードができます）。',
    'key' => 'c67c0fc0b9eb842d2ec9d5000db0bcec',
  ),
);
?>
