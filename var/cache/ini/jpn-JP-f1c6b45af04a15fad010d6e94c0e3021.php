<?php
// This is a auto generated ini cache file, time created:Fri, 12 Jul 13 09:04:29 +0900
$data = array(
'rev' => 2,
'created' => '2013-07-12T09:04:29+09:00',
'charset' => "utf-8",
'files' => array (0 => '/home/eric/eZ/multisite/lib/ezutils/classes/../../../share/locale/jpn-JP.ini',
),
'file' => '/home/eric/eZ/multisite/lib/ezutils/classes/../../../share/locale/jpn-JP.ini',
'val' => array ('RegionalSettings' => array ('Country' => 'Japan','LanguageName' => '日本語','InternationalLanguageName' => 'Japanese',),'Charset' => array ('Preferred' => 'utf-8',),'HTTP' => array ('ContentLanguage' => 'ja-JP',),'Currency' => array ('Symbol' => '円','Name' => '日本円','ShortName' => '円','DecimalSymbol' => '.','ThousandsSeparator' => ',','FractDigits' => '0','PositiveSymbol' => '','NegativeSymbol' => '-','PositiveFormat' => '%p%q%c','NegativeFormat' => '%p%q%c',),'Numbers' => array ('DecimalSymbol' => '.','ThousandsSeparator' => ',','FractDigits' => '2','PositiveSymbol' => '','NegativeSymbol' => '-',),'DateTime' => array ('TimeFormat' => '%g:%i:%s %a','ShortTimeFormat' => '%g:%i %a','DateFormat' => '%Y年 %F %d日 %l','ShortDateFormat' => '%Y.%m.%d','DateTimeFormat' => '%Y年 %F %d日 %l %g:%i:%s %a','ShortDateTimeFormat' => '%Y.%m.%d %g:%i %a','MondayFirst' => 'no',),'ShortMonthNames' => array ('jan' => '1月','feb' => '2月','mar' => '3月','apr' => '4月','may' => '5月','jun' => '6月','jul' => '7月','aug' => '8月','sep' => '9月','oct' => '10月','nov' => '11月','dec' => '12月',),'LongMonthNames' => array ('jan' => '1月','feb' => '2月','mar' => '3月','apr' => '4月','may' => '5月','jun' => '6月','jul' => '7月','aug' => '8月','sep' => '9月','oct' => '10月','nov' => '11月','dec' => '12月',),'ShortDayNames' => array ('mon' => '月','tue' => '火','wed' => '水','thu' => '木','fri' => '金','sat' => '土','sun' => '日',),'LongDayNames' => array ('mon' => '月曜日','tue' => '火曜日','wed' => '水曜日','thu' => '木曜日','fri' => '金曜日','sat' => '土曜日','sun' => '日曜日',),
));
?>